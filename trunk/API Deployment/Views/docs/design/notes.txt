FC8 Rest API - http://www.welcometofleetcomplete.com/#!fc-web-api/c37y

MEDTRANS-FC8 Sync Service Tasks
-------------------------------

* check for NEW objects (drivers, assets, users) in MEDTRANS database. add objects to FC8. Lower priority.
  
  1. fetch object list from MEDTRANS. Determine which objects are new (not in unified database.) Is this managed by a "new" flag?)
  
  2. insert new records into unified database.
  
  3. update FC8 (backfill FC8 object identifiers in unified database)

  4. log errors to unified database
  
* check for UPDATES to existing objects (including NEW jobs/dispatches linked to existing objects) in MEDTRANS. push updates to FC8. Higher priority.

  1. fetch object list from MEDTRANS. Determine which objects have been updated. Is this managed by an "updated" flag?

  2. update existing record in unified database.
		
  3. update FC8

  4. log errors to unified database
  
* check for UPDATES to existing objects in FC8. push updates to MEDTRANS. Higher priority.

  1. fetch object list from unified database. perform FC8 api query to determine if something has been updated (brute force compare?, compare serialized POCOs)
  
  2. update existing record in unified database.
	
  3. update MEDTRANS database via Sybase Adapter	
  
  4. log errors to unified database
 

Shared Model Class Lib
----------------------
Unified database entity model and repo layer


Status & Timesheet Webapp
-------------------------
 - standalone authentication mechanism
 - mobile friendly (bootstrap)
 - log time in/out for drivers to unified database
 - driver/dispatch status dashboard using unified database status information (read-only)
 