USE [Aries]
GO

/****** Object:  Table [dbo].[LocationAlert]    Script Date: 5/31/2017 7:27:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LocationAlert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.LocationAlert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LocationAlert]  WITH CHECK ADD  CONSTRAINT [FK_dbo.LocationAlert_dbo.Employee_EmployeeID] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[LocationAlert] CHECK CONSTRAINT [FK_dbo.LocationAlert_dbo.Employee_EmployeeID]
GO


