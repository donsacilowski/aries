package com.ariestransportation.ariesmobile;

import java.util.ArrayList;

/**
 * Created by dsacilowski on 7/10/2016.
 */
public class TripSet {
    public ArrayList<TripDetail> trips;

    public TripSet() {
        trips = new ArrayList<TripDetail>();
    }

    public ArrayList<TripDetail> getTripss() {
        return trips;
    }

    public TripDetail getTrips(String id) {
        for (TripDetail td : trips) {
            if (td.id.equals(id))
                return td;
        }
        return null;
    }
}
