package com.ariestransportation.ariesmobile;

/**
 * Created by dsacilowski on 7/8/2016.
 */
public class TripDetail {
    /* Info coming from model */
    public Integer id;
    public String pickupName;
    public String pickupTime;
    public String pickupAddress;
    public String dropoffName;
    public String dropoffTime;
    public String dropoffAddress;
    public String service;
    public String serviceType;
    public String notes;
    public Boolean DNLA;
    public Boolean signatureRequired;
    public Boolean check;
    public String checkAmount;
    public TripDatabaseHelper.Statuses status;
    public String driverNotes;

    /* Info local to the mobile device */
    public Boolean sent;
    public Boolean pickUpComplete;
    public Boolean tripComplete;
    public Integer combinedPickup;
    public Integer combinedDropoff;
    public boolean DNLADone;
    public boolean checkDone;
    public boolean signatureDone;


    public TripDetail(){
        super();
    }

    public TripDetail(Integer myID, String myPickupName, String myPickupTime, String myPickupAddress,
                      String myDropoffName, String myDropffTime,String myDropoffAddress,
                      String myService, String myServiceType, String myNotes,
                      Boolean myDNLA, Boolean mySig, Boolean myCheck, String myCheckAmount,
                      TripDatabaseHelper.Statuses myStatus, String myDriverNotes, Boolean mySent, Boolean myPickUpComplete,
                      Boolean myTripComplete, Integer myCombinedPickupTrip, Integer myCombinedDropoffTrip,
                      Boolean myDNLADone, Boolean myCheckDone, Boolean mySigDone) {
        super();
        this.id = myID;
        this.pickupName = myPickupName;
        this.pickupTime = myPickupTime;
        this.pickupAddress = myPickupAddress;
        this.dropoffName = myDropoffName;
        this.dropoffTime = myDropffTime;
        this.dropoffAddress = myDropoffAddress;
        this.service = myService;
        this.serviceType = myServiceType;
        this.notes = myNotes;
        this.DNLA = myDNLA;
        this.signatureRequired = mySig;
        this.check = myCheck;
        this.checkAmount = myCheckAmount;
        this.status = myStatus;
        this.sent = mySent;
        this.pickUpComplete = myPickUpComplete;
        this.tripComplete = myTripComplete;
        this.combinedPickup = myCombinedPickupTrip;
        this.combinedDropoff = myCombinedDropoffTrip;
        this.DNLADone = myDNLADone;
        this.checkDone = myCheckDone;
        this.signatureDone = mySigDone;
    };
}

