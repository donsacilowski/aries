package com.ariestransportation.ariesmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by dsacilowski on 7/8/2016.
 */
public class TripAdapter extends ArrayAdapter<TripDetail> {
    private Context context;
    private int resource;
    private ArrayList<TripDetail> objects;
    private Button acceptButtonView = null;
    //private Button rejectButtonView = null;
    private Button notesButtonView = null;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater=
                ((Activity) context).getLayoutInflater();

        View row=inflater.inflate(resource, parent, false);

        final TripDatabaseHelper dbHelper = TripDatabaseHelper.getInstance(getContext());

        Integer intSta = objects.get(position).status.getValue();
        Integer accSta = TripDatabaseHelper.Statuses.Accepted.getValue();
        if (intSta < accSta){

        } else {

        }

        acceptButtonView = (Button) row.findViewById(R.id.buttonAcceptTrip);
        acceptButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                TripDetail td =  dbHelper.readTrip(objects.get(position).id);
                td.status = TripDatabaseHelper.Statuses.Accepted;
                dbHelper.updateTrip(td);

                objects.get(position).status = TripDatabaseHelper.Statuses.Accepted;

                String date = Dates.formatNowDateTime(getContext());

                Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.Accepted, false);
                dbHelper.insertStatus(sta, getContext());

                //rejectButtonView.setEnabled(false);
                //disableReject();

                Toast.makeText(v.getContext(), "Order Accepted", Toast.LENGTH_LONG).show();
            }
        });

        /*rejectButtonView = (Button) row.findViewById(R.id.buttonRejectTrip);
        rejectButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                TripDetail td = dbHelper.readTrip(objects.get(position).id);
                td.status = TripDatabaseHelper.Statuses.Rejected;
                dbHelper.updateTrip(td);

                String date = Dates.formatNowDateTime(getBaseContext());

                Status sta = new Status(td.id, date, TripDatabaseHelper.Statuses.Rejected);
                dbHelper.insertStatus(sta);

                acceptButtonView.setEnabled(false);
                Toast.makeText(v.getContext(), "Order Rejected", Toast.LENGTH_LONG).show();
            }
        });
*/
        notesButtonView = (Button) row.findViewById(R.id.buttonTripNotes);
        notesButtonView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                View promptView = inflater.inflate(R.layout.trip_notes, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setView(promptView);

                final TripDetail td = dbHelper.readTrip(objects.get(position).id);

                final EditText editText = (EditText) promptView.findViewById(R.id.notestext);

                editText.setText(td.notes);
                editText.setSelection(td.notes.length());
                // setup a dialog window
                alertDialogBuilder.setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                editText.getText();

                                td.notes = editText.getText().toString();
                                dbHelper.updateTrip(td);
                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create an alert dialog
                AlertDialog alert = alertDialogBuilder.create();
                alert.show();

            }
        });

        /*if (position % 2 == 1) {
            row.setBackgroundColor(Color.GRAY);
        }*/
        /*else {
            row.setBackgroundColor(Color.WHITE);
        }*/

        TextView title= (TextView)
                row.findViewById(R.id.textViewTripPickupTime);
        title.setText((CharSequence)objects.get(position).pickupTime);

        TextView name= (TextView) row.findViewById(R.id.textViewTripPickupName);
        name.setText("Pickup " + (CharSequence) objects.get(position).pickupName);

        TextView address= (TextView) row.findViewById(R.id.textViewTripPickupAddress);
        address.setText((CharSequence) objects.get(position).pickupAddress);

        TextView instructions = (TextView) row.findViewById(R.id.textViewTripInstructions);
        if (!objects.get(position).service.isEmpty() || !objects.get(position).serviceType.isEmpty()) {
            instructions.setText(objects.get(position).service + " " +
                    objects.get(position).serviceType);
        } else{
            instructions.setVisibility(View.GONE);
        }

        TextView dropoffName= (TextView) row.findViewById(R.id.textViewTripDropoffName);
        dropoffName.setText("Dropoff at " + (CharSequence) objects.get(position).dropoffName);

        TextView dropoffAddress= (TextView) row.findViewById(R.id.textViewTripDropoffAddress);
        dropoffAddress.setText((CharSequence) objects.get(position).dropoffAddress);

        Button dispCancelButtonView = (Button) row.findViewById(R.id.buttonDispatcherCancel);
        dispCancelButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                TripDetail td = dbHelper.readTrip(objects.get(position).id);
                Toast.makeText(v.getContext(), "Order Cancel Confirmed", Toast.LENGTH_LONG).show();

                td.tripComplete = true;
                td.status = TripDatabaseHelper.Statuses.Closed;
                dbHelper.updateTrip(td);

                String date = Dates.formatNowDateTime(getContext());

                Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.Closed, false);
                dbHelper.insertStatus(sta, getContext());

                objects.remove(position);
                notifyDataSetChanged();
            }
        });

        dispCancelButtonView.setVisibility(View.INVISIBLE);
        dispCancelButtonView.setEnabled(false);

        if (objects.get(position).status == TripDatabaseHelper.Statuses.CancelClient) {
            dispCancelButtonView.setVisibility(View.VISIBLE);
            dispCancelButtonView.setEnabled(true);

            acceptButtonView.setEnabled(false);
            //rejectButtonView.setEnabled(false);
        }

        TextView puCombined = (TextView)row.findViewById(R.id.textViewTripPickupCombined);
        puCombined.setEnabled(false);
        puCombined.setVisibility(View.GONE);
        if (objects.get(position).combinedPickup > 0) {
            puCombined.setVisibility(View.VISIBLE);
            puCombined.setText("Combined Pick Up #" + objects.get(position).combinedPickup.toString());
        }

        TextView doCombined = (TextView)row.findViewById(R.id.textViewTripDropoffCombined);
        doCombined.setEnabled(false);
        doCombined.setVisibility(View.GONE);
        if (objects.get(position).combinedDropoff > 0) {
            doCombined.setVisibility(View.VISIBLE);
            doCombined.setText("Combined Dropoff #" + objects.get(position).combinedDropoff.toString());
        }

        return row;
    }

    @Override
    public boolean isEnabled(int position) {

        Integer id = objects.get(position).id;
        Integer intSta = objects.get(position).status.getValue();
        Integer accSta = TripDatabaseHelper.Statuses.Accepted.getValue();
        if (intSta < accSta){
        //if (objects.get(position).tripComplete.equals("false")) {
            return false;
        }
        return true;
    }

    /*private void disableReject() {
        rejectButtonView.setEnabled(false);
    }*/

    public TripAdapter(Context context, int resource, ArrayList<TripDetail> objects) {
        super(context, resource, objects);

        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
}
