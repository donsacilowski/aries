package com.ariestransportation.ariesmobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    public static String userName = "";
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView tv = (TextView)findViewById(R.id.txtVolley);
        final TripDatabaseHelper dbHelper;
        dbHelper = TripDatabaseHelper.getInstance(getBaseContext());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        spinner=(ProgressBar)findViewById(R.id.loginprogressBar);
        spinner.setVisibility(View.INVISIBLE);

        /*Bundle itm = getIntent().getExtras();
        if (itm != null) {
            Boolean newOrders = itm.getBoolean("newOrders");
            if (newOrders == true){
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("shouldPoll", true);
                editor.commit();
            }

        }*/
        File dbFile = getBaseContext().getDatabasePath("trip_detail");

        if(dbFile.exists()) {
            dbHelper.onCreate(db);
        } else {
            dbHelper.onOpen(db);
        }
        //dbHelper.onCreate(db);

        Button loginButton = (Button) findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                spinner.setVisibility(View.VISIBLE);

                /* Update users token */
                TextView uname = (TextView) findViewById(R.id.txtUserName);
                userName = uname.getText().toString();
                TextView pass = (TextView) findViewById(R.id.txtPassword);
                String password = pass.getText().toString();

                final RequestQueue queue = Volley.newRequestQueue(getBaseContext());

                String url = getBaseContext().getResources().getString(R.string.portal_server_site) + "Account/MobileLogin?username=" + userName + "&password=" + password;

                url = url.replaceAll(" ", "%20");

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.equals("True")) {
                                    TextView uname = (TextView) findViewById(R.id.txtUserName);
                                    String userName = uname.getText().toString();

                                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putBoolean("userLoggedIn", true);
                                    editor.commit();

                                    String url = getBaseContext().getResources().getString(R.string.api_server_site) + "api/trip/SendRegistrationKey?regKey=" + refreshedToken +
                                            "&username=" + userName;

                                    url = url.replaceAll(" ", "%20");

                                    // Request a string response from the provided URL.
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    spinner.setVisibility(View.INVISIBLE);

                                                    if (response.equals("0")) {
                                                        Intent intent = new Intent(LoginActivity.this, TripsActivity.class);
                                                        startActivity(intent);
                                                    } else if (response.equals("7")) {
                                                        Toast.makeText(getBaseContext(), "User does not exist ", Toast.LENGTH_LONG).show();
                                                    } else if (response.equals("8")) {
                                                        Toast.makeText(getBaseContext(), "Driver does not exist ", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        spinner.setVisibility(View.INVISIBLE);

                                                        Toast.makeText(getBaseContext(), "Error updating key: " + error.toString(), Toast.LENGTH_LONG).show();

                                        }
                                    }) {
                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<String, String>();
                                            return params;
                                        }
                                    };
                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                            VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                    // Add the request to the RequestQueue.
                                    queue.add(stringRequest);
                                } else {
                                    Toast.makeText(getBaseContext(), "Login Failed; Try Again", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                spinner.setVisibility(View.INVISIBLE);

                                Toast.makeText(getBaseContext(), "Error Logging in: " + error.toString(), Toast.LENGTH_LONG).show();
                            }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                queue.add(stringRequest);
            }
        });

        Button resetButton = (Button) findViewById(R.id.buttonReset);
        resetButton.setVisibility(View.INVISIBLE);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dbHelper.onCreate(db);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("shouldPoll", true);
                editor.commit();

                RequestQueue queue = Volley.newRequestQueue(getBaseContext());

                String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                String url = getBaseContext().getResources().getString(R.string.api_server_site) + "api/trip/resetStatus?regKey=" + refreshedToken + "&username="+userName;

                url = url.replaceAll(" ", "%20");

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (response.equals("0")) {
                                    Toast.makeText(getBaseContext(), "Reset Successful ", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), "Error resetting status: " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        TextView uname = (TextView) findViewById(R.id.txtUserName);
        userName = uname.getText().toString();

        Bundle itm = getIntent().getExtras();
        if (itm != null) {
            String newOrders = itm.getString("newOrders");
            if (newOrders.equals("true")) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("shouldPoll", true);
                editor.commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }
        if (id == R.id.action_timesheet) {
            return true;
        }
        if (id == R.id.action_orderHistory) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
