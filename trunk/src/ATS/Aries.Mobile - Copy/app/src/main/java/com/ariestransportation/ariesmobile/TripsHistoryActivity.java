package com.ariestransportation.ariesmobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class TripsHistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TripDatabaseHelper dbHelper = TripDatabaseHelper.getInstance(this);
        /*Trips myTrips[] = new Trips[]{
                new Trips("9:30 Pickup Mary Jones" +
                        System.getProperty("line.separator") + "\t\t\t65 Main St." +
                        System.getProperty("line.separator") + "\t\t\tArcade, NY" +
                        System.getProperty("line.separator") + "\t\t\tAmbulatory" +
                        System.getProperty("line.separator") + "Drop off at Buffalo General Hospital"),
                new Trips("10:30 Pickup Al Smith" +
                        System.getProperty("line.separator") + "\t\t\t65 Bryant St." +
                        System.getProperty("line.separator") + "\t\t\tBuffalo, NY" +
                        System.getProperty("line.separator") + "Drop off at Buffalo General Hospital")
        };
*/
        ArrayList<TripDetail> myTrips = new ArrayList<TripDetail>();
        myTrips = dbHelper.getOpenTripsList();

        TripHistoryAdapter tripAdapter = new TripHistoryAdapter(this, R.layout.trip_history_item, myTrips);

        ListView myList=(ListView)
                findViewById(R.id.listViewTripHistory);
        myList.setAdapter(tripAdapter);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

}
