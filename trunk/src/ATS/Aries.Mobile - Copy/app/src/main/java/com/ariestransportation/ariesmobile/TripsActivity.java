package com.ariestransportation.ariesmobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TripsActivity extends AppCompatActivity {

    TripDatabaseHelper dbHelper;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void continueActivity() {

        ArrayList<TripDetail> allTrips = new ArrayList<TripDetail>();
        allTrips = dbHelper.getAllTripsList();
        dbHelper.setCombinedTrips();
        allTrips = dbHelper.getAllTripsList();

        ArrayList<TripDetail> myTrips = new ArrayList<TripDetail>();
        myTrips = dbHelper.getTripsList();
        TripAdapter tripAdapter = new TripAdapter(this, R.layout.trip_item, myTrips);

        ListView myList=(ListView)
                findViewById(R.id.listViewTrips);
        myList.setAdapter(tripAdapter);

        myList.setOnItemClickListener(new OnItemClickListener() {
                                          @Override
                                          public void onItemClick(AdapterView<?> parent, View view, int position,
                                                                  long id) {
                                              TripDetail td = ((TripAdapter) parent.getAdapter()).getItem(position);

                                              if (td.status != TripDatabaseHelper.Statuses.CancelClient) {
                                                  Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);

                                                  Bundle extras = new Bundle();
                                                  extras.putInt(getResources().getString(R.string.trip_detail_id), td.id);

                                                  intent.putExtras(extras);

                                                  startActivity(intent);

                                              }
                                          }
                                      }

        );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        dbHelper = TripDatabaseHelper.getInstance(this);

        spinner=(ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean shouldPoll = prefs.getBoolean("shouldPoll", true);

        if (shouldPoll) {

            spinner.setVisibility(View.VISIBLE);
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            //TextView uname = LoginActivity.userName; //(TextView) findViewById(R.id.txtUserName);
            String userName = LoginActivity.userName;

            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            String url = getBaseContext().getResources().getString(R.string.api_server_site) + "api/trip/SendTrips?mobileAppKey=" + refreshedToken +
                    "&username=" + userName;

            url = url.replaceAll(" ", "%20");

            HashMap<String, String> mRequestParams = new HashMap<String, String>();
            JSONArray jarr = new JSONArray();
            // Request a string response from the provided URL.
            JsonArrayRequest tripStringRequest = new JsonArrayRequest(Request.Method.POST, url, jarr,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject jresponse = response.getJSONObject(i);

                                    TripDatabaseHelper.Statuses sta = TripDatabaseHelper.Statuses.valueOf(jresponse.getString("status"));

                                    TripDetail td = new TripDetail(
                                            jresponse.getInt("id"),
                                            jresponse.getString("pickupName"),
                                            jresponse.getString("pickupTime"),
                                            jresponse.getString("pickupAddress"),
                                            jresponse.getString("dropoffName"),
                                            jresponse.getString("dropoffTime"),
                                            jresponse.getString("dropoffAddress"),
                                            jresponse.getString("service"),
                                            jresponse.getString("serviceType"),
                                            jresponse.getString("notes"),
                                            jresponse.getBoolean("DNLA"),
                                            jresponse.getBoolean("signatureRequired"),
                                            jresponse.getBoolean("check"),
                                            jresponse.getString("checkAmount"),
                                            sta,
                                            "", false, false, false, 0, 0,
                                            false, false, false);

                                    //Check to see if the trip already exists and if it does, update it; otherwise, add a new one
                                    if (dbHelper.readTrip(td.id) == null) {
                                        long lint = dbHelper.insertTrip(td);
                                    } else {
                                        long lint = dbHelper.updateTrip(td);
                                    }
                                }
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putBoolean("shouldPoll", false);
                                editor.commit();
                                continueActivity();
                                spinner.setVisibility(View.GONE);

                            /*try {
                                Intent intent = new Intent(LoginActivity.this, TripsActivity.class);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getBaseContext(), "Error: Sending New Trips " + error.toString(), Toast.LENGTH_LONG).show();
                }
            });

            tripStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(tripStringRequest);
        }
        continueActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("userLoggedIn", false);
            editor.commit();

            return true;
        }
        if (id == R.id.action_timesheet) {
            goToUrl ( getBaseContext().getResources().getString(R.string.portal_server_site));
            return true;
        }
        if (id == R.id.action_orderHistory) {
            Intent intent = new Intent(TripsActivity.this, TripsHistoryActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
