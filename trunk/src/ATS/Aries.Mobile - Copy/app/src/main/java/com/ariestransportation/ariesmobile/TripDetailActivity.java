package com.ariestransportation.ariesmobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class TripDetailActivity extends AppCompatActivity {

    Button departButton = null;
    TripDatabaseHelper dbHelper;
    CheckBox cbDNLA = null;
    CheckBox cbCheck = null;
    TextView tvPayAmount = null;
    Button signatureButton = null;
    TripDetail trip = null;
    TextView tv = null;
    String desc = "";

    boolean DNLAhit = false;
    boolean checkHit = false;

    boolean DNLADone = false;
    boolean checkDone = false;
    boolean signatureDone = false;

    static Integer numPickups = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        departButton = (Button) findViewById(R.id.buttonDepart);
        departButton.setEnabled(false);

        cbDNLA = (CheckBox)findViewById(R.id.checkBoxDNLA);
        cbCheck = (CheckBox)findViewById(R.id.checkBoxCheck);

        signatureButton = (Button) findViewById(R.id.buttonGetSignature);

        tv = (TextView) findViewById(R.id.textLegInstructions);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        String strID = getResources().getString(R.string.trip_detail_id);
        final Integer tripID = bundle.getInt(strID);

        dbHelper = TripDatabaseHelper.getInstance(this);

        trip = dbHelper.readTrip(tripID);

        /* Pickup not done, display the pickup instructions */
        if (!trip.pickUpComplete) {
            desc = trip.pickupTime + "\r\n\t\t" + "Pickup " + trip.pickupName + "\r\n\t\t" + trip.pickupAddress + " \r\n\t\t" +
                   trip.service + " " + trip.serviceType + " " + trip.notes;
            tv.setText(desc);
        } else {
            desc = trip.dropoffTime + "\r\n" + "Dropoff " + trip.pickupName + " at " + trip.dropoffName + "\r\n" + trip.dropoffAddress ;
            tv.setText(desc);
        }

        /* Only process check, DNLA and signature when this is not a combined leg */
        if (!trip.pickUpComplete){
            if (trip.combinedPickup == 0) {
                processSelectionButtons(trip);
            } else {
                cbCheck.setVisibility(View.INVISIBLE);
                cbDNLA.setVisibility(View.INVISIBLE);
                signatureButton.setVisibility(View.INVISIBLE);
            }
        }

        if (!trip.tripComplete && trip.pickUpComplete){
            if (trip.combinedDropoff == 0) {
                processSelectionButtons(trip);
            } else {
                cbCheck.setVisibility(View.INVISIBLE);
                cbDNLA.setVisibility(View.INVISIBLE);
                signatureButton.setVisibility(View.INVISIBLE);
            }
        }

        final Button cancelButton = (Button) findViewById(R.id.buttonCancel);
        cancelButton.setVisibility(View.INVISIBLE);
        //if (trip.status == TripDatabaseHelper.Statuses.CancelNoShow) {
            //cancelButton.setVisibility(View.VISIBLE);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    PopupMenu popup = new PopupMenu(TripDetailActivity.this, cancelButton);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.menu_trip_cancel, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        TripDetail locTrip = dbHelper.readTrip(tripID);

                        public boolean onMenuItemClick(MenuItem item) {
                            Toast.makeText(
                                    TripDetailActivity.this,
                                    "You Canceled the Trip with the reason : " + item.getTitle(),
                                    Toast.LENGTH_SHORT
                            ).show();

                            locTrip.status = TripDatabaseHelper.Statuses.CancelNoShow;
                            dbHelper.updateTrip(locTrip);

                            String date = Dates.formatNowDateTime(view.getContext());

                            Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.CancelNoShow, false);

                            dbHelper.insertStatus(sta, view.getContext());

                            Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                            startActivity(intent);

                            return true;
                        }
                    });

                    popup.show(); //showing popup menu
                }
            });
        //}

        Button arriveButton = (Button) findViewById(R.id.buttonArrive);
        arriveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TripDetail locTrip = dbHelper.readTrip(tripID);

                cancelButton.setVisibility(View.VISIBLE);

                String date = Dates.formatNowDateTime(view.getContext());

                if (!locTrip.pickUpComplete) {
                    Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.AtPickUp, false);
                    locTrip.status = TripDatabaseHelper.Statuses.AtPickUp;
                    dbHelper.insertStatus(sta, view.getContext());

                    if (locTrip.combinedPickup != 0){
                        DNLADone = true;
                        checkDone = true;
                        signatureDone = true;
                    }
                } else {
                    Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.AtDropOff, false);
                    locTrip.status = TripDatabaseHelper.Statuses.AtDropOff;
                    dbHelper.insertStatus(sta, view.getContext());

                    if (locTrip.combinedDropoff != 0){
                        DNLADone = true;
                        checkDone = true;
                        signatureDone = true;
                    }
                }
                dbHelper.updateTrip(locTrip);

                cbDNLA.setEnabled(true);
                if (locTrip.DNLADone) {
                    cbDNLA.setEnabled(false);
                }

                if (!locTrip.checkDone) {
                    cbCheck.setEnabled(true);
                }

                if (!locTrip.signatureDone){
                    signatureButton.setEnabled(true);
                }

                setDepartButton(locTrip);
            }
        });

        departButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TripDetail locTrip = dbHelper.readTrip(tripID);

                String date = Dates.formatNowDateTime(view.getContext());

                ArrayList<TripDetail> tdList = null;
                if (!locTrip.pickUpComplete){

                    locTrip.status = TripDatabaseHelper.Statuses.PickedUp;
                    locTrip.pickUpComplete = true;

                    DNLADone = false;
                    checkDone = false;
                    signatureDone = false;

                    tdList = dbHelper.getCombinedPickupTrips(locTrip.id);
                    ArrayList<TripDetail> tdCombList = dbHelper.getCombinedDropoffTrips(locTrip.id);

                    if (tdList.size() > 0){
                        confirmCombinedChoice(view.getContext(), "Did you pickup ", tdList, "Pickup", tdList, locTrip);
                    } else {
                        Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.PickedUp, false);
                        dbHelper.insertStatus(sta, view.getContext());

                        if (tdCombList.size()>0) {
                            dbHelper.updateTrip(locTrip);

                            numPickups++;

                            if (numPickups == tdCombList.size()){
                                desc = locTrip.dropoffTime + "\r\n" + "Dropoff" + locTrip.pickupName + " at " +
                                        locTrip.dropoffName + "\r\n" + locTrip.dropoffAddress;

                                tv.setText(desc);
                            } else {
                                Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                                startActivity(intent);
                            }

                        } else {
                            desc = locTrip.dropoffTime + "\r\n" + "Dropoff " + locTrip.pickupName + " at " +
                                    locTrip.dropoffName + "\r\n" + locTrip.dropoffAddress;

                            tv.setText(desc);
                            processSelectionButtons(locTrip);
                        }
                    }
                    renameSigFile();
                    //departButton.setEnabled(false);
                }
                else {

                    locTrip.tripComplete = true;
                    locTrip.status = TripDatabaseHelper.Statuses.DroppedOff;

                    tdList = dbHelper.getCombinedDropoffTrips(locTrip.id);

                    if (tdList.size() > 0){
                        confirmCombinedChoice(view.getContext(), "Did you dropoff ", tdList, "Dropoff", tdList, locTrip);
                    } else {
                        Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.DroppedOff, false);
                        dbHelper.insertStatus(sta, view.getContext());

                        renameSigFile();
                        Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                        startActivity(intent);
                    }
                }
                dbHelper.updateTrip(locTrip);
            }
        });
    }

    private void processSelectionButtons(final TripDetail trip) {
        cbDNLA.setVisibility(View.INVISIBLE);
        cbDNLA.setEnabled(false);

        if (trip.DNLA) {
            if (trip.pickUpComplete) {
                cbDNLA.setVisibility(View.VISIBLE);

                cbDNLA.setChecked(trip.DNLADone);
                DNLADone = false;
                //trip.DNLADone = false;
                if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtDropOff.getValue()) {
                    cbDNLA.setEnabled(true);
                }
                cbDNLA.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        confirmDialog(view.getContext(), "Did you NOT leave the person alone?", locTrip);
                        DNLAhit = true;

                        setDepartButton(locTrip);
                    }
                });
            } else {
                DNLADone = true;
            }
        }
        else {
            DNLADone = true;
        }
        if (trip.DNLADone){
            DNLADone = true;
        }
        dbHelper.updateTrip(trip);

        signatureButton.setVisibility(View.INVISIBLE);
        signatureButton.setEnabled(false);

        if (trip.signatureRequired) {
            if (!trip.signatureDone) {
                signatureButton.setVisibility(View.VISIBLE);
                signatureButton.setEnabled(false);
                trip.signatureDone = false;
                if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtPickUp.getValue()) {
                    signatureButton.setEnabled(true);
                }
                signatureButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        locTrip.signatureDone = true;
                        signatureDone = true;
                        dbHelper.updateTrip(locTrip);

                        setDepartButton(locTrip);

                        Bundle extras = new Bundle();
                        extras.putInt(getResources().getString(R.string.trip_detail_id), locTrip.id);

                        Intent intent = new Intent(TripDetailActivity.this, SignatureActivity.class);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });
            } else {
                signatureDone = true;
            }
        }
        else {
           signatureDone = true;
        }
        dbHelper.updateTrip(trip);

        cbCheck.setVisibility(View.INVISIBLE);
        cbCheck.setEnabled(false);

        if (trip.check) {
            cbCheck.setVisibility(View.VISIBLE);
            cbCheck.setText(cbCheck.getText() + trip.checkAmount.toString());
            cbCheck.setChecked(trip.checkDone);

            if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtPickUp.getValue()) {
                cbCheck.setEnabled(true);
            }

            if(!trip.checkDone) {

                cbCheck.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        confirmDialog(view.getContext(), "Did you pick up a check?", locTrip);
                        checkHit = true;
                    }
                });
            } else {
                cbCheck.setEnabled(false);
                //cbCheck.setVisibility(View.INVISIBLE);
                checkDone = true;
            }
        } else {
            checkDone = true;
        }
        dbHelper.updateTrip(trip);
        setDepartButton(trip);
    }

    private void setDepartButton(TripDetail trip) {
        if (DNLADone && checkDone && signatureDone) {
            departButton.setEnabled(true);
        }
    }

    private void renameSigFile () {
        //Rename the signature file, then send it to the server
        // the directory where the signature will be saved
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_signature");

        // make the directory if it does not exist yet
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        // set the file name of your choice
        String fname = "signature.png";
        File file = new File(myDir, fname);
        File renamedFile = new File("signature.png"+trip.id.toString());
        if (file.exists()) {

            boolean err = file.renameTo(renamedFile);
            Toast.makeText(
                    TripDetailActivity.this,
                    "Rename signature file = " + err,
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
    private void confirmDialog(Context context, String message, final TripDetail trip){

        final AlertDialog alert = new AlertDialog.Builder(
                new ContextThemeWrapper(context,android.R.style.Theme_Dialog))
                .create();

        alert.setTitle("Alert");
        alert.setMessage(message);
        alert.setIcon(R.drawable.sign_warning_icon);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);

        alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        alert.dismiss();
                        returnUserChoice("Yes", trip);
                    }
                });

        alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        alert.dismiss();
                        returnUserChoice("No", trip);
                    }
                });

        alert.show();
    }

    private void confirmCombinedChoice(Context context,
                                       String message,
                                       final ArrayList<TripDetail> trips,
                                       final String leg,
                                       final ArrayList<TripDetail> trip,
                                       final TripDetail tr){

        final AlertDialog alert = new AlertDialog.Builder(
                new ContextThemeWrapper(context,android.R.style.Theme_Dialog))
                .create();

        alert.setTitle("Combined Alert");

        String fullMessage = message;

        for (TripDetail td : trips) {
            if (leg == "Pickup") {
                fullMessage += td.pickupName + ", ";
            } else {
                fullMessage += td.pickupName + ", ";
            }
        }
        alert.setMessage(fullMessage);

        alert.setIcon(R.drawable.sign_warning_icon);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);

        try {
            alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            alert.dismiss();
                            returnCombinedChoice("Yes", trip, leg, tr);

                            if (!tr.pickUpComplete) {
                                Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                                startActivity(intent);
                            }
                        }
                    });

            alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            alert.dismiss();
                            returnCombinedChoice("No", trip, leg, tr);
                        }
                    });

            alert.show();
        } catch (Exception ex){
            Toast.makeText(
                    TripDetailActivity.this,
                    "Alert Error = " + ex.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    private void returnCombinedChoice(String choice, ArrayList<TripDetail> trip, String leg, TripDetail tr) {
        for (TripDetail td : trip) {
            if (choice == "Yes") {
                String date = Dates.formatNowDateTime(getBaseContext());

                if (leg == "Pickup") {
                    Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.PickedUp, false);
                    dbHelper.insertStatus(sta, getBaseContext());

                    td.pickUpComplete = true;
                    desc = tr.dropoffTime + "\r\n" + "Dropoff " + tr.pickupName + " at " +
                           tr.dropoffName + "\r\n" + tr.dropoffAddress;

                    tv.setText(desc);
                    processSelectionButtons(tr);

                } else {
                    Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.DroppedOff, false);
                    dbHelper.insertStatus(sta, getBaseContext());

                    td.tripComplete = true;
                    Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                    startActivity(intent);
                }
            } else {
                if (leg == "Pickup") {
                    td.pickUpComplete = false;
                } else {
                    td.tripComplete = false;
                }
            }
            dbHelper.updateTrip(td);
        }
    }

    private void returnUserChoice(String choice, TripDetail trip) {

        String date = Dates.formatNowDateTime(getBaseContext());

        if (DNLAhit) {
            DNLAhit = false;
            if (choice == "Yes") {

                Status sta = new Status(0, trip.id, date, TripDatabaseHelper.Statuses.NotAlone, false);
                dbHelper.insertStatus(sta, getBaseContext());

                trip.status = TripDatabaseHelper.Statuses.NotAlone;
                DNLADone = true;
                trip.DNLADone = true;
            } else {
                trip.DNLADone = false;
            }
        }
        if (checkHit) {
            checkHit = false;
            if (choice == "Yes") {
                Status sta = new Status(0, trip.id, date, TripDatabaseHelper.Statuses.GotCheck, false);

                trip.status = TripDatabaseHelper.Statuses.GotCheck;
                dbHelper.insertStatus(sta, getBaseContext());
                checkDone = true;
                trip.checkDone = true;
                cbCheck.setChecked(true);
            } else {
                trip.checkDone = false;

                cbCheck.setChecked(false);
            }
        }
        dbHelper.updateTrip(trip);
        setDepartButton(trip);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

}
