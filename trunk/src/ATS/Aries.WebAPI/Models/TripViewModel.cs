﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Aries.Model.Entities;

namespace Aries.WebAPI.Models
{
    public class TripDetailsViewModel
    {
       // [Required]
        public int DispatchID { get; set; }

        /* This will be where the trip is to be dispatched to */
        //[Required]
        public string DriverPhoneNumber { get; set; }

        ///* This will be a combination of the time of the legs (dropoff and pickup), the persons name and address,
        // * and any further instructions for the driver needs at the Trip level. */
        //[Required]
        public string TripInstructions { get; set; }

        ///* This will be a combination of the time of the leg, the persons name and address,
        // * and any further instructions for the driver needs at the Detail level. */
        //[Required]
        public string TripDetailInstructions { get; set; }

        //[Required]
        public bool CheckRequired { get; set; }

        //[Required]
        public bool DNLA { get; set; }

        //[Required]
        public bool SignatureRequired { get; set; }
    }

    public class TripStatusViewModel
    {
        [Required]
        public string DispatchID { get; set; }

        [Required]
        public string Status { get; set; }

        public string CancellationReason { get; set; }
        public string Notes { get; set; }

        public bool CheckConfirmed { get; set; }
        public bool DNLAConfirmed { get; set; }
        public byte[] Signature { get; set; }
    }
}