﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Aries.Model.Entities;


namespace Aries.WebAPI.Models
{
    public class DispatchViewModel
    {
        [Required]
        public int id { get; set; }
        
        public string pickupName { get; set; }
        public string pickupTime { get; set; }
        public string pickupAddress { get; set; }

        public string dropoffName { get; set; }
        public string dropoffTime { get; set; }
        public string dropoffAddress { get; set; }

        public string service { get; set; }
        public string serviceType { get; set; }

        public string notes { get; set; }

        public bool DNLA { get; set; }
        public bool signatureRequired { get; set; }
        public bool check { get; set; }
        public string checkAmount { get; set; }
        public string status { get; set; }
        public string pickupAddressFull { get; set; }
        public string dropoffAddressFull { get; set; }

    }
}