﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Script.Serialization;

using Aries.WebAPI.Models;

using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Model;
using MedTrans;

using log4net;
using System.Configuration;

namespace Aries.WebAPI.Controllers
{
    [RoutePrefix("api/trip")]
    public class TripController : ApiController
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(TripController));

        /* Send a notice to the phone there is data available */
        [HttpGet, Route("SendNotice")]
        public ErrorCode SendNotice(string key, MobileMessageTypes messageType, string message = "")
        //public ErrorCode SendNotice()
        {
            log.Info("WebAPI Send Notice Arrival");

            //Driver driver = (new DriverRepository()).GetByMedtransID(driverMedTransId);
            //if (driver == null) {
            //    log.Error("Driver " + driverMedTransId.ToString() + " Not Found");
            //    return ErrorCode.DriverNotFound;
            //}

            AndroidFCMPushNotificationStatus result = new AndroidFCMPushNotificationStatus();

            // TODO: Send notification out to phone/mobile app using either driver.MobileAppKey and/or driver.PhoneNumber 
            // driver - holds addition info about the driver
            try {

                var value = "New Work";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                //tRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8;application/json";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", "AIzaSyBUMzJ4GvaokXVjFYoluKAFEjJZTjbP1Jw"));
                //tRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAAr3X3OFc:APA91bFZZ5einGcNYZljsMha3lGGUCk1ptQi---hK3NQkm8vUWAsHfiIIp9ZkhxAfuZYtM5XPxhg7D2t4XI_NkmhwyWcjfolM5gt3RaV_bGg6aaxlXzowHLALEO1ocEeN4vHgbAS6JVQceUPxU_f6I77WrCTSZwlUg"));
                //tRequest.Headers.Add(string.Format("Sender: id={0}", "753598412887"));

                //string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&to=" + "eZdPCe89EwU:APA91bGS3mwHGT0mdurE49bh1bfDfJKsjxzurxMXaZ1k7aCNf-hvLC1IXsct2YuMptc_2DqphjVLNeqwgaW81V8X7qIBhbITuDc785Ai0uPWjs538_5rED40i9Tywa33oNtjvhffa25I";
                //string postData = "collapse_key=com.ariestransportation.ariesmobile&notification.title=New Work2&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&to=" + driver.MobileAppKey;
                //string postData = "&to=" + driver.MobileAppKey;
                string bodyMessage = "";
                switch (messageType)
                {
                    case MobileMessageTypes.Broadcast:
                        bodyMessage = message;
                        break;
                    case MobileMessageTypes.LogOff:
                        bodyMessage = message;
                        break;
                    case MobileMessageTypes.NewOrders:
                        bodyMessage = "New Orders";
                        break;
                }
                var postData = new
                {
                    to = key,
                    //notification = new
                    //{
                    //    body = "Click this message",
                    //    title = "Aries Transportation",
                    //    icon = "myicon"
                    //},
                    data = new
                    {
                        body = bodyMessage,
                        title = "Aries Transportation"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.ContentLength = byteArray.Length;

                log.Info("WebAPI Send Notice Sending Message");

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;

                                log.Info("WebAPI Send Notice Message Sent");

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("WebAPI Send Notice ERROR " + ex.Message);
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }

            
//Server key:AIzaSyBUMzJ4GvaokXVjFYoluKAFEjJZTjbP1Jw


//    Sender ID: 753598412887

            return ErrorCode.OK;
        }

        public class AndroidFCMPushNotificationStatus
        {
            public bool Successful
            {
                get;
                set;
            }

            public string Response
            {
                get;
                set;
            }
            public Exception Error
            {
                get;
                set;
            }
        }

        /* Phone will ask for data to be sent */
                       
        [Route("SendTripDetails")]
        public TripDetailsViewModel SendTripDetails()
        {
            return null;
        }
        
        /* Phone will ask for data to be sent */

        [Route("SendErrorMessage")]
        public string SendErrorMessage(int errorCode)
        {
            ErrorCode errCode = (ErrorCode)errorCode;
            if (errCode == ErrorCode.OK)
                return "OK";
            else if (errCode == ErrorCode.OrderNotFound)
                return "Order not found";
            else if (errCode == ErrorCode.OrderNotAssignedToDriver)
                return "Order is not assigned to driver";
            else if (errCode == ErrorCode.InvalidStatusListId)
                return "Unsupport Status code";
            else if (errCode == ErrorCode.InvalidTimestamped)
                return "Invalid Timestamped";
            else if (errCode == ErrorCode.InvalidMobileAppKey)
                return "Invalid Mobile App Key";
            else if (errCode == ErrorCode.UnableToSaveSignature)
                return "Unexpect Error occured while saving signature.";
            else if (errCode == ErrorCode.DriverNotFound)
                return "Driver not found.";
            else if (errCode == ErrorCode.UserNameNotFound)
                return "UserName not found.";
            else if (errCode == ErrorCode.MissingRegistrationInfo)
                return "Missing Registration Info.";
            else if (errCode == ErrorCode.MedtransTripNotFound)
                return "Order number does not exist in Medtrans system";
            else
                return "Unknown Error";
        }


        [Route("SendTrips")]
        public List<DispatchViewModel> SendTrips(string mobileAppKey, string username)
        {
            List<DispatchViewModel> dispatchViews = new List<DispatchViewModel>();
            try
            {
                log.Error("In SendTrips Arrival trips to " + username);
                Employee emp = (new EmployeeRepository().GetByName(username));
                // Need to query all orders for status of assigned/new and cancelled (by dispatch)

                string driverName = emp.FirstName + " " + emp.LastName;
                log.Error("In SendTrips Arrival Employee " + emp.FirstName + " " + emp.LastName);

                // Need to query all orders for status of assigned/new and cancelled (by dispatch)
                Driver driver = (new DriverRepository()).GetByMobileAppKey(mobileAppKey, driverName);
                log.Error("In SendTrips Arrival Driver " + mobileAppKey);

                dispatchViews = new List<DispatchViewModel>();

                if (driver == null)
                {
                    log.Error("In SendTrips Arrival " + " no driver found " + driverName);
                    // return a null is an indication of failure
                    DispatchViewModel dvm = new DispatchViewModel();
                    dvm.id = 9999;
                    dvm.pickupName = "Driver not found";
                    dvm.pickupTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now); 

                    dvm.pickupAddress = "85 Garfield St., Lancaster, NY 14086 " + mobileAppKey;
                    dvm.dropoffName = "";
                    dvm.dropoffTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now);

                    dvm.dropoffAddress = "";
                    dvm.service = "";
                    dvm.serviceType = "";
                    dvm.notes = "";
                    dvm.DNLA = false;
                    dvm.signatureRequired = false;
                    dvm.pickupAddressFull = "PLEASE LOGOFF AND LOGON AGAIN!!!";
                    dvm.dropoffAddressFull = "";

                    dvm.check = false;
                    dvm.checkAmount = "0";

                    dvm.status = "Assigned";
                    dispatchViews.Add(dvm);
                    return dispatchViews;
                }
               log.Error("In SendTrips Arrival " + " trips to " + username);
                // TODO: Need to verify the correct status description
                DateTime frDate = DateTime.Now.Date;

                //var assignedList = (new DispatchRepository()).GetAllByStatusDescription("Dispatched").Where(d => d.DriverID == driver.Id &&
                //                                                                                           (d.PickupTime >= frDate));

                //var cancelledList = (new DispatchRepository()).GetAllByStatusDescription("CancelDispatch").Where(d => d.DriverID == driver.Id &&                  
                //                                                                                              (d.PickupTime >= frDate));

                //List<Dispatch> dispatches = assignedList.Concat(cancelledList).ToList();
                //List<Dispatch> sortedDisp = dispatches.OrderBy(o => o.PickupTime).ToList();
                List<Dispatch> sortedDisp = (new DispatchRepository()).GetAllNewOrders(driver.Id);
                //log.Error("In SendTrips after sortedDisp" + sortedDisp.Count().ToString() + " trips to " + username);
                //if (sortedDisp != null) log.Error("In SendTrips Sending " + sortedDisp.Count().ToString() + " trips to " + username);

                string tripNumbers = "";

                foreach (Dispatch d in sortedDisp)
                {
                    /* Only send trips when status is Dispatched or Cancelled by Dispatcher */
                    if ((LatestStatus(d.Id).Equals("Dispatched")) ||
                        (LatestStatus(d.Id).Equals("CancelDispatch")) ||
                        (LatestStatus(d.Id).Equals("ClosedCancelUnitCh")) ||
                        (LatestStatus(d.Id).Equals("ClosedCancelReset")) ||
                        (LatestStatus(d.Id).Equals("ClosedCancelDispatch")) )
                    {
                        log.Error("In SendTrips Latest Status = " + LatestStatus(d.Id));

                        DispatchViewModel dvm = new DispatchViewModel();
                        dvm.id = d.Id;
                        dvm.pickupName = d.Customer.Name;
                        dvm.pickupTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", d.PickupTime); 

                        dvm.pickupAddress = strAddress(d.PickupAddress);
                        dvm.dropoffName = d.DropoffAddress.Description; 
                        dvm.dropoffTime = (d.DropoffTime == null) ? String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", (DateTime?)DateTime.Now.AddYears(-50)) :
                                                                    String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", d.DropoffTime);

                        dvm.dropoffAddress = strAddress(d.DropoffAddress);
                        dvm.service = d.Service;
                        dvm.serviceType = d.ServiceType;
                        dvm.notes = d.Notes;
                        dvm.DNLA = d.DNLA;
                        dvm.signatureRequired = d.NeedSignature;
                        dvm.pickupAddressFull = d.PickupFullAddress;
                        dvm.dropoffAddressFull = d.DropoffFullAddress;
                        log.Error("In SendTrips addresses " + dvm.pickupAddressFull + " " + dvm.dropoffAddressFull + " "
                                                            + d.PickupFullAddress + " " + d.DropoffFullAddress);

                        dvm.check = false;
                        dvm.checkAmount = "0";
                        if (d.Payment != null)
                        {
                            dvm.check = d.Payment.PaymentToDriver;
                            dvm.checkAmount = String.Format("{0:C}", d.Payment.PaymentToDriverAmount);
                        }
                        if ((LatestStatus(d.Id).Equals("CancelDispatch")) ||
                            (LatestStatus(d.Id).Equals("ClosedCancelUnitCh")) ||
                            (LatestStatus(d.Id).Equals("ClosedCancelReset")) ||
                            (LatestStatus(d.Id).Equals("ClosedCancelDispatch")))
                        {
                            //dvm.status = LatestStatus(d.Id);
                            dvm.status = "CancelDispatch";
                            log.Error("In SendTrips Cancel Status Sent = " + dvm.status);
                        } else
                        {
                            dvm.status = LatestStatus(d.Id);
                        }
                        tripNumbers += dvm.id.ToString() + ", ";
                        dispatchViews.Add(dvm);
                    }
                }

                log.Error("In SendTrips Sent " + dispatchViews.Count().ToString() + " " + tripNumbers.ToString() + " trips to " + username);

                if (dispatchViews == null || dispatchViews.Count == 0) {
                    log.Error("In SendTrips Arrival " + " no dispatches " + driverName);
                    DispatchViewModel dvm = new DispatchViewModel();
                    dvm.id = 9998;
                    dvm.pickupName = "No Trips for driver " + username;
                    dvm.pickupTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now);

                    dvm.pickupAddress = "85 Garfield St., Lancaster, NY 14086";
                    dvm.dropoffName = "";
                    dvm.dropoffTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now);

                    dvm.dropoffAddress = "";
                    dvm.service = "";
                    dvm.serviceType = "";
                    dvm.notes = "";
                    dvm.DNLA = false;
                    dvm.signatureRequired = false;
                    dvm.pickupAddressFull = "Go to the Messages tab, then go back ";
                    dvm.dropoffAddressFull = "";

                    dvm.check = false;
                    dvm.checkAmount = "0";

                    dvm.status = "Assigned";
                    dispatchViews.Add(dvm);
                }
                return dispatchViews;

            } catch (Exception ex) {
                log.Error("In SendTrips Exception " + username + ex.Message);

                DispatchViewModel dvm = new DispatchViewModel();
                dvm.id = 9997;
                dvm.pickupName = "Exception: " + ex.Message;
                dvm.pickupTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now);

                dvm.pickupAddress = "85 Garfield St., Lancaster, NY 14086";
                dvm.dropoffName = "";
                dvm.dropoffTime = String.Format("{0:yyyy-MM-dd hh:mm:ss tt}", DateTime.Now);

                dvm.dropoffAddress = "";
                dvm.service = "";
                dvm.serviceType = "";
                dvm.notes = "";
                dvm.DNLA = false;
                dvm.signatureRequired = false;
                dvm.pickupAddressFull = "With Registration Key " + mobileAppKey;
                dvm.dropoffAddressFull = "";

                dispatchViews.Add(dvm);
                return dispatchViews;
            }
        }


        [Route("SendStatus")]
        public ErrorCode SendStatus(int orderNumber, string timestamped, int statusListId)
        {
            try
            {
                log.Info("SendStatus receiving status " + orderNumber.ToString() + " " + statusListId.ToString());

                StatusList status_list = (new StatusListRepository()).GetByID(statusListId);
                if (status_list == null) { 
                    log.Info("SendStatus InvalidStatusListId");
                    return ErrorCode.InvalidStatusListId;
                }

                // Obtain dispatch record using orderNumber (orderNumber is Dispatch.Id)            
                Dispatch dispatch = (new DispatchRepository()).GetByID(orderNumber);
                if (dispatch == null)
                {
                    log.Info("SendStatus OrderNotFound");
                    return ErrorCode.OrderNotFound;
                }

                //using DriverId from dispatch record, obtain MobileAppKey (should be the same for both Driver and Employee tables)
                Driver driver = (new DriverRepository()).GetByID(dispatch.DriverID);
                if (driver == null)
                {
                    log.Info("SendStatus DriverNotFound");
                    return ErrorCode.DriverNotFound;
                }

                log.Info("SendStatus Driver " + driver.Name + " " + driver.MobileAppKey);

                Employee employee = (new EmployeeRepository()).GetByRegKey(driver.MobileAppKey, driver.Name);
                if (employee == null)
                {
                    log.Info("SendStatus InvalidMobileAppKey, driver = " + driver.Name + driver.MobileAppKey == null? " NULL" : driver.MobileAppKey);
                    return ErrorCode.InvalidMobileAppKey;
                }

                log.Info("SendStatus employee " + employee.LastName + " " + employee.FirstName + " " + driver.MobileAppKey);

                MedtransEntities medtransDB = new MedtransEntities();
                trans_bridge tripTable = medtransDB.trans_bridge.Where(p => p.trans_id == dispatch.MedTransId).FirstOrDefault();
                if (tripTable == null)
                {
                    log.Info("SendStatus MedtransTripNotFound");
                    return ErrorCode.MedtransTripNotFound;
                }


                // Add new status for the order if its not closed
                //if (status_list.Description != "Closed")
                //{log.Info("SendStatus MedtransTripNotFound");
                log.Info("SendStatus saving status " + dispatch.Id.ToString() + " " + statusListId.ToString());

                (new StatusRepository()).Add(dispatch.Id, Timestamped(timestamped), statusListId);
                //}

                if (statusListId == (int)Statuses.OperatorCancelled) //this is a cancel client from the phone
                {
                    (new StatusRepository()).Add(dispatch.Id, Timestamped(timestamped).AddSeconds(1), (int)Statuses.ClosedCancelClient);
                    tripTable.bridge_flag = "CancelClient";
                }

                if (statusListId == (int)Statuses.DriverAttested) //this is a driver attested from the phone
                {
                    (new StatusRepository()).Add(dispatch.Id, Timestamped(timestamped).AddSeconds(1), (int)Statuses.ClosedAttested);
                }

                #region Auto Sign In/Out
                //TODO: Registration
                // If (status is pickup off)
                if ((status_list.Description == "AtPickUp") ||
                    (status_list.Description == "PickedUp") ||
                    (status_list.Description == "AtDropOff"))
                {
//// Scheduled pickup of 7:00

//// Driver arrives at 7:00, system logs his start time as 6:45
//// Driver arrives at 7:30, system logs his start time as 7:15
//// Driver arrives at 6:30, system logs his start time as 6:45

                    DateTime timeOfEvent = Timestamped(timestamped);
                    DateTime signIntime = (DateTime)dispatch.PickupTime;
                    
                    if (timeOfEvent > (DateTime)dispatch.PickupTime) signIntime = timeOfEvent;
                    (new TimesheetRepository()).SignInViaMobileApp(employee.Id, signIntime, dispatch.PickupAddress.PostalCode);//Timestamped(timestamped));
                }

                if (status_list.Description == "DroppedOff")
                {
                    (new TimesheetRepository()).SignOutViaMobileApp(employee.Id, Timestamped(timestamped));
                }
                #endregion


                if (status_list.Description == "Accepted")
                {
                    tripTable.OnTheWay = Timestamped(timestamped);
                }

                //#region Updating Medtrans database.

                // Always update status 
                if ((!status_list.Description.Equals("GotCheck")) &&
                    (!status_list.Description.Equals("NotAlone")))
                {
                    //log.Info("Status saved to MedTrans = " + status_list.Description + " Trip Id = " + dispatch.Id.ToString());
                    tripTable.bridge_flag = status_list.Description;
                }

                // Need to update Medtrans database on all status change.. Update timestamped when appropriate
                if (status_list.Description.Equals("AtPickUp"))
                {
                    // update AtPickup time
                    tripTable.At_pickup_time = Timestamped(timestamped);
                }

                if (status_list.Description.Equals("AtDropOff"))
                {
                    //Update AtDroppedoff time
                    tripTable.At_dropoff_time = Timestamped(timestamped);
                }

                if (status_list.Description.Equals("PickedUp"))
                {
                    // update AtPickup time
                    tripTable.RealPickipTime = Timestamped(timestamped);
                }

                if (status_list.Description.Equals("DroppedOff"))
                {
                    //Update AtDroppedoff time
                    tripTable.DoneTime = Timestamped(timestamped);
                }

                if (status_list.Description.Equals("GotCheck"))
                {
                    //Update AtDroppedoff time
                    tripTable.CheckStatus = "GotCheck";
                }

                if (status_list.Description.Equals("NotAlone"))
                {
                    //Update AtDroppedoff time
                    tripTable.NotAloneStatus = "NotAlone";
                }

                if (status_list.Description.Equals("Closed"))
                {
                    //Update AtDroppedoff time
                    tripTable.bridge_flag = "ClosedAttested";
                }
                // Save change in MedTrans DB
                medtransDB.SaveChanges();

                //#endregion


                return ErrorCode.OK;
            } catch  (Exception ex) {
                log.Info("Send Status error = " + ex.Message);
                return ErrorCode.MedTransNotSaved;
            }
        }

        [Route("SendSignature")]
        public ErrorCode SendSignature(int orderNumber, string timestamped)
        {
            Dispatch dispacth = (new DispatchRepository()).GetByID(orderNumber);
            if (dispacth == null)
                return ErrorCode.OrderNotFound;
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                HttpContext.Current.Request.Files[0] : null;
            
            //string imageFolder = HttpContext.Current.Server.MapPath("~/Signatures");
            string imageFolder = ConfigurationManager.AppSettings["SignaturesLocation"].ToString();
            string imageFile = Timestamped(timestamped).ToString("MM_dd_yyyy HH_mm") + "_" + file.FileName;
            string imageFullName = System.IO.Path.Combine(imageFolder, imageFile);

            try
            {
                file.SaveAs(imageFullName);
                
                dispacth.SignaturePath = imageFullName;
                (new DispatchRepository()).Update(dispacth);
                
                return ErrorCode.OK;
            }
            catch (Exception _Exception)
            {
                log.ErrorFormat("Signature Save Error" + _Exception.Message);
                return ErrorCode.UnableToSaveSignature;        
            }
        }

        [Route("SendNotes")]
        public ErrorCode SendNotes(int orderNumber, string notes)
        {
            Dispatch dispatch = (new DispatchRepository()).GetByID(orderNumber);
            if (dispatch == null)
                return ErrorCode.OrderNotFound;
            
            try
            {
                dispatch.Notes = notes;
                (new DispatchRepository()).Update(dispatch);

                return ErrorCode.OK;
            }
            catch (Exception _Exception)
            {
                return ErrorCode.UnableToSaveNotes;
            }
        }


        [Route("SendRegistrationKey")]
        //public ErrorCode SendRegistrationKey(string regKey, string userName, string driverName)
        public ErrorCode SendRegistrationKey(string regKey, string userName)
        {
            try
            {
                log.Error("In SendRegistrationKey, key = " + regKey + " " + userName);
                bool regKeyMissing = (regKey == "") || (regKey == null);
                bool userNameMissing = (userName == "") || (userName == null);
                //bool driverNameMissing = (driverName == "") || (driverName == null);

                if (regKeyMissing || userNameMissing)
                    return ErrorCode.MissingRegistrationInfo;

                /* Get the employees name to update reg key in driver table */
                Employee employee = (new EmployeeRepository()).GetByName(userName);
                if (employee == null)
                    return ErrorCode.UserNameNotFound;

                string driverName = employee.FirstName + " " + employee.LastName;
                log.Error("In SendRegistrationKey, driver = " + driverName);
                Driver driver = (new DriverRepository()).GetByName(driverName);
                if (driver == null)
                    return ErrorCode.DriverNotFound;

                log.Error("In SendRegistrationKey, Before assigning key to driver = " + driver.Name);
                driver.MobileAppKey = regKey;
                (new DriverRepository()).Update(driver);
                log.Error("In SendRegistrationKey, after assigning key to driver = " + driver.MobileAppKey);

                employee.MobileAppKey = regKey;
                (new EmployeeRepository()).Update(employee);
                log.Error("In SendRegistrationKey, After assigning key to employee = " + employee.MobileAppKey);

                /* Remove all other entries in driver and employee database which has the same token. This would happen when the
                 * phone is give to someone else.
                 */
                 //List<Employee> empsWithSameRegKey = (new EmployeeRepository()).
            }
            catch (Exception ex) {
                log.Error("Send Reg Key error = " + ex.Message);
                return ErrorCode.MedTransNotSaved;
            }

            return ErrorCode.OK;
        }

        [Route("ResetStatus")]
        public ErrorCode ResetStatus(string regKey, string username)
        {
            Employee emp = (new EmployeeRepository().GetByName(username));
            // Need to query all orders for status of assigned/new and cancelled (by dispatch)

            string driverName = emp.FirstName + " " + emp.LastName;
            
            Driver driver = (new DriverRepository()).GetByMobileAppKey(regKey, driverName);
            if (driver == null)
            {
                log.ErrorFormat("Reset Trips for user {0}. failed", username);

                // return a null is an indication of failure
                return ErrorCode.DriverNotFound;
            }

            StatusRepository staRep = new StatusRepository();
            staRep.DeleteAllByNotStatusDescription(driver.Id);

            log.ErrorFormat("Reset Trips for user {0}. passed", username);

            return ErrorCode.OK;
        }
        #region private function
        private bool IsDateTime(string timestamped)
        {
            DateTime testDate;
            return DateTime.TryParse(timestamped, out testDate);
        }
        private string strAddress(Address address)
        {
            return address.Street + " // " + address.City + ", " + address.State + ", " + address.PostalCode;
        }

        private DateTime Timestamped(string timestamped)
        {
            DateTime dateValue;

            DateTime.TryParse(timestamped, out dateValue);
            // TODO: Need to implement conversion timestamped string into DateTime.

            return dateValue;
        }


        private string LatestStatus(int dispatchId)
        {
            string status = "";
            List<Status> statuses = (new StatusRepository()).GetByDispatchID(dispatchId);
            if (statuses.Count > 0)
            {
                status = statuses[0].StatusList.Description;
            }           
            return status;
        }
        #endregion
    }
}