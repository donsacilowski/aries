package com.ariestransportation.ariesmobile;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity implements TripDatabaseHelper.PollerMessage {
    TripDatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TripDatabaseHelper dbHelper = TripDatabaseHelper.getInstance(this);

        ArrayList<Message> myMessages = new ArrayList<Message>();
        myMessages = dbHelper.getMessageList();

        MessageAdapter messageAdapter = new MessageAdapter(this, R.layout.message_item, myMessages);

        ListView myList=(ListView)
                findViewById(R.id.listViewMessages);
        myList.setAdapter(messageAdapter);
    }
    @Override
    protected void onPause() {
        super.onPause();

        dbHelper.messageActivity = null; //deregistering for trips polling
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onStart() {
        super.onStart();
        dbHelper = TripDatabaseHelper.getInstance(this);
        //GPSLocation gps = new GPSLocation(getApplicationContext());
        dbHelper.messageActivity = this; //registering for polling of trips

        pollMessages();
    }

    public void pollMessages(){
        ArrayList<Message> myMessages = new ArrayList<Message>();
        myMessages = dbHelper.getMessageList();

        MessageAdapter messageAdapter = new MessageAdapter(this, R.layout.message_item, myMessages);

        ListView myList=(ListView)
                findViewById(R.id.listViewMessages);
        myList.setAdapter(messageAdapter);
    }
}
