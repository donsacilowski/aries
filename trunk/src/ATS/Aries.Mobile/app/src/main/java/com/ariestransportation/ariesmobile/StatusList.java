package com.ariestransportation.ariesmobile;

/**
 * Created by dsacilowski on 7/18/2016.
 */
public class StatusList {

    public Integer id; //primary key id from the server
    public String description;

    public StatusList(){
        super();
    }

    public StatusList(Integer myID, String myDescription) {
        super();
        this.id = myID;
        this.description = myDescription;
    };
}
