package com.ariestransportation.ariesmobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.R.attr.format;

public class TripDetailActivity extends AppCompatActivity {

    private static final float METERS_TO_MILES = 0.000621371192f;
    private static final float DISTANCE_TOLERANCE = 0.25F;

    private static Context ctx;
    Button departButton = null;
    static Button arriveButton = null;
    TripDatabaseHelper dbHelper;
    CheckBox cbDNLA = null;
    CheckBox cbCheck = null;
    TextView tvPayAmount = null;
    Button signatureButton = null;
    TripDetail trip = null;
    TextView tv = null;
    TextView notes = null;
    String desc = "";

    static SharedPreferences.Editor editor;

    boolean DNLAhit = false;
    boolean checkHit = false;

    boolean DNLADone = false;
    boolean checkDone = false;
    boolean signatureDone = false;
    boolean arrived = false;

    CharSequence baseCheckText;

    static Integer numPickups = 0;

    static GPSLocation gps;
    static Location pickupGPS;

    Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ctx = this;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean arrivedToday = prefs.getBoolean("arrivedToday", false);

        if (!arrivedToday) {
            gps = new GPSLocation(getApplicationContext());
        }

        departButton = (Button) findViewById(R.id.buttonDepart);
        departButton.setEnabled(false);

        arriveButton = (Button) findViewById(R.id.buttonArrive);

        final Button cancelButton = (Button) findViewById(R.id.buttonCancel);
        cancelButton.setVisibility(View.INVISIBLE);

        cbDNLA = (CheckBox)findViewById(R.id.checkBoxDNLA);
        cbCheck = (CheckBox)findViewById(R.id.checkBoxCheck);
        baseCheckText = cbCheck.getText();

        signatureButton = (Button) findViewById(R.id.buttonGetSignature);

        tv = (TextView) findViewById(R.id.textLegInstructions);
        notes = (TextView) findViewById(R.id.textNotes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        String strID = getResources().getString(R.string.trip_detail_id);
        final Integer tripID = bundle.getInt(strID);

        dbHelper = TripDatabaseHelper.getInstance(this);

        trip = dbHelper.readTrip(tripID);

        /* Pickup not done, display the pickup instructions */
        if (!trip.pickUpComplete) {
            desc = trip.pickupTime + "\r\n\t\t" + "Pickup " + trip.pickupName + "\r\n\t\t" + trip.pickupAddressFull + " \r\n\t\t" +
                   trip.service + " " + trip.serviceType;
            tv.setText(desc);
            notes.setText(trip.notes);
        } else {
            desc = trip.dropoffTime + "\r\n" + "Dropoff " + trip.pickupName + " at " + trip.dropoffName + "\r\n" + trip.dropoffAddressFull ;
            tv.setText(desc);
        }

        /* Only process check, DNLA and signature when this is not a combined leg */
        if (!trip.pickUpComplete){
            if (trip.combinedPickup == 0) {
                processSelectionButtons(trip);
            } else {
                cbCheck.setVisibility(View.INVISIBLE);
                cbDNLA.setVisibility(View.INVISIBLE);
                signatureButton.setVisibility(View.INVISIBLE);
            }
        }

        if (!trip.tripComplete && trip.pickUpComplete){
            if (trip.combinedDropoff == 0) {
                processSelectionButtons(trip);
            } else {
                cbCheck.setVisibility(View.INVISIBLE);
                cbDNLA.setVisibility(View.INVISIBLE);
                signatureButton.setVisibility(View.INVISIBLE);
            }
        }

        if ((trip.atPickup && !trip.pickUpComplete) || trip.atDropOff) {
        //if (trip.atDropOff) {
            arriveButton.setEnabled(false);
            arrived = true;
            cancelButton.setVisibility(View.VISIBLE);

            processSelectionButtons(trip);
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final AlertDialog alert = new AlertDialog.Builder(
                        new ContextThemeWrapper(view.getContext(),android.R.style.Theme_Dialog))
                        .create();

                alert.setTitle("Cancel Trip Confirmation");
                alert.setMessage("Are you sure you want to cancel this trip");
                alert.setCancelable(false);
                alert.setCanceledOnTouchOutside(false);

                alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                TripDetail locTrip = dbHelper.readTrip(tripID);

                                alert.dismiss();
                                locTrip.status = TripDatabaseHelper.Statuses.CancelClient;
                                locTrip.tripComplete = true;
                                dbHelper.updateTrip(locTrip);

                                String date = Dates.formatNowDateTime(view.getContext());

                                Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.CancelClient, false);

                                dbHelper.insertStatus(sta, view.getContext());

                                Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                                startActivity(intent);
                            }
                        });

                alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                alert.dismiss();
                            }
                        });

                alert.show();
            }
        });

        arriveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TripDetail locTrip = dbHelper.readTrip(tripID);

                arrived = true;

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                boolean arrivedToday = prefs.getBoolean("arrivedToday", false);

                Date today = new Date();

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String dateNow = format.format(today);
                Calendar c = Calendar.getInstance();

                try{
                    c.setTime(format.parse(dateNow));
                }catch(Exception ex) {

                }

                c.add(Calendar.DATE, -1);
                String yesterday = format.format(c.getTime());

                String arrivedDay = prefs.getString("arrivedDay", yesterday);

                if (!arrivedToday || !arrivedDay.equals(dateNow)) {
                    Toast.makeText(
                            TripDetailActivity.this,
                            "First Arrival",
                            Toast.LENGTH_SHORT
                    ).show();
                    editor = prefs.edit();
                    editor.putBoolean("arrivedToday", true);
                    editor.putString("arrivedDay", dateNow);

                    pickupGPS = getLocationFromAddress(locTrip.pickupAddress);

                    gps.getLastLocation(ctx);

                    editor.commit();

                }
                cancelButton.setVisibility(View.VISIBLE);

                arriveButton.setEnabled(false);
                String date = Dates.formatNowDateTime(view.getContext());

                if (!locTrip.pickUpComplete) {
                    ArrayList<TripDetail> tdList = null;
                    tdList = dbHelper.getCombinedPickupTrips(locTrip.id);

                    if (tdList.size() > 0) {
                        for (TripDetail td : tdList) {

                            Toast.makeText(
                                    TripDetailActivity.this,
                                    "Processing Multiple Pickups...",
                                    Toast.LENGTH_SHORT
                            ).show();

                            date = Dates.formatNowDateTime(getBaseContext());
                            Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.AtPickUp, false);
                            dbHelper.insertStatus(sta, getBaseContext());

                            try {
                                Thread.sleep(1000);
                            }
                            catch (InterruptedException ex)
                            { }

                            td.status = TripDatabaseHelper.Statuses.AtPickUp;
                            td.pickUpComplete = false;
                            td.atPickup = true;

                            dbHelper.updateTrip(td);
                        }
                        DNLADone = true;
                        checkDone = true;
                        signatureDone = true;

                    } else {
                        Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.AtPickUp, false);
                        locTrip.status = TripDatabaseHelper.Statuses.AtPickUp;
                        dbHelper.insertStatus(sta, view.getContext());

                        locTrip.atPickup = true;

                        if (locTrip.combinedPickup != 0) {
                            DNLADone = true;
                            checkDone = true;
                            signatureDone = true;
                        }

                        dbHelper.updateTrip(locTrip);
                    }
                } else {
                    ArrayList<TripDetail> tdList = dbHelper.getCombinedDropoffTripsPickedUp(locTrip.id);
                    if (tdList.size() > 0) {
                        for (TripDetail td : tdList) {

                            Toast.makeText(
                                    TripDetailActivity.this,
                                    "Processing Multiple Dropoffs...",
                                    Toast.LENGTH_SHORT
                            ).show();

                            date = Dates.formatNowDateTime(getBaseContext());
                            Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.AtDropOff, false);
                            dbHelper.insertStatus(sta, getBaseContext());

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                            }

                            td.status = TripDatabaseHelper.Statuses.AtDropOff;

                            td.atDropOff = true;

                            dbHelper.updateTrip(td);
                        }
                    } else {
                        date = Dates.formatNowDateTime(getBaseContext());
                        Status sta = new Status(0, locTrip.id, date, TripDatabaseHelper.Statuses.AtDropOff, false);
                        dbHelper.insertStatus(sta, getBaseContext());

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                        }

                        locTrip.status = TripDatabaseHelper.Statuses.AtDropOff;

                        locTrip.atDropOff = true;
                        dbHelper.updateTrip(locTrip);
                    }

                    if (locTrip.combinedDropoff != 0){
                        DNLADone = true;
                        checkDone = true;
                        signatureDone = true;
                    }
                }

                cbDNLA.setEnabled(true);
                if (locTrip.DNLADone) {
                    cbDNLA.setEnabled(false);
                }

                if (!locTrip.checkDone) {
                    cbCheck.setEnabled(true);
                }

                if (!locTrip.signatureDone){
                    signatureButton.setEnabled(true);
                }

                setDepartButton(locTrip);
            }
        });

        departButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TripDetail locTrip = dbHelper.readTrip(tripID);

                String date = Dates.formatNowDateTime(view.getContext());

                ArrayList<TripDetail> tdList = null;
                if (!locTrip.pickUpComplete){

                    arriveButton.setEnabled(true);
                    locTrip.status = TripDatabaseHelper.Statuses.PickedUp;
                    locTrip.pickUpComplete = true;
                    dbHelper.updateTrip(locTrip);

                    DNLADone = false;
                    checkDone = false;
                    signatureDone = false;

                    tdList = dbHelper.getCombinedPickupTrips(locTrip.id);
                    ArrayList<TripDetail> tdCombList = dbHelper.getCombinedDropoffTrips(locTrip.id);

                    if (tdList.size() > 0){
                        confirmCombinedChoice(view.getContext(), "Did you pickup ", tdList, "Pickup", tdList, locTrip);
                    } else {
                        Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.PickedUp, false);
                        dbHelper.insertStatus(sta, view.getContext());

                        locTrip.atPickup = false;

                        if (tdCombList.size()>0) {
                            dbHelper.updateTrip(locTrip);

                            numPickups++;

                            Integer combinedPickedUp = dbHelper.getCombinedDropoffTripsPickedUpCount(locTrip.id);
                            if (combinedPickedUp == tdCombList.size()){
                                desc = locTrip.dropoffTime + "\r\n" + "Dropoff" + locTrip.pickupName + " at " +
                                        locTrip.dropoffName + "\r\n" + locTrip.dropoffAddress;

                                numPickups = 0;

                                tv.setText(desc);
                            } else {
                                Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                                startActivity(intent);
                            }

                        } else {
                            desc = locTrip.dropoffTime + "\r\n" + "Dropoff " + locTrip.pickupName + " at " +
                                    locTrip.dropoffName + "\r\n" + locTrip.dropoffAddress;

                            tv.setText(desc);
                            processSelectionButtons(locTrip);
                        }
                    }
                    renameSigFile();
                    departButton.setEnabled(false);
                }
                else {

                    tdList = dbHelper.getCombinedDropoffTripsPickedUp(locTrip.id);

                    if (tdList.size() > 0){
                        confirmCombinedChoice(view.getContext(), "Did you dropoff ", tdList, "Dropoff", tdList, locTrip);
                    } else {

                        locTrip.tripComplete = true;
                        locTrip.status = TripDatabaseHelper.Statuses.DroppedOff;

                        locTrip.atDropOff = false;

                        attestToTrip(view.getContext(), locTrip.id);
                        renameSigFile();
                    }
                }
                dbHelper.updateTrip(locTrip);
            }
        });
    }

    private void processSelectionButtons(final TripDetail trip) {
        cbDNLA.setVisibility(View.INVISIBLE);
        cbDNLA.setEnabled(false);

        if (trip.DNLA) {
            if (trip.pickUpComplete) {
                cbDNLA.setVisibility(View.VISIBLE);

                cbDNLA.setChecked(trip.DNLADone);
                DNLADone = false;
                //trip.DNLADone = false;
                if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtDropOff.getValue()) {
                    cbDNLA.setEnabled(true);
                }
                cbDNLA.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        confirmDialog(view.getContext(), "Do you confirm that this person was Not Left Alone", locTrip);
                        DNLAhit = true;

                        setDepartButton(locTrip);
                    }
                });
            } else {
                DNLADone = true;
            }
        }
        else {
            DNLADone = true;
        }
        if (trip.DNLADone){
            DNLADone = true;
        }
        dbHelper.updateTrip(trip);

        signatureButton.setVisibility(View.INVISIBLE);
        signatureButton.setEnabled(false);

        if (trip.signatureRequired) {
            if (!trip.signatureDone) {
                signatureButton.setVisibility(View.VISIBLE);
                signatureButton.setEnabled(false);
                trip.signatureDone = false;
                if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtPickUp.getValue()) {
                    signatureButton.setEnabled(true);
                }
                signatureButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        locTrip.signatureDone = true;
                        signatureDone = true;
                        dbHelper.updateTrip(locTrip);

                        setDepartButton(locTrip);

                        Bundle extras = new Bundle();
                        extras.putInt(getResources().getString(R.string.trip_detail_id), locTrip.id);

                        Intent intent = new Intent(TripDetailActivity.this, SignatureActivity.class);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });
            } else {
                signatureDone = true;
            }
        }
        else {
           signatureDone = true;
        }
        dbHelper.updateTrip(trip);

        cbCheck.setVisibility(View.INVISIBLE);
        cbCheck.setEnabled(false);

        if (trip.check) {
            cbCheck.setVisibility(View.VISIBLE);
            cbCheck.setText(baseCheckText + trip.checkAmount.toString());
            cbCheck.setChecked(trip.checkDone);

            if (trip.status.getValue() >= TripDatabaseHelper.Statuses.AtPickUp.getValue()) {
                cbCheck.setEnabled(true);
            }

            if(!trip.checkDone) {

                cbCheck.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        TripDetail locTrip = dbHelper.readTrip(trip.id);
                        confirmDialog(view.getContext(), "Did you pick up a check?", locTrip);
                        checkHit = true;
                    }
                });
            } else {
                cbCheck.setEnabled(false);
                //cbCheck.setVisibility(View.INVISIBLE);
                checkDone = true;
            }
        } else {
            checkDone = true;
        }
        dbHelper.updateTrip(trip);
        setDepartButton(trip);
    }

    private void setDepartButton(TripDetail trip) {
        if (DNLADone && checkDone && signatureDone && arrived) {
        //if (DNLADone && signatureDone && arrived) {
            departButton.setEnabled(true);
        }
    }

    private void attestToTrip(final Context context, final int tripID) {
        try {


        final AlertDialog alert = new AlertDialog.Builder(
                new ContextThemeWrapper(context,android.R.style.Theme_Dialog))
                .create();

        alert.setTitle("Trip Attesting");
        alert.setMessage("Do you confirm all that was required in this trip has been accomplished?");
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);

        alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                TripDetail locTrip = dbHelper.readTrip(tripID);

                String date = Dates.formatNowDateTime(context);

                Status sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.DroppedOff, false);
                dbHelper.insertStatus(sta, context);

                locTrip.combinedDropoff = 0;
                locTrip.combinedPickup = 0;

                alert.dismiss();
                locTrip.status = TripDatabaseHelper.Statuses.DriverAttested;
                locTrip.tripComplete = true;
                locTrip.atDropOff = false;

                dbHelper.updateTrip(locTrip);

                sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.DriverAttested, false);
                dbHelper.insertStatus(sta, context);

                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ex)
                { }

                date = Dates.formatNowDateTime(getBaseContext());
                sta = new Status(0, tripID, date, TripDatabaseHelper.Statuses.Closed, false);
                dbHelper.insertStatus(sta, context);

                Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
                startActivity(intent);
            }
        });

        alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alert.dismiss();
            }
        });

        alert.show();
        } catch (Exception ex){
            Toast.makeText(
                    TripDetailActivity.this,
                    "Alert Error = " + ex.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();

        }
    }

    private void renameSigFile () {
        //Rename the signature file, then send it to the server
        // the directory where the signature will be saved
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_signature");
        //File myDir = new File(root);

        // make the directory if it does not exist yet
        if (!myDir.exists()) {
            boolean err = myDir.mkdirs();
            if (err) {

            }
        }

        // set the file name of your choice
        String fname = "signature.png";
        File file = new File(myDir, fname);
        File renamedFile = new File("signature.png"+trip.id.toString());
        if (file.exists()) {

            boolean err = file.renameTo(renamedFile);
            Toast.makeText(
                    TripDetailActivity.this,
                    "Rename signature file = " + err,
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
    private void confirmDialog(Context context, String message, final TripDetail trip){

        final AlertDialog alert = new AlertDialog.Builder(
                new ContextThemeWrapper(context,android.R.style.Theme_Dialog))
                .create();

        alert.setTitle("Alert");
        alert.setMessage(message);
        alert.setIcon(R.drawable.sign_warning_icon);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);

        alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        alert.dismiss();
                        returnUserChoice("Yes", trip);
                    }
                });

        alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        alert.dismiss();
                        returnUserChoice("No", trip);
                    }
                });

        alert.show();
    }

    private void confirmCombinedChoice(final Context context,
                                       String message,
                                       final ArrayList<TripDetail> trips,
                                       final String leg,
                                       final ArrayList<TripDetail> trip,
                                       final TripDetail tr){

        final AlertDialog alert = new AlertDialog.Builder(
                new ContextThemeWrapper(context,android.R.style.Theme_Dialog))
                .create();

        alert.setTitle("Combined Alert");

        String fullMessage = message + trip.size() + " People - ";

        for (TripDetail td : trips) {
            if (leg == "Pickup") {
                fullMessage += td.pickupName;
            } else {
                fullMessage += td.pickupName;
            }
            fullMessage += "\r\n";
        }
        alert.setMessage(fullMessage);

        alert.setIcon(R.drawable.sign_warning_icon);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);

        try {
            alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            alert.dismiss();
                            returnCombinedChoice("Yes", trip, leg, tr);

                            /*if (!tr.pickUpComplete) {
                                attestToTrip(context, tr.id);
                            }*/
                        }
                    });

            alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            alert.dismiss();
                            returnCombinedChoice("No", trip, leg, tr);
                        }
                    });

            alert.show();
        } catch (Exception ex){
            Toast.makeText(
                    TripDetailActivity.this,
                    "Alert Error = " + ex.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    private void returnCombinedChoice(String choice, ArrayList<TripDetail> trip, String leg, TripDetail tr) {

        boolean goToTrips = false;

        for (TripDetail td : trip) {
            if (choice == "Yes") {
                String date = Dates.formatNowDateTime(getBaseContext());

                if (leg == "Pickup") {

                    date = Dates.formatNowDateTime(getBaseContext());
                    Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.PickedUp, false);
                    dbHelper.insertStatus(sta, getBaseContext());

                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException ex)
                    { }

                    td.status = TripDatabaseHelper.Statuses.PickedUp;
                    td.pickUpComplete = true;
                    td.atPickup = false;

                    desc = td.dropoffTime + "\r\n" + "Dropoff " + td.pickupName + " at " +
                            td.dropoffName + "\r\n" + td.dropoffAddress;

                    tv.setText(desc);
                    processSelectionButtons(td);
                    dbHelper.updateTrip(td);

                    ArrayList<TripDetail> tdCombList = dbHelper.getCombinedDropoffTrips(td.id);
                    /* If there's still more people to pickup for the combined dropoff, go back to the trips screen */
                    if (tdCombList.size() > trip.size()) {
                        goToTrips = true;
                    }


                } else {
                    //if (td.status.ordinal() == TripDatabaseHelper.Statuses.PickedUp.ordinal()) {
                        Status sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.DroppedOff, false);
                        dbHelper.insertStatus(sta, getBaseContext());

                        try {
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException ex)
                        { }

                        date = Dates.formatNowDateTime(getBaseContext());
                        sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.DriverAttested, false);
                        dbHelper.insertStatus(sta, getBaseContext());

                        try {
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException ex)
                        { }

                        date = Dates.formatNowDateTime(getBaseContext());
                        sta = new Status(0, td.id, date, TripDatabaseHelper.Statuses.Closed, false);
                        dbHelper.insertStatus(sta, getBaseContext());

                        td.tripComplete = true;
                        td.combinedDropoff = 0;
                        td.combinedPickup = 0;
                        td.atDropOff = false;
                    //}

                    goToTrips = true;
                }
            } else {
                if (leg == "Pickup") {
                    td.pickUpComplete = false;
                    td.atPickup = true;
                } else {
                    td.tripComplete = false;
                    td.atDropOff = true;
                }
            }
            dbHelper.updateTrip(td);
        }
        if (goToTrips){
            Intent intent = new Intent(TripDetailActivity.this, TripsActivity.class);
            startActivity(intent);
        }
    }

    private void returnUserChoice(String choice, TripDetail trip) {

        String date = Dates.formatNowDateTime(getBaseContext());

        if (DNLAhit) {
            DNLAhit = false;
            if (choice == "Yes") {

                Status sta = new Status(0, trip.id, date, TripDatabaseHelper.Statuses.NotAlone, false);
                dbHelper.insertStatus(sta, getBaseContext());

                trip.status = TripDatabaseHelper.Statuses.NotAlone;
                DNLADone = true;
                trip.DNLADone = true;
            } else {
                trip.DNLADone = false;
            }
        }
        if (checkHit) {
            checkHit = false;
            if (choice == "Yes") {
                Status sta = new Status(0, trip.id, date, TripDatabaseHelper.Statuses.GotCheck, false);

                trip.status = TripDatabaseHelper.Statuses.GotCheck;
                dbHelper.insertStatus(sta, getBaseContext());

                trip.checkDone = true;
                cbCheck.setChecked(true);
            } else {
                trip.checkDone = false;

                cbCheck.setChecked(false);
            }
            checkDone = true;
        }
        dbHelper.updateTrip(trip);
        setDepartButton(trip);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    public Location getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address = null;
        Location p1 = new Location(getBaseContext().toString());

        try {
            address = coder.getFromLocationName(strAddress,5);
            }
        catch (IOException e) {
                e.printStackTrace();
            }

        if (address==null || address.size() == 0) {
            return null;
        }
        Address location=address.get(0);
        location.getLatitude();
        location.getLongitude();

        p1.setLongitude(location.getLongitude());
        p1.setLatitude(location.getLatitude());

        return p1;
    }

    public static void gotPos(Location loc){

        if (pickupGPS != null) {
            float distance = pickupGPS.distanceTo(loc) * METERS_TO_MILES;

            if (distance > DISTANCE_TOLERANCE) {

                final AlertDialog alert = new AlertDialog.Builder(
                        new ContextThemeWrapper(ctx, android.R.style.Theme_Dialog))
                        .create();
                alert.setTitle("Not at Destination");

                String fullMessage = "You are not close to your pickup. Do you still want to consider you have arrived?";
                alert.setMessage(fullMessage);

                alert.setIcon(R.drawable.sign_warning_icon);
                alert.setCancelable(false);
                alert.setCanceledOnTouchOutside(false);

                try {
                    alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    arriveButton.setEnabled(false);
                                    alert.dismiss();

                                    RequestQueue queue = Volley.newRequestQueue(ctx);
                                    String url = ctx.getResources().getString(R.string.portal_server_site) + "location/Add?username=" + LoginActivity.userName;

                                    url = url.replaceAll(" ", "%20");

                                    // Request a string response from the provided URL.
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {

                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            //Toast.makeText(context, "Status Save Error: " + staList.size() + " " + staNum + " " + error.toString(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                    queue.add(stringRequest);
                                    gps.endUpdates();
                                }
                            });

                    alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    arriveButton.setEnabled(true);
                                    editor.remove("arrivedDay");
                                    editor.putBoolean("arrivedToday", false);
                                    editor.commit();
                                    alert.dismiss();
                                }
                            });

                    alert.show();
                } catch (Exception ex) {
                    Toast.makeText(
                            ctx,
                            "Alert Error = " + ex.getMessage(),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        }
    }
}
