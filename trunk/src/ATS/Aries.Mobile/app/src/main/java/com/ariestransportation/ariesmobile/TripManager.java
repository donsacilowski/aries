package com.ariestransportation.ariesmobile;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dsacilowski on 7/13/2016.
 */
public class TripManager {

    private static final String PREFS_FILE = "runs";
    private static final String PREF_CURRENT_RUN_ID = "TripManager.currentRunId";

    private static TripManager sRunManager;
    private Context mAppContext;
    private long mCurrentRunId;

    private TripDatabaseHelper mHelper;
    private SharedPreferences mPrefs;

    private TripManager(Context appContext) {
        mAppContext = appContext;
        mHelper = TripDatabaseHelper.getInstance(mAppContext);
        mPrefs = mAppContext.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mCurrentRunId = mPrefs.getLong(PREF_CURRENT_RUN_ID, -1);
    }
}
