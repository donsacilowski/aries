package com.ariestransportation.ariesmobile;

/**
 * Created by dsacilowski on 7/18/2016.
 */
public class Status {

    public Integer id; //primary key
    public Integer tripId;
    public String timestamp;
    public TripDatabaseHelper.Statuses statusID;
    public boolean sent;

    public Status(){
        super();
    }

    public Status(Integer id, Integer myID, String myTimestamp, TripDatabaseHelper.Statuses myStatusID, boolean mySent) {
        super();
        this.id = id;
        this.tripId = myID;
        this.timestamp = myTimestamp;
        this.statusID = myStatusID;
        this.sent = mySent;
    };
}
