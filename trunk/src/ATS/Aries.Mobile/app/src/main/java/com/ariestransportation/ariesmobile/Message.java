package com.ariestransportation.ariesmobile;

/**
 * Created by dsacilowski on 1/4/2017.
 */

public class Message {
    public Integer id; //primary key
    public String  message;
    public String dateReceived;

    public Message(){
        super();
    }

    public Message(Integer id, String msg, String myTimestamp) {
        super();
        this.id = id;
        this.message = msg;
        this.dateReceived = myTimestamp;
    };
}
