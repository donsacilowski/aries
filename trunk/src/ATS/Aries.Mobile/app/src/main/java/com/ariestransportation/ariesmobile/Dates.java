package com.ariestransportation.ariesmobile;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by dsacilowski on 8/4/2016.
 */
public class Dates {
    public static String formatNowDateTime(Context context) {

        Date ts = new Date(System.currentTimeMillis());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(ts);

        return date;
    }
}
