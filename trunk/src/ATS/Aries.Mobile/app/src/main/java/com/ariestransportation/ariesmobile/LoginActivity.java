package com.ariestransportation.ariesmobile;

import android.*;
import android.content.ContentResolver;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.*;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    public static String userName = "";
    public static String password = "";
    private ProgressBar spinner;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int permission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int gpspermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean userLoggedIn = prefs.getBoolean("userLoggedIn", false);

        if (permission != PackageManager.PERMISSION_GRANTED ||
            gpspermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user

            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView tv = (TextView)findViewById(R.id.txtVolley);
        final TripDatabaseHelper dbHelper;
        dbHelper = TripDatabaseHelper.getInstance(getBaseContext());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        spinner=(ProgressBar)findViewById(R.id.loginprogressBar);
        spinner.setVisibility(View.INVISIBLE);

        SharedPreferences.Editor editor = prefs.edit();

        userName = prefs.getString("username", "empty");
        password = prefs.getString("password", "empty");
        //File dbFile = getBaseContext().getDatabasePath(dbHelper.DB_NAME);
        prefs.edit().remove("arrivedToday").commit();
        prefs.edit().remove("arrivedDay").commit();
        //db.execSQL("DROP TABLE IF EXISTS trip_detail");
        dbHelper.onCreate(db);
        dbHelper.createMsgtable(db);

        //GPSLocation gps = new GPSLocation(getApplicationContext());

        /*if (gps.location != null) {
            Toast.makeText(getBaseContext(), "Lat = " + gps.location.getLatitude() +
                                              " Long = " + gps.location.getLongitude(),
                           Toast.LENGTH_LONG).show();
        }
*/
        dbHelper.deleteOldTrips();
        dbHelper.deleteOldMessages();
        /*dbHelper.deleteAllTrips();*/
        ArrayList<TripDetail> tris = dbHelper.getAllTripsList();
        /*editor.putBoolean("shouldPoll", true);
        editor.commit();*/
        if (userName.equals("empty") || password.equals("empty"))
        {

        }
/*
        String where = "update trip_detail set trip_complete=0";
        db.execSQL(where);
*/

        if (userLoggedIn) {
            Intent intent = new Intent(LoginActivity.this, TripsActivity.class);
            startActivity(intent);
        }

        //dbHelper.setCombinedTrips();
        Button loginButton = (Button) findViewById(R.id.buttonLogin);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    spinner.setVisibility(View.VISIBLE);

                /* Update users token */
                    TextView uname = (TextView) findViewById(R.id.txtUserName);
                    userName = uname.getText().toString();
                    TextView pass = (TextView) findViewById(R.id.txtPassword);
                    password = pass.getText().toString();

                    final RequestQueue queue = Volley.newRequestQueue(getBaseContext());

                    String url = getBaseContext().getResources().getString(R.string.portal_server_site) + "Account/MobileLogin?username=" + userName + "&password=" + password;

                    url = url.replaceAll(" ", "%20");

                    // Request a string response from the provided URL.
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    if (response.equals("True")) {
                                        TextView uname = (TextView) findViewById(R.id.txtUserName);
                                        String userName = uname.getText().toString();

                                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putBoolean("userLoggedIn", true);
                                        editor.putString("username",userName);
                                        editor.putString("password",password);
                                        editor.commit();

                                        String url = getBaseContext().getResources().getString(R.string.api_server_site) + "api/trip/SendRegistrationKey?regKey=" + refreshedToken +
                                                "&userName=" + userName;

                                        url = url.replaceAll(" ", "%20");

                                        // Request a string response from the provided URL.
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        spinner.setVisibility(View.INVISIBLE);

                                                        if (response.equals("0")) {
                                                            Intent intent = new Intent(LoginActivity.this, TripsActivity.class);
                                                            startActivity(intent);
                                                        } else if (response.equals("7")) {
                                                            Toast.makeText(getBaseContext(), "User does not exist ", Toast.LENGTH_LONG).show();
                                                        } else if (response.equals("8")) {
                                                            Toast.makeText(getBaseContext(), "Driver does not exist ", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                spinner.setVisibility(View.INVISIBLE);

                                                Toast.makeText(getBaseContext(), "Error updating key: " + error.toString(), Toast.LENGTH_LONG).show();

                                            }
                                        }) {
                                            @Override
                                            protected Map<String, String> getParams() {
                                                Map<String, String> params = new HashMap<String, String>();
                                                return params;
                                            }
                                        };
                                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                        // Add the request to the RequestQueue.
                                        queue.add(stringRequest);
                                    } else {
                                        spinner.setVisibility(View.INVISIBLE);

                                        Toast.makeText(getBaseContext(), "Login Failed; Try Again", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            spinner.setVisibility(View.INVISIBLE);

                            Toast.makeText(getBaseContext(), "Error Logging in: " + error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    queue.add(stringRequest);
                }
            });
        //}
        Button resetButton = (Button) findViewById(R.id.buttonReset);
        resetButton.setVisibility(View.INVISIBLE);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dbHelper.onCreate(db);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("shouldPoll", true);
                editor.commit();

                RequestQueue queue = Volley.newRequestQueue(getBaseContext());

                String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                String url = getBaseContext().getResources().getString(R.string.api_server_site) + "api/trip/resetStatus?regKey=" + refreshedToken + "&username="+userName;

                url = url.replaceAll(" ", "%20");

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (response.equals("0")) {
                                    Toast.makeText(getBaseContext(), "Reset Successful ", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), "Error resetting status: " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            Bundle itm = getIntent().getExtras();
            if (itm != null) {
                String newOrders = itm.getString("newOrders");
                if (newOrders.equals("true")) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("shouldPoll", true);
                    editor.commit();
                }
            }
        } catch (Exception ex){

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }
        if (id == R.id.action_timesheet) {
            return true;
        }
        if (id == R.id.action_orderHistory) {
            return true;
        }
        if (id == R.id.action_RequestTimeOff) {
            return true;
        }
        if (id == R.id.action_Messages) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
