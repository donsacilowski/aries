package com.ariestransportation.ariesmobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ariestransportation.ariesmobile.Trips;

import java.util.ArrayList;

/**
 * Created by dsacilowski on 7/8/2016.
 */
public class MessageAdapter extends ArrayAdapter<Message> {
    private Context context;
    private int resource;
    private ArrayList<Message> objects;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=
                ((Activity) context).getLayoutInflater();

        View row=inflater.inflate(resource,parent,false);

        TextView title= (TextView)
                row.findViewById(R.id.textViewMessageTimeReceived);
        title.setText("");

        TextView name= (TextView) row.findViewById(R.id.textViewMessagemessage);
        String msg = objects.get(position).message.concat(objects.get(position).dateReceived);
        name.setText((CharSequence) msg);

        return row;
    }

    public MessageAdapter(Context context, int resource, ArrayList<Message> objects) {
        super(context, resource, objects);

        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
}
