package com.ariestransportation.ariesmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.WindowManager;
import android.widget.Toast;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;
import static com.ariestransportation.ariesmobile.TripDatabaseHelper.DB_NAME;

/**
 * Created by dsacilowski on 4/22/2017.
 */

public class GPSLocation implements LocationListener {
    public LocationManager locationManager;
    private Context mCtx;
    private static GPSLocation mInstance;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1; // 1 minute

    public Location location;

    public Criteria criteria;
    public String bestProvider;
    private boolean gpsOn;

    public GPSLocation(Context context) {
        super();
        mCtx = context;
        //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager = (LocationManager) mCtx.getSystemService(LOCATION_SERVICE);
        try {
            boolean gpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (gpsOn) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        0,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
                if (location == null){
                    Toast.makeText(mCtx, "Null Location", Toast.LENGTH_LONG).show();
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            0,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
            }
        } catch(SecurityException ex) {
            Toast.makeText(mCtx, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static GPSLocation getInstance(Context context) {
        if (mInstance == null) {
            mInstance =  new GPSLocation(context);
        }
        return mInstance;
    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(mCtx, "Location changed", Toast.LENGTH_LONG).show();
        TripDatabaseHelper dbHelper;
        try {
            location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
            /*Toast.makeText(mCtx, location.getLatitude() + " " +
                                 location.getLongitude(),
                           Toast.LENGTH_LONG).show();*/
            String date = Dates.formatNowDateTime(mCtx);
            //Message msg = new Message(0, location.getLatitude()+" "+location.getLongitude(), date);
//            dbHelper = TripDatabaseHelper.getInstance(mCtx);
//            dbHelper.insertMessage(msg);

            //locationManager.removeUpdates(this);
            Geocoder geocoder = new Geocoder(mCtx, Locale.getDefault());
            List<Address> addresses = null;

            //TripDetailActivity.gotPos(location);

        } catch(SecurityException ex) {
            Toast.makeText(mCtx, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void getLastLocation(Context ctx) {
        int gpspermission = ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (gpspermission != -1) {
            location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);

            if (location != null) {
               // Toast.makeText(mCtx, "Got the location", Toast.LENGTH_LONG).show();
                TripDetailActivity.gotPos(location);
            } else{

                Toast.makeText(mCtx, "Can't get the location", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void endUpdates() {
        locationManager.removeUpdates(this);
    }
}
