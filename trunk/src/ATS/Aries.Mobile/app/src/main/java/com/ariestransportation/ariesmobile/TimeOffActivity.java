package com.ariestransportation.ariesmobile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class TimeOffActivity extends AppCompatActivity {
    public static TextView tvDisplayDate;

    private int year;
    private int month;
    private int day;

    public static class DatePickerFragment extends android.support.v4.app.DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            tvDisplayDate.setText((month + 1) + "-" + day + "-" + year);
        }
    }

    public void showDatePickerDialog(View v) {
        android.support.v4.app.DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_off);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TripDatabaseHelper dbHelper;
        dbHelper = TripDatabaseHelper.getInstance(getBaseContext());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        tvDisplayDate = (TextView) findViewById(R.id.tvDate);

        Button submitButton = (Button) findViewById(R.id.buttonSubmitRequest);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView hrsRequested = (TextView) findViewById(R.id.txtHoursRequested);
                String hoursReq = hrsRequested.getText().toString();
                TextView dRequested = (TextView) findViewById(R.id.tvDate);
                String dateRequested = dRequested.getText().toString();
                TextView comments = (TextView) findViewById(R.id.textComments);
                String comment = comments.getText().toString();
                final RequestQueue queue = Volley.newRequestQueue(getBaseContext());

                String url = getBaseContext().getResources().getString(R.string.portal_server_site) +
                                        "Timesheet/TimeOffRequest?username=" + LoginActivity.userName +
                                        "&timeRequested=" + hoursReq +
                                        "&dateRequested=" + dateRequested +
                                        "&comments="+comment;

                url = url.replaceAll(" ", "%20");

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.equals("True")) {
                                    Toast.makeText(getBaseContext(), "Time Off Requested was submitted", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getBaseContext(), "Request was not processed; try again later", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getBaseContext(), "Error Logging in: " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        VolleySingleton.getInstance(getBaseContext()).getTimeout(),
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                queue.add(stringRequest);
            }
        });

    }

    @Override
    public void onBackPressed() {
    }

}
