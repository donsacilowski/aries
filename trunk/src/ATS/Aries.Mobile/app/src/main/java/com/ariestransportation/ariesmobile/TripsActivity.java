package com.ariestransportation.ariesmobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.CookieManager;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.UnsupportedEncodingException;

public class TripsActivity extends AppCompatActivity implements TripDatabaseHelper.Poller {

    TripDatabaseHelper dbHelper;
    private ProgressBar spinner;

    private static Context ctx;

    GPSLocation gps;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int permission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int gpspermission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED ||
                gpspermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user

            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        gps = new GPSLocation(getApplicationContext());

        setContentView(R.layout.activity_trips);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ctx=this;
    }

    public void continueActivity() {

        ArrayList<TripDetail> allTrips = new ArrayList<TripDetail>();
        allTrips = dbHelper.getAllTripsList();

        ArrayList<TripDetail> myTrips = new ArrayList<TripDetail>();
        myTrips = dbHelper.getTripsList();
        TripAdapter tripAdapter = new TripAdapter(this, R.layout.trip_item, myTrips);

        ListView myList=(ListView)
                findViewById(R.id.listViewTrips);
        myList.setAdapter(tripAdapter);

        myList.setOnItemClickListener(new OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view, int position,
                                      long id) {
                  final TripDetail td = ((TripAdapter) parent.getAdapter()).getItem(position);

                  Date today = new Date();
                  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                  Date startDate = null;
                  try {
                      startDate = (Date)format.parse(td.pickupTime);
                  } catch (ParseException e) {

                  }

                  if (!startDate.after(today)) {
                      final Integer tripID = td.id;

                      if (td.status != TripDatabaseHelper.Statuses.CancelDispatch) {

                          final ArrayList<TripDetail> tdCombinedDropoff = dbHelper.getCombinedDropoffTrips(td.id);
                          Integer numDropoffs = tdCombinedDropoff.size();
                          Integer numDropOffsRemaining = 0;
                          String peopleToBePickedup = "";

                          for (TripDetail trip : tdCombinedDropoff) {
                              //if (!trip.id.equals(td.id)) {
                              if ((trip.status.getValue() == TripDatabaseHelper.Statuses.Accepted.getValue()) ||
                                      (trip.status.getValue() == TripDatabaseHelper.Statuses.AtPickUp.getValue()) ||
                                      (trip.status.getValue() == TripDatabaseHelper.Statuses.Dispatched.getValue())) {

                                  numDropOffsRemaining++;
                                  peopleToBePickedup += trip.pickupName + "\r\n";
                              }
                              //}
                          }

                          if (numDropoffs > 0) {

                          /* If this is a combined trip, remind the driver there are more combined trips to be picked up */
                              if (!td.pickUpComplete) {

                                  if (numDropOffsRemaining > 0) {
                                      final AlertDialog alert = new AlertDialog.Builder(
                                              new ContextThemeWrapper(view.getContext(), android.R.style.Theme_Dialog))
                                              .create();

                                      alert.setTitle("Pickup Reminder");
                                      String message = "You have " + numDropOffsRemaining +
                                              " people (" + peopleToBePickedup + ") to pickup before proceeding to the drop off site ";

                                      alert.setMessage(message);
                                      alert.setCancelable(false);
                                      alert.setCanceledOnTouchOutside(false);

                                      alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                                              new DialogInterface.OnClickListener() {
                                                  public void onClick(DialogInterface dialog, int which) {

                                                      Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);

                                                      Bundle extras = new Bundle();
                                                      extras.putInt(getResources().getString(R.string.trip_detail_id), td.id);

                                                      intent.putExtras(extras);

                                                      startActivity(intent);
                                                      alert.dismiss();
                                                  }
                                              });

                                      alert.show();
                                  }
                              } else {

                                  if (numDropOffsRemaining > 0) {
                                      final AlertDialog alert = new AlertDialog.Builder(
                                              new ContextThemeWrapper(view.getContext(), android.R.style.Theme_Dialog))
                                              .create();

                                      alert.setTitle("Pickup Reminder");
                                      String message = "You still have " + numDropOffsRemaining +
                                              " people (" + peopleToBePickedup + ") to pickup before proceeding to the drop off site." +
                                              " Click YES to proceed to the dropff site, NO to pickup the remaining people. ";

                                      alert.setMessage(message);
                                      alert.setCancelable(false);
                                      alert.setCanceledOnTouchOutside(false);

                                      alert.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
                                              new DialogInterface.OnClickListener() {
                                                  public void onClick(DialogInterface dialog, int which) {

                                                      /* This is a trip which had been picked up, so erase the remaining combined trips as combined trips */
                                                      /*for (TripDetail trip : tdCombinedDropoff) {
                                                          if ((trip.status.getValue() == TripDatabaseHelper.Statuses.Accepted.getValue()) ||
                                                              (trip.status.getValue() == TripDatabaseHelper.Statuses.AtPickUp.getValue()) ||
                                                              (trip.status.getValue() == TripDatabaseHelper.Statuses.Dispatched.getValue())) {

                                                              trip.combinedDropoff = 0;
                                                              dbHelper.updateTrip(trip);
                                                          }
                                                      }
*/
                                                      alert.dismiss();

                                                      Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);

                                                      Bundle extras = new Bundle();
                                                      extras.putInt(getResources().getString(R.string.trip_detail_id), td.id);

                                                      intent.putExtras(extras);

                                                      startActivity(intent);
                                                      alert.dismiss();
                                                  }
                                              });

                                      alert.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
                                              new DialogInterface.OnClickListener() {
                                                  public void onClick(DialogInterface dialog, int which) {

                                                      alert.dismiss();
                                                  }
                                              });

                                      alert.show();
                                  } else {
                                      Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);

                                      Bundle extras = new Bundle();
                                      extras.putInt(getResources().getString(R.string.trip_detail_id), td.id);

                                      intent.putExtras(extras);

                                      startActivity(intent);
                                  }
                              }
                          } else {

                              Intent intent = new Intent(TripsActivity.this, TripDetailActivity.class);

                              Bundle extras = new Bundle();
                              extras.putInt(getResources().getString(R.string.trip_detail_id), td.id);

                              intent.putExtras(extras);

                              startActivity(intent);
                          }

                      }
                  } else {
                      final AlertDialog alert = new AlertDialog.Builder(
                              new ContextThemeWrapper(view.getContext(), android.R.style.Theme_Dialog))
                              .create();

                      alert.setTitle("Pickup is not for today");

                      String message = "This pickup is not for today";

                      alert.setMessage(message);
                      alert.setCancelable(false);
                      alert.setCanceledOnTouchOutside(false);

                      alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                              new DialogInterface.OnClickListener() {
                                  public void onClick(DialogInterface dialog, int which) {
                                      alert.dismiss();
                                  }
                              });

                      alert.show();
                  }
              }
          }
        );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        dbHelper = TripDatabaseHelper.getInstance(this);

        dbHelper.deleteOldTrips();
        dbHelper.deleteOldMessages();

        dbHelper.tripsActivity = this; //registering for polling of trips

        spinner=(ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean shouldPoll = prefs.getBoolean("shouldPoll", true);

        if (shouldPoll) {
            pollTrips();
        } else {
            continueActivity();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        dbHelper.tripsActivity = null; //deregistering for trips polling
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("userLoggedIn", false);
            editor.commit();

            Intent intent = new Intent(TripsActivity.this, LoginActivity.class);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_timesheet) {

            Intent intent = new Intent(TripsActivity.this, TimesheetActivity.class);

            startActivity(intent);
            VolleyLog.DEBUG = true;

            return true;
        }
        if (id == R.id.action_orderHistory) {
            Intent intent = new Intent(TripsActivity.this, TripsHistoryActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_RequestTimeOff) {
            Intent intent = new Intent(TripsActivity.this, TimeOffActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_Messages) {
            Intent intent = new Intent(TripsActivity.this, MessageActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void pollTrips(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("section", 0);
        editor.commit();

        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //TextView uname = LoginActivity.userName; //(TextView) findViewById(R.id.txtUserName);
        String userName = LoginActivity.userName;

        String date = Dates.formatNowDateTime(getApplicationContext());
        Message msg = new Message(0, "Started Polling at " + prefs.getInt("section", 0) + " ", date);
            dbHelper = TripDatabaseHelper.getInstance(getApplicationContext());
            dbHelper.insertMessage(msg);


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = getApplicationContext().getResources().getString(R.string.api_server_site) + "api/trip/SendTrips?mobileAppKey=" + refreshedToken +
                "&username=" + userName;

        url = url.replaceAll(" ", "%20");

        //spinner.setVisibility(View.VISIBLE);
        HashMap<String, String> mRequestParams = new HashMap<String, String>();
        JSONArray jarr = new JSONArray();

        // Request a string response from the provided URL.
        JsonArrayRequest tripStringRequest = new JsonArrayRequest(Request.Method.POST, url, jarr,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt("section", 1);
                            editor.commit();

                            String date = Dates.formatNowDateTime(getApplicationContext());
                            Message msg = new Message(0, "Received trips from server at " + prefs.getInt("section", 0) + " ", date);
                            dbHelper = TripDatabaseHelper.getInstance(getApplicationContext());
                            dbHelper.insertMessage(msg);

                            if (response != null) {
                                editor.putInt("section", 2);
                                editor.commit();
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject jresponse = response.getJSONObject(i);

                                    editor.putInt("section", 3);
                                    TripDatabaseHelper.Statuses sta = TripDatabaseHelper.Statuses.valueOf(jresponse.getString("status"));

                                    SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                                    Date newDate = null;

                                    editor.putInt("section", 4);
                                    editor.commit();

                                    try {
                                        newDate = parseFormat.parse(jresponse.getString("pickupTime"));
                                    } catch (ParseException pe) {
                                    }
                                    editor.putInt("section", 5);
                                    editor.commit();

                                    String puMilTime = displayFormat.format(newDate);

                                    editor.putInt("section", 6);
                                    editor.commit();

                                    TripDetail td = new TripDetail(
                                            jresponse.getInt("id"),
                                            jresponse.getString("pickupName"),
                                            jresponse.getString("pickupTime"),
                                            jresponse.getString("pickupAddress"),
                                            jresponse.getString("dropoffName"),
                                            jresponse.getString("dropoffTime"),
                                            jresponse.getString("dropoffAddress"),
                                            jresponse.getString("service"),
                                            jresponse.getString("serviceType"),
                                            jresponse.getString("notes"),
                                            jresponse.getBoolean("DNLA"),
                                            jresponse.getBoolean("signatureRequired"),
                                            jresponse.getBoolean("check"),
                                            jresponse.getString("checkAmount"),
                                            sta,
                                            "", false, false, false, 0, 0,
                                            false, false, false, puMilTime, false, false,
                                            jresponse.getString("pickupAddressFull"),
                                            jresponse.getString("dropoffAddressFull")

                                    );

                                    if (td.id == 9999){
                                        try{
                                            final AlertDialog alert = new AlertDialog.Builder(
                                                    new ContextThemeWrapper(ctx, android.R.style.Theme_Dialog))
                                                    .create();

                                            alert.setTitle("Login Required");
                                            alert.setCanceledOnTouchOutside(false);
                                            String message = "Hit OK and you will need to login again.";

                                            alert.setMessage(message);
                                            alert.setCancelable(false);
                                            alert.setCanceledOnTouchOutside(false);
                                            prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                            editor = prefs.edit();
                                            editor.putBoolean("userLoggedIn", false);
                                            editor.commit();

                                            alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                        Intent intent = new Intent(TripsActivity.this, LoginActivity.class);
                                                        startActivity(intent);
                                                        alert.dismiss();
                                                        }
                                                    });

                                            alert.show();

                                        } catch (Exception ex){
                                            Toast.makeText(
                                                    TripsActivity.this,
                                                    "Alert Error = " + ex.getMessage(),
                                                    Toast.LENGTH_SHORT
                                            ).show();

                                        }
                                    }
                                    else if(td.id == 9998) {
                                        try {
                                            final AlertDialog alert = new AlertDialog.Builder(
                                                    new ContextThemeWrapper(ctx, android.R.style.Theme_Dialog))
                                                    .create();

                                            alert.setTitle("Login Required");
                                            alert.setCanceledOnTouchOutside(false);
                                            String message = "Hit OK and you will need to login again.";

                                            alert.setMessage(message);
                                            alert.setCancelable(false);
                                            alert.setCanceledOnTouchOutside(false);
                                            prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                            editor = prefs.edit();
                                            editor.putBoolean("userLoggedIn", false);
                                            editor.commit();

                                            alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent intent = new Intent(TripsActivity.this, LoginActivity.class);
                                                            startActivity(intent);
                                                            alert.dismiss();
                                                        }
                                                    });

                                            alert.show();

                                        } catch (Exception ex) {
                                            Toast.makeText(
                                                    TripsActivity.this,
                                                    "Alert Error = " + ex.getMessage(),
                                                    Toast.LENGTH_SHORT
                                            ).show();

                                        }
                                    }
                                    else if(td.id == 9997) {
                                        try {
                                            final AlertDialog alert = new AlertDialog.Builder(
                                                    new ContextThemeWrapper(ctx, android.R.style.Theme_Dialog))
                                                    .create();

                                            alert.setTitle("Login Required");
                                            alert.setCanceledOnTouchOutside(false);
                                            String message = "Hit OK and you will need to login again.";

                                            alert.setMessage(message);
                                            alert.setCancelable(false);
                                            alert.setCanceledOnTouchOutside(false);
                                            prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                            editor = prefs.edit();
                                            editor.putBoolean("userLoggedIn", false);
                                            editor.commit();

                                            alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent intent = new Intent(TripsActivity.this, LoginActivity.class);
                                                            startActivity(intent);
                                                            alert.dismiss();
                                                        }
                                                    });

                                            alert.show();

                                        } catch (Exception ex) {
                                            Toast.makeText(
                                                    TripsActivity.this,
                                                    "Alert Error = " + ex.getMessage(),
                                                    Toast.LENGTH_SHORT
                                            ).show();

                                        }
                                    }
                                    else {
                                        editor.putInt("section", 7);
                                        editor.commit();

                                        //Check to see if the trip already exists and if it does, update it; otherwise, add a new one
                                        TripDetail tripOnPhone = dbHelper.readTrip(td.id);

                                        editor.putInt("section", 8);
                                        editor.commit();

                                        if (tripOnPhone == null) {
                                            long lint = dbHelper.insertTrip(td);
                                            editor.putInt("section", 9);
                                            editor.commit();
                                        } else {
                                            editor.putInt("section", 10);
                                            editor.commit();

                                            td.tripComplete = false;
                                            td.atPickup = tripOnPhone.atPickup;
                                            td.atDropOff = tripOnPhone.atDropOff;

                                            editor.putInt("section", 11);
                                            editor.commit();

                                            long lint = dbHelper.updateTrip(td);

                                            editor.putInt("section", 12);
                                            editor.commit();
                                        }
                                    }
                                }
                                editor.putInt("section", 13);
                                editor.commit();
                                dbHelper.setCombinedTrips();
                                editor.putInt("section", 14);
                                editor.commit();
                                editor.putBoolean("shouldPoll", false);
                                editor.commit();
                                continueActivity();
                                spinner.setVisibility(View.GONE);

                                date = Dates.formatNowDateTime(getApplicationContext());
                                msg = new Message(0, "Finished processing trips at " +  prefs.getInt("section", 0) + " ", date);
                                dbHelper = TripDatabaseHelper.getInstance(getApplicationContext());
                                dbHelper.insertMessage(msg);

                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();

                        String date = Dates.formatNowDateTime(getApplicationContext());
                        Message msg = new Message(0, "Error Sending Trips " + prefs.getInt("section", 0) + error.toString(), date);
                        dbHelper = TripDatabaseHelper.getInstance(getApplicationContext());
                        dbHelper.insertMessage(msg);
                        Toast.makeText(getApplicationContext(), "Error: Sending New Trips " +  prefs.getInt("section", 0) + " " + error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        tripStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                VolleySingleton.getInstance(getApplicationContext()).getTimeout(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(tripStringRequest);
    }
}
