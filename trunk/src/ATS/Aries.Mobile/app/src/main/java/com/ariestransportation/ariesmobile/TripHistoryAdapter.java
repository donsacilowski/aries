package com.ariestransportation.ariesmobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ariestransportation.ariesmobile.Trips;

import java.util.ArrayList;

/**
 * Created by dsacilowski on 7/8/2016.
 */
public class TripHistoryAdapter extends ArrayAdapter<TripDetail> {
    private Context context;
    private int resource;
    private ArrayList<TripDetail> objects;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=
                ((Activity) context).getLayoutInflater();

        View row=inflater.inflate(resource,parent,false);

        /*if (position % 2 == 1) {
            row.setBackgroundColor(Color.GRAY);
        }*/
        /*else {
            row.setBackgroundColor(Color.WHITE);
        }*/

        TextView title= (TextView)
                row.findViewById(R.id.textViewTripPickupTimeHistory);
        title.setText((CharSequence)objects.get(position).pickupTime);

        TextView name= (TextView) row.findViewById(R.id.textViewTripPickupNameHistory);
        name.setText("Pickup " + (CharSequence) objects.get(position).pickupName);

        TextView address= (TextView) row.findViewById(R.id.textViewTripPickupAddressHistory);
        address.setText((CharSequence) objects.get(position).pickupAddress);

        TextView instructions = (TextView) row.findViewById(R.id.textViewTripInstructionsHistory);
        if (!objects.get(position).service.isEmpty() || !objects.get(position).serviceType.isEmpty()) {
            instructions.setText(objects.get(position).service + " " +
                    objects.get(position).serviceType);
        } else {
            instructions.setVisibility(View.GONE);
        }

        TextView dropoffName= (TextView) row.findViewById(R.id.textViewTripDropoffNameHistory);
        dropoffName.setText("Dropoff at " + (CharSequence) objects.get(position).dropoffName);

        TextView dropoffAddress= (TextView) row.findViewById(R.id.textViewTripDropoffAddressHistory);
        dropoffAddress.setText((CharSequence) objects.get(position).dropoffAddress);

        return row;
    }

    public TripHistoryAdapter(Context context, int resource, ArrayList<TripDetail> objects) {
        super(context, resource, objects);

        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
}
