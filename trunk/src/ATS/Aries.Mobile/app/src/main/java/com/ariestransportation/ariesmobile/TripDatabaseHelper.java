package com.ariestransportation.ariesmobile;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dsacilowski on 7/12/2016.
 */
public class TripDatabaseHelper extends SQLiteOpenHelper {

    public interface Poller{

        public void pollTrips();
    }
    public interface PollerMessage{

        public void pollMessages();
    }

    public enum Statuses
    {
        Open(1),
        Assigned(2),
        Dispatched(3),
        Accepted(4),
        AtPickUp(5),
        PickedUp(6),
        AtDropOff(7),
        DroppedOff(8),
        Rejected(9),
        CancelDispatch(10),
        CancelClient(11),
        GotCheck(12),
        NotAlone(13),
        DriverAttested(14),
        Closed(15),
        BadOrder(16);

        private final int value;
        private Statuses(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public int getStatusValue() {
            Statuses tax = Statuses.Accepted; // Or whatever
            return tax.getValue();
        }
    }

    private ProgressBar spinner;

    private int combinedWindow = 35 * 1000 * 60;
    public static final String DB_NAME = "trips.sqlite";
    private static final int VERSION = 4;

    public static final String MESSAGE_TABLE = "message_list"; //Order ID from the service
    public static final String MESSAGE_COLUMN_ID = "id"; //Order ID from the service
    public static final String MESSAGE_COLUMN_MESSAGE = "msg"; //Order ID from the service
    public static final String MESSAGE_COLUMN_TIMESTAMP = "timestamp";

    public static final String STATUS_COLUMN_ID = "id"; //Order ID from the service
    public static final String STATUS_COLUMN_TRIP_ID = "trip_id"; //Order ID from the service
    public static final String STATUS_COLUMN_TIMESTAMP = "timestamp";
    public static final String STATUS_COLUMN_STATUS_ID = "status_id";
    public static final String STATUS_COLUMN_SENT = "sentGood";

    public static final String COLUMN_ID = "id"; //Order ID from the service
    public static final String COLUMN_PICKUP_NAME = "pickup_name";
    public static final String COLUMN_PICKUP_TIME = "pickup_time";
    public static final String COLUMN_PICKUP_TIME_MILITARY = "pickup_time_military";
    public static final String COLUMN_PICKUP_ADDRESS = "pickup_address";
    public static final String COLUMN_DROPOFF_NAME = "dropoff_name";
    public static final String COLUMN_DROPOFF_TIME = "dropoff_time";
    public static final String COLUMN_DROPOFF_ADDRESS = "dropoff_address";
    public static final String COLUMN_SERVICE = "service";
    public static final String COLUMN_SERVICE_TYPE = "service_type";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_DNLA = "dnla"; //Do not leave alone
    public static final String COLUMN_SIGNATURE = "signature"; //Signature required?
    public static final String COLUMN_CHECK = "need_check"; //Check required?
    public static final String COLUMN_CHECK_AMOUNT = "check_amount"; //Amount of the check
    public static final String COLUMN_STATUS = "status"; //Trip status
    public static final String COLUMN_DRIVERNOTES = "driver_notes"; //Driver entered notes
    public static final String COLUMN_SENT = "sent"; //Order synch status with the server
    public static final String COLUMN_PICKUP_COMPLETE = "pickup_complete"; //Pickup leg complete
    public static final String COLUMN_TRIP_COMPLETE = "trip_complete"; //Both pickup and drop off are completed
    public static final String COLUMN_COMBINED_PICKUP_TRIP_NUMBER = "combined_pickup_trip_id"; // Daily combined trip id (starts at 1 each day).
                                                                                                // This will be used to indicate to the user on the
                                                                                                // Trips screen this order is part of a combined order.
    public static final String COLUMN_COMBINED_DROPOFF_TRIP_NUMBER = "combined_dropoff_trip_id"; // Daily combined trip id (starts at 1 each day).
                                                                                                // This will be used to indicate to the user on the
                                                                                                // Trips screen this order is part of a combined order.

    public static final String COLUMN_DNLA_DONE = "dnla_done";
    public static final String COLUMN_CHECK_DONE = "check_done";
    public static final String COLUMN_SIGNATURE_DONE = "signature_done";
    public static final String COLUMN_ATPICKUP = "atPickup";
    public static final String COLUMN_ATDROPOFF = "atDropoff";

    public static final String COLUMN_PICKUP_ADDRESS_FULL = "pickup_address_full";
    public static final String COLUMN_DROPOFF_ADDRESS_FULL = "dropoff_address_full";

    public Poller tripsActivity; // So we can call pollTrips while TripsActivity is on screen
    public PollerMessage messageActivity; // So we can call pollTrips while TripsActivity is on screen

    private static TripDatabaseHelper mInstance;
    private Context mCtx;

    private TripDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
        mCtx = context.getApplicationContext();
    }

    public static TripDatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance =  new TripDatabaseHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // DB has already been opened in the Login Activity
        /*db.execSQL("DROP TABLE IF EXISTS trip_detail");
        db.execSQL("DROP TABLE IF EXISTS status_list");
*/
        db.execSQL("create table IF NOT EXISTS trip_detail (" +
                "id integer, " +
                "pickup_name text, " +
                "pickup_time text," +
                "pickup_address text," +
                "dropoff_name text," +
                "dropoff_time text," +
                "dropoff_address text," +
                "service text," +
                "service_type text," +
                "notes text," +
                "dnla integer," +
                "signature integer," +
                "need_check integer," +
                "check_amount real," +
                "status integer," +
                "driver_notes text," +
                "sent integer," +
                "pickup_complete integer," +
                "trip_complete integer," +
                "combined_pickup_trip_id integer," +
                "combined_dropoff_trip_id integer," +
                "dnla_done integer," +
                "check_done integer," +
                "signature_done integer," +
                "pickup_time_military text," +
                "atPickup integer," +
                "atDropoff integer," +
                "pickup_address_full text," +
                "dropoff_address_full text" +
                ")");

        db.execSQL("create table IF NOT EXISTS status_list (" +
                "id integer primary key AUTOINCREMENT," +
                "trip_id integer, " +
                "timestamp text," +
                "status_id integer," +
                "sentGood integer" +
                ")");

        createMsgtable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Implement schema changes and data massage here when upgrading
        //db.execSQL("DROP TABLE IF EXISTS trip_detail");
        //onCreate(db);
        if (oldVersion == 1) {
            db.execSQL("ALTER TABLE trip_detail ADD COLUMN pickup_address_full text DEFAULT '' ");
            db.execSQL("ALTER TABLE trip_detail ADD COLUMN dropoff_address_full text DEFAULT '' ");

            /* Transfer the pickup and drop off addresses to the full addresses */
            db.execSQL("UPDATE trip_detail SET pickup_address_full=pickup_address, dropoff_address_full=dropoff_address");
        }
    }

    public void createMsgtable(SQLiteDatabase db) {
        db.execSQL("create table IF NOT EXISTS " + MESSAGE_TABLE + "(" +
                MESSAGE_COLUMN_ID + " integer primary key AUTOINCREMENT," +
                MESSAGE_COLUMN_MESSAGE + " text, " +
                MESSAGE_COLUMN_TIMESTAMP + " text" +
                ")");
    }

    public void insertMessage(final Message msg) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = this.getReadableDatabase();

        cv.put(MESSAGE_COLUMN_MESSAGE, msg.message);
        cv.put(MESSAGE_COLUMN_TIMESTAMP, msg.dateReceived);
        getWritableDatabase().insert(MESSAGE_TABLE, null, cv);

        String qry = "select * from " + MESSAGE_TABLE;
        Cursor message =  db.rawQuery(qry, null);
        Boolean err = message.moveToFirst();
        Integer j = message.getCount();
    }

    public ArrayList<Message> getMessageList() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Message> msgList = new ArrayList<Message>();

        String qry = "select * from " + MESSAGE_TABLE + " order by "+ MESSAGE_COLUMN_ID + " desc";
        Cursor msg =  db.rawQuery(qry, null);
        Boolean err = msg.moveToFirst();
        Integer j = msg.getCount();

        for (int i =0; i<msg.getCount(); i++) {

            Message message = new Message(
                    msg.getInt(msg.getColumnIndex(TripDatabaseHelper.MESSAGE_COLUMN_ID)),
                    msg.getString(msg.getColumnIndex(TripDatabaseHelper.MESSAGE_COLUMN_MESSAGE)),
                    msg.getString(msg.getColumnIndex(TripDatabaseHelper.MESSAGE_COLUMN_TIMESTAMP))
            );
            msgList.add(message);
            msg.moveToNext();
        }

        return msgList;
    }

    public void insertStatus(final Status status, final Context context) {
        ContentValues cv = new ContentValues();
        cv.put(STATUS_COLUMN_TRIP_ID, status.tripId);
        cv.put(STATUS_COLUMN_TIMESTAMP, status.timestamp);

        final Integer sta = status.statusID.getValue();
        cv.put(STATUS_COLUMN_STATUS_ID, sta);
        cv.put(STATUS_COLUMN_SENT, status.sent);
        getWritableDatabase().insert("status_list", null, cv);
        //final ArrayList<Status> staList = getToBeSentStatus();

        Status staItem = status;
        /*for (Status staItem : staList) {*/
            final Integer staID = staItem.id;
            final Integer staNum = staItem.statusID.getValue();

            RequestQueue queue = Volley.newRequestQueue(context);
            String url = context.getResources().getString(R.string.api_server_site) + "api/trip/SendStatus?orderNumber=" + staItem.tripId.toString() +
                    "&timestamped=" + staItem.timestamp +
                    "&statusListId=" + staNum.toString();

            url = url.replaceAll(" ", "%20");

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            //Toast.makeText(context, "Status Save Result: " + response.toString(), Toast.LENGTH_LONG).show();
                            if (response.equals("0")) {
                                TripDetail locTrip = readTrip(status.tripId);
                                locTrip.sent = true;
                                locTrip.status = status.statusID;
                                updateTrip(locTrip);

                                //updateTripSent(1, staID);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(context, "Status Save Error: " + staList.size() + " " + staNum + " " + error.toString(), Toast.LENGTH_LONG).show();
                        }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    VolleySingleton.getInstance(context).getTimeout(),
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
       /* }*/
    }

    public long updateTripSent(Integer sent, Integer id) {
        ContentValues cv = new ContentValues();

        String where = STATUS_COLUMN_ID + '=' + id;

        return getWritableDatabase().update("status_list", cv, where, null);
    }

    public boolean deleteOldTrips() {

        SQLiteDatabase db = this.getReadableDatabase();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date today = new Date();

        String dateNow = format.format(today);
        Calendar c = Calendar.getInstance();

        try{
            c.setTime(format.parse(dateNow));
        }catch(Exception ex) {

        }

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();
        c.add(Calendar.DATE, -1);  // number of days to add
        String weekAgo = format.format(c.getTime());  // dt is now the new date

        int err = db.delete("trip_detail",
                COLUMN_PICKUP_TIME + "<?",
                new String[] {dateNow});

        tdList = getAllTripsList();
        return true;
    }

    public boolean deleteOldMessages() {

        SQLiteDatabase db = this.getReadableDatabase();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date today = new Date();
        ArrayList<Message> myMessages = new ArrayList<Message>();
        myMessages = getMessageList();

        String dateNow = format.format(today);
        Calendar c = Calendar.getInstance();

        try{
            c.setTime(format.parse(dateNow));
        }catch(Exception ex) {

        }

        c.add(Calendar.DATE, -1);  // number of days to add
        String dayAgo = format.format(c.getTime());

        String sql = "DELETE FROM message_list WHERE timestamp <" + "'" + dateNow + "'" ;
        db.execSQL(sql);
        myMessages = getMessageList();

        return true;
    }

    public boolean deleteAllTrips() {

        SQLiteDatabase db = this.getReadableDatabase();

        String where = "delete from trip_detail";
        db.execSQL(where);

        return true;
    }

    public long insertTrip(TripDetail trip) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, trip.id);
        cv.put(COLUMN_PICKUP_NAME, trip.pickupName);
        cv.put(COLUMN_PICKUP_ADDRESS, trip.pickupAddress);
        cv.put(COLUMN_PICKUP_TIME, trip.pickupTime);
        cv.put(COLUMN_DROPOFF_NAME, trip.dropoffName);
        cv.put(COLUMN_DROPOFF_TIME, trip.dropoffTime);
        cv.put(COLUMN_DROPOFF_ADDRESS, trip.dropoffAddress);
        cv.put(COLUMN_SERVICE, trip.service);
        cv.put(COLUMN_SERVICE_TYPE, trip.serviceType);
        cv.put(COLUMN_NOTES, trip.notes);
        cv.put(COLUMN_DNLA, trip.DNLA);
        cv.put(COLUMN_SIGNATURE, trip.signatureRequired);
        cv.put(COLUMN_CHECK, trip.check);
        cv.put(COLUMN_CHECK_AMOUNT, trip.checkAmount);

        Integer sta = trip.status.getValue();
        cv.put(COLUMN_STATUS, sta);

        cv.put(COLUMN_DRIVERNOTES, trip.notes);
        cv.put(COLUMN_SENT, trip.sent);
        cv.put(COLUMN_PICKUP_COMPLETE, trip.pickUpComplete);
        cv.put(COLUMN_TRIP_COMPLETE, trip.tripComplete);
        cv.put(COLUMN_COMBINED_PICKUP_TRIP_NUMBER, trip.combinedPickup);
        cv.put(COLUMN_COMBINED_DROPOFF_TRIP_NUMBER, trip.combinedDropoff);
        cv.put(COLUMN_DNLA_DONE, trip.DNLADone);
        cv.put(COLUMN_CHECK_DONE, trip.checkDone);
        cv.put(COLUMN_SIGNATURE_DONE, trip.signatureDone);
        cv.put(COLUMN_PICKUP_TIME_MILITARY, trip.pickupTimeMilitary);
        cv.put(COLUMN_ATPICKUP, trip.atPickup);
        cv.put(COLUMN_ATDROPOFF, trip.atDropOff);
        cv.put(COLUMN_PICKUP_ADDRESS_FULL, trip.pickupAddressFull);
        cv.put(COLUMN_DROPOFF_ADDRESS_FULL, trip.dropoffAddressFull);

        getWritableDatabase().insert("trip_detail", null, cv);

        return 1;
    }

    public long updateTrip(TripDetail trip) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, trip.id);
        cv.put(COLUMN_PICKUP_NAME, trip.pickupName);
        cv.put(COLUMN_PICKUP_ADDRESS, trip.pickupAddress);
        cv.put(COLUMN_PICKUP_TIME, trip.pickupTime);
        cv.put(COLUMN_DROPOFF_NAME, trip.dropoffName);
        cv.put(COLUMN_DROPOFF_TIME, trip.dropoffTime);
        cv.put(COLUMN_DROPOFF_ADDRESS, trip.dropoffAddress);
        cv.put(COLUMN_SERVICE, trip.service);
        cv.put(COLUMN_SERVICE_TYPE, trip.serviceType);
        cv.put(COLUMN_NOTES, trip.notes);
        cv.put(COLUMN_DNLA, trip.DNLA);
        cv.put(COLUMN_SIGNATURE, trip.signatureRequired);
        cv.put(COLUMN_CHECK, trip.check);
        cv.put(COLUMN_CHECK_AMOUNT, trip.checkAmount);

        Integer sta = trip.status.getValue();
        cv.put(COLUMN_STATUS, sta);

        cv.put(COLUMN_DRIVERNOTES, trip.notes);
        cv.put(COLUMN_SENT, trip.sent);
        cv.put(COLUMN_PICKUP_COMPLETE, trip.pickUpComplete);
        cv.put(COLUMN_TRIP_COMPLETE, trip.tripComplete);
        cv.put(COLUMN_COMBINED_PICKUP_TRIP_NUMBER, trip.combinedPickup);
        cv.put(COLUMN_COMBINED_DROPOFF_TRIP_NUMBER, trip.combinedDropoff);
        cv.put(COLUMN_DNLA_DONE, trip.DNLADone);
        cv.put(COLUMN_CHECK_DONE, trip.checkDone);
        cv.put(COLUMN_SIGNATURE_DONE, trip.signatureDone);
        cv.put(COLUMN_PICKUP_TIME_MILITARY, trip.pickupTimeMilitary);
        cv.put(COLUMN_ATPICKUP, trip.atPickup);
        cv.put(COLUMN_ATDROPOFF, trip.atDropOff);
        cv.put(COLUMN_PICKUP_ADDRESS_FULL, trip.pickupAddressFull);
        cv.put(COLUMN_DROPOFF_ADDRESS_FULL, trip.dropoffAddressFull);

        String where = COLUMN_ID + '=' + trip.id;

        int succ = getWritableDatabase().update("trip_detail", cv, where, null);
        TripDetail td = readTrip(trip.id);
        return 1;
    }

    public TripDetail readTrip(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor trip =  db.rawQuery("select * from trip_detail where id=" + id + "", null);
        if (trip.getCount() <= 0){
            return null;
        }

        Boolean err = trip.moveToFirst();
        while (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)) != id) {
            trip.moveToNext();
        }

        Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_STATUS));
        Statuses sta = Statuses.values()[ista-1];

        Integer check = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_DONE));
        Integer sig = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE_DONE));

        TripDetail td = new TripDetail(
                trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_NAME)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_NAME)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_TIME)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE_TYPE)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_NOTES)),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK)) > 0),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_AMOUNT)),
                sta,
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DRIVERNOTES)),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SENT)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_COMPLETE)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_TRIP_COMPLETE)) > 0),
                trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_PICKUP_TRIP_NUMBER)),
                trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER)),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA_DONE)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_DONE)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE_DONE)) > 0),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME_MILITARY)),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATPICKUP)) > 0),
                (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATDROPOFF)) > 0),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS_FULL)),
                trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS_FULL))

                );
        return td;
    }

    public ArrayList<TripDetail> getTripsList() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        String qry = "select * from trip_detail where trip_complete=0 order by " + COLUMN_PICKUP_TIME_MILITARY;
        Cursor trip =  db.rawQuery(qry, null);
        Boolean err = trip.moveToFirst();
        for (int i =0; i<trip.getCount(); i++) {

            Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_STATUS));
//ista = 3;
            Statuses sta = Statuses.values()[ista-1];

            TripDetail td = new TripDetail(
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE_TYPE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_NOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_AMOUNT)),
                    sta,
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DRIVERNOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SENT)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_COMPLETE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_TRIP_COMPLETE)) > 0),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_PICKUP_TRIP_NUMBER)),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE_DONE)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME_MILITARY)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATPICKUP)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATDROPOFF)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS_FULL)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS_FULL))
            );
            tdList.add(td);
            trip.moveToNext();
        }

        return tdList;
    }

    public ArrayList<TripDetail> getOpenTripsList() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date today = new Date();

        String dateNow = format.format(today);
        Calendar c = Calendar.getInstance();

        try{
            c.setTime(format.parse(dateNow));
        }catch(Exception ex) {

        }

        c.add(Calendar.DATE, 1);  // number of days to add
        String tomorrow = format.format(c.getTime());  // dt is now the new date

        ArrayList<TripDetail> tList = getAllTripsList();
        int count = tList.size();

        ///DDS set dates to mm-dd-yyyy
        int startOpenStat = Statuses.Accepted.getValue();
        String query = "select * from trip_detail where " +
                "status >= " + Statuses.Accepted.getValue() +
                " and status <> " + Statuses.CancelDispatch.getValue()
                + " and pickup_time >= "+ "'"+dateNow.toString() + "'" + " and pickup_time <" + "'" + tomorrow.toString()+"'"
                + " order by " + COLUMN_PICKUP_TIME_MILITARY;
        Cursor trip =  db.rawQuery(query , null);

        Boolean err = trip.moveToFirst();
        for (int i =0; i<trip.getCount(); i++) {

            Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_STATUS));
            Statuses sta = Statuses.values()[ista-1];

            TripDetail td = new TripDetail(
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE_TYPE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_NOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_AMOUNT)),
                    sta,
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DRIVERNOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SENT)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_COMPLETE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_TRIP_COMPLETE)) > 0),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_PICKUP_TRIP_NUMBER)),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE_DONE)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME_MILITARY)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATPICKUP)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATDROPOFF)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS_FULL)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS_FULL))
            );
            tdList.add(td);
            trip.moveToNext();
        }

        return tdList;
    }

    public ArrayList<TripDetail> getAllTripsList() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        int startOpenStat = Statuses.Accepted.getValue();
        Cursor trip =  db.rawQuery("select * from trip_detail ", null);
        Boolean err = trip.moveToFirst();
        for (int i =0; i<trip.getCount(); i++) {

            Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_STATUS));
            Statuses sta = Statuses.values()[ista-1];

            TripDetail td = new TripDetail(
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_NAME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_TIME)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SERVICE_TYPE)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_NOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_AMOUNT)),
                    sta,
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DRIVERNOTES)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SENT)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_COMPLETE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_TRIP_COMPLETE)) > 0),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_PICKUP_TRIP_NUMBER)),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DNLA_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_CHECK_DONE)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_SIGNATURE_DONE)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME_MILITARY)),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATPICKUP)) > 0),
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ATDROPOFF)) > 0),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS_FULL)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS_FULL))
            );
            tdList.add(td);
            trip.moveToNext();
        }

        return tdList;
    }

    public void setCombinedTrips() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor maxPU = db.rawQuery("select MAX(combined_pickup_trip_id) as MAXPU from trip_detail ", null);
        maxPU.moveToFirst();
        int combinedPickupID = maxPU.getInt(maxPU.getColumnIndex("MAXPU"));

        Cursor maxDO = db.rawQuery("select MAX(combined_dropoff_trip_id) as MAXDO from trip_detail ", null);
        maxDO.moveToFirst();
        int combinedDropoffID = maxDO.getInt(maxDO.getColumnIndex("MAXDO"));

        Cursor trip = db.rawQuery("select * from trip_detail ", null);
        Boolean err = trip.moveToFirst();
        for (int i =0; i<trip.getCount(); i++) {

            /* get next pickup time*/
            String puTime = trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_TIME));

            //Parse the MedTrans date
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            Date newDate = null;
            try {
                newDate = format.parse(puTime);
            } catch (ParseException pe)
            {}

            //Change the date to our global format
            format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            String date = format.format(newDate);

            //Get a date object with the new pattern
            Date ts = null;
            try {
                ts = format.parse(date);
            } catch (ParseException pe)
            {}

            Long tsMilli= ts.getTime();

            Long tsMilliEnd = tsMilli + combinedWindow;
            Date tmp = new Date(tsMilliEnd);

            Long tsMilliStart = tsMilli - combinedWindow;
            Date tmpStart = new Date(tsMilliStart);

            String puTimeEnd = format.format(tmp);
            String puTimeStart = format.format(tmpStart);

            String address = trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_PICKUP_ADDRESS));

            /* The # is an apartment, so don't use it as part of the address */
            String[] addressParts = address.split("#");
            address = addressParts[0].trim();

            String[] selectionArgs = new String[] { address + "%" };
            String qry = "select * from trip_detail where combined_pickup_trip_id = 0 "
                    + " and status <> " + Statuses.CancelDispatch.getValue()
                    + " and trip_complete=0"
                    + " and status <> " + Statuses.DroppedOff.getValue()
                    + " and pickup_address Like ?"
                    + " and (pickup_time between " + "'"+puTimeStart+"'" + " and " + "'"+puTimeEnd+"')";
            /* Query for all records for the same pickup time and combined pickup id = 0 */
            Cursor combPUTrips = db.rawQuery(qry, selectionArgs);

            Integer numT = combPUTrips.getCount();
            Boolean err1 = combPUTrips.moveToFirst();
            /* If there is more than one record */
            if (combPUTrips.getCount() > 1) {
                /* increase combined pickup id */
                combinedPickupID++;

                String where = "update trip_detail set combined_pickup_trip_id = " + Integer.toString(combinedPickupID )
                                + " where pickup_time between " + "'"+puTimeStart+"'" + " and " + "'"+puTimeEnd+"'"
                                + " and trip_complete=0"
                                + " and " + "status <> " + Statuses.CancelDispatch.getValue()
                                + " and status <> " + Statuses.DroppedOff.getValue()
                                + " and pickup_address Like " + "'" + address + "%'";

                db.execSQL(where);
            }

            /* get next dropoff time*/
            String doTime = trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_TIME));

            //Parse the MedTrans date
            SimpleDateFormat doformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");

            newDate = null;
            try {
                newDate = doformat.parse(doTime);
            } catch (ParseException pe)
            {}

            //Change the date to our global format
            doformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            date = doformat.format(newDate);

            ts = null;
            try {
                ts = doformat.parse(date);
            } catch (ParseException pe)
            {}

            tsMilli= ts.getTime();

            tsMilliEnd = tsMilli + combinedWindow;
            tmp = new Date(tsMilliEnd);

            tsMilliStart = tsMilli - combinedWindow;
            tmpStart = new Date(tsMilliStart);

            String doTimeEnd = format.format(tmp);
            String doTimeStart = format.format(tmpStart);

            String doAddress = trip.getString(trip.getColumnIndex(TripDatabaseHelper.COLUMN_DROPOFF_ADDRESS));

            String[] doAddressParts = doAddress.split("#");
            doAddress = doAddressParts[0].trim();
            selectionArgs = new String[] { doAddress + "%" };
            qry = "select * from trip_detail where combined_dropoff_trip_id = 0 and "
                    + " status <> " + Statuses.CancelDispatch.getValue()
                    + " and trip_complete=0"
                    + " and status <> " + Statuses.DroppedOff.getValue()
                    + " and dropoff_address Like ?"
                    + " and (dropoff_time between " + "'"+doTimeStart+"'" + " and " + "'"+doTimeEnd+"')"
            ;
            /* Query for all records for the same pickup time and combined pickup id = 0 */
            Cursor combDOTrips = db.rawQuery(qry, selectionArgs);
            ArrayList<TripDetail> tris = getAllTripsList();

            Boolean err2 = combDOTrips.moveToFirst();
            /* If there is more than one record */
            if (combDOTrips.getCount() > 1) {
                /* increase combined dropoff id */
                combinedDropoffID++;

                String where = "update trip_detail set combined_dropoff_trip_id = " + Integer.toString(combinedDropoffID )
                        + " where trip_complete=0"
                        + " and " + "status <> " + Statuses.CancelDispatch.getValue()
                        + " and status <> " + Statuses.DroppedOff.getValue()
                        + " and dropoff_address Like " + "'" + doAddress + "%'"
                        + " and dropoff_time between " + "'"+doTimeStart+"'" + " and " + "'"+doTimeEnd+"'"
                        ;

                db.execSQL(where);

                ArrayList<TripDetail> myTrips = new ArrayList<TripDetail>();
                myTrips = getTripsList();

                int c = myTrips.size();
            }
            trip.moveToNext();
        }
    }


    public ArrayList<TripDetail> getCombinedPickupTrips(Integer recordID) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        Cursor trip = db.rawQuery("select * from trip_detail where id = " + Integer.toString(recordID)
                , null);
        trip.moveToFirst();

        Integer combinedID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_PICKUP_TRIP_NUMBER));

        /* Query for all records for the same pickup time and combined pickup id = 0 */
        Cursor combPUTrips = db.rawQuery("select * from trip_detail where combined_pickup_trip_id = " + Integer.toString(combinedID) +
                " and combined_pickup_trip_id <> 0 and trip_complete=0", null);

        if (combPUTrips.getCount() > 1) {
            Boolean err1 = combPUTrips.moveToFirst();

            for (int i = 0; i < combPUTrips.getCount(); i++) {

                Integer recID = combPUTrips.getInt(combPUTrips.getColumnIndex(TripDatabaseHelper.COLUMN_ID));

                tdList.add(readTrip(recID));
                combPUTrips.moveToNext();
            }
        }
        return tdList;
    }

    public ArrayList<TripDetail> getCombinedDropoffTrips(Integer recordID) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        Cursor trip = db.rawQuery("select * from trip_detail where id = " + Integer.toString(recordID)
                , null);
        trip.moveToFirst();

        Integer combinedID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER));

        /* Query for all records for the same pickup time and combined pickup id = 0 */
        Cursor combDOTrips = db.rawQuery("select * from trip_detail where combined_dropoff_trip_id = " + Integer.toString(combinedID) +
                " and combined_dropoff_trip_id <> 0 and trip_complete=0", null);

        if (combDOTrips.getCount() > 1) {
            Boolean err1 = combDOTrips.moveToFirst();

            for (int i =0; i<combDOTrips.getCount(); i++) {

                Integer recID = combDOTrips.getInt(combDOTrips.getColumnIndex(TripDatabaseHelper.COLUMN_ID));

                tdList.add(readTrip(recID));
                combDOTrips.moveToNext();
            }
        }
        return tdList;
    }

    public Integer getCombinedDropoffTripsPickedUpCount(Integer recordID) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        Cursor trip = db.rawQuery("select * from trip_detail where id = " + Integer.toString(recordID)
                , null);
        trip.moveToFirst();

        Integer combinedID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER));

        /* Query for all records for the same pickup time and combined pickup id = 0 */
        Cursor combDOTrips = db.rawQuery("select * from trip_detail where combined_dropoff_trip_id = " + Integer.toString(combinedID)
                + " and " + "status = " + Statuses.PickedUp.getValue()
                + " and combined_dropoff_trip_id <> 0 and trip_complete=0", null);

        return combDOTrips.getCount();
    }

    public ArrayList<TripDetail>  getCombinedDropoffTripsPickedUp(Integer recordID) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripDetail> all = getAllTripsList();
        Integer iii = all.size();
        ArrayList<TripDetail> tdList = new ArrayList<TripDetail>();

        Cursor trip = db.rawQuery("select * from trip_detail where id = " + Integer.toString(recordID)
                , null);
        trip.moveToFirst();

        Integer combinedID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_COMBINED_DROPOFF_TRIP_NUMBER));

        /* Query for all records for the same pickup time and combined pickup id = 0 */
        Cursor combDOTrips = db.rawQuery("select * from trip_detail where combined_dropoff_trip_id = " + Integer.toString(combinedID)
                + " and " + "pickup_complete = 1"
                + " and combined_dropoff_trip_id <> 0 and trip_complete=0", null);

        if (combDOTrips.getCount() > 1) {
            Boolean err1 = combDOTrips.moveToFirst();

            for (int i =0; i<combDOTrips.getCount(); i++) {

                Integer recID = combDOTrips.getInt(combDOTrips.getColumnIndex(TripDatabaseHelper.COLUMN_ID));

                tdList.add(readTrip(recID));
                combDOTrips.moveToNext();
            }
        }
        return tdList;
    }

    public ArrayList<Status> getStatuses() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Status> tdList = new ArrayList<Status>();
        Cursor trip = db.rawQuery("select * from status_list", null);
        Boolean err = trip.moveToFirst();

        if (trip.getCount() > 0) {
            Integer recID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_STATUS_ID));

            Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_STATUS_ID));
            Statuses sta = Statuses.values()[ista-1];

            Status td = new Status(
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.COLUMN_ID)),
                    trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_STATUS_ID)),
                    trip.getString(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_TIMESTAMP)),
                    sta,
                    (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_SENT))>0));
            tdList.add(td);
            trip.moveToNext();
        }
        return tdList;
    }
    public ArrayList<Status> getToBeSentStatus() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Status> tdList = new ArrayList<Status>();

        Cursor trip = db.rawQuery("select * from status_list where sentGood = 0", null);
        trip.moveToFirst();

        if (trip.getCount() > 0) {

            for (int i =0; i<trip.getCount(); i++) {

                Integer recID = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_STATUS_ID));

                Integer ista = trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_STATUS_ID));
                Statuses sta = Statuses.values()[ista-1];

                Status td = new Status(
                        trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_ID)),
                        trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_TRIP_ID)),
                        trip.getString(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_TIMESTAMP)),
                        sta,
                        (trip.getInt(trip.getColumnIndex(TripDatabaseHelper.STATUS_COLUMN_SENT))>0));
                tdList.add(td);
                trip.moveToNext();
            }
        }
        return tdList;
    }
}

