﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Web;
using System.Web.Hosting;

namespace Aries.Utility
{
    public static class Logger
    {
        private static readonly ILog _logger =
               LogManager.GetLogger(typeof(Logger));

        static Logger()
        {
            string configFile = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["log4net-config-file"].ToString());
            XmlConfigurator.Configure(new FileInfo(configFile));
        }

        public static void Error(string msg)
        {
            _logger.Error(msg);
        }

        public static void Debug(string msg)
        {
            _logger.Debug(msg);
        }

        public static void Info(string msg)
        {
            _logger.Info(msg);
        }
    }
}
