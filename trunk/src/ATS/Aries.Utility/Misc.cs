﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Utility
{
    public class Misc
    {

        public DateTime FirstDayOfWeek(DateTime day, DayOfWeek weekStarts)
        {
            DateTime d = day;
            while (d.DayOfWeek != weekStarts)
            {
                d = d.AddDays(-1);
            }
            return d;       
        }

        public DateTime RoundTo15Minutes(DateTime refDatetime)
        {
            // DateTime dtSecondsRemoved = refDatetime.AddSeconds(-refDatetime.Second);
            DateTime dtSecondsRemoved = new DateTime(refDatetime.Year, refDatetime.Month, refDatetime.Day, refDatetime.Hour, refDatetime.Minute, 0, 0);
            int minutes = dtSecondsRemoved.Minute % 15;
            DateTime adjustedDatetime = (minutes >= 8) ? dtSecondsRemoved.AddMinutes(15 - minutes) : dtSecondsRemoved.AddMinutes(-minutes);
            return adjustedDatetime;
        }
    }
}
