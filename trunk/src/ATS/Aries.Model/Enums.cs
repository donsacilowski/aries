﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Model {

    public enum Statuses : int
    {
        Open = 1,
        Assigned,
        Dispatched,
        Accepted,
        ArrivedPickedUp,
        PickedUp,
        ArrivedAtDroppedOff, 
        DroppedOff, 
        Rejected,
        CancelDispatch,
        OperatorCancelled,
        PickedUpCheck,
        DidNotLeaveAlone,
        DriverAttested,
        Closed,
        BadOrder,
        CancelReset,
        CancelUnitCh,
        ClosedCancelUnitCh,
        ClosedCancelReset,
        ClosedCancelDispatch,
        ClosedCancelClient,
        ClosedAttested
    }

    public enum ErrorCode
    {
        OK,
        OrderNotFound,
        OrderNotAssignedToDriver,
        InvalidStatusListId,
        InvalidTimestamped,
        InvalidMobileAppKey,
        UnableToSaveSignature,
        UserNameNotFound,
        DriverNotFound,
        MissingRegistrationInfo,
        MedtransTripNotFound,
        UnableToSaveNotes,
        MedTransNotSaved
    };

    public enum MobileMessageTypes
    {
        NewOrders,
        LogOff,
        Broadcast
    };
}
