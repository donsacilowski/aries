namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentCodes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employee", "DeptCodeOverride", c => c.Boolean(nullable: false));
            AddColumn("dbo.Employee", "DeptCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employee", "DeptCode");
            DropColumn("dbo.Employee", "DeptCodeOverride");
        }
    }
}
