namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        Street = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        PostalCode = c.String(maxLength: 50),
                        FC8Id = c.Int(nullable: false),
                        MedTransId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CancelReasonList",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CreditHour",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimesheetID = c.Int(nullable: false),
                        RegularTime = c.Single(nullable: false),
                        OverTime = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Timesheet", t => t.TimesheetID, cascadeDelete: true)
                .Index(t => t.TimesheetID);
            
            CreateTable(
                "dbo.Timesheet",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        EmployeeApproved = c.Boolean(nullable: false),
                        OfficeApproved = c.Boolean(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        MI = c.String(),
                        Role = c.String(nullable: false),
                        MobileAppKey = c.String(),
                        Active = c.Boolean(nullable: false),
                        EmployeeNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddressID = c.Int(),
                        Name = c.String(maxLength: 50),
                        PhoneNumber = c.String(maxLength: 50),
                        FC8Id = c.Int(nullable: false),
                        MedTransId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressID)
                .Index(t => t.AddressID);
            
            CreateTable(
                "dbo.Dispatch",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        DriverID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                        PaymentID = c.Int(),
                        PickupTime = c.DateTime(nullable: false),
                        DropoffTime = c.DateTime(),
                        PickupAddressID = c.Int(),
                        DropoffAddressID = c.Int(),
                        Notes = c.String(),
                        Service = c.String(maxLength: 50),
                        ServiceType = c.String(maxLength: 50),
                        DNLA = c.Boolean(nullable: false),
                        NeedSignature = c.Boolean(nullable: false),
                        SignaturePath = c.String(),
                        FC8Id = c.Int(nullable: false),
                        MedTransId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Driver", t => t.DriverID, cascadeDelete: true)
                .ForeignKey("dbo.Address", t => t.DropoffAddressID)
                .ForeignKey("dbo.Payment", t => t.PaymentID)
                .ForeignKey("dbo.Address", t => t.PickupAddressID)
                .Index(t => t.DriverID)
                .Index(t => t.CustomerID)
                .Index(t => t.PaymentID)
                .Index(t => t.PickupAddressID)
                .Index(t => t.DropoffAddressID);
            
            CreateTable(
                "dbo.Driver",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VehicleID = c.Int(),
                        Name = c.String(maxLength: 255),
                        PhoneNumber = c.String(maxLength: 20),
                        DriversLicenseNumber = c.String(maxLength: 50),
                        MobileAppKey = c.String(),
                        FC8Id = c.Int(nullable: false),
                        MedTransId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicle", t => t.VehicleID)
                .Index(t => t.VehicleID);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        LicensePlateNumber = c.String(maxLength: 20),
                        FC8Id = c.Int(nullable: false),
                        MedTransId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentToDriver = c.Boolean(nullable: false),
                        Paid = c.Boolean(nullable: false),
                        PaymentToDriverAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GroupEmployee",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.EmployeeId })
                .ForeignKey("dbo.Employee", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.Group", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        Description = c.String(maxLength: 1000),
                        CreationDate = c.DateTime(nullable: false),
                        LastModifiedDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        Thread = c.String(nullable: false, maxLength: 255),
                        Level = c.String(nullable: false, maxLength: 50),
                        Logger = c.String(nullable: false, maxLength: 255),
                        Message = c.String(nullable: false, maxLength: 4000),
                        Exception = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DispatchID = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        StatusListID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dispatch", t => t.DispatchID, cascadeDelete: true)
                .ForeignKey("dbo.StatusList", t => t.StatusListID, cascadeDelete: true)
                .Index(t => t.DispatchID)
                .Index(t => t.StatusListID);
            
            CreateTable(
                "dbo.StatusList",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TimeOffRequest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        TimeOfRequest = c.DateTime(nullable: false),
                        TimeOffRequested = c.DateTime(nullable: false),
                        AmountOfTime = c.Double(nullable: false),
                        ApprovalStatus = c.Boolean(nullable: false),
                        ApprovalState = c.String(),
                        Comments = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.TimesheetDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimesheetID = c.Int(nullable: false),
                        ServiceCode = c.String(),
                        TimeIn = c.DateTime(nullable: false),
                        TimeOut = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Timesheet", t => t.TimesheetID, cascadeDelete: true)
                .Index(t => t.TimesheetID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        First = c.String(),
                        Last = c.String(),
                        MI = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TimesheetDetail", "TimesheetID", "dbo.Timesheet");
            DropForeignKey("dbo.TimeOffRequest", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Status", "StatusListID", "dbo.StatusList");
            DropForeignKey("dbo.Status", "DispatchID", "dbo.Dispatch");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.GroupEmployee", "GroupId", "dbo.Group");
            DropForeignKey("dbo.GroupEmployee", "EmployeeId", "dbo.Employee");
            DropForeignKey("dbo.Dispatch", "PickupAddressID", "dbo.Address");
            DropForeignKey("dbo.Dispatch", "PaymentID", "dbo.Payment");
            DropForeignKey("dbo.Dispatch", "DropoffAddressID", "dbo.Address");
            DropForeignKey("dbo.Dispatch", "DriverID", "dbo.Driver");
            DropForeignKey("dbo.Driver", "VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.Dispatch", "CustomerID", "dbo.Customer");
            DropForeignKey("dbo.Customer", "AddressID", "dbo.Address");
            DropForeignKey("dbo.CreditHour", "TimesheetID", "dbo.Timesheet");
            DropForeignKey("dbo.Timesheet", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.TimesheetDetail", new[] { "TimesheetID" });
            DropIndex("dbo.TimeOffRequest", new[] { "EmployeeID" });
            DropIndex("dbo.Status", new[] { "StatusListID" });
            DropIndex("dbo.Status", new[] { "DispatchID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.GroupEmployee", new[] { "EmployeeId" });
            DropIndex("dbo.GroupEmployee", new[] { "GroupId" });
            DropIndex("dbo.Driver", new[] { "VehicleID" });
            DropIndex("dbo.Dispatch", new[] { "DropoffAddressID" });
            DropIndex("dbo.Dispatch", new[] { "PickupAddressID" });
            DropIndex("dbo.Dispatch", new[] { "PaymentID" });
            DropIndex("dbo.Dispatch", new[] { "CustomerID" });
            DropIndex("dbo.Dispatch", new[] { "DriverID" });
            DropIndex("dbo.Customer", new[] { "AddressID" });
            DropIndex("dbo.Timesheet", new[] { "EmployeeID" });
            DropIndex("dbo.CreditHour", new[] { "TimesheetID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.TimesheetDetail");
            DropTable("dbo.TimeOffRequest");
            DropTable("dbo.StatusList");
            DropTable("dbo.Status");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Log");
            DropTable("dbo.Group");
            DropTable("dbo.GroupEmployee");
            DropTable("dbo.Payment");
            DropTable("dbo.Vehicle");
            DropTable("dbo.Driver");
            DropTable("dbo.Dispatch");
            DropTable("dbo.Customer");
            DropTable("dbo.Employee");
            DropTable("dbo.Timesheet");
            DropTable("dbo.CreditHour");
            DropTable("dbo.CancelReasonList");
            DropTable("dbo.Address");
        }
    }
}
