namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Aries.Model.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<AriesDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            
            AutomaticMigrationDataLossAllowed = true;            
        }

        protected override void Seed(Aries.Model.AriesDataContext context)
        {
            //  This method will be called after migrating to the latest version.
            context.StatusLists.AddOrUpdate(p => p.Description,
                new StatusList { Description = "Open" },
                new StatusList { Description = "Assigned" },
                new StatusList { Description = "Dispatched" },
                new StatusList { Description = "Accepted" },
                new StatusList { Description = "AtPickUp" },
                new StatusList { Description = "PickedUp" },
                new StatusList { Description = "AtDropOff" },
                new StatusList { Description = "DroppedOff" },
                new StatusList { Description = "Rejected" },
                new StatusList { Description = "CancelDispatch" },
                new StatusList { Description = "CancelClient" },
                new StatusList { Description = "GotCheck" },
                new StatusList { Description = "NotAlone" },
                new StatusList { Description = "DriverAttested"},
                new StatusList { Description = "Closed" },
                new StatusList { Description = "BadOrder" },
                new StatusList { Description = "CancelReset" },
                new StatusList { Description = "CancelUnitCh" },
                new StatusList { Description = "ClosedCancelUnitCh" },
                new StatusList { Description = "ClosedCancelReset" },
                new StatusList { Description = "ClosedCancelDispatch" },
                new StatusList { Description = "ClosedCancelClient" },
                new StatusList { Description = "ClosedAttested" });

            context.CancelReasonLists.AddOrUpdate(p => p.Reason,
                            new CancelReasonList { Reason = "Flat Tire" },
                            new CancelReasonList { Reason = "Lost" },
                            new CancelReasonList { Reason = "Don't Care" });

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
