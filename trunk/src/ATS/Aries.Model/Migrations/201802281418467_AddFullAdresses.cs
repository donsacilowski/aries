namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFullAdresses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dispatch", "PickupFullAddress", c => c.String());
            AddColumn("dbo.Dispatch", "DropoffFullAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dispatch", "DropoffFullAddress");
            DropColumn("dbo.Dispatch", "PickupFullAddress");
        }
    }
}
