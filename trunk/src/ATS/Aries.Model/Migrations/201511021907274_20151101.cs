namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20151101 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dispatch",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        DriverID = c.Int(nullable: false),
                        VehicleID = c.Int(nullable: false),
                        PickupTime = c.DateTime(nullable: false),
                        Notes = c.String(nullable: false, maxLength: 4000),
                        Service = c.String(nullable: false, maxLength: 50),
                        ServiceType = c.String(nullable: false, maxLength: 50),
                        DNLA = c.Boolean(nullable: false),
                        PaymentToDriver = c.Boolean(nullable: false),
                        PaymentToDriverAmount = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Driver", t => t.DriverID, cascadeDelete: true)
                .ForeignKey("dbo.Vehicle", t => t.VehicleID, cascadeDelete: true)
                .Index(t => t.DriverID)
                .Index(t => t.VehicleID);
            
            CreateTable(
                "dbo.Driver",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        PhoneNumber = c.String(nullable: false, maxLength: 20),
                        DriversLicenseNumber = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        LicensePlateNumber = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dispatch", "VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.Dispatch", "DriverID", "dbo.Driver");
            DropIndex("dbo.Dispatch", new[] { "VehicleID" });
            DropIndex("dbo.Dispatch", new[] { "DriverID" });
            DropTable("dbo.Vehicle");
            DropTable("dbo.Driver");
            DropTable("dbo.Dispatch");
        }
    }
}
