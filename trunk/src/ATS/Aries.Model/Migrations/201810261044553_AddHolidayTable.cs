namespace Aries.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHolidayTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HolidayHours",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        Length = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HolidayHours", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.HolidayHours", new[] { "EmployeeID" });
            DropTable("dbo.HolidayHours");
        }
    }
}
