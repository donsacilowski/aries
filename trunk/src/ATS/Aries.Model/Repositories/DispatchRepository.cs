﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using MedTrans;
using log4net;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {

    public interface IDispatchRepository {

        List<Dispatch> GetAll();
        Dispatch GetByID(int id);
        int GetIDByOrderNumber(int orderNumber);
        int Add(Dispatch dispatch);
        bool Delete(int id);
        void Update(Dispatch dispatch);
        List<Dispatch> GetAllByDate(DateTime date);
    }


    public class DispatchRepository : BaseRepository, IDispatchRepository {

        private static readonly ILog log = LogManager.GetLogger(typeof(DispatchRepository));
        string routeExcelLocation = ConfigurationManager.AppSettings["ExportedRoutesLocation"].ToString();
        private const int headerLength = 2;

        public List<Dispatch> GetAll()
        {
            return db.Dispatchs.ToList();
        }
        public List<Dispatch> GetAllByDate(DateTime date)
        {
            DateTime frDate = date;
            DateTime toDate = new DateTime(frDate.Year, frDate.Month, frDate.Day, 23, 59, 59);

            List<Dispatch> dispatches = db.Dispatchs.Where(d => (d.PickupTime >= frDate) && (d.PickupTime <= toDate)).ToList();
            return dispatches;
            
        }

        public Dispatch GetByID(int id)
        {
            return db.Dispatchs.SingleOrDefault(p => p.Id == id);
        }

        public Dispatch GetByMedtransID(int id)
        {
            return db.Dispatchs.Where(p => p.MedTransId == id).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public int GetIDByOrderNumber(int orderNumber)
        {
            return db.Dispatchs.SingleOrDefault(p => p.FC8Id == orderNumber).Id;
        }

        public int Add(Dispatch dispatch) {
            db.Dispatchs.Add(dispatch);
            db.SaveChanges();

            return dispatch.Id;
        }


        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {

            Dispatch disp = db.Dispatchs.SingleOrDefault(p => p.Id == id);
            db.Dispatchs.Remove(disp);
            
            db.SaveChanges();

            return true;
        }

        public void Update(Dispatch dispatch) {
            var dbDispatch = GetByID(dispatch.Id);

            dbDispatch.CustomerID = dispatch.CustomerID;
            dbDispatch.DNLA = dispatch.DNLA;
            dbDispatch.DriverID = dispatch.DriverID;
            dbDispatch.FC8Id = dispatch.FC8Id;
            dbDispatch.NeedSignature = dispatch.NeedSignature;
            dbDispatch.Notes = dispatch.Notes;
            dbDispatch.PaymentID = dispatch.PaymentID;
            dbDispatch.PickupTime = dispatch.PickupTime;
            dbDispatch.Service = dispatch.Service;
            dbDispatch.ServiceType = dispatch.ServiceType;
            dbDispatch.SignaturePath = dispatch.SignaturePath;
            dbDispatch.Timestamp = dispatch.Timestamp;
            
            db.SaveChanges();
        }

        public List<DispatchSummaryView> GetDispatchSummary(DateTime frDate, DateTime toDate)
        {
            List<DispatchSummaryView> summaryList = new List<DispatchSummaryView>();
            StatusRepository sr = new StatusRepository();
            StatusListRepository sl = new StatusListRepository();

            log.Info("Calling DispatchSummary " + DateTime.Now.ToLongTimeString());

            List<Dispatch> dispatches = db.Dispatchs.Where(d => (d.PickupTime >= frDate) && (d.PickupTime <= toDate)).ToList();
            log.Info("In DispatchSummary after list" + DateTime.Now.ToLongTimeString());

            foreach (Dispatch d in dispatches)
            {
                Status s = sr.GetLatestStatus(d.Id);
                summaryList.Add(new DispatchSummaryView
                {
                    Id = d.Id,
                    OrderNumber = d.Id.ToString(),
                    Timestamp = d.PickupTime,
                    Customer = d.Customer.Name,
                    Driver = d.Driver.Name,
                    Status = sl.GetByID(s.StatusListID).Description,
                    StatusTimestamp = s.Timestamp
                });
            }
            log.Info("In DispatchSummary after foreach" + DateTime.Now.ToLongTimeString());

            return summaryList;
        }

        public List<Dispatch> GetDispatchListByDriver(List<Driver> driverlist, DateTime frDate, DateTime toDate)
        {
            List<Dispatch> dispList = new List<Dispatch>();
            StatusRepository sr = new StatusRepository();
            StatusListRepository sl = new StatusListRepository();
           
            foreach (Driver driver in driverlist)
            {
                List<Dispatch> dispatches = db.Dispatchs.Where(d => (d.PickupTime >= frDate) &&
                                                                    (d.PickupTime <= toDate) &&
                                                                    (d.DriverID==driver.Id)).OrderBy(p => p.PickupTime).ToList();

                dispList.AddRange(dispatches);
            }

            List<Dispatch> finalDispList = new List<Dispatch>();
            foreach (Dispatch disp in dispList)
            {
                Status sta = sr.GetByStatusID((int)Statuses.DriverAttested, disp.Id);
                if (sta != null)
                {
                    finalDispList.Add(disp);
                }
            }
            return finalDispList;
        }


        public List<DispatchSummaryView> GetOpenDispatchSummary(DateTime frDate, DateTime toDate)
        {
            List<DispatchSummaryView> summaryList = new List<DispatchSummaryView>();
            StatusRepository sr = new StatusRepository();
            StatusListRepository sl = new StatusListRepository();

            List<Dispatch> dispatches = db.Dispatchs.Where(d => (d.PickupTime >= frDate) && 
                                                                (d.PickupTime <= toDate)).ToList();

            foreach (Dispatch d in dispatches)
            {
                Status s = sr.GetLatestStatus(d.Id);
                if (s != null)
                {
                    if ((s.StatusListID != (int)Statuses.DroppedOff) &&
                        (s.StatusListID != (int)Statuses.CancelDispatch) &&
                        (s.StatusListID != (int)Statuses.Closed))
                    {
                        summaryList.Add(new DispatchSummaryView
                        {
                            Id = d.Id,
                            OrderNumber = d.Id.ToString(),
                            Timestamp = d.PickupTime,
                            Customer = d.Customer.Name,
                            Driver = d.Driver.Name,
                            Status = sl.GetByID(s.StatusListID).Description,
                            StatusTimestamp = s.Timestamp
                        });
                    }
                }
            }
            return summaryList;
        }

        public List<Dispatch> GetAllByStatusDescription(string statusDescription)
        {
            DateTime frDate = DateTime.Now.Date;
            DateTime toDate = new DateTime(frDate.Year, frDate.Month, frDate.Day, 23, 59, 59);

            List<Dispatch> dispatches = new List<Dispatch>();
            List<Status> statuses = db.Statuses.Where(s => s.StatusList.Description == statusDescription).ToList();
            
            foreach (Status s in statuses)
            {
                dispatches.Add(GetByID(s.DispatchID));
            }
            return dispatches;
        }

        public List<Dispatch> GetAllNewOrders(int driverID) {

            List<Dispatch> dispatches = new List<Dispatch>();
            List<Dispatch> dispatchesToSend = new List<Dispatch>();

            try
            {
                DateTime now = DateTime.Now.Date;
                dispatches = db.Dispatchs.Where(d => d.DriverID == driverID &&
                                                     d.PickupTime >= now).OrderBy(o=>o.PickupTime).ToList();

                StatusRepository statusRepo = new StatusRepository();

                foreach (Dispatch d in dispatches)
                {
                    int latestStatus = statusRepo.GetLatestStatus(d.Id).StatusListID;
                    if (latestStatus == (int)Statuses.CancelDispatch || latestStatus == (int)Statuses.Dispatched)
                    {
                        dispatchesToSend.Add(d);
                    }
                }
            }
            catch (Exception ex) { }

            return dispatches;                                  
                                                           //.Join(db.Statuses,d=>d.Id, s=>s.DispatchID, (db,s)=>new Statuses()).Where(s=>s.

        }
        public List<Dispatch> GetAllByNotStatusDescription(string statusDescription)
        {
            List<Dispatch> dispatches = new List<Dispatch>();
            List<Status> statuses = db.Statuses.Where(s => s.StatusList.Description != statusDescription).ToList();
            foreach (Status s in statuses)
            {
                dispatches.Add(GetByID(s.DispatchID));
            }
            return dispatches;
        }

        public List<Route> GetRoutes(DateTime frDate, DateTime toDate)
        {
            return db.Routes.Where(r => r.Timestamp >= frDate && r.Timestamp < toDate).ToList();
        }
        public Route GetRoute(int medtransID)
        {
            return db.Routes.Where(r => r.MedTransId == medtransID).FirstOrDefault();
        }

        public void SaveRoute(Route route)
        {
            db.Routes.Add(route);
            db.SaveChanges();
        }
        public void DeleteRoutes()
        {
            IEnumerable<Route> list = db.Routes.ToList();
            db.Routes.RemoveRange(list);
            db.SaveChanges();
        }
        public void ExportRoutes(DateTime frDate, DateTime toDate)
        {
            string imageFile = "ExportedRoutes" + Timestamped(DateTime.Now.ToString()).ToString("MM_dd_yyyy HH_mm") + ".csv";
            string imageFullName = System.IO.Path.Combine(routeExcelLocation, imageFile);

            try
            {
                log.Error("Route Display " + frDate.ToShortDateString() + " " + toDate.ToShortDateString());

                List<Route> allRoutes = GetRoutes(frDate.Date, toDate.Date);

                string delimiter = ",";
                string[] lines = new string[allRoutes.Count + headerLength];
                int lineNumber = 0;

                lines[lineNumber++] = "Route Date, Route Name,Driver, Vehicle, Route Time,Route Distance,Revenue";

                foreach (Route route in allRoutes)
                {
                    lines[lineNumber++] = route.Timestamp.ToShortDateString() + delimiter +
                                          route.Name.Replace(',', ' ') + delimiter +
                                          route.Driver.Name.Replace(',', ' ') + delimiter +
                                          route.Driver.Vehicle.Description + delimiter +
                                          route.TotalTime.TotalHours + delimiter +
                                          route.Distance + delimiter +
                                          route.Revenue;
                }

                System.IO.File.WriteAllLines(imageFullName, lines);
            }
            catch (Exception _Exception)
            {
                log.ErrorFormat("Error exporting routes " + _Exception.Message, _Exception);
            }
        }
        private DateTime Timestamped(string timestamped)
        {
            DateTime dateValue;

            DateTime.TryParse(timestamped, out dateValue);

            return dateValue;
        }
    }

}
