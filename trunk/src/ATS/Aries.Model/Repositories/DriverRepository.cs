﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface IDriverRepository {

        List<Driver> GetAll();
        Driver GetByID(int id);
        Driver GetByMedtransID(int id);
        int Add(Driver driver);
        bool Delete(int id);
        void Update(Driver driver);
    }

    public class DriverRepository : BaseRepository, IDriverRepository {

        public List<Driver> GetAll() {
            return db.Drivers.ToList();
        }
        public List<Driver> GetAllByGroup()
        {
            var tst = db.Drivers.GroupBy(p => p.Name)
  .Select(g => g.First())
  .ToList();
            return db.Drivers.GroupBy(p => p.Name)
  .Select(g => g.First())
  .ToList();
        }


        
                
        public Driver GetByID(int id) {
            return db.Drivers.SingleOrDefault(p => p.Id == id);
        }

        public Driver GetByName(string name)
        {
            return db.Drivers.Where(p => p.Name == name).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public List<Driver> GetLastInListByName(string name)
        {
            return db.Drivers.Where(p => p.Name == name).OrderByDescending(p => p.Id).ToList();
        }
        public List<Driver> GetListByName(string name)
        {
            return db.Drivers.Where(p => p.Name == name).ToList();
        }

        public Driver GetByMedtransID(int id)
        {
            return db.Drivers.Where(p => p.MedTransId == id).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public Driver GetByMobileAppKey(string mobileAppKey, string username)
        {

            return db.Drivers.Where(p => p.MobileAppKey == mobileAppKey && p.Name == username).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public int Add(Driver driver) {
            db.Drivers.Add(driver);
            db.SaveChanges();

            return driver.Id;
        }

        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {
            Driver driver = db.Drivers.SingleOrDefault(p => p.Id == id);

            db.Drivers.Remove(driver);
            db.SaveChanges();

            return true;
        }

        public void Update(Driver driver) {
            var dbDriver = GetByID(driver.Id);

            dbDriver.DriversLicenseNumber = driver.DriversLicenseNumber;
            dbDriver.Name = driver.Name;
            dbDriver.PhoneNumber = driver.PhoneNumber;
            dbDriver.VehicleID = driver.VehicleID;
            dbDriver.MobileAppKey = driver.MobileAppKey;
            db.SaveChanges();
        }
    }

}
