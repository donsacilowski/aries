﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface ICancelReasonListRepository {

        List<CancelReasonList> GetAll();
        CancelReasonList GetByID(int id);
        int GetByDescription(string description);
    }

    public class CancelReasonListRepository : BaseRepository, ICancelReasonListRepository
    {
        public List<CancelReasonList> GetAll()
        {
            return db.CancelReasonLists.ToList();
        }

        public CancelReasonList GetByID(int id)
        {
            return db.CancelReasonLists.SingleOrDefault(p => p.Id == id);
        }

        public int GetByDescription(string desc) {
            return db.CancelReasonLists.SingleOrDefault(p => p.Reason == desc).Id;
        }

    }
}