﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface ITimeOffRepository {

        List<TimeoffSummary> GetAll();
        TimeOffRequest GetByID(int id);
        int Add(TimeOffRequest TimeOffRequest);
        bool Delete(int id);
        void Update(TimeOffRequest TimeOffRequest);
    }

    public class TimeOffRepository : BaseRepository, ITimeOffRepository
    {

        public List<TimeoffSummary> GetAll() {
            List<TimeoffSummary> tSumList = new List<TimeoffSummary>();
            List<TimeOffRequest> tList = db.TimeOffRequests.Where(t=>t.ApprovalState.Trim().Equals("Not Entered")).OrderBy(t => t.TimeOfRequest).ToList();

            foreach (TimeOffRequest tReq in tList) {
                TimeoffSummary tSum = new TimeoffSummary();
                tSum.ID = tReq.Id;
                tSum.AmountOfTime = tReq.AmountOfTime;
                tSum.ApprovalStatus = tReq.ApprovalStatus;
                tSum.ApprovalState = tReq.ApprovalState;
                tSum.Comments = tReq.Comments;
                tSum.EmployeeName = tReq.Employee.FirstName + " " + tReq.Employee.LastName;
                tSum.TimeOffRequested = tReq.TimeOffRequested;
                tSum.TimeOfRequest = tReq.TimeOfRequest;
                tSumList.Add(tSum);
            }
            return tSumList;
        }

        public TimeOffRequest GetByID(int id) {
            return db.TimeOffRequests.FirstOrDefault(p => p.Id == id);
        }

        public int Add(TimeOffRequest TimeOffRequest) {
            db.TimeOffRequests.Add(TimeOffRequest);
            db.SaveChanges();

            return TimeOffRequest.Id;
        }

        public bool Delete(int id) {

            TimeOffRequest addr = db.TimeOffRequests.FirstOrDefault(p => p.Id == id);

            db.TimeOffRequests.Remove(addr);
            db.SaveChanges();

            return true;
        }

        public void Update(TimeOffRequest TimeOffRequest) {
            var dbTimeOffRequest = GetByID(TimeOffRequest.Id);

            dbTimeOffRequest.EmployeeID = TimeOffRequest.EmployeeID;
            dbTimeOffRequest.TimeOffRequested = TimeOffRequest.TimeOffRequested;
            dbTimeOffRequest.TimeOfRequest = TimeOffRequest.TimeOfRequest;
            dbTimeOffRequest.AmountOfTime = TimeOffRequest.AmountOfTime;
            dbTimeOffRequest.ApprovalStatus = TimeOffRequest.ApprovalStatus;
            dbTimeOffRequest.ApprovalState = TimeOffRequest.ApprovalState;
            dbTimeOffRequest.Comments = TimeOffRequest.Comments;

            db.SaveChanges();
        }
    }

}