﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface IPaymentRepository {

        List<Payment> GetAll();
        Payment GetByID(int id);
        int Add(Payment Payment);
        bool Delete(int id);
        void Update(Payment Payment);
    }

    public class PaymentRepository : BaseRepository, IPaymentRepository {

        public List<Payment> GetAll() {
            return db.Payments.ToList();
        }

        public Payment GetByID(int id) {
            return db.Payments.SingleOrDefault(p => p.Id == id);
        }

        public int Add(Payment Payment) {
            db.Payments.Add(Payment);
            db.SaveChanges();

            return Payment.Id;
        }


        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {

            Payment addr = db.Payments.SingleOrDefault(p => p.Id == id);

            db.Payments.Remove(addr);
            db.SaveChanges();

            return true;
        }

        public void Update(Payment Payment) {
            var dbPayment = GetByID(Payment.Id);

            dbPayment.Paid = Payment.Paid;
            dbPayment.PaymentToDriver = Payment.PaymentToDriver;
            dbPayment.PaymentToDriverAmount = Payment.PaymentToDriverAmount;
            
            db.SaveChanges();
        }
    }

}