﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;
using log4net;

namespace Aries.Model.Repositories
{

    public interface IEmployeeRepository
    {
        List<Employee> GetAll();
        List<Employee> GetByActive(bool bActive);
        Employee GetByID(int id);
        Employee GetByName(string username);
        Employee GetByRegKey(string regKey, string username);
        int Add(Employee Employee);
        bool Delete(int id);
        void Update(Employee Employee);
        string GetEmployeeName(string username);
        void UpdateHoliday(int id, int length);
    }

    public class EmployeeRepository : BaseRepository, IEmployeeRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployeeRepository));
        public List<Employee> GetAll()
        {
            return db.Employees.ToList();
        }
        public List<Employee> GetByActive(bool bActive)
        {
            return db.Employees.Where(p => p.Active == bActive).ToList();
        }
        public Employee GetByID(int id)
        {
            return db.Employees.SingleOrDefault(p => p.Id == id);
        }
        public Employee GetByRegKey(string regKey, string username)
        {
            string[] name = username.ToLower().Split(new string[] { " " }, StringSplitOptions.None);
            string fName = "";
            string lname = "";

            if (name.Length > 2 && (name[2].ToLower().Equals("jr") ||
                                    name[2].ToLower().Equals("sr") ||
                                    name[2].ToLower().Equals("ii") ||
                                    name[2].ToLower().Equals("iii") ||
                                    name[2].ToLower().Equals("iv"))) { //this is a jr, sr, III or IV as part of the last name

                fName = name[0];
                lname = name[1] + " " + name[2];
            }
            else {

                fName = name.Length > 2 ? name[0] + " " + name[1] : name[0]; // first name 2 words, i.e. middle name/initial
                lname = name.Length > 2 ? name[2] : name[1]; 
            }
            //return db.Employees.SingleOrDefault(p => p.MobileAppKey == regKey && p.FirstName.ToLower() == fName &&
            //                                                                     p.LastName.ToLower() == lname);
            return db.Employees.SingleOrDefault(p => p.MobileAppKey == regKey && p.FirstName == fName && p.LastName==lname && p.Active==true);
        }

        public Employee GetByName(string username)
        {
            return db.Employees.SingleOrDefault(p => p.UserName.ToLower() == username.ToLower());
        }

        public Employee GetByFullName(string fName, string lName)
        {
            return db.Employees.Where(p => p.FirstName.ToLower() == fName.ToLower() &&
                                      p.LastName == lName).FirstOrDefault();
        }

        public int Add(Employee Employee)
        {
            db.Employees.Add(Employee);
            db.SaveChanges();

            return Employee.Id;
        }

        // TODO - Delete by Id instead of Name
        public bool Delete(int id)
        {

            var dbemployee = GetByID(id);
            dbemployee.Active = false;
            db.SaveChanges();

            return true;
        }

        public void Update(Employee Employee)
        {
            var dbEmployee = GetByID(Employee.Id);
            dbEmployee.UserName = Employee.UserName;
            dbEmployee.EmployeeNumber = Employee.EmployeeNumber;
            dbEmployee.LastName = Employee.LastName;
            dbEmployee.FirstName = Employee.FirstName;
            dbEmployee.MI = Employee.MI;
            dbEmployee.Role = Employee.Role;
            dbEmployee.MobileAppKey = Employee.MobileAppKey;
            dbEmployee.Active = Employee.Active;
            dbEmployee.DeptCode = Employee.DeptCode;
            dbEmployee.DeptCodeOverride = Employee.DeptCodeOverride;
            db.SaveChanges();
        }

        public string GetEmployeeName(string username)
        {
            Employee emp = GetByName(username);
            if (emp == null)
            {
                return "";
            }
            else
            {
                return emp.LastName + ", " + emp.FirstName + (emp.MI == null ? "" : " " + emp.MI);
            }
        }
        public Employee GetEmployeeByBridgeName(string bridgeName)
        {
            var names = bridgeName.Split(',');
            
            Employee emp = GetByFullName(names[1].Trim(), names[0].Trim());
            return emp;
        }
        public HolidayHours GetHoliday(int id)
        {
            return db.HolidayHours.Where(h => h.EmployeeID == id).FirstOrDefault();     
        }
        public void UpdateHoliday(int id, int length)
        {
            HolidayHours hrs = db.HolidayHours.Where(h => h.EmployeeID == id).FirstOrDefault();

            if (hrs == null)
            {
                HolidayHours newHrs = new HolidayHours();
                newHrs.Length = length;
                newHrs.EmployeeID = id;
                db.HolidayHours.Add(newHrs);
            }
            else
            {
                hrs.Length = length;
            }
            db.SaveChanges();
        }
    }
}
