﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface IVehicleRepository {

        List<Vehicle> GetAll();
        Vehicle GetByID(int id);
        Vehicle GetByMedtransID(int id);
        int Add(Vehicle vehicle);
        bool Delete(int id);
        void Update(Vehicle vehicle);
    }

    public class VehicleRepository : BaseRepository, IVehicleRepository {

        public List<Vehicle> GetAll() {
            return db.Vehicles.ToList();
        }

        public Vehicle GetByID(int id) {
            return db.Vehicles.SingleOrDefault(p => p.Id == id);
        }

        public Vehicle GetByMedtransID(int id) {
            return db.Vehicles.Where(p => p.MedTransId == id).OrderByDescending(p=>p.Id).FirstOrDefault();
        }

        public int Add(Vehicle vehicle) {
            db.Vehicles.Add(vehicle);
            db.SaveChanges();

            return vehicle.Id;
        }

        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {
            Vehicle veh = db.Vehicles.SingleOrDefault(p => p.Id == id);

            db.Vehicles.Remove(veh);
            db.SaveChanges();

            return true;
        }

        public void Update(Vehicle vehicle) {
            var dbVehicle = GetByID(vehicle.Id);

            dbVehicle.Description = vehicle.Description;
            dbVehicle.LicensePlateNumber = vehicle.LicensePlateNumber;

            db.SaveChanges();
        }
    }

}
