﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;
using Aries.Model;
using Aries.Utility;

namespace Aries.Model.Repositories {
    public interface ILocationRepository {

        List<LocationAlert> GetAll();
        int Add(int employeeID);
        
    }

    public class LocationRepository : BaseRepository, ILocationRepository
    {

        public List<LocationAlert> GetAll() {
            return db.LocationAlerts.ToList();
        }
       
        public int Add(int employeeID)
        {
            LocationAlert newAlert = new LocationAlert();
            newAlert.EmployeeID = employeeID;
            newAlert.Timestamp = DateTime.Now.Date;
            db.LocationAlerts.Add(newAlert);
            db.SaveChanges();

            return newAlert.Id;
        }

        public List<LocationAlertSummary> GetLocationAlertSummary(DateTime date)
        {
            DateTime startdate = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime enddate = startdate.AddDays(6);

            List<LocationAlert> locAlertSummary = (new LocationRepository()).GetAll().Where(l=>l.Timestamp >= startdate && l.Timestamp <= enddate).ToList();

            List<LocationAlertSummary> summaryList = new List<LocationAlertSummary>();

            bool addItem = true;
            foreach (LocationAlert locAlert in locAlertSummary)
            {
                LocationAlertSummary item = null;
                addItem = false;
                if (summaryList.Count > 0)
                {
                    item = summaryList.FirstOrDefault(s => s.EmployeeId == locAlert.EmployeeID);
                }
                
                if (item == null) {
                    item = new LocationAlertSummary();
                    addItem = true;
                }

                item.EmployeeId = locAlert.EmployeeID;
                Employee employee = (new EmployeeRepository()).GetByID(item.EmployeeId);
                item.EmployeeName = employee.LastName + ", " + employee.FirstName + (employee.MI == null ? "" : " " + employee.MI);

                item.Mon = item.Mon == false ? startdate == locAlert.Timestamp ? true : false : true;
                item.Tue = item.Tue == false ? startdate.AddDays(1) == locAlert.Timestamp ? true : false : true;
                item.Wed = startdate.AddDays(2) == locAlert.Timestamp ? true : false;
                item.Thu = startdate.AddDays(3) == locAlert.Timestamp ? true : false;
                item.Fri = startdate.AddDays(4) == locAlert.Timestamp ? true : false;
                item.Sat = startdate.AddDays(5) == locAlert.Timestamp ? true : false;
                item.Sun = startdate.AddDays(6) == locAlert.Timestamp ? true : false;

                if (addItem)
                {
                    summaryList.Add(item);
                }
            }

            return summaryList;
        }

        private List<SummaryData> GetSummayByDate(List<SummaryData> summaryData, DateTime searchDate)
        {
            return (from sl in summaryData where (sl.Date == searchDate) select sl).ToList();
        }
    }

}