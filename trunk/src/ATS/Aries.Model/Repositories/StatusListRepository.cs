﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface IStatusListRepository {

        List<StatusList> GetAll();
        StatusList GetByID(int id);
        int GetByDescription(string description);
        StatusList GetStatusListByDescription(string description);
    }

    public class StatusListRepository : BaseRepository, IStatusListRepository {

        public List<StatusList> GetAll() {
            return db.StatusLists.ToList();
        }

        public StatusList GetByID(int id) {
            return db.StatusLists.SingleOrDefault(p => p.Id == id);
        }

        public int GetByDescription(string desc) {
            return db.StatusLists.SingleOrDefault(p => p.Description == desc).Id;
        }

        public StatusList GetStatusListByDescription(string desc)
        {
            return db.StatusLists.SingleOrDefault(p => p.Description == desc);
        }

    }
}