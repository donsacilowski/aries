﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Text;
using System.Threading.Tasks;


using Aries.Model.Entities;
using Aries.Utility;
using log4net;

namespace Aries.Model.Repositories
{
    public interface ITimesheetRepository
    {
        List<Timesheet> GetAll();
        List<TimesheetDetail> GetAllTodaysDetails();
        TimesheetDetail GetLastDetail(int timesheetID);
        Timesheet GetByID(int id);
        int Add(Timesheet Timesheet);
        bool Delete(int id);
        void Update(Timesheet Timesheet);
    }

    public class TimesheetRepository : BaseRepository, ITimesheetRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TimesheetRepository));
        public AriesDataContext DBContext()
        {
            return db;
        }

        #region Basic Functionalities
        public List<TimesheetDetail> GetAllDetails()
        {
            return db.TimesheetDetails.ToList();
        }
        public List<TimesheetDetail> GetAllTodaysDetails()
        {
            DateTime now = DateTime.Now.Date;
            DateTime today = now.AddDays(1);
            List<TimesheetDetail> tsList = new List<TimesheetDetail>();
            var res = from element in db.TimesheetDetails
                      where element.TimeOut >= now && element.TimeOut <= today
                      group element by element.TimesheetID
                     into groups
                      select groups.OrderByDescending(p => p.TimeOut).FirstOrDefault();
            int count = res.Count();
            foreach (TimesheetDetail td in res)
            {
                tsList.Add(td);
            }
            return tsList;
        }
        public TimesheetDetail GetLastDetail(int timesheetID)
        {
            DateTime now = DateTime.Now.Date;
            DateTime today = now.AddDays(1);
            TimesheetDetail tsDetail = new TimesheetDetail();

            //var res = from element in db.TimesheetDetails
            //          where element.TimeOut >= now && element.TimeOut <= today
            //          group element by element.TimesheetID
            //         into groups
            //          select groups.OrderByDescending(p => p.TimeOut).FirstOrDefault();
            tsDetail = db.TimesheetDetails.Where(td => td.TimesheetID == timesheetID && 
                                                (td.ServiceCode != "Lunch") &&
                                                (td.ServiceCode != "Holiday")).OrderByDescending(td => td.TimeOut).FirstOrDefault();

            if (tsDetail != null)
            {
                if (tsDetail.ServiceCode == "Logged Out") tsDetail = null;
            }

            return tsDetail;
        }
        public TimesheetDetail GetDetailByID(int id)
        {
            return db.TimesheetDetails.SingleOrDefault(p => p.Id == id);
        }
        public int AddDetail(TimesheetDetail toBeAdded)
        {
            if (toBeAdded.TimeIn <= toBeAdded.TimeOut)
            {
                db.TimesheetDetails.Add(toBeAdded);
                db.SaveChanges();
            }
            return toBeAdded.Id;
        }
        public bool DeleteDetail(int id)
        {
            TimesheetDetail toBeDeleted = db.TimesheetDetails.SingleOrDefault(p => p.Id == id);
            db.TimesheetDetails.Remove(toBeDeleted);
            db.SaveChanges();
            return true;
        }
        public void UpdateDetail(TimesheetDetail toBeUpdated)
        {
            if (toBeUpdated.TimeIn <= toBeUpdated.TimeOut)
            {
                var dbRec = GetDetailByID(toBeUpdated.Id);
                //dbRec.EmployeeID = toBeUpdated.EmployeeID;
                dbRec.TimesheetID = toBeUpdated.TimesheetID;
                dbRec.ServiceCode = toBeUpdated.ServiceCode;
                dbRec.TimeIn = toBeUpdated.TimeIn;
                dbRec.TimeOut = toBeUpdated.TimeOut;
                db.SaveChanges();
            }
        }

        public void UpdateDetail(TimesheetDetail toBeUpdated, int id)
        {
            TimesheetDetail tsDetail = GetDetailByID(id);
            DateTime oldDate = new DateTime(tsDetail.Timesheet.Date.Year, tsDetail.Timesheet.Date.Month, tsDetail.Timesheet.Date.Day);
            DateTime newDate = new DateTime(toBeUpdated.TimeIn.Year, toBeUpdated.TimeIn.Month, toBeUpdated.TimeIn.Day);

            if (oldDate != newDate)
            {
                Timesheet newTimesheet = GetTimesheet(tsDetail.Timesheet.EmployeeID, newDate, true);
                toBeUpdated.TimesheetID = newTimesheet.Id;

            }
            UpdateDetail(toBeUpdated);
        }

        public List<Timesheet> GetAll()
        {
            return db.Timesheets.ToList();
        }
        public Timesheet GetByID(int id)
        {
            return db.Timesheets.SingleOrDefault(p => p.Id == id);
        }

        public Timesheet GetByEmployeeIdAndDate(int employeeId, DateTime date)
        {
            return db.Timesheets.SingleOrDefault(p => (p.EmployeeID == employeeId) && (p.Date == date.Date));
        }

        public int Add(Timesheet Timesheet)
        {
            db.Timesheets.Add(Timesheet);
            db.SaveChanges();

            return Timesheet.Id;
        }
        public bool Delete(int id)
        {
            Timesheet timesheet = db.Timesheets.SingleOrDefault(p => p.Id == id);
            db.Timesheets.Remove(timesheet);
            db.SaveChanges();
            return true;
        }
        public void Update(Timesheet Timesheet)
        {
            var dbTimesheet = GetByID(Timesheet.Id);
            dbTimesheet.EmployeeID = Timesheet.EmployeeID;
            dbTimesheet.Date = Timesheet.Date;
            dbTimesheet.EmployeeApproved = Timesheet.EmployeeApproved;
            dbTimesheet.OfficeApproved = Timesheet.OfficeApproved;
            dbTimesheet.Notes = Timesheet.Notes;
            db.SaveChanges();
        }
        #endregion

        #region Specific Functionalities

        private DateTime RoundTo15Minutes(DateTime refDatetime)
        {
            return (new Utility.Misc()).RoundTo15Minutes(refDatetime);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="timeIn"></param>
        /// <param name="timeOut"></param>
        public void UpdateDetail(string timesheetDetailId, DateTime newTimeIn, DateTime newTimeOut)
        {
            int Id = 0;
            int.TryParse(timesheetDetailId, out Id);
            if (Id == 0)
            {
                return;
            }
            TimesheetDetail editTimesheetDetail = GetDetailByID(Id);

            if (editTimesheetDetail.Timesheet.OfficeApproved == true)
            {
            }

            Timesheet editTimesheet = GetByID(editTimesheetDetail.TimesheetID);
            int employeeId = editTimesheet.EmployeeID;

            if (newTimeIn.Date == editTimesheet.Date.Date)
            {
                // Date does
                editTimesheetDetail.TimeIn = newTimeIn;
                editTimesheetDetail.TimeOut = newTimeOut;
                UpdateDetail(editTimesheetDetail);
            }
            else
            {
                // Date has change 
                editTimesheet = GetTimesheet(employeeId, newTimeIn.Date, true);
                editTimesheetDetail.TimesheetID = editTimesheet.Id;
                editTimesheetDetail.TimeIn = newTimeIn;
                editTimesheetDetail.TimeOut = newTimeOut;
                UpdateDetail(editTimesheetDetail);
            }

        }


        /// <summary>
        /// Find timesheet record by employee id and date. 
        /// If not found and bAddIfNotExist is set, then add timesheet record with specified date
        /// Assume employeeID is valid ( employeeID > 0)
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="date"></param>
        /// <param name="bAddIfNotExist"></param>
        /// <returns></returns>
        public Timesheet GetTimesheet(int employeeID, DateTime date, bool bAddIfNotExist = true)
        {
            Timesheet rec = db.Timesheets.SingleOrDefault(p => (p.EmployeeID == employeeID) && (p.Date == date.Date));
            if ((rec == null) && (bAddIfNotExist == true))
            {
                rec = new Timesheet();
                rec.EmployeeID = employeeID;
                rec.Date = date.Date; // new DateTime(date.Year, date.Month, date.Day);
                rec.EmployeeApproved = false;
                rec.OfficeApproved = false;
                rec.Notes = "";
                Add(rec);
            }
            return rec;
        }

        /// <summary>
        /// Get employee's timesheets for the week starting with the specified date
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<Timesheet> GetEmployeeTimesheets(int employeeID, DateTime date)
        {
            DateTime endDate = date.AddDays(6);
            if (employeeID == 0)
            {
                return db.Timesheets.Where(t => (t.Date >= date.Date) && (t.Date <= endDate.Date)).ToList();
            }
            else
            {
                return db.Timesheets.Where(t => (t.EmployeeID == employeeID) && (t.Date >= date.Date) && (t.Date <= endDate.Date)).ToList();
            }
        }
        public void EmployeeApproveTimesheet(int employeeID, DateTime date)
        {            
            foreach (Timesheet t in GetEmployeeTimesheets(employeeID, date))
            {
                if (t.EmployeeApproved == false)
                {
                    t.EmployeeApproved = true;
                    Update(t);
                }
            }
        }

        public void OfficeApproveTimesheet(int employeeID, DateTime date)
        {
            foreach (Timesheet t in GetEmployeeTimesheets(employeeID, date))
            {
                if (t.OfficeApproved == false)
                {
                    t.OfficeApproved = true;
                    Update(t);
                }
            }
        }
        public void OfficeUnApproveTimesheet(int employeeID, DateTime date)
        {
            foreach (Timesheet t in GetEmployeeTimesheets(employeeID, date))
            {
                if (t.OfficeApproved == true)
                {
                    t.OfficeApproved = false;
                    Update(t);
                }
            }
        }

        public void OfficeApproveAllTimesheet(DateTime date)
        {
            foreach (Timesheet t in GetEmployeeTimesheets(0, date))
            {
                if (t.OfficeApproved == false)
                {
                    t.OfficeApproved = true;
                    Update(t);
                }
            }
        }


        /// <summary>
        /// Find 
        /// </summary>
        /// <param name="timesheetID"></param>
        /// <returns></returns>
        public TimesheetDetail FindSignIn(int timesheetID)
        {
            List<TimesheetDetail> tsdList = db.TimesheetDetails.Where(t => (t.ServiceCode == "Time-Charged") && (t.TimesheetID == timesheetID)).OrderByDescending(o => o.Id).ToList();
            return (tsdList.Count > 0) ? tsdList[0] : null;
            // return db.TimesheetDetails.SingleOrDefault(p => (p.TimesheetID == timesheetID) && (p.TimeIn == p.TimeOut));
        }
        public TimesheetDetail FindLunch(int timesheetID)
        {
            List<TimesheetDetail> tsdList = db.TimesheetDetails.Where(t => (t.ServiceCode == "Lunch") && (t.TimesheetID == timesheetID)).OrderByDescending(o => o.Id).ToList();
            return (tsdList.Count > 0) ? tsdList[0] : null;

        }
        public TimesheetDetail FindHoliday(int timesheetID)
        {
            List<TimesheetDetail> tsdList = db.TimesheetDetails.Where(t => (t.ServiceCode == "Holiday") && (t.TimesheetID == timesheetID)).OrderByDescending(o => o.Id).ToList();
            return (tsdList.Count > 0) ? tsdList[0] : null;
        }

        private void AddPaidTime(int employeeId, DateTime dateIn, int hours, string serviceCode)
        {
            int timesheetID = GetTimesheet(employeeId, dateIn, true).Id;
            DateTime dateOut = dateIn.AddHours(hours);

            /* See if there's a record which has the same start time, If there is, don't add another one */
            List<TimesheetDetail> tsd = GetEmployeeTimesheetDetail(employeeId, dateIn);
            if (tsd.Count == 0)
            {
                AddDetail(new TimesheetDetail
                {
                    TimesheetID = timesheetID,
                    ServiceCode = serviceCode,
                    TimeIn = dateIn,
                    TimeOut = dateOut
                });
            }
        
        }
        public void AddNewEvent(int employeeId, DateTime eventDate, DateTime dateIn, DateTime dateOut, string serviceCode)
        {
            int timesheetID = GetTimesheet(employeeId, eventDate, true).Id;
            
            AddDetail(new TimesheetDetail
            {
                TimesheetID = timesheetID,
                ServiceCode = serviceCode,
                TimeIn = RoundTo15Minutes(dateIn),
                TimeOut = RoundTo15Minutes(dateOut)
            });
        }
        /// <summary>
        /// Add vacation hours
        /// </summary>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="date">vaction date</param>
        /// <param name="hours">vacation hours</param>
        public void AddVacation(int employeeId, DateTime date, int hours)
        {
            AddPaidTime(employeeId, RoundTo15Minutes(date), hours, "Vacation");
        }
        public void AddHoliday(int employeeId, DateTime date)
        {
            //Get this employees holiday hours; get any timesheet detail for this date and add it to the end, 
            //otherwise use the date which should be midnite
            HolidayHours hol = (new EmployeeRepository()).GetHoliday(employeeId);
            if (hol != null) // holiday had been entered
            {
                Timesheet ts = GetTimesheet(employeeId, date);
                TimesheetDetail tsdLast = GetLastDetail(ts.Id);

                TimesheetDetail tsdHol = FindHoliday(ts.Id);
                DateTime timeIn = new DateTime(date.Year, date.Month, date.Day, 6, 0, 0);

                if (tsdHol != null)
                {
                    tsdHol.TimeOut = tsdHol.TimeIn.AddHours(hol.Length);
                    UpdateDetail(tsdHol);
                }
                else
                {
                    if (tsdLast != null)
                    {
                        timeIn = tsdLast.TimeOut;
                    }
                    AddPaidTime(employeeId, RoundTo15Minutes(timeIn), (int)hol.Length, "Holiday");
                }
            }
        }        
        public void SignIn(int employeeId, DateTime dt)
        {
            DateTime dtValue = RoundTo15Minutes(dt);
            AddPaidTime(employeeId, dtValue, 0, "Time-Charged");
        }
        public void SignOut(int employeeId, DateTime dt)
        {
            try
            {
                int timesheetID = GetTimesheet(employeeId, DateTime.Today, true).Id;
                TimesheetDetail timesheetDetail = FindSignIn(timesheetID);
                if (timesheetDetail == null)
                {
                    return;
                }
                DateTime dtValue = RoundTo15Minutes(DateTime.Now);
                timesheetDetail.TimeOut = dtValue;
                UpdateDetail(timesheetDetail);
            } catch (Exception ex) {

            }      
        }

        public void TakeLunch(int employeeId, int duration)
        {
            Timesheet timesheet = GetTimesheet(employeeId, DateTime.Today, false);
            log.Info("Getting timesheet employee id = " + employeeId.ToString());
            if (timesheet == null)
            {
                log.Info("Didnt find timesheet = " + employeeId.ToString());
                return;
            }
            TimesheetDetail timesheetDetail = FindSignIn(timesheet.Id); //Get the latest time charged entry
            log.Info("Getting timesheet detail employee id = " + employeeId.ToString());
            if (timesheetDetail == null)
            {
                log.Info("Didnt find timesheet detail = " + employeeId.ToString());
                return;
            }
            DateTime dtValue = RoundTo15Minutes(DateTime.Now);
            timesheetDetail.TimeOut = dtValue;
            UpdateDetail(timesheetDetail);

            // Add lunch time
            timesheetDetail = new TimesheetDetail();
            timesheetDetail.TimesheetID = timesheet.Id;
            timesheetDetail.ServiceCode = "Lunch";
            timesheetDetail.TimeIn = dtValue;
            timesheetDetail.TimeOut = dtValue.AddMinutes(duration);
            AddDetail(timesheetDetail);
            
            // Sign employee backin
            timesheetDetail = new TimesheetDetail();
            timesheetDetail.TimesheetID = timesheet.Id;
            timesheetDetail.ServiceCode = "Time-Charged";
            timesheetDetail.TimeIn = dtValue.AddMinutes(duration);
            timesheetDetail.TimeOut = dtValue.AddMinutes(duration);
            AddDetail(timesheetDetail);

            
            
        }

        public List<TimesheetDetail> GetEmployeeTimesheetDetails(int employeeId)
        {

            var employeeTimesheet = from detail in db.TimesheetDetails
                                    from timesheet in db.Timesheets
                                    where ((detail.TimesheetID == timesheet.Id) && (timesheet.EmployeeID == employeeId))
                                    select detail;

            //int timesheetID = GetTimesheet(employeeId, DateTime.Today, false).Id;
            return employeeTimesheet.ToList();
        }
        public List<TimesheetDetail> GetEmployeeTimesheetDetail(int employeeId, DateTime startTime)
        {
            var employeeTimesheet = from detail in db.TimesheetDetails
                                    from timesheet in db.Timesheets
                                    where ((detail.TimesheetID == timesheet.Id) && 
                                           (timesheet.EmployeeID == employeeId) &&
                                           (detail.TimeIn == startTime))
                                    select detail;

            //int timesheetID = GetTimesheet(employeeId, DateTime.Today, false).Id;
            return employeeTimesheet.ToList();
        }

        public List<TimesheetDetail> GetEmployeeTimesheetDetails(int employeeId, DateTime startDate, DateTime endDate)
        {

            var employeeTimesheet = from detail in db.TimesheetDetails
                                    from timesheet in db.Timesheets
                                    where ((detail.TimesheetID == timesheet.Id) && 
                                           (timesheet.EmployeeID == employeeId) && 
                                           (timesheet.Date >= startDate.Date) && 
                                           (timesheet.Date <= endDate.Date) &&
                                           (detail.ServiceCode != "Logged Out"))
                                    select detail;

            //int timesheetID = GetTimesheet(employeeId, DateTime.Today, false).Id;
            return employeeTimesheet.ToList();
        }
         
        public List<TimesheetDetailDisplay> GetEmployeeTimesheetDetailsDisplay(int employeeId, DateTime startDate, DateTime endDate)
        {

            List<TimesheetDetail> employeeTimesheet = GetEmployeeTimesheetDetails(employeeId, startDate, endDate);
            List<TimesheetDetailDisplay> employeeTimesheetDisplay = new List<TimesheetDetailDisplay>(); 
            
            for (int i = 0; i < employeeTimesheet.Count; i++)
            {
                TimeSpan TotalMinutes = employeeTimesheet[i].TimeOut - employeeTimesheet[i].TimeIn;

                TimesheetDetailDisplay tsDisplay = new TimesheetDetailDisplay();
                tsDisplay.ID = employeeTimesheet[i].Id;
                //tsDisplay.ServiceCode = employeeTimesheet[i].ServiceCode + ": " + TotalMinutes.ToString("hh\\:mm");



                tsDisplay.TimeIn = employeeTimesheet[i].TimeIn;
                tsDisplay.TimeOut = employeeTimesheet[i].TimeOut;

                string tOut = (tsDisplay.TimeOut == tsDisplay.TimeIn) ? "" : tsDisplay.TimeOut.ToString("hh\\:mm tt");
                string fromTo = "(" + tsDisplay.TimeIn.ToString("hh\\:mm tt") + " - " + tOut + ")";

                float subMin = 100 * (float)TotalMinutes.Minutes / 60;
                string subTotal = TotalMinutes.Hours.ToString() + "." + subMin.ToString("00");

                tsDisplay.ServiceCode = employeeTimesheet[i].ServiceCode + Environment.NewLine + fromTo + Environment.NewLine + subTotal;
                    employeeTimesheetDisplay.Add(tsDisplay);
            }


            //int timesheetID = GetTimesheet(employeeId, DateTime.Today, false).Id;
            return employeeTimesheetDisplay;
        }

        public TimesheetTotal GetEmployeeTimesheetTotal(int employeeId, DateTime startDate, DateTime endDate)
        {
            TimesheetTotal tsTotal = new TimesheetTotal();
            var tsDetails = GetEmployeeTimesheetDetails(employeeId, startDate, endDate);
            TimeSpan[] ts = new TimeSpan[5];
            foreach (TimesheetDetail t in tsDetails)
            {
                switch (t.ServiceCode)
                {
                    case "Time-Charged":
                        ts[0] += t.TimeOut - t.TimeIn;
                        break;
                    case "Vacation":
                        ts[1] += t.TimeOut - t.TimeIn;
                        break;
                    case "Holiday":
                        ts[2] += t.TimeOut - t.TimeIn;
                        break;
                    case "Bereavement":
                        ts[3] += t.TimeOut - t.TimeIn;
                        break;
                }
                ts[3] += t.TimeOut - t.TimeIn;
            }


            tsTotal.TimeCharged = (int)ts[0].TotalHours + ts[0].ToString(@"\:mm");  // ts[0].ToString("hh\\:mm");
            tsTotal.Vacation = (int)ts[1].TotalHours + ts[1].ToString(@"\:mm");  // ts[1].ToString("hh\\:mm");
            tsTotal.Holiday = (int)ts[2].TotalHours + ts[2].ToString(@"\:mm");  // ts[2].ToString("hh\\:mm");
            tsTotal.Bereavement = (int)ts[3].TotalHours + ts[3].ToString(@"\:mm");  // ts[2].ToString("hh\\:mm");
            tsTotal.Total = (int)ts[4].TotalHours + ts[4].ToString(@"\:mm");  // ts[3].ToString("hh\\:mm");



            return tsTotal;
        }
        
        public List<TimesheetSummary> GetTimesheetSummary(DateTime date, string role="ALL")
        {
            DateTime startdate = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime enddate = startdate.AddDays(6);

            /* Find and remove duplicate timesheets for all employees */
            EmployeeRepository empRepo = new EmployeeRepository();
            List<Employee> empList = empRepo.GetAll();
            foreach (Employee emp in empList) {
                RemoveDuplicateTimesheets(emp.Id, date);
            }

            var timesheetDetails = from d in db.TimesheetDetails
                                   from t in db.Timesheets
                                   where ((d.TimesheetID == t.Id) && (t.Date >= startdate) && (t.Date <= enddate))
                                   select new { t.EmployeeID, t.Date, t.EmployeeApproved, t.OfficeApproved, d.ServiceCode, d.TimeIn, d.TimeOut };
            
            var summaryData = (from e in db.Employees
                              join t in timesheetDetails on e.Id equals t.EmployeeID into et
                              where e.Active == true 
                               from all_et in et.DefaultIfEmpty()
                               select new SummaryData {
                                   EmployeeId = (e.Id),
                                   UserId = (e.UserName),
                                   First = (e.FirstName),
                                   Last = (e.LastName),
                                   MI = (e.MI),
                                   Date = (all_et == null ? new DateTime() : all_et.Date),
                                   EmployeeApproved = (all_et == null ? false : all_et.EmployeeApproved),
                                   OfficeApproved = (all_et == null ? false : all_et.OfficeApproved),
                                   ServiceCode = (all_et == null ? string.Empty : all_et.ServiceCode),
                                   TimeIn = (all_et == null ? new DateTime() : all_et.TimeIn),
                                   TimeOut = (all_et == null ? new DateTime() : all_et.TimeOut),
                                   Role = (e.Role)
                               });
            
            var mon = GetSummayByDate(summaryData.ToList(), startdate.AddDays(0));
            var tue = GetSummayByDate(summaryData.ToList(), startdate.AddDays(1));
            var wed = GetSummayByDate(summaryData.ToList(), startdate.AddDays(2));
            var thu = GetSummayByDate(summaryData.ToList(), startdate.AddDays(3));
            var fri = GetSummayByDate(summaryData.ToList(), startdate.AddDays(4));
            var sat = GetSummayByDate(summaryData.ToList(), startdate.AddDays(5));
            var sun = GetSummayByDate(summaryData.ToList(), startdate.AddDays(6));

            List<TimesheetSummary> summaryList = new List<TimesheetSummary>();
            var employees = summaryData.GroupBy(i => i.EmployeeId).Select(group => group.FirstOrDefault()).ToList();
            foreach (SummaryData employee in employees)
            {
                if ((role.Equals("Dispatch") && employee.Role.Equals("Driver") ||
                    (role.Equals("ALL"))))
                {
                    TimesheetSummary item = new TimesheetSummary();
                    item.EmployeeId = employee.EmployeeId;
                    item.EmployeeName = employee.Last + ", " + employee.First + (employee.MI == null ? "" : " " + employee.MI);

                    item.Mon = DailyData(employee.EmployeeId, mon);
                    item.Tue = DailyData(employee.EmployeeId, tue);
                    item.Wed = DailyData(employee.EmployeeId, wed);
                    item.Thu = DailyData(employee.EmployeeId, thu);
                    item.Fri = DailyData(employee.EmployeeId, fri);
                    item.Sat = DailyData(employee.EmployeeId, sat);
                    item.Sun = DailyData(employee.EmployeeId, sun);

                    double subtotal = item.Mon.Worked + item.Tue.Worked + item.Wed.Worked + item.Thu.Worked + item.Fri.Worked + item.Sat.Worked + item.Sun.Worked;
                    double subvacation = item.Mon.Vacation + item.Tue.Vacation + item.Wed.Vacation + item.Thu.Vacation + item.Fri.Vacation + item.Sat.Vacation + item.Sun.Vacation;
                    double subholiday = item.Mon.Holiday + item.Tue.Holiday + item.Wed.Holiday + item.Thu.Holiday + item.Fri.Holiday + item.Sat.Holiday + item.Sun.Holiday;
                    double bereavement = item.Mon.Bereavement + item.Tue.Bereavement + item.Wed.Bereavement + item.Thu.Bereavement + item.Fri.Bereavement + item.Sat.Bereavement + item.Sun.Bereavement;
                    item.SubTotal = new TotalHours();
                    item.SubTotal.Reg = (subtotal > 40) ? 40 : Math.Round(subtotal, 2);
                    item.SubTotal.OT = (subtotal > 40) ? Math.Round(subtotal - 40, 2) : 0;

                    CreditHour creditHour = (new TimesheetRepository()).GetEmployeeCreditHours(employee.EmployeeId, startdate);
                    item.Credit = new TotalHours();
                    item.Credit.Reg = (creditHour != null) ? creditHour.RegularTime : 0;
                    item.Credit.OT = (creditHour != null) ? creditHour.OverTime : 0;
                    item.Total = new TotalHours();
                    item.Total.Reg = item.SubTotal.Reg + item.Credit.Reg;
                    item.Total.OT = item.SubTotal.OT + item.Credit.OT;


                    item.OfficeApproved = OfficeApproved(employee.EmployeeId, startdate, startdate.AddDays(6)) ? "YES" : "NO";
                    item.EmployeeApproved = EmployeedApproved(employee.EmployeeId, startdate, startdate.AddDays(6)) ? "YES" : "NO";

                    summaryList.Add(item);
                }
            }

            return summaryList;
        }
        private List<SummaryData> GetSummayByDate(List<SummaryData> summaryData, DateTime searchDate)
        {
            return (from sl in summaryData where (sl.Date == searchDate) select sl).ToList();
        }
        private DailyHours DailyData(int Id, List<SummaryData> dataList)
        {
            DailyHours daily_hours = new DailyHours();
            var datas = (from d in dataList where (d.EmployeeId == Id) select d).ToList();
            TimeSpan[] ts = new TimeSpan[4];
            foreach (SummaryData data in datas)
            {
                switch (data.ServiceCode)
                {
                    case "Time-Charged":
                        ts[0] += data.TimeOut - data.TimeIn;
                        break;
                    case "Vacation":
                        ts[1] += data.TimeOut - data.TimeIn;
                        break;
                    case "Holiday":
                        ts[2] += data.TimeOut - data.TimeIn;
                        break;
                    case "Bereavement":
                        ts[3] += data.TimeOut - data.TimeIn;
                        break;
                }
            }
            daily_hours.Worked = Math.Round(ts[0].TotalHours, 2);
            daily_hours.Vacation = Math.Round(ts[1].TotalHours, 2);
            daily_hours.Holiday = Math.Round(ts[2].TotalHours, 2);
            daily_hours.Bereavement = Math.Round(ts[3].TotalHours, 2);

            return daily_hours;
        }
        private bool OfficeApproved(int userId, DateTime startDate, DateTime endDate)
        {
            List<TimesheetDetail> timesheets = GetEmployeeTimesheetDetails(userId, startDate, endDate);
            bool bOfficeApproved = (timesheets.Count > 0);
            foreach (TimesheetDetail td in timesheets)
            {
                if (td.Timesheet.OfficeApproved == false)
                {
                    bOfficeApproved = false;
                    break;
                }
            }
            return bOfficeApproved; 
        }
        private bool EmployeedApproved(int userId, DateTime startDate, DateTime endDate)
        {
            List<TimesheetDetail> timesheets = GetEmployeeTimesheetDetails(userId, startDate, endDate);
            bool bEmployeeApproved = (timesheets.Count > 0);
            foreach (TimesheetDetail td in timesheets)
            {
                if (td.Timesheet.EmployeeApproved == false)
                {
                    bEmployeeApproved = false;
                    break;
                }
            }
            return bEmployeeApproved; 
        }
        #endregion

        #region Mobile App
        public void SignInViaMobileApp(int employeeId, DateTime timestamp, string zipCode)
        {
            /* Remove and duplicate timesheets for this employee */
            RemoveDuplicateTimesheets(employeeId, timestamp);
            int timesheetId = 0;
            
            Timesheet timesheet = GetByEmployeeIdAndDate(employeeId, timestamp.Date);           

            if (timesheet == null)
            {
                timesheet = new Timesheet();
                timesheet.EmployeeID = employeeId;
                timesheet.Date = timestamp.Date;  
                timesheet.EmployeeApproved = false;
                timesheet.OfficeApproved = false;
                timesheet.Notes = "";
                timesheetId = Add(timesheet);

                DateTime dt = timestamp.AddMinutes(-15);
                if (zipCode.Equals("14043")) dt = timestamp.AddMinutes(-30);

                AddPaidTime(employeeId, RoundTo15Minutes(dt), 0, "Time-Charged");
            } else {

                TimesheetDetail tsDetails = GetLastDetail(timesheet.Id);
               
                if (tsDetails != null)
                {
                    TimeSpan diff1 = DateTime.Now.Subtract(tsDetails.TimeOut);
                    if (diff1.TotalMinutes > 120)
                    {
                        SignOutViaMobileApp(employeeId, tsDetails.TimeOut);
                        AddPaidTime(employeeId, RoundTo15Minutes(timestamp), 0, "Time-Charged");
                    }
                    else
                    {
                        DateTime newEndTime = RoundTo15Minutes(timestamp);
                        
                        tsDetails.TimeOut = RoundTo15Minutes(timestamp);
                        
                        UpdateDetail(tsDetails);
                        
                        //AddPaidTime(employeeId, tsDetails.TimeOut, 0, "Time-Charged");
                    }
                }
                else // This would occur when last detail is Logged Out
                {                    
                    (new TimesheetRepository()).AddNewEvent
                                (employeeId, DateTime.Now, DateTime.Now, DateTime.Now, "Time-Charged");
                }
            }
        }
        public void SignOutViaMobileApp(int employeeId, DateTime timestamp)
        {
            DateTime dtValue = timestamp;
            Timesheet timesheet = GetByEmployeeIdAndDate(employeeId, dtValue.Date);
            if (timesheet == null)
                return;

            TimesheetDetail timesheetDetail = FindSignIn(timesheet.Id);
            if (timesheetDetail == null)
                return;

            timesheetDetail.TimeOut = RoundTo15Minutes(timestamp);
            UpdateDetail(timesheetDetail);
        }
        #endregion


        #region Sign In/Out
        


        #endregion

        #region Credit Hours
        public List<CreditHour> CreditHoursGetAll()
        {
            return db.CreditHours.ToList();
        }
        public CreditHour CreditHourGetByID(int id)
        {
            return db.CreditHours.SingleOrDefault(p => p.Id == id);
        }
        public CreditHour CreditHourGetByTimesheetID(int id)
        {
            return db.CreditHours.SingleOrDefault(p => p.TimesheetID == id);
        }
        public int CreditHourAdd(CreditHour toBeAdded)
        {
            db.CreditHours.Add(toBeAdded);
            db.SaveChanges();
            return toBeAdded.Id;
        }
        public bool CreditHourDelete(int id)
        {
            CreditHour toBeDeleted = db.CreditHours.SingleOrDefault(p => p.Id == id);
            db.CreditHours.Remove(toBeDeleted);
            db.SaveChanges();
            return true;
        }
        public void CreditHourUpdate(CreditHour toBeUpdated)
        {
            CreditHour dbRec = CreditHourGetByID(toBeUpdated.Id);
            //dbRec.EmployeeID = toBeUpdated.EmployeeID;
            dbRec.TimesheetID = toBeUpdated.TimesheetID;
            dbRec.RegularTime = toBeUpdated.RegularTime;
            dbRec.OverTime = toBeUpdated.OverTime;
            db.SaveChanges();
        }

        public CreditHour GetEmployeeCreditHours(int employeeId, DateTime weekOf)
        {
            Timesheet timesheet = GetTimesheet(employeeId, (new Misc()).FirstDayOfWeek(weekOf, DayOfWeek.Monday), false);
            if (timesheet == null)
                return null;

            return CreditHourGetByTimesheetID(timesheet.Id);
        }
        public void AddCredits(int employeeId, DateTime weekOf, float regHours, float otHours)
        {
            CreditHour ch = GetEmployeeCreditHours(employeeId, weekOf);

            if (ch == null)
            {
                Timesheet timesheet = GetTimesheet(employeeId, weekOf, true);
                CreditHour creditHour = new CreditHour();
                creditHour.TimesheetID = timesheet.Id;
                creditHour.RegularTime = regHours;
                creditHour.OverTime = otHours;
                CreditHourAdd(creditHour);
            }
            else
            {
                ch.RegularTime = regHours;
                ch.OverTime = otHours;
                CreditHourUpdate(ch);
            }
           
        }
        #endregion

        public void RemoveDuplicateTimesheets (int employeeId, DateTime dt) {
            List<Timesheet> timesheets = db.Timesheets.Where(t => t.EmployeeID == employeeId && t.Date.Equals(dt.Date)).ToList();
            //object timesheets1 = db.Timesheets.GroupBy(c => c.EmployeeID)
            //                                                .Where(grp => grp.Count() > 1)
            //                                                .Select(grp => grp.Key);

            if (timesheets.Count > 1) {
                /* Look for the first timesheet with no details and remove it */
                foreach (Timesheet ts in timesheets) {
                    int detailCount = db.TimesheetDetails.Where(td => td.TimesheetID == ts.Id).Count();

                    if (detailCount == 0) {
                        TimesheetRepository trRepo = new TimesheetRepository();
                        trRepo.Delete(ts.Id);
                        log.Info("Duplicate timesheet deleted for " + employeeId.ToString());
                        return;
                    }
                }
            }
        }
    }
}
