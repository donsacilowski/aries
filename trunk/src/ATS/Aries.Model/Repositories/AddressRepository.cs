﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface IAddressRepository {

        List<Address> GetAll();
        Address GetByID(int id);
        Address GetByMedtransID(int? id);
        int Add(Address Address);
        bool Delete(int id);
        void Update(Address Address);
    }

    public class AddressRepository : BaseRepository, IAddressRepository {

        public List<Address> GetAll() {
            return db.Addresses.ToList();
        }

        public Address GetByID(int id) {
            return db.Addresses.SingleOrDefault(p => p.Id == id);
        }

        public Address GetByMedtransID(int? id) {

            return db.Addresses.Where(p => p.MedTransId == id).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public int Add(Address Address) {
            db.Addresses.Add(Address);
            db.SaveChanges();

            return Address.Id;
        }


        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {

            Address addr = db.Addresses.SingleOrDefault(p => p.Id == id);
            
            db.Addresses.Remove(addr);
            db.SaveChanges();

            return true;
        }

        public void Update(Address Address) {
            var dbAddress = GetByID(Address.Id);

            dbAddress.City = Address.City;
            dbAddress.Description = Address.Description;
            dbAddress.PostalCode = Address.PostalCode;
            dbAddress.State = Address.State;
            dbAddress.Street = Address.Street;
            
            db.SaveChanges();
        }
    }

}
