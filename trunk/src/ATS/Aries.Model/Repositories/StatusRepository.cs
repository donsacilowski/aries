﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;
using Aries.Model;

namespace Aries.Model.Repositories {
    public interface IStatusRepository {

        List<Status> GetAll();
        List<Status> GetAllOpenOrders();
        Status GetByID(int id);
        int Add(Status Status);
        bool Delete(int id);
        void Update(Status Status);
    }

    public class StatusRepository : BaseRepository, IStatusRepository {

        public List<Status> GetAll() {
            return db.Statuses.ToList();
        }

        public List<Status> GetAllOpenOrders()
        {
            List<Status> openOrders2 = new List<Status>();
            List<Status> allOrders = db.Statuses.GroupBy(x => x.DispatchID)
                                                 .Select(group => group.Where(x => x.StatusListID == group.Max(y => y.StatusListID))
                                                 .FirstOrDefault()).ToList();

            List<Status> openOrders = new List<Status>();
            foreach (Status status in allOrders)
            {
                if (status.StatusListID < (int)Statuses.DroppedOff)
                {
                    openOrders.Add(status);
                }
            }

            return openOrders;
        }
        public List<Status> GetTodaysOpenOrders()
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = DateTime.Today.AddDays(1);
            List<Status> allOrders = db.Statuses.Where(s => s.Timestamp >= today && s.Timestamp < tomorrow).ToList();
            //.OrderByDescending(s => s.StatusListID).First().;//.StatusListID == (int)Statuses.Dispatched;

            List<Status> allOrders2 = new List<Status>();
            foreach (Status s in allOrders) {
                if (GetLatestStatus(s.DispatchID).StatusListID == (int)Statuses.Dispatched) {
                    allOrders2.Add(s);
                }
            }
            //List<Status> allOrders2 = allOrders.(s => s.StatusListID == (int)Statuses.Dispatched &&
            //                                          (s.Timestamp >= today && s.Timestamp < tomorrow)).ToList();

            return allOrders2;
        }

        public Status GetByID(int id)
        {
            return db.Statuses.SingleOrDefault(p => p.Id == id);
        }

        public Status GetByStatusID(int status, int orderID)
        {
            return db.Statuses.FirstOrDefault(p => p.DispatchID == orderID && p.StatusListID == status);
        }

        public List<Status> GetByDispatchID(int dispatchId)
        {
            return db.Statuses.Where(p => p.DispatchID == dispatchId).OrderByDescending(p => p.Timestamp).ToList();
        }

        public Status GetStatusByDispatchID(int dispatchId, Statuses sta)
        {
            return db.Statuses.Where(p => p.DispatchID == dispatchId && p.StatusListID == (int)sta).FirstOrDefault();
        }

        public Status GetLatestStatus(int dispatchId)
        {
            //return db.Statuses.Where(p => p.DispatchID == dispatchId).OrderByDescending(p => p.Timestamp).ThenByDescending(p => p.StatusListID).First();
            return db.Statuses.Where(p => p.DispatchID == dispatchId).OrderByDescending(p => p.Timestamp).First();
        }

        public bool TripHasCancelDispatch(int dispatchId)
        {
            //return db.Statuses.Where(p => p.DispatchID == dispatchId).OrderByDescending(p => p.Timestamp).ThenByDescending(p => p.StatusListID).First();
            Status disp = db.Statuses.Where(d => d.DispatchID == dispatchId &&
                                               (d.StatusListID == (int)Statuses.ClosedCancelDispatch ||
                                                d.StatusListID == (int)Statuses.ClosedCancelReset ||
                                                d.StatusListID == (int)Statuses.ClosedCancelUnitCh ||
                                                d.StatusListID == (int)Statuses.ClosedCancelClient ||
                                                d.StatusListID == (int)Statuses.CancelDispatch
                                                )).FirstOrDefault();

            return disp == null ? false : true;
        }

        public int Add(Status Status) {
            db.Statuses.Add(Status);
            db.SaveChanges();

            return Status.Id;
        }

        public int Add(int dispatchId, DateTime timestamped, int statusListId)
        {
            Status newStatus = new Status();
            newStatus.DispatchID = dispatchId;
            newStatus.Timestamp = timestamped;
            newStatus.StatusListID = statusListId;
            return (Add(newStatus));
        }

        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {

            Status addr = db.Statuses.SingleOrDefault(p => p.Id == id);

            db.Statuses.Remove(addr);
            db.SaveChanges();

            return true;
        }

        public void Update(Status Status) {
            var dbStatus = GetByID(Status.Id);

            dbStatus.StatusListID = Status.StatusListID;
            dbStatus.Timestamp = Status.Timestamp;
            dbStatus.DispatchID = Status.DispatchID;
            
            db.SaveChanges();
        }

        public void DeleteAllByNotStatusDescription(int driverID)
        {
            List<Status> statuses = new List<Status>();
            List<Dispatch> disps = db.Dispatchs.Where(d => d.DriverID == driverID).ToList();
            
            foreach (Dispatch d in disps)
            {
                statuses.AddRange(GetByDispatchID(d.Id));
            }

            foreach (Status s in statuses)
            {
                if (s.StatusList.Description != "Dispatched"
                    && s.StatusList.Description != "CancelDispatch"
                    && s.StatusList.Description != "CancelClient")
                {
                    db.Statuses.Remove(s);
                }
            }

            db.SaveChanges();
        }
    }

}