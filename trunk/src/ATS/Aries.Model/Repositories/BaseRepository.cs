﻿using System;


namespace Aries.Model.Repositories {
   
    public class BaseRepository : IDisposable {

        protected AriesDataContext db;

        public BaseRepository() {
            db = new AriesDataContext();
            // DEBUG db.Database.Log = LogSQL;
        }

        protected void Dispose(bool disposing) {
            if (disposing) {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private static void LogSQL(string sql) {
            System.Diagnostics.Debug.Write(sql);
        }
    }
}
