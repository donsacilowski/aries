﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aries.Model.Entities;

namespace Aries.Model.Repositories {
    public interface ICustomerRepository {

        List<Customer> GetAll();
        Customer GetByID(int id);
        Customer GetByMedtransID(int? id);
        int Add(Customer Customer);
        bool Delete(int id);
        void Update(Customer Customer);
    }

    public class CustomerRepository : BaseRepository, ICustomerRepository {

        public List<Customer> GetAll() {
            return db.Customers.ToList();
        }

        public Customer GetByID(int id) {
            return db.Customers.FirstOrDefault(p => p.Id == id);
        }

        public Customer GetByMedtransID(int? id) {

            return db.Customers.Where(p => p.MedTransId == id).OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public int Add(Customer Customer) {
            db.Customers.Add(Customer);
            db.SaveChanges();

            return Customer.Id;
        }


        // TODO - Delete by Id instead of Name
        public bool Delete(int id) {

            Customer addr = db.Customers.FirstOrDefault(p => p.Id == id);

            db.Customers.Remove(addr);
            db.SaveChanges();

            return true;
        }

        public void Update(Customer Customer) {
            var dbCustomer = GetByID(Customer.Id);

            dbCustomer.FC8Id = Customer.FC8Id;
            dbCustomer.MedTransId = Customer.MedTransId;
            dbCustomer.Name = Customer.Name;
            dbCustomer.PhoneNumber = Customer.PhoneNumber;
            dbCustomer.AddressID = Customer.AddressID;
            
            db.SaveChanges();
        }
    }

}