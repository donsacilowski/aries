﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;

namespace Aries.Model.Entities {
    [Table("Payment")]
    public class Payment : IEntity {

        [Required]
        public bool PaymentToDriver { get; set; }

        [Required]
        public bool Paid { get; set; }

        [Required]
        public decimal PaymentToDriverAmount { get; set; }

    }
}
