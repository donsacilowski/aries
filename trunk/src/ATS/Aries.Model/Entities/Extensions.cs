﻿using Aries.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Model.Entities
{
    public static class Extensions
    {
        public static GroupView ToGroupViewModel(this Group group)
        {
            GroupView groupView = new GroupView()
            {
                Id = group.Id,
                Name = group.Name,
                Description = group.Description,
                CreationDate = group.CreationDate,
                LastModifiedDate = group.LastModifiedDate,
                Active = group.Active
            };

            return groupView;
        }
        
        public static EmployeeView ToEmployeeViewModel(this Employee employee)
        {
            EmployeeView employeeView = new EmployeeView()
            {
                Id = employee.Id.ToString(),
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                MI = employee.MI,
                Role = employee.Role,
                UserName = employee.UserName,
                EmployeeNumber = employee.EmployeeNumber,
                Active = employee.Active,
                MobileAppKey = employee.MobileAppKey
            };

            return employeeView;
        }
    }
}