﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Model.Entities {
    [Table("CancelReasonList")]
    public class CancelReasonList : IEntity {

        public string Reason { get; set; }
    }
}
