﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Model.Entities {
    /// <summary>
    /// Entity class for the Dispatches.
    /// </summary>
    [Table("Dispatch")]
    public class Dispatch : ISyncedEntity {

        [Required]
        public DateTime Timestamp { get; set; }

        [ForeignKey("Driver")]
        public int DriverID { get; set; }
        public virtual Driver Driver { get; set; }

        [ForeignKey("Customer")]
        public int CustomerID { get; set; }
        public virtual Customer Customer {get; set; }

        [ForeignKey("Payment")]
        public int? PaymentID { get; set; }
        public virtual Payment Payment { get; set; }

        [Required]
        public DateTime PickupTime { get; set; }

        public DateTime? DropoffTime { get; set; }

        [ForeignKey("PickupAddress")]
        public int? PickupAddressID { get; set; }
        public virtual Address PickupAddress { get; set; }

        [ForeignKey("DropoffAddress")]
        public int? DropoffAddressID { get; set; }
        public virtual Address DropoffAddress { get; set; }

        public string Notes { get; set; }

        [StringLength(50)]
        public string Service { get; set; }

        [StringLength(50)]
        public string ServiceType { get; set; }

        [Required]
        public bool DNLA { get; set; }

        [Required]
        public bool NeedSignature { get; set; }

        public string SignaturePath { get; set; }
        public string PickupFullAddress { get; set; }
        public string DropoffFullAddress { get; set; }
    }

    [Table("Route")]
    public class Route : ISyncedEntity
    {
        public string Name { get; set; }

        [ForeignKey("Driver")]
        public int DriverID { get; set; }
        public virtual Driver Driver { get; set; }

        [Required]
        public double Revenue { get; set; }

        [Required]
        public TimeSpan TotalTime { get; set; }

        public double Distance { get; set; }

        [Required]
        public string Status { get; set; }
        public DateTime Timestamp { get; set; }

    }

    public class DispatchSummaryView
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime Timestamp { get; set; }
        public string Customer { get; set; }
        public string Driver { get; set; }
        public string Status { get; set; }
        public DateTime StatusTimestamp { get; set; }
    }

    public class DispatchAlertView
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime Timestamp { get; set; }
        public string Customer { get; set; }
        public string Driver { get; set; }
        public string Status { get; set; }
        public long Minutes { get; set; }
        public string BGcss { get; set; }
    }
    public class RouteView
    {
        public string Name { get; set; }
        public string DriverName { get; set; }
        public string Duration { get; set; }
        public string Distance { get; set; }
        public string Revenue { get; set; }
        public string Status { get; set; }
        public string Timestamp { get; set; }
    }

}
