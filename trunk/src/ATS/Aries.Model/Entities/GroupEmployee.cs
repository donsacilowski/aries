﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Model.Entities
{
    [Table("GroupEmployee")]
    public class GroupEmployee
    {
        #region Properties

        [Required]
        [Display(Name = "Group ID")]
        [Key, ForeignKey("Group"), Column(Order = 1)]
        public int GroupId { get; set; }
        
        [Display(Name = "Employee ID")]
        [Key, ForeignKey("Employee"), Column(Order = 2)]
        public int? EmployeeId { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        
        public virtual Group Group { get; set; }
        
        public virtual Employee Employee { get; set; }

        #endregion

        #region Initialization

        public GroupEmployee()
        {
            CreationDate = DateTime.Now;
        }

        #endregion
    }

    public class GroupEmployeeView
    {
        [Display(Name = "ID")]
        public int EmployeeId { get; set; }

        [Display(Name = "Group ID")]
        public int GroupId { get; set; }
        
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "User ID")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "MI")]
        public string MI { get; set; }

        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Mobile App Key")]
        public string MobileAppKey { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        [Display(Name = "Employee Number")]
        public string EmployeeNumber { get; set; }
    }
}
