﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;

namespace Aries.Model.Entities {

    /// <summary>
    /// Entity class for the Drivers.
    /// </summary>
    [Table("Driver")]
    public class Driver : ISyncedEntity {

        [ForeignKey("Vehicle")]
        public int? VehicleID { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string DriversLicenseNumber { get; set; }

        [Display(Name = "Mobile App Key")]
        public string MobileAppKey { get; set; }

    }
}
