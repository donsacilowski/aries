﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Model.Entities
{
    [Table("Group")]
    public class Group : IEntity
    {
        #region Properties

        [Required]
        [Display(Name = "Group Name")]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Group Description")]
        [StringLength(1000)]
        public string Description { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Last Modified Date")]
        public DateTime LastModifiedDate { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        public virtual ICollection<GroupEmployee> Employees { get; set; }

        #endregion

        #region Initialization

        public Group()
        {
            CreationDate = DateTime.Now;
        }

        #endregion
    }

    public class GroupView
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Group Name")]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Group Description")]
        [StringLength(1000)]
        public string Description { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Last Modified Date")]
        public DateTime LastModifiedDate { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        public List<GroupEmployeeView> Employees { get; set; }
    }
}
