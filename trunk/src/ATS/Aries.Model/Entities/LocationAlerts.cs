﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Model.Entities {
    [Table("LocationAlert")]
    public class LocationAlert : IEntity {

        [ForeignKey("Employee")]
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }
        
    }

    public class LocationAlertSummary
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
    }

}
