﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Aries.Model.Entities {

    /// <summary>
    /// Entity class for the Drivers.
    /// </summary>
    [Table("Vehicle")]
    public class Vehicle : ISyncedEntity {

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(20)]
        public string LicensePlateNumber { get; set; }

    }
}
