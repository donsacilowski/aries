﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;

namespace Aries.Model.Entities {
    [Table("Customer")]
    public class Customer : ISyncedEntity {

        // TODO - Pickup details - Name, address, phone ..
        [ForeignKey("Address")]
        public int? AddressID { get; set; }
        public virtual Address Address { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

    }
}
