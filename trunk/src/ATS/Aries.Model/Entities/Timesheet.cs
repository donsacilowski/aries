﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using Aries.Model.Entities;

namespace Aries.Model.Entities
{
    [Table("Timesheet")]
    public class Timesheet : IEntity
    {
        [ForeignKey("Employee")]
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }        
        public DateTime Date { get; set; }
        public bool EmployeeApproved { get; set; }
        public bool OfficeApproved { get; set; }
        public string Notes { get; set; }
    }

    [Table("TimesheetDetail")]
    public class TimesheetDetail : IEntity
    {
        
        [ForeignKey("Timesheet")]
        public int TimesheetID { get; set; }
        public virtual Timesheet Timesheet { get; set; }
        
        public string ServiceCode { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
    }

    [Table("CreditHour")]
    public class CreditHour : IEntity
    {
        [ForeignKey("Timesheet")]
        public int TimesheetID { get; set; }
        public virtual Timesheet Timesheet { get; set; }

        public float RegularTime  { get; set; }
        public float OverTime { get; set; }
    }

    [Table("HolidayHours")]
    public class HolidayHours : IEntity
    {
        [ForeignKey("Employee")]
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }

        public float Length { get; set; }
    }

    public class TimesheetDetailDisplay 
    {
        public int ID { get; set; }
        public string ServiceCode { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
    }

    public class TimesheetTotal
    {       
        public string TimeCharged { get; set; }
        public string Holiday { get; set; }
        public string Vacation { get; set; }
        public string Bereavement { get; set; }
        public string Total { get; set; }
    }

    public class SummaryData 
    {
        public int EmployeeId { get; set; }
        public string UserId { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string MI { get; set; }
        public DateTime Date { get; set; }
        public bool EmployeeApproved { get; set; }
        public bool OfficeApproved { get; set; }
        public string ServiceCode { get; set; }
        public string Role { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
        // Computed memebers
        public long Minutes 
        {
            get
            {
                return DateAndTime.DateDiff("n", TimeIn, TimeOut);
            }
        }
        public long VacationMinutes
        {
            get
            {

                return (ServiceCode == "Vacation" ? DateAndTime.DateDiff("n", TimeIn, TimeOut) : 0);
            }
        }
        public long HolidayMinutes
        {
            get
            {
                return (ServiceCode == "Holiday" ? DateAndTime.DateDiff("n", TimeIn, TimeOut) : 0);
            }
        }
        public long TimeChargedMinutes
        {
            get
            {
                return (ServiceCode == "Time-Charged" ? DateAndTime.DateDiff("n", TimeIn, TimeOut) : 0);
            }
        }
    }
    public class DailyHours
    {
        public double Worked { get; set; }
        public double Vacation { get; set; }
        public double Bereavement { get; set; }
        public double Holiday { get; set; }
    }
    public class TotalHours
    {
        public double Reg { get; set; }
        public double OT { get; set; }
    }
    public class TimesheetSummary
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public DailyHours Mon { get; set; }
        public DailyHours Tue { get; set; }
        public DailyHours Wed { get; set; }
        public DailyHours Thu { get; set; }
        public DailyHours Fri { get; set; }
        public DailyHours Sat { get; set; }
        public DailyHours Sun { get; set; }
        public TotalHours SubTotal { get; set; }
        public TotalHours Credit { get; set; }
        public TotalHours Total { get; set; }
        public string OfficeApproved { get; set; }
        public string EmployeeApproved { get; set; }
    }
    public class LunchSummary
    {
        public int ID { get; set; }
        public string EmployeeName { get; set; }
        public string LunchStart { get; set; }
        public string LunchEnd { get; set; }
    }
    public class HolidaySummary
    {
        public int ID { get; set; }
        public string EmployeeName { get; set; }
        public string HolidayLength { get; set; }
    }
}
