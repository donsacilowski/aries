﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aries.Model.Entities
{
    [Table("GroupMessage")]
    public class GroupMessage : IEntity
    {
        #region Properties

        [Required]
        [StringLength(2000)]
        public string Message { get; set; }

        [Required]
        [Display(Name = "Group ID")]
        public int GroupId { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }

        #endregion
    }
}
