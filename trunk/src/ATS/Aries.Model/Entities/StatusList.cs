﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Model.Entities {
    [Table("StatusList")]
    public class StatusList : IEntity {

        public string Description { get; set; }
    }
}
