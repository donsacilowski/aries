﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Model.Entities {
    [Table("Status")]
    public class Status : IEntity {

        [ForeignKey("Dispatch")]
        public int DispatchID { get; set; }
        public virtual Dispatch Dispatch { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }

        [ForeignKey("StatusList")]
        public int StatusListID { get; set; }
        public virtual StatusList StatusList { get; set; }
    }
}
