﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Aries.Model.Entities;

namespace Aries.Model.Entities {
    [Table("TimeOffRequest")]
    public class TimeOffRequest : IEntity {

        [ForeignKey("Employee")]
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }
        public DateTime TimeOfRequest { get; set; }
        public DateTime TimeOffRequested { get; set; }
        public double AmountOfTime { get; set; }
        public bool ApprovalStatus { get; set; }
        public string ApprovalState { get; set; }
        public string Comments { get; set; }
    }
    public class TimeoffSummary
    {
        public int ID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime TimeOfRequest { get; set; }
        public DateTime TimeOffRequested { get; set; }
        public double AmountOfTime { get; set; }
        public bool ApprovalStatus { get; set; }
        public string ApprovalState { get; set; }
        public string Comments { get; set; }
    }
}
