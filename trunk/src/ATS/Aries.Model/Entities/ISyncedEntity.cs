﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Aries.Model.Entities {
    public class ISyncedEntity : IEntity {

        public int FC8Id { get; set; }

        public int? MedTransId { get; set; }
    }
}
