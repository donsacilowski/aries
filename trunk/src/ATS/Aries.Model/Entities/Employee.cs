﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Aries.Model.Entities
{
    [Table("Employee")]
    public class Employee : IEntity
    {
        [Required]
        [Display(Name = "User ID")]
        public string UserName { get; set; }
        
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "MI")]
        public string MI { get; set; }
        
        [Required]
        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Mobile App Key")]
        public string MobileAppKey { get; set; }
        
        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        [Display(Name = "Employee Number")]
        public string EmployeeNumber { get; set; }

        [Required]
        public bool DeptCodeOverride { get; set; }
        public string DeptCode { get; set; }

    }

    public class EmployeeView
    {
        [Display(Name = "ID")]
        public string Id { get; set; }
        
        [Display(Name = "User ID")]
        public string UserName { get; set; }
        
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        
        [Display(Name = "MI")]
        public string MI { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Mobile App Key")]
        public string MobileAppKey { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }

        [Display(Name = "Employee Number")]
        public string EmployeeNumber { get; set; }

    }
}
