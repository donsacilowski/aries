﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Aries.Model.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using Aries.Model.Migrations;

namespace Aries.Model {

    public class AriesDataContext : IdentityDbContext<ApplicationUser>
    {

        public AriesDataContext()
            : base("DefaultConnection", throwIfV1Schema: false) {


            Database.SetInitializer<AriesDataContext>(
                new MigrateDatabaseToLatestVersion<AriesDataContext, Configuration>());
        }

        public static AriesDataContext Create()
        {
            return new AriesDataContext();
        }

        public DbSet<Log> Logs { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<CancelReasonList> CancelReasonLists { get; set; }
        public DbSet<Dispatch> Dispatchs { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<StatusList> StatusLists { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Timesheet> Timesheets { get; set; }
        public DbSet<TimesheetDetail> TimesheetDetails { get; set; }
        public DbSet<CreditHour> CreditHours { get; set; }
        public DbSet<TimeOffRequest> TimeOffRequests { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupEmployee> GroupEmployeess { get; set; }
        public DbSet<LocationAlert> LocationAlerts { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<HolidayHours> HolidayHours { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
