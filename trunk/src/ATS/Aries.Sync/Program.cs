﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;

using Microsoft.VisualBasic;
using Newtonsoft.Json;

using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Model.UpdateDB;
using Aries.WebAPI.Models;

using MedTrans;
using Aries.FleetComplete.Services;
using Aries.FleetComplete.Models;
using Aries.FleetComplete.Orders;
using System.IO;
using System.Xml.Linq;
using Google.Api.Maps.Service;
using System.Xml;

namespace Aries.Sync {
    class Program {
        public static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args) {
            log.Info("Sync started");

            try
            {
                MedtransEntities medtransDBtest = new MedtransEntities();
                trans_bridge bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 362).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////bridgetest.DriverName = "Harper, Thaddeus C";
                //////bridgetest.bridge_flag = "RTClosed";

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 398).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////DateTime dt = DateTime.Now.AddHours(1).AddMinutes(30).AddSeconds(0); //new DateTime(2018, 10, 10, 12, 10, 0);
                //////bridgetest.At_pickup_time = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////dt = DateTime.Now.AddHours(2).AddMinutes(45).AddSeconds(0); //new DateTime(2018, 10, 10, 13, 10, 0)
                //////bridgetest.Pickup_street = "190 Fulton Street";
                //////bridgetest.Pickup_city = "Buffalo";
                //////bridgetest.Pickup_zip = "14215";
                //////bridgetest.DropOff_city = "Buffalo";
                //////bridgetest.DropOff_street = "1461 Kensington Ave. #1";
                //////bridgetest.DoneTime = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////bridgetest.DriverName = "Harper, Thaddeus C";
                //////bridgetest.bridge_flag = "ClosedAttested";
                //////bridgetest.trip_base_amt = (decimal)50.12;
                //////bridgetest.trip_mile_amt = (decimal)8.00;

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 199).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////dt = DateTime.Now.AddHours(1).AddMinutes(30).AddSeconds(1); //new DateTime(2018, 10, 10, 12, 10, 1);
                //////bridgetest.At_pickup_time = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////dt = DateTime.Now.AddHours(3).AddMinutes(12).AddSeconds(2); //new DateTime(2018, 10, 10, 14, 12, 0);
                //////bridgetest.Pickup_street = "190 Fulton Street";
                //////bridgetest.Pickup_city = "Buffalo";
                //////bridgetest.Pickup_zip = "14215";
                //////bridgetest.DoneTime = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////bridgetest.DriverName = "Harper, Thaddeus C";
                //////bridgetest.bridge_flag = "ClosedAttested";
                //////bridgetest.trip_base_amt = (decimal)40.00;
                //////bridgetest.trip_mile_amt = (decimal)8.36;

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 14).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////bridgetest.DriverName = "Cothran, Sam";
                //////bridgetest.bridge_flag = "RTClosed";

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 400).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////dt = DateTime.Now.AddHours(1).AddMinutes(10).AddSeconds(0); //new DateTime(2018, 10, 10, 12, 10, 0);
                //////bridgetest.At_pickup_time = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////dt = DateTime.Now.AddHours(3).AddMinutes(12).AddSeconds(0); //new DateTime(2018, 10, 10, 14, 12, 0);
                //////bridgetest.DoneTime = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////bridgetest.unit_n = 14;
                //////bridgetest.DriverName = "Cothran, Sam";
                //////bridgetest.bridge_flag = "canceldispatch";
                //////bridgetest.trip_base_amt = (decimal)5.34;
                //////bridgetest.trip_mile_amt = (decimal)7.00;

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 402).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////bridgetest.unit_n = 14;
                //////dt = DateTime.Now.AddHours(1).AddMinutes(34).AddSeconds(15); // new DateTime(2018, 10, 10, 11, 34, 15);
                //////bridgetest.At_pickup_time = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////dt = DateTime.Now.AddHours(4).AddMinutes(12).AddSeconds(1); // new DateTime(2018, 10, 10, 14, 12, 1);
                //////bridgetest.DoneTime = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////bridgetest.DropOff_street = "462 Grider St.";
                //////bridgetest.DropOff_city = "Buffalo";
                //////bridgetest.Pickup_street = "527 22nd Street";
                //////bridgetest.Pickup_city = "Niagara Falls";
                //////bridgetest.Pickup_zip = "14305";
                //////bridgetest.DriverName = "Cothran, Sam";
                //////bridgetest.bridge_flag = "canceldispatch";
                //////bridgetest.trip_base_amt = (decimal)22.22;
                //////bridgetest.trip_mile_amt = (decimal)3.12;

                //////bridgetest = medtransDBtest.trans_bridge.Where(p => p.trans_id == 399).FirstOrDefault();
                //////bridgetest.serv_date = DateTime.Now;
                //////bridgetest.unit_n = 14;
                //////dt = DateTime.Now.AddHours(0).AddMinutes(10).AddSeconds(0); // new DateTime(2018, 10, 10, 10, 00, 00);
                //////bridgetest.At_pickup_time = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////dt = DateTime.Now.AddHours(4).AddMinutes(12).AddSeconds(2); // new DateTime(2018, 10, 10, 14, 12, 1);
                //////bridgetest.DropOff_street = "462 Grider St.";
                //////bridgetest.DropOff_city = "Buffalo";
                //////bridgetest.Pickup_city = "Buffalo";
                //////bridgetest.Pickup_zip = "14215";
                //////bridgetest.DriverName = "Cothran, Sam";
                //////bridgetest.DoneTime = Convert.ToDateTime(dt.ToString("hh:mm:ss tt"));
                //////bridgetest.bridge_flag = "canceldispatch";
                //////bridgetest.trip_base_amt = (decimal)11.11;
                //////bridgetest.trip_mile_amt = (decimal)2.34;

                //////medtransDBtest.SaveChanges();


                //Employee emp111 = (new EmployeeRepository().GetByRegKey("99", "Leon C Wiggins"));
                //Employee emp111 = (new EmployeeRepository().GetByRegKey("99", "Leon C Wiggins"));

                //DateTime dt = new DateTime(2018, 8,28, 7, 19, 0);
                //(new StatusRepository()).Add(37, dt, (int)Statuses.ClosedCancelClient);
                if (args[0] == "newOrders")
                {                    
                    MedtransEntities medtransDB = new MedtransEntities();
                    List<trans_bridge> tripTable = null;                    

                    log.Info("After MedTrans objects");

                    DateTime today1 = DateTime.Now.Date;
                    DateTime tomorrow1 = DateTime.Now.Date.AddDays(1);

                    /* Look for any of todays trips that are closed, but not closed in MedTrans and close them */
                    List<Dispatch> notClosedMedtrans = new List<Dispatch>();
                    DispatchRepository dRepo = new DispatchRepository();

                    int numClosedAttested = 0;
                    int numClosedNonAttested = 0;
                    int numCancelDispatch = 0;
                    int cancelClient = 0;

                    List<trans_bridge> rtSent = medtransDB.trans_bridge.Where(
                           p => ((p.bridge_flag.ToLower().Equals("rtsent") ||
                                 p.bridge_flag.ToLower().Equals("rtcancelclient"))) &&
                                 p.unit_n == 0
                               &&
                                 p.serv_date >= today1 && p.serv_date < tomorrow1).ToList();

                    log.Error("RT sent or RT cancelclient trips = " + rtSent.Count.ToString());

                    foreach (trans_bridge bridge in rtSent)
                    {
                        log.Error("Processing route trip bridge = " + bridge.trans_id.ToString());

                        //get number of trips in this route in Medtrans that are cancel dispatch
                        numCancelDispatch = medtransDB.trans_bridge.Where(
                               p => (p.unit_n == bridge.trans_id) &&
                               (p.bridge_flag.ToLower().Equals("canceldispatch"))).Count();

                        //get all the trips from SQL database which is in the route
                        List<trans_bridge> rtTrips = medtransDB.trans_bridge.Where(
                               p => (p.unit_n == bridge.trans_id)).ToList();

                        numClosedAttested = 0;
                        numClosedNonAttested = 0;
                        numCancelDispatch = 0;
                        cancelClient = 0;

                        foreach (trans_bridge brTrip in rtTrips)
                        {
                            log.Error("Processing route trip bridge = " + rtTrips.Count.ToString() + " " + brTrip.trans_id.ToString());

                            Dispatch disp = dRepo.GetByMedtransID(brTrip.trans_id);

                            if (disp == null) break;

                            StatusRepository statusRepo = new StatusRepository();
                            Status closedAttested = statusRepo.GetLatestStatus(disp.Id);
                            
                            if (closedAttested.StatusListID == (int)Statuses.ClosedAttested)
                            {
                                numClosedAttested++;
                                log.Error("Closed attested for trip bridge = " + bridge.trans_id.ToString());
                            }

                            Status closedNonAttested = statusRepo.GetLatestStatus(disp.Id);
                            if (closedNonAttested.StatusListID == (int)Statuses.ClosedCancelClient)
                            {
                                cancelClient++;
                                log.Error("Closed Cancel Client for trip bridge = " + bridge.trans_id.ToString());
                            }

                           if (closedNonAttested.StatusListID == (int)Statuses.ClosedCancelDispatch)
                            {
                                numClosedNonAttested++;
                                log.Error("Closed Cancel Dispatch for trip bridge = " + bridge.trans_id.ToString());
                            }

                            if (closedNonAttested.StatusListID == (int)Statuses.ClosedCancelReset)
                            {
                                numClosedNonAttested++;
                                log.Error("Closed Cancel Reset for trip bridge = " + bridge.trans_id.ToString());
                            }

                            if (closedNonAttested.StatusListID == (int)Statuses.ClosedCancelUnitCh)
                            {
                                numClosedNonAttested++;
                                log.Error("Closed Cancel Unit Ch for trip bridge = " + bridge.trans_id.ToString());
                            }

                            if (closedNonAttested.StatusListID == (int)Statuses.Closed)
                            {
                                numClosedNonAttested++;
                                log.Error("Closed for trip bridge = " + bridge.trans_id.ToString());
                            }

                            //cancelClient = medtransDB.trans_bridge.Where(
                            //   p => (p.unit_n == bridge.trans_id) &&
                            //   (p.bridge_flag.ToLower().Equals("cancelclient"))).Count();                           
                        }
                        //int rtAssignedNonClosedTrips = medtransDB.trans_bridge.Where(
                        //    p => (p.unit_n == bridge.trans_id) &&
                        //    (p.bridge_flag.ToLower().Equals("closedattested") &&
                        //     p.bridge_flag.ToLower().Equals("cancelclient") ) ).Count();

                        log.Error("Num Closed Attested = " + numClosedAttested.ToString() +
                                  " Num Closed Non Attested = " + numClosedNonAttested.ToString() +
                                  " Num Closed Cancel Client = " + cancelClient.ToString()
                                  );
                        if ((numClosedAttested + numClosedNonAttested) == rtTrips.Count())
                        {
                            bridge.bridge_flag = "RTClosed";
                            log.Error("RTClosed for trip bridge = " + bridge.trans_id.ToString());
                            bridge.DoneTime = DateTime.Now;
                            medtransDB.SaveChanges();
                        }

                        if (cancelClient > 0)
                        {
                            bridge.bridge_flag = "RTCancelClient";
                            log.Error("RTCancelClient for trip bridge = " + bridge.trans_id.ToString());
                            medtransDB.SaveChanges();
                        }

                        //log.Error("Non Closed Trips in Rt Assigned " + bridge.trans_id + " " + rtAssignedNonClosedTrips.ToString());
                    }

                    //DateTime frDate = DateTime.Now.Date;

                    //notClosedMedtrans = (new DispatchRepository()).GetAllByStatusDescription("Closed")
                    //                    .Where(d => d.PickupTime >= frDate).ToList();

                    //log.Debug("After All todays closed = " + notClosedMedtrans.Count);
                    //trans_bridge tb = null;
                    //foreach (Dispatch d in notClosedMedtrans) {
                    //    tb = medtransDB.trans_bridge.Where(m => m.trans_id == d.MedTransId && m.bridge_flag.ToLower() != "closed"
                    //                                                                       && m.bridge_flag.ToLower() != "cancelclient"
                    //                                                                       && m.bridge_flag.ToLower() != "canceldispatch")
                    //                                                                       .FirstOrDefault();
                    //    if (tb != null) {
                    //        tb.bridge_flag = "Closed";
                    //        log.Debug("Force Medtrans close = " + d.Id);
                    //    }
                    //}
                    //medtransDB.SaveChanges();
                    // log.Debug("After Medtrans closed checked");

                    try
                    {
                        DateTime tomorrow = DateTime.Now.Date.AddDays(1);
                        DateTime today = DateTime.Now.Date;
                        /* Get a list of all new orders from MedTrans */
                        tripTable = medtransDB.trans_bridge.Where(
                            (p => (p.bridge_flag.ToLower().Equals("assigned")
                            || p.bridge_flag.ToLower().Equals("canceldispatch")
                            || p.bridge_flag.ToLower().Equals("cancelreset")
                            || p.bridge_flag.ToLower().Equals("cancelunitch")
                            || p.bridge_flag.ToLower().Equals("cancelacknowledged")
                            ) &&
                            p.serv_date >= today)).ToList();

                        log.Info("After MedTrans trip table entries " + tripTable.Count.ToString());
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                    int processedTrips = 0;

                    foreach (trans_bridge bridge in tripTable)
                    {
                        bool orderSaved = false;
                        Dispatch newDispatch = new Dispatch();
                        Dispatch disp = null;
                        DispatchRepository dispatchRepo = new DispatchRepository();

                        log.Info("Processing trip start " + bridge.trans_id.ToString());

                        try
                        {
                            if (!bridge.DriverName.Contains(",")) { 
                                continue;
                            }

                            if (bridge.serv_date < DateTime.Now.Date) {
                                continue;
                            }

                            Employee emp = (new EmployeeRepository().GetEmployeeByBridgeName(bridge.DriverName));
                            if (emp == null || String.IsNullOrEmpty(emp.MobileAppKey)) {
                                continue;
                            }

                            processedTrips++;

                           // log.Info("Processing trip " + bridge.trans_id.ToString());
                            UpdateDB updateDB = new UpdateDB();

                            newDispatch.Timestamp = DateTime.Now;
                            newDispatch.NeedSignature = false;

                            int customerID = updateDB.UpdateCustomer(bridge);
                            int driverID = updateDB.UpdateDriver(bridge);
                            
                            PaymentRepository payRepo = new PaymentRepository();
                            int payID = 0;
                            if (bridge.PaymentToDriver != null)
                            {
                                if (bridge.PaymentToDriver.ToLower().Equals("1") ||
                                    bridge.PaymentToDriver.ToLower().Equals("2"))
                                {
                                    payID = updateDB.AddPayment(bridge);
                                    newDispatch.PaymentID = payID;
                                }
                            }

                            newDispatch.NeedSignature = false;
                            if (bridge.NeedSignature.ToLower().Equals("1") ||
                                bridge.NeedSignature.ToLower().Equals("2")) {

                                newDispatch.NeedSignature = true;
                            }

                            int pickUpID = updateDB.UpdatePickup(bridge);
                            int dropOffID = updateDB.UpdateDropOff(bridge);

                            DateTime puDate = (DateTime)bridge.serv_date;
                            DateTime puTime = (DateTime)bridge.PickupTime;

                            if (bridge.AppointTime != null)
                            {
                                DateTime doTime = (DateTime)bridge.AppointTime;
                                DateTime fullDODate = puDate.AddHours(doTime.Hour).AddMinutes(doTime.Minute).AddSeconds(doTime.Second);
                                newDispatch.DropoffTime = fullDODate;
                            }

                            DateTime fullPUDate = puDate.AddHours(puTime.Hour).AddMinutes(puTime.Minute).AddSeconds(puTime.Second);
                            newDispatch.PickupTime = fullPUDate;

                            newDispatch.DriverID = driverID;
                            newDispatch.DropoffAddressID = dropOffID;
                            newDispatch.PickupAddressID = pickUpID;
                            newDispatch.CustomerID = customerID;

                            newDispatch.Service = bridge.ServiceName;
                            newDispatch.ServiceType = bridge.ServiceType;
                            
                            /* TODO: Might have to change based on what the data is in Medtrans */
                            if (bridge.DNLAIndication != null)
                            {
                                newDispatch.DNLA = bridge.DNLAIndication.ToLower().Equals("y") ? true : false;
                            }
                            newDispatch.MedTransId = bridge.trans_id;

                            newDispatch.Notes = bridge.TripNotes;
                            newDispatch.PickupFullAddress = bridge.origin_ll;
                            newDispatch.DropoffFullAddress = bridge.destin_ll;

                            StatusRepository statusRepo = new StatusRepository();

                            int dispID = 0;
                            Dispatch existingDisp = dispatchRepo.GetByMedtransID(bridge.trans_id);
                            if (existingDisp == null)
                            {
                                dispatchRepo.Add(newDispatch);
                                orderSaved = true;
                                dispID = newDispatch.Id;

                                Status newStatus = new Status();
                                newStatus.DispatchID = dispID;
                                newStatus.Timestamp = DateTime.Now;
                                
                                newStatus.StatusListID = (int)Statuses.Assigned;
                                statusRepo.Add(newStatus);
                            }
                            else
                            {
                                existingDisp.PickupTime = newDispatch.PickupTime;
                                existingDisp.Timestamp = newDispatch.Timestamp;
                                existingDisp.PaymentID = newDispatch.PaymentID;
                                existingDisp.DriverID = newDispatch.DriverID;

                                dispatchRepo.Update(existingDisp);
                                dispID = existingDisp.Id;
                            }

                            DispatchRepository dispRepo = new DispatchRepository();

                            /* Load the just created dispatch so the virtual classes get loaded */
                            disp = dispRepo.GetByID(dispID);

                            /* Using the dispatch record just saved, create a FC order and retrieve the order number,
                             * add it to the dispatch 
                             */
                            dispatchRepo.Update(disp);
                            orderSaved = true;

                            bool canDispExists = false;

                            if (bridge.bridge_flag.ToLower().Equals("cancelacknowledged"))
                            {
                                Status s = statusRepo.GetLatestStatus(dispID);
                                
                                Status closedStatus = new Status();
                                closedStatus.DispatchID = dispID;
                                closedStatus.Timestamp = DateTime.Now;
                                closedStatus.StatusListID = (int)Statuses.ClosedCancelDispatch;

                                statusRepo.Add(closedStatus);

                                bridge.bridge_flag = "Closed";
                            }
                            
                            if (bridge.bridge_flag.ToLower().Equals("canceldispatch") ||
                                bridge.bridge_flag.ToLower().Equals("cancelreset") ||
                                bridge.bridge_flag.ToLower().Equals("cancelunitch") ||
                                bridge.bridge_flag.ToLower().Equals("cancelacknowledged"))
                            {
                                ////Status s = statusRepo.GetLatestStatus(dispID);
                                //////log.Debug("Cancel Disp " + dispID.ToString() + " " + s.StatusListID.ToString());

                                ////if (s.StatusListID == (int)Statuses.ClosedCancelDispatch || 
                                ////    s.StatusListID == (int)Statuses.ClosedCancelReset ||
                                ////    s.StatusListID == (int)Statuses.ClosedCancelUnitCh ||
                                ////    s.StatusListID == (int)Statuses.ClosedCancelClient ||
                                ////    s.StatusListID == (int)Statuses.CancelDispatch
                                ////    )
                                ////{
                                ////    canDispExists = true;
                                ////    //log.Debug("Cancel Disp exists " + dispID.ToString());
                                ////}
                                canDispExists = statusRepo.TripHasCancelDispatch(dispID);
                            }

                            if (bridge.bridge_flag.ToLower().Equals("cancelreset"))
                            {
                                Status s = statusRepo.GetLatestStatus(dispID);
                                Status sCanClient = statusRepo.GetStatusByDispatchID(dispID, Statuses.ClosedCancelReset);

                                if (s.StatusListID != (int)Statuses.ClosedCancelReset)
                                {
                                    Status newStatus = new Status();
                                    newStatus.DispatchID = dispID;
                                    newStatus.Timestamp = DateTime.Now;

                                    newStatus.StatusListID = (int)Statuses.ClosedCancelReset;
                                    //newStatus.StatusListID = (int)Statuses.CancelReset;
                                    statusRepo.Add(newStatus);
                                }                                                                
                            }

                            if (bridge.bridge_flag.ToLower().Equals("cancelunitch"))
                            {
                                Status s = statusRepo.GetLatestStatus(dispID);
                                Status sCanClient = statusRepo.GetStatusByDispatchID(dispID, Statuses.ClosedCancelUnitCh);

                                if (s.StatusListID != (int)Statuses.ClosedCancelUnitCh)
                                {
                                    Status newStatus = new Status();
                                    newStatus.DispatchID = dispID;
                                    newStatus.Timestamp = DateTime.Now;

                                    newStatus.StatusListID = (int)Statuses.ClosedCancelUnitCh;
                                    //newStatus.StatusListID = (int)Statuses.CancelReset;
                                    statusRepo.Add(newStatus);
                                }
                            }
                            if (bridge.bridge_flag.ToLower().Equals("canceldispatch"))
                            {
                                Status s = statusRepo.GetLatestStatus(dispID);
                                Status sCanClient = statusRepo.GetStatusByDispatchID(dispID, Statuses.ClosedCancelDispatch);

                                if (s.StatusListID != (int)Statuses.ClosedCancelDispatch &&
                                    sCanClient == null)
                                {
                                    Status cancelStatus = new Status();
                                    cancelStatus.DispatchID = dispID;
                                    cancelStatus.Timestamp = DateTime.Now;
                                    cancelStatus.StatusListID = (int)Statuses.ClosedCancelDispatch;
                                    // cancelStatus.StatusListID = (int)Statuses.CancelDispatch;

                                    statusRepo.Add(cancelStatus);
                                }
                            }
                            log.Info("Before trip send " + bridge.trans_id.ToString());

                            if (disp.PickupTime.Date >= DateTime.Now.Date && !canDispExists)
                            {
                                HttpClient client = new HttpClient();

                                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServerURI"].ToString());

                                emp = (new EmployeeRepository().GetEmployeeByBridgeName(bridge.DriverName));
                                if (emp != null && !String.IsNullOrEmpty(emp.MobileAppKey))
                                {
                                    var response = client.GetAsync("api/trip/SendNotice?key=" + (new EmployeeRepository().GetEmployeeByBridgeName(bridge.DriverName).MobileAppKey) +
                                                                    "&messageType=" + MobileMessageTypes.NewOrders).Result;
                                    log.Info("AfterGetAsync");
                                    HttpContent content = response.Content;

                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    string res = result.Result;
                                    log.Info("After Reading resonse string");

                                    ErrorCode resOK;
                                    Enum.TryParse<ErrorCode>(ErrorCode.OK.ToString(), out resOK);

                                    Status newStatus = new Status();
                                    newStatus.DispatchID = dispID;
                                    newStatus.Timestamp = DateTime.Now;

                                    if (response.StatusCode == System.Net.HttpStatusCode.OK && resOK == ErrorCode.OK)
                                    {
                                        log.Info("After Reading resonse string Successful");
                                        if (bridge.bridge_flag.ToLower().Equals("assigned"))
                                        {
                                            newStatus.StatusListID = (int)Statuses.Dispatched;
                                            statusRepo.Add(newStatus);

                                            bridge.bridge_flag = "dispatched";
                                        }
                                    }
                                    else
                                    {
                                        log.Info("After Reading resonse string Unsuccessful = " + response.StatusCode.ToString());
                                    }
                                    
                                    if (bridge.bridge_flag.ToLower().Equals("cancelreset"))
                                    {
                                        Status s = statusRepo.GetLatestStatus(dispID);

                                        if (s.StatusListID != (int)Statuses.ClosedCancelReset)
                                        {
                                            Status cancelStatus = new Status();
                                            cancelStatus.DispatchID = dispID;
                                            cancelStatus.Timestamp = DateTime.Now;
                                            cancelStatus.StatusListID = (int)Statuses.ClosedCancelReset;
                                           // cancelStatus.StatusListID = (int)Statuses.CancelDispatch;

                                            statusRepo.Add(cancelStatus);
                                        }
                                    }
                                    else if (bridge.bridge_flag.ToLower().Equals("cancelunitch"))
                                    {
                                        Status s = statusRepo.GetLatestStatus(dispID);

                                        if (s.StatusListID != (int)Statuses.ClosedCancelUnitCh)
                                        {
                                            Status cancelStatus = new Status();
                                            cancelStatus.DispatchID = dispID;
                                            cancelStatus.Timestamp = DateTime.Now;
                                            cancelStatus.StatusListID = (int)Statuses.ClosedCancelUnitCh;
                                            //cancelStatus.StatusListID = (int)Statuses.CancelDispatch;

                                            statusRepo.Add(cancelStatus);
                                        }
                                    }
                                    
                                    else if (bridge.bridge_flag.ToLower().Equals("droptrip"))
                                    {
                                        newStatus.StatusListID = (int)Statuses.CancelDispatch;
                                        statusRepo.Add(newStatus);
                                    }                                   
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Status badStatus = new Status();
                            badStatus.Timestamp = DateTime.Now;
                            badStatus.StatusListID = (int)Statuses.BadOrder;

                            if (orderSaved)
                            {
                                badStatus.DispatchID = disp.Id;                               
                                dispatchRepo.Update(disp);
                            } else {
                                
                                dispatchRepo.Add(newDispatch);
                                badStatus.DispatchID = newDispatch.Id;
                            }
                            (new StatusRepository()).Add((badStatus));
                            log.Error("Within loop Medtrans ID = " + bridge.trans_id.ToString() + " " + ex);
                        }
                        //log.Info("before medtrans save");

                        medtransDB.SaveChanges();
                       // log.Info("after medtrans save");
                    }
                    //log.Error("End Sync Processed Trips = " + processedTrips.ToString());
                    tripTable = medtransDB.trans_bridge.Where(
                            p => (p.bridge_flag.ToLower().Equals("dispatched"))
                             &&
                            p.serv_date >= today1 && p.serv_date < tomorrow1).ToList();

                    foreach (trans_bridge bridge in tripTable)
                    {
                        //foreach (Status s in dispatchToday)
                        //{
                        Dispatch disp = (new DispatchRepository()).GetByMedtransID(bridge.trans_id);
                        long minutes = DateTime.Now.Subtract(disp.Timestamp).Minutes;
                        
                        if (minutes >= 15)
                        {
                            //log.Error("Processing Late Trip = " + disp.Id.ToString());

                            //trans_bridge trip = medtransDB.trans_bridge.Where(m => m.trans_id == disp.MedTransId).FirstOrDefault();

                            if (!bridge.bridge_flag.Equals(".Late30Minutes") && !bridge.bridge_flag.Equals(".Late15Minutes"))
                            {
                                if (minutes > 30)
                                {
                                    bridge.bridge_flag = "Late30Minutes";
                                }
                                else
                                {
                                    bridge.bridge_flag = "Late15Minutes";
                                }
                                medtransDB.SaveChanges();
                            }
                        }
                    }
                    log.Error("End Sync Processed Trips = " + processedTrips.ToString());

                    List<trans_bridge> rtAssigned = medtransDB.trans_bridge.Where(
                            p => (p.bridge_flag.ToLower().Equals("rt assigned") &&
                            p.unit_n == 0)
                             &&
                            p.serv_date >= today1 && p.serv_date < tomorrow1).ToList();

                    log.Error("Rt Assigned Number " + rtAssigned.Count.ToString());

                    foreach (trans_bridge bridge in rtAssigned)
                    {
                        int rtNumberOfTrips = medtransDB.trans_bridge.Where(
                               p => (p.unit_n == bridge.trans_id)).Count();

                        int rtAcceptedTrips = medtransDB.trans_bridge.Where(
                               p => (p.unit_n == bridge.trans_id) &&
                               (!p.bridge_flag.ToLower().Equals("assigned") &&
                                !p.bridge_flag.ToLower().Equals("dispatched"))).Count();

                        if (rtAcceptedTrips == rtNumberOfTrips)
                        {
                            bridge.bridge_flag = "RTSent"; // "RTAccepted";
                            bridge.OnTheWay = DateTime.Now;
                            medtransDB.SaveChanges();
                        }
                    }

                    //////numClosedAttested = 0;
                    //////numClosedNonAttested = 0;
                    //////numCancelDispatch = 0;
                    //////cancelClient = 0;

                    //////rtSent = medtransDB.trans_bridge.Where(
                    //////       p => ((p.bridge_flag.ToLower().Equals("rtsent") ||
                    //////             p.bridge_flag.ToLower().Equals("rtcancelclient"))) &&
                    //////             p.unit_n == 0
                    //////           &&
                    //////             p.serv_date >= today1 && p.serv_date < tomorrow1).ToList();

                    //////foreach (trans_bridge bridge in rtSent)
                    //////{                       
                    //////     //get number of trips in this route in Medtrans that are cancel dispatch
                    //////    numCancelDispatch = medtransDB.trans_bridge.Where(
                    //////           p => (p.unit_n == bridge.trans_id) &&
                    //////           (p.bridge_flag.ToLower().Equals("canceldispatch"))).Count();

                    //////    //get all the trips from SQL database which is in the route
                    //////    List<trans_bridge> rtTrips = medtransDB.trans_bridge.Where(
                    //////           p => (p.unit_n == bridge.trans_id)).ToList();

                    //////    numClosedAttested = 0;
                    //////    numClosedNonAttested = 0;
                    //////    numCancelDispatch = 0;
                    //////    cancelClient = 0;

                    //////    foreach (trans_bridge brTrip in rtTrips)
                    //////    {
                    //////        Dispatch disp = dRepo.GetByMedtransID(brTrip.trans_id);

                    //////        StatusRepository statusRepo = new StatusRepository();
                    //////        Status closedAttested = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.ClosedAttested);

                    //////        if (closedAttested != null) numClosedAttested++;

                    //////        Status closedNonAttested = statusRepo.GetLatestStatus(disp.Id);
                    //////        if (closedNonAttested.StatusListID == (int)Statuses.OperatorCancelled)
                    //////        {
                    //////            cancelClient++;
                    //////        }
                    //////        else if (closedNonAttested.StatusListID == (int)Statuses.ClosedCancelClient)
                    //////        {
                    //////            numClosedNonAttested++;
                    //////        }

                    //////        closedNonAttested = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.ClosedCancelDispatch);
                    //////        if (closedNonAttested != null) numClosedNonAttested++;

                    //////        closedNonAttested = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.ClosedCancelReset);
                    //////        if (closedNonAttested != null) numClosedNonAttested++;

                    //////        closedNonAttested = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.ClosedCancelUnitCh);
                    //////        if (closedNonAttested != null) numClosedNonAttested++;

                    //////        closedNonAttested = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.Closed);
                    //////        if (closedNonAttested != null) numClosedNonAttested++;

                    //////        //cancelClient = medtransDB.trans_bridge.Where(
                    //////        //   p => (p.unit_n == bridge.trans_id) &&
                    //////        //   (p.bridge_flag.ToLower().Equals("cancelclient"))).Count();                           
                    //////    }
                    //////    //int rtAssignedNonClosedTrips = medtransDB.trans_bridge.Where(
                    //////    //    p => (p.unit_n == bridge.trans_id) &&
                    //////    //    (p.bridge_flag.ToLower().Equals("closedattested") &&
                    //////    //     p.bridge_flag.ToLower().Equals("cancelclient") ) ).Count();

                    //////    if ((numClosedAttested + numClosedNonAttested) == rtTrips.Count()) {
                    //////        bridge.bridge_flag = "RTClosed";
                    //////        bridge.DoneTime = DateTime.Now;
                    //////        medtransDB.SaveChanges();
                    //////    } 

                    //////    if (cancelClient > 0)
                    //////    {
                    //////        bridge.bridge_flag = "RTCancelClient";
                    //////        medtransDB.SaveChanges();
                    //////    }
                        
                    //////    //log.Error("Non Closed Trips in Rt Assigned " + bridge.trans_id + " " + rtAssignedNonClosedTrips.ToString());
                    //////}                    
                }
                else if (args[0] == "logoffCheck")
                {
                    TimesheetRepository tsDetailRepo = new TimesheetRepository();

                    EmployeeRepository empRepo = new EmployeeRepository();
                    List<Employee> empList = empRepo.GetAll();
                    foreach (Employee emp in empList)
                    {
                        tsDetailRepo.RemoveDuplicateTimesheets(emp.Id, DateTime.Today.Date);
                    }
                    List<TimesheetDetail> tsDetails = (new TimesheetRepository()).GetAllTodaysDetails();
                    log.Info("In Log Off Check");
                    (new TimesheetRepository()).SignInViaMobileApp(1, DateTime.Now, "14043");
                    foreach (TimesheetDetail td in tsDetails)
                    {
                        TimeSpan diff1 = DateTime.Now.Subtract(td.TimeOut);
                        if ((diff1.TotalMinutes > 120) &&
                            (td.TimeIn != td.TimeOut) &&
                            (td.ServiceCode != "Logged Out"))
                        {
                            Timesheet ts = (new TimesheetRepository()).GetByID(td.TimesheetID);
                            Employee emp = (new EmployeeRepository()).GetByID(ts.EmployeeID);

                            if (emp.Role.Equals("Driver"))
                            {
                                (new TimesheetRepository()).AddNewEvent
                                    (emp.Id, DateTime.Now, DateTime.Now, DateTime.Now, "Logged Out");
                                //(new TimesheetRepository()).SignInViaMobileApp(emp.Id, DateTime.Now);
                                if (emp != null)
                                {
                                    HttpClient client = new HttpClient();
                                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServerURI"].ToString());

                                    string message = "You were logged out at " + DateTime.Now.ToShortTimeString();
                                    var response = client.GetAsync("api/trip/SendNotice?key=" + emp.MobileAppKey +
                                                                    "&messageType=" + MobileMessageTypes.LogOff +
                                                                    "&message=" + message).Result;
                                    log.Info("AfterGetAsync");
                                    HttpContent content = response.Content;

                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    string res = result.Result;
                                    log.Info("After Reading resonse string");

                                    ErrorCode resOK;
                                    Enum.TryParse<ErrorCode>(ErrorCode.OK.ToString(), out resOK);

                                    if (response.StatusCode == System.Net.HttpStatusCode.OK && resOK == ErrorCode.OK)
                                    {
                                        log.Info("After Reading resonse string Successful");
                                    }
                                    else
                                    {
                                        log.Info("After Reading resonse string Unsuccessful");
                                    }
                                }
                            }
                        }
                    }
                }
                else if (args[0] == "routes")
                {
                    //MedtransEntities medtransDB = new MedtransEntities();
                    //DispatchRepository dispRepo = new DispatchRepository();
                    //StatusRepository statusRepo = new StatusRepository();
                    //DriverRepository driverRepo = new DriverRepository();

                    //log.Info("Processing Routes");                   

                    //List<trans_bridge> rtClosed = medtransDB.trans_bridge.Where(
                    //       p => ((p.bridge_flag.ToLower().Equals("billed"))) &&
                    //             p.unit_n == 0).ToList();

                    //foreach (trans_bridge bridge in rtClosed)
                    //{
                    //    log.Error("Billed Routes for " + bridge.AppointTime + "  = " + rtClosed.Count.ToString());

                    //    //Only process the route if this MedtransID is not in the Route table
                    //    Route route = dispRepo.GetRoute(bridge.trans_id);

                    //    var names = bridge.DriverName.Split(',');
                    //    string dName = names[1].Trim() + ' ' + names[0].Trim();
                    //    int routeDriver = driverRepo.GetByName(dName).Id;

                    //    if (route == null)
                    //    {
                    //        Route newRoute = new Route();
                    //        newRoute.Name = bridge.ClientName;
                    //        newRoute.MedTransId = bridge.trans_id;
                    //        newRoute.DriverID = routeDriver;
                    //        newRoute.Status = "No Status";
                    //        newRoute.Revenue = 0.0;

                    //        newRoute.TotalTime = new TimeSpan();
                    //        newRoute.Timestamp = DateTime.Now;

                    //        dispRepo.SaveRoute(newRoute);
                    //        //route.Revenue = bridge.???;
                    //    }

                    //    route = dispRepo.GetRoute(bridge.trans_id);
                    //    //Add the Medtrans ID and Cost the the Routes table.    
                    //    route.MedTransId = bridge.trans_id;
                    //    route.DriverID = routeDriver;
                    //    //route.Revenue = bridge.???;

                    //    //add the closedcancelclient
                    //    List<trans_bridge> rtTrips = medtransDB.trans_bridge.Where(
                    //          p => (p.unit_n == bridge.trans_id) &&
                    //           p.bridge_flag.ToLower().Equals("closedattested")).OrderBy(p => p.At_pickup_time).ToList();

                    //    string tripString = "";
                    //    bool firstWaypoint = true;
                    //    bool isPickupRoute = false;
                    //    bool isDropoffRoute = false;

                    //    foreach (trans_bridge closedTrip in rtTrips)
                    //    {
                    //        //Was there a cancel dispatch in this trip
                    //        Dispatch disp = dispRepo.GetByMedtransID(closedTrip.trans_id);
                    //        Status sta = statusRepo.GetStatusByDispatchID(disp.Id, Statuses.CancelDispatch);

                    //        // need at least 2 trips to do the route calculations and no cancel clients in the dispatch history                         
                    //        if (rtTrips.Count >= 2 && sta == null)
                    //        {

                    //            if (firstWaypoint)
                    //            {
                    //                trans_bridge firstPickup = rtTrips.OrderBy(t => t.At_pickup_time).FirstOrDefault();
                    //                trans_bridge lastDropoff = rtTrips.OrderByDescending(t => t.DoneTime).FirstOrDefault();

                    //                TimeSpan routeTime = Convert.ToDateTime(lastDropoff.DoneTime).Subtract(Convert.ToDateTime(firstPickup.At_pickup_time));
                    //                log.Error("Route Time = " + closedTrip.trans_id.ToString() + " " + routeTime.ToString());

                    //                //Save the routetime to the Route database table.
                    //                route.TotalTime = routeTime;

                    //                tripString = firstPickup.Pickup_street + ", " +
                    //                                    firstPickup.Pickup_city + ", " +
                    //                                    firstPickup.Pickup_state + " " +
                    //                                    firstPickup.Pickup_zip.Trim() +
                    //                                    "&destination=" +
                    //                                    lastDropoff.DropOff_street + ", " +
                    //                                    lastDropoff.DropOff_city + ", " +
                    //                                    lastDropoff.DropOff_state + " " +
                    //                                    lastDropoff.DropOff_zip.Trim();

                    //                isPickupRoute = rtTrips[0].Pickup_street == rtTrips[1].Pickup_street ? true : false;
                    //                isDropoffRoute = rtTrips[0].DropOff_street == rtTrips[1].DropOff_street ? true : false;
                    //                log.Error("pickup route = " + closedTrip.trans_id.ToString() + " " + isPickupRoute.ToString());
                    //                log.Error("dropoff route = " + closedTrip.trans_id.ToString() + " " + isDropoffRoute.ToString());
                    //            }

                    //            if (isPickupRoute)
                    //            {
                    //                if (firstWaypoint)
                    //                {
                    //                    tripString += "&waypoints=via:" +
                    //                                    closedTrip.DropOff_street + ", " +
                    //                                    closedTrip.DropOff_city + ", " +
                    //                                    closedTrip.DropOff_state + " " +
                    //                                    closedTrip.DropOff_zip.Trim();
                    //                    firstWaypoint = false;
                    //                }
                    //                else
                    //                {
                    //                    tripString += "|=via:" +
                    //                                    closedTrip.DropOff_street + ", " +
                    //                                    closedTrip.DropOff_city + ", " +
                    //                                    closedTrip.DropOff_state + " " +
                    //                                    closedTrip.DropOff_zip.Trim();
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if (firstWaypoint)
                    //                {
                    //                    tripString += "&waypoints=via:" +
                    //                                    closedTrip.Pickup_street + ", " +
                    //                                    closedTrip.Pickup_city + ", " +
                    //                                    closedTrip.Pickup_state + " " +
                    //                                    closedTrip.Pickup_zip.Trim();
                    //                    firstWaypoint = false;
                    //                }
                    //                else
                    //                {
                    //                    tripString += "|via:" +
                    //                                    closedTrip.Pickup_street + ", " +
                    //                                    closedTrip.Pickup_city + ", " +
                    //                                    closedTrip.Pickup_state + " " +
                    //                                    closedTrip.Pickup_zip.Trim();
                    //                }
                    //            }
                    //        }
                    //    }
                    //    ////System.Threading.Thread.Sleep(1000);
                    //    //string url1 = "https://maps.googleapis.com/maps/api/directions/json?origin=" + "65 Garfield St, Lancaster, NY 14086" + "&destination=" + "2495 Main ST, Buffalo NY 14214" + "&waypoints=via:32 Quincy Ave, Lancaster, NY 14086" + "|via:3852 Lynn Dr, Orchard Park, NY 14127" + "|via:6319 May Ave., Hamburg, NY 14075" + "&key=AIzaSyD0x7RGskXkJEo_LuXC0jrjAHqLv6IBXHc";
                    //    string url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + tripString + "&key=AIzaSyD0x7RGskXkJEo_LuXC0jrjAHqLv6IBXHc";
                    //    string url1 = System.Text.RegularExpressions.Regex.Replace(url, "#", "%23");
                    //    string requesturl = url;

                    //    System.Net.WebRequest request = System.Net.WebRequest.Create(url1);

                    //    System.Net.WebResponse response = request.GetResponse();
                    //    Stream data = response.GetResponseStream();
                    //    StreamReader reader = new StreamReader(data);

                    //    string responseFromServer = reader.ReadToEnd();

                    //    RootObject r = JsonConvert.DeserializeObject<RootObject>(responseFromServer);
                    //    if (r.status.Equals("OK"))
                    //    {
                    //        double distance = 0;
                    //        foreach (Leg lg in r.routes[0].legs)
                    //        {
                    //            string[] distanceStrings = lg.distance.text.Split(new string[] { " " }, StringSplitOptions.None);
                    //            if (!distanceStrings[1].ToLower().Equals("ft"))
                    //            {
                    //                distance += Convert.ToDouble(distanceStrings[0]);
                    //            }
                    //            bridge.bridge_flag = "Billed Processed";
                    //            //Save the OK status and the distance to the Route table
                    //            route.Status = "OK";
                    //        }
                    //        route.Distance = distance;
                    //    }
                    //    else
                    //    {
                    //        string err = r.status + " " + url1;

                    //        //Save the error status to the Route table
                    //        route.Status = err;
                    //    }
                    //    medtransDB.SaveChanges();
                    //    dispRepo.SaveRoute(route);

                    //    response.Close();
                    //}
                    ////dispRepo.ExportRoutes(frDate, toDate);

                    //log.Error("Route Processing Complete ");

                }
            } catch (Exception ex) {
                log.Error("Sync exception = " + ex.Message);
                // Console.WriteLine("exc = " + ex.Message.ToString());
                throw new Exception(ex.Message);
            }
        }      
    }    
}
