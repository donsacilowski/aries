﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

using Aries.Model.Entities;
using Aries.Model.Repositories;
using MedTrans;

namespace Aries.Model.UpdateDB {
    
    public class UpdateDB {

        public static readonly ILog log = LogManager.GetLogger(typeof(UpdateDB));

        public UpdateDB() {
                       
        }

        public int UpdateCustomer(trans_bridge bridge) {
            CustomerRepository customerRepo = new CustomerRepository();
            AddressRepository addressRepo = new AddressRepository();

            Address addr = null;
            Address medaddr = null;
            Customer customer = null;

            bool newCustomer = false;

            try {
                newCustomer = false;

                /* Customer is only meant to be the client; not any office/hospital etc... */
                customer = customerRepo.GetByMedtransID(bridge.ClientID);

                if (customer != null) {
                    addr = addressRepo.GetByID((int)customer.AddressID);
                }
                if (customer == null) {
                    customer = new Customer();
                    customerRepo.Add(customer);

                    log.Info("New Customer");
                    newCustomer = true;
                }

            } catch (Exception ex) {
                log.Error(ex);
            }

            try {

                if ((addr == null) || (addr.Street.Trim() != bridge.home_address.Trim())
                                   || (addr.City.Trim() != bridge.home_city.Trim())
                                   || (addr.State.Trim() != bridge.home_state.Trim())
                                   || (addr.PostalCode.Trim() != bridge.home_zip.Trim()))
                {
                    if (addr == null) {
                        log.Info("New Address addr is null " + bridge.home_address);
                    }
                    addr = new Model.Entities.Address();
                    addressRepo.Add(addr);
                    
                    log.Info("New Address" + bridge.home_address);
                    if (!newCustomer)
                    {
                        log.Info("New Customer 2 " + customer.Name);
                        customer = new Customer();
                        customerRepo.Add(customer);
                    }
                }

                ///* Update the customers address from the pickups address if a home indication for pickup */
                //if ((bridge.PickupID == -10) || (bridge.PickupID == -11))
                //{
                //    addr.City = bridge.Pickup_city;
                //    addr.PostalCode = bridge.Pickup_zip;
                //    addr.State = bridge.Pickup_state;
                //    addr.Street = bridge.Pickup_street;
                //    addr.MedTransId = bridge.PickupID;
                //    addr.Description = bridge.PickupOffice;
                //}

                ///* Update the customers address from the dropoffs address if a home indication for dropoff */
                //else if ((bridge.DropoffID == -10) || (bridge.DropoffID == -11))
                //{
                //    addr.City = bridge.DropOff_city;
                //    addr.PostalCode = bridge.DropOff_zip;
                //    addr.State = bridge.DropOff_state;
                //    addr.Street = bridge.DropOff_street;
                //    addr.MedTransId = bridge.DropoffID;
                //    addr.Description = bridge.DropOffOffice;
                //}

                //else
                //{
                    addr.City = bridge.home_city;
                    addr.PostalCode = bridge.home_zip;
                    addr.State = bridge.home_state;
                    addr.Street = bridge.home_address;
                    addr.MedTransId = bridge.ClientID;
                    addr.Description = "Home Address";
                //}
                addressRepo.Update(addr);

            } catch (Exception ex) {
                log.Error(ex);
            }

            try {

                customer.AddressID = addr.Id;
                customer.MedTransId = bridge.ClientID;
                customer.Name = bridge.ClientName;
                customer.PhoneNumber = bridge.ClientPhone;

                customerRepo.Update(customer);

            } catch (Exception ex) {
                log.Error(ex);
            }

            return customer.Id;
        }

        public int UpdateDriver(trans_bridge bridge) {
            VehicleRepository vehicleRepo = new VehicleRepository();
            DriverRepository driverRepo = new DriverRepository();

            Vehicle veh = null;
            Driver driver = null;

            try {
                veh = vehicleRepo.GetByMedtransID((int)bridge.VanIdentifier);
                if ((veh == null) || (veh.LicensePlateNumber != bridge.VanLicense)){
                    veh = new Vehicle();
                    vehicleRepo.Add(veh);
                }
            } catch (Exception ex) {
                log.Error(ex);
            }
            
            try {                
                veh.LicensePlateNumber = bridge.VanLicense;
                veh.Description = bridge.VanName;
                veh.MedTransId = (int)bridge.VanIdentifier;

                vehicleRepo.Update(veh);

            } catch (Exception ex) {
                log.Error(ex);
            }

            try {

                
                var names = bridge.DriverName.Split(',');
                string dName = names[1].Trim() + ' ' + names[0].Trim();

                driver = driverRepo.GetByName(dName);
                string appKey = "";
                if ((driver != null) && (!string.IsNullOrEmpty(driver.MobileAppKey)))
                {
                    appKey = driver.MobileAppKey;
                }

                if ((driver == null) || (driver.DriversLicenseNumber != bridge.DriverLicense) ||
                    (driver.Vehicle.Description != bridge.VanName))
                {
                    driver = new Driver();
                    if (!appKey.Equals(""))
                    {
                        driver.MobileAppKey = appKey;
                    }
                    driverRepo.Add(driver);
                }
                
            } catch (Exception ex) {
                log.Error(ex);
            }

            try {
                driver.DriversLicenseNumber = bridge.DriverLicense;
                driver.PhoneNumber = bridge.DriverPhoneNumber;
                driver.MedTransId = (int)bridge.DriverIdentifier;
                var names = bridge.DriverName.Split(',');
                driver.Name = names[1].Trim() + ' ' + names[0].Trim();
                driver.VehicleID = veh.Id;

                driverRepo.Update(driver);

            } catch (Exception ex) {
                log.Error(ex);
            }

            return driver.Id;
        }

        public int AddPayment(trans_bridge bridge) {
            Payment pay = new Payment();

            try {
                PaymentRepository payRepo = new PaymentRepository();

                /* TODO: Might have to change based on what the data is in Medtrans */
                pay.PaymentToDriver = true;
                pay.PaymentToDriverAmount = Convert.ToDecimal(bridge.PaymentAmount);

                payRepo.Add(pay);
            } catch (Exception ex) {
                log.Error(ex);
            }

            return pay.Id;                
        }

        public int AddPayment() {
            Payment pay = new Payment();

            try {
                PaymentRepository payRepo = new PaymentRepository();

                /* TODO: Might have to change based on what the data is in Medtrans */
                pay.PaymentToDriver = false;
                pay.PaymentToDriverAmount = 0;
                pay.Paid = false;

                payRepo.Add(pay);
            } catch (Exception ex) {
                log.Error(ex);
            }

            return pay.Id;
        }

        public int UpdateDropOff(trans_bridge bridge) {

            AddressRepository addressRepo = new AddressRepository();
            CustomerRepository customerRepo = new CustomerRepository();

            Address dropOffAddr = null;

            try {
                if ((bridge.DropoffID == -10) || (bridge.DropoffID == -11)) {
                    dropOffAddr = customerRepo.GetByMedtransID(bridge.ClientID).Address;
                } else {
                    dropOffAddr = addressRepo.GetByMedtransID(bridge.DropoffID);
                }

                if (dropOffAddr == null) {
                    dropOffAddr = new Model.Entities.Address();
                    addressRepo.Add(dropOffAddr);
                }

                dropOffAddr.City = bridge.DropOff_city;
                dropOffAddr.Description = bridge.DropOffOffice;
                dropOffAddr.PostalCode = bridge.DropOff_zip;
                dropOffAddr.State = bridge.DropOff_state;
                dropOffAddr.Street = bridge.DropOff_street;
                dropOffAddr.MedTransId = bridge.DropoffID;

                addressRepo.Update(dropOffAddr);

            } catch (Exception ex) {
                log.Error(ex);
            }

            return dropOffAddr.Id;
        }

        public int UpdatePickup(trans_bridge bridge) {

            AddressRepository addressRepo = new AddressRepository();
            CustomerRepository customerRepo = new CustomerRepository();

            Address PickupAddr = null;

            try {
                if ((bridge.PickupID == -10) || (bridge.PickupID == -11)) {
                    PickupAddr = customerRepo.GetByMedtransID(bridge.ClientID).Address;
                } else {
                    PickupAddr = addressRepo.GetByMedtransID(bridge.PickupID);
                }

                if (PickupAddr == null) {
                    PickupAddr = new Model.Entities.Address();
                    addressRepo.Add(PickupAddr);
                }

                PickupAddr.City = bridge.Pickup_city;
                PickupAddr.Description = bridge.PickupOffice;
                PickupAddr.PostalCode = bridge.Pickup_zip;
                PickupAddr.State = bridge.Pickup_state;
                PickupAddr.Street = bridge.Pickup_street;
                PickupAddr.MedTransId = bridge.PickupID;

                addressRepo.Update(PickupAddr);

            } catch (Exception ex) {
                log.Error(ex);
            }

            return PickupAddr.Id;
        }
    }
}
