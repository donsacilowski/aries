﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aries.FleetComplete.Services;
using Aries.FleetComplete.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace FleetCompleteServiceTest {
    [TestClass]
    public class DbTest {
        private readonly IDriverRepository driverRepo = new DriverRepository();
        private readonly IDispatchRepository dispRepo = new DispatchRepository();
        private readonly IVehicleRepository vehRepo = new VehicleRepository();


        [TestMethod]
        public void TestDB() {

            Address addr = new Address();
            addr.City = "Lancaster";
            addr.PostalCode = "14086-2220";
            addr.State = "New York";
            addr.Street = "341 Main St.";

            Customer cust = new Customer();
            cust.AddressID = addr.Id;
            cust.Name = "Thuan Tran";
            cust.PhoneNumber = "716-777-9876";
            cust.MedTransId = 321;
            cust.FC8Id = 876;

            Driver driver = new Driver();
            driver.DriversLicenseNumber = "BHBH-678";
            driver.Name = "My Vehicle:";
            driver.PhoneNumber = "(716)632-7878";
            driver.FC8Id = 24;
            driver.MedTransId = 67;
            driverRepo.Add(driver);

            Payment pay = new Payment();
            pay.Paid = true;
            pay.PaymentToDriver = true;
            pay.PaymentToDriverAmount = Convert.ToDecimal(10.12);

            Vehicle veh = new Vehicle();
            veh.Description = "abc234";
            veh.LicensePlateNumber = "123456";
            veh.FC8Id = 56;
            veh.MedTransId = 980;
            vehRepo.Add(veh);

            Dispatch disp = new Dispatch();
            disp.DNLA = true;
            disp.DriverID =  driver.Id;
            disp.Notes = "pick up at 123 main";
            disp.PaymentID = pay.Id;
            disp.PickupTime = DateTime.Now.AddDays(-1);
            disp.Service = "wheelchair";
            disp.ServiceType = "wide";
            disp.Timestamp = DateTime.Now;
            dispRepo.Add(disp);
        }
    }
}
