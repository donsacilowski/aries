﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Json;

using Aries.Portal.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Portal.Controllers
{
    [HandleError]
    public class DialogController : Controller
    {
        #region New event - Currently not used - Need to modification
        public ActionResult New(string id, string start, string end)
        {
            string role = (new EmployeeRepository()).GetByName(HttpContext.User.Identity.Name).Role;
            bool isAdmin = (new Helpers.Misc()).IsAdmin();
            bool isManager = (new Helpers.Misc()).IsManager();
            bool isDispatcher = (new Helpers.Misc()).IsDispatch();

            DateTime dtStart = DateTime.Parse(start);
            DateTime dtEnd = DateTime.Parse(end);

            //if ((isDispatcher || isManager || isAdmin) && (DateTime.Now.Date.Equals(dtStart.Date)))
            //{

                int employeeId = int.Parse(id);
                ViewData["UserId"] = employeeId;
                ViewData["EventDate"] = dtStart.ToString(@"MM\/dd\/yyyy");
                ViewData["EventStart"] = dtStart.ToString("HH:mm");
                ViewData["EventEnd"] = dtEnd.ToString("HH:mm");

                ViewData["AdjustOptions"] = (isAdmin || isManager) ? "1" : "0";
                return View();
            //}
            //return View("~/Timesheet/Edit?Id=" + id.ToString() + "&date=" + dtStart.ToString(@"MM\/dd\/yyyy"));
            //return JavaScript("window.close();");

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(FormCollection form)
        {

            int UserId = Convert.ToInt32(form["UserId"]);
            string serviceCode = Convert.ToString(form["ServiceCode"]);
            DateTime eventDate = EventDate(form["EventDate"]);
            DateTime timeIn = EventDatetime(form["EventDate"], form["EventStart"]);
            DateTime timeOut = EventDatetime(form["EventDate"], form["EventEnd"]);
            if (timeIn <= timeOut)
            {
                (new TimesheetRepository()).AddNewEvent(UserId, eventDate, timeIn, timeOut, serviceCode);
            }

            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }
        #endregion

        #region Edit/Delete
        /// <summary>
        /// Edit/Delete event
        /// </summary>
        /// <param name="id">event (TimesheetDetail) Id</param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            int timesheetDetailId = 0;
            int.TryParse(id, out timesheetDetailId);
            TimesheetDetail timesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetDetailId);


            //ViewBag.Editable = true;
            //bool isAdmin = (new Helpers.Misc()).IsAdmin();
            //bool isManager = (new Helpers.Misc()).IsManager();
            //ViewData["AdjustOptions"] = (isAdmin || isManager) ? "1" : "0";

            return View(timesheetDetail);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(FormCollection form)
        {
            int timesheetId = 0;
            int.TryParse(form["id"], out timesheetId);
            TimesheetDetail editTimesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetId);
            
            if (editTimesheetDetail != null)
            {
                string sTem = form["delete_event"];
                if (sTem.Substring(0, 4).ToUpper() == "TRUE")
                {
                    (new TimesheetRepository()).DeleteDetail(editTimesheetDetail.Id);
                }
                else
                {
                    editTimesheetDetail.ServiceCode = form["ServiceCode"];
                    editTimesheetDetail.TimeIn = (new Utility.Misc()).RoundTo15Minutes(EventDatetime(form["EventDate"], form["EventStart"]));
                    editTimesheetDetail.TimeOut = (new Utility.Misc()).RoundTo15Minutes(EventDatetime(form["EventDate"], form["EventEnd"]));
                    if (editTimesheetDetail.TimeIn <= editTimesheetDetail.TimeOut)
                    {
                        (new TimesheetRepository()).UpdateDetail(editTimesheetDetail, timesheetId);
                    }
                }
            }
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }
        #endregion

        #region Holidays
        /// <summary>
        /// Add Holiday event (default to 8 hours)
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        public ActionResult Holiday(int id)
        {
            ViewBag.UserID = id.ToString();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Holiday(FormCollection form)
        {

            int employeeID = Convert.ToInt32(form["UserId"]);
            DateTime eventDate = (new Utility.Misc()).RoundTo15Minutes(EventDatetime(form["EventDate"], "7:00:00"));
            (new TimesheetRepository()).AddHoliday(employeeID, eventDate);           
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }
        public ActionResult HolidayEdit(string id)
        {
            int timesheetDetailId = 0;
            int.TryParse(id, out timesheetDetailId);
            TimesheetDetail timesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetDetailId);
            return View(timesheetDetail);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HolidayEdit(FormCollection form)
        {

            int timesheetId = 0;
            int.TryParse(form["id"], out timesheetId);
            TimesheetDetail editTimesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetId);

            if (editTimesheetDetail != null)
            {
                string sTem = form["delete_event"];
                if (sTem.Substring(0, 4).ToUpper() == "TRUE")
                {
                    (new TimesheetRepository()).DeleteDetail(editTimesheetDetail.Id);
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(form["EventDate"]);
                    editTimesheetDetail.ServiceCode = "Holiday";
                    editTimesheetDetail.TimeIn = new DateTime(dt.Year, dt.Month, dt.Day, 7, 0, 0, 0);
                    editTimesheetDetail.TimeOut = new DateTime(dt.Year, dt.Month, dt.Day, 15, 0, 0, 0);
                    (new TimesheetRepository()).UpdateDetail(editTimesheetDetail, timesheetId);
                }
            }
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));

        }
        #endregion

        #region Vacations
        /// <summary>
        /// Add Vacation event
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        public ActionResult Vacation(int id)
        {
            ViewBag.UserID = id.ToString();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Vacation(FormCollection form)
        {
            int employeeID = Convert.ToInt32(form["UserId"]);
            DateTime eventDate = EventDatetime(form["EventDate"], form["EventTime"]);
            int hours = Convert.ToInt32(form["Hours"]);
            (new TimesheetRepository()).AddVacation(employeeID, eventDate, hours);                       
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }
        public ActionResult VacationEdit(string id)
        {
            int timesheetDetailId = 0;
            int.TryParse(id, out timesheetDetailId);
            TimesheetDetail timesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetDetailId);
            TimeSpan ts = timesheetDetail.TimeOut - timesheetDetail.TimeIn;
            ViewData["VacationHours"] = ts.TotalHours.ToString();
            return View(timesheetDetail);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VacationEdit(FormCollection form)
        {
            int timesheetId = 0;
            int.TryParse(form["id"], out timesheetId);
            TimesheetDetail editTimesheetDetail = (new TimesheetRepository()).GetDetailByID(timesheetId);

            if (editTimesheetDetail != null)
            {
                string sTem = form["delete_event"];
                if (sTem.Substring(0, 4).ToUpper() == "TRUE")
                {
                    (new TimesheetRepository()).DeleteDetail(editTimesheetDetail.Id);
                }
                else
                {
                    DateTime dIn = (new Utility.Misc()).RoundTo15Minutes(EventDatetime(form["EventDate"], form["EventTime"]));
                    int hours = int.Parse(form["Hours"]);

                    editTimesheetDetail.ServiceCode = "Vacation";
                    editTimesheetDetail.TimeIn = dIn;
                    editTimesheetDetail.TimeOut = dIn.AddHours(hours);
                    (new TimesheetRepository()).UpdateDetail(editTimesheetDetail, timesheetId);
                }
            }
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        #endregion

        #region Credit Hours
        public ActionResult Credit(int id)
        {
            CreditHour creditHours = (new TimesheetRepository()).GetEmployeeCreditHours(id, DateTime.Today);
            DateTime monday = (new Utility.Misc()).FirstDayOfWeek(DateTime.Today, DayOfWeek.Monday);
            // 
            if (creditHours == null)
            {
                ViewBag.UserID = id.ToString();
                ViewData["WeekOf"] = monday.ToString(@"MM\/dd\/yyyy");
                ViewData["RegHour"] = 0;
                ViewData["OTHour"] = 0;
            }
            else
            {
                ViewBag.UserID = id.ToString();
                ViewData["WeekOf"] = creditHours.Timesheet.Date.ToString(@"MM\/dd\/yyyy");
                ViewData["RegHour"] = creditHours.RegularTime;
                ViewData["OTHour"] = creditHours.OverTime;
            }

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Credit(FormCollection form)
        {
            int employeeID = Convert.ToInt32(form["UserId"]);
            DateTime eventDate = DateTime.Parse(form["txWeekOf"]);
            float regHours = Convert.ToSingle(form["txRegHour"]);
            float otHours = Convert.ToSingle(form["txOTHour"]);
            (new TimesheetRepository()).AddCredits(employeeID, (new Utility.Misc()).FirstDayOfWeek(eventDate, DayOfWeek.Monday), regHours, otHours);
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        #endregion

        #region Helper
        DateTime EventDate(string eventDate)
        {
            return Convert.ToDateTime(eventDate).Date;
        }
        DateTime EventDatetime(string eventDate, string eventTime)
        {
            DateTime dtDate = Convert.ToDateTime(eventDate);
            DateTime dtTime = Convert.ToDateTime(eventTime);
            return new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, dtTime.Hour, dtTime.Minute, 0 , 0);
        }
        #endregion

        #region Test Codes
        public ActionResult NewAddress()
        {
            return View(new Address
            {
                Street = "",
                City = "",
                State = "",
                PostalCode = ""
            });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAddress(FormCollection form)
        {
            (new AddressRepository()).Add(new Address {
                Street = Convert.ToString(form["Street"]),
                City = Convert.ToString(form["City"]),
                State = Convert.ToString(form["State"]),
                PostalCode = Convert.ToString(form["PostalCode"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        #endregion
    }
}