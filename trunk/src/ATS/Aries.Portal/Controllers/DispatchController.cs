﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic;
using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Model;
using MedTrans;
using System.IO;
using Newtonsoft.Json;
using log4net;

namespace Aries.Portal.Controllers
{
    public class DispatchController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DispatchController));
        public class GeocodedWaypoint
        {
            public string geocoder_status { get; set; }
            public string place_id { get; set; }
            public List<string> types { get; set; }
        }

        public class Northeast
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Southwest
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Bounds
        {
            public Northeast northeast { get; set; }
            public Southwest southwest { get; set; }
        }

        public class Distance
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class EndLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class StartLocation
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Distance2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class Duration2
        {
            public string text { get; set; }
            public int value { get; set; }
        }

        public class EndLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Polyline
        {
            public string points { get; set; }
        }

        public class StartLocation2
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }
        public class Step
        {
            public Distance2 distance { get; set; }
            public Duration2 duration { get; set; }
            public EndLocation2 end_location { get; set; }
            public string html_instructions { get; set; }
            public Polyline polyline { get; set; }
            public StartLocation2 start_location { get; set; }
            public string travel_mode { get; set; }
            public string maneuver { get; set; }
        }

        public class Location
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class ViaWaypoint
        {
            public Location location { get; set; }
            public int step_index { get; set; }
            public double step_interpolation { get; set; }
        }
        public class Leg
        {
            public Distance distance { get; set; }
            public Duration duration { get; set; }
            public string end_address { get; set; }
            public EndLocation end_location { get; set; }
            public string start_address { get; set; }
            public StartLocation start_location { get; set; }
            public List<Step> steps { get; set; }
            public List<object> traffic_speed_entry { get; set; }
            public List<ViaWaypoint> via_waypoint { get; set; }
        }

        public class OverviewPolyline
        {
            public string points { get; set; }
        }

        public class Routes
        {
            public Bounds bounds { get; set; }
            public string copyrights { get; set; }
            public List<Leg> legs { get; set; }
            public OverviewPolyline overview_polyline { get; set; }
            public string summary { get; set; }
            public List<object> warnings { get; set; }
            public List<object> waypoint_order { get; set; }
        }

        public class RootObject
        {
            public List<GeocodedWaypoint> geocoded_waypoints { get; set; }
            public List<Routes> routes { get; set; }
            public string status { get; set; }
        }
        public DispatchController()
        {
            ViewBag.UserName = (new EmployeeRepository()).GetEmployeeName(System.Web.HttpContext.Current.User.Identity.Name);
        }
        string AUTO_REFRESH_RATE = "10"; // Auto Refresh Rate in Seconds
        //
        // GET: /Dispatch/
        public ActionResult Status()
        {
            return View();
        }

        public ActionResult Overview()
        {
            return View();

        }
        public JsonResult GetTrips(string fr = "", string to = "")
        {
            // TODO: 
            // May need to add filter to just get all dispatch for the day only    
            DateTime frDate = DateTime.Now.Date;
            DateTime toDate = DateTime.Now.Date;

            if (fr != "")
            {
                DateTime.TryParse(fr, out frDate);
            }
            if (to != "")
            {
                DateTime.TryParse(to, out toDate);
            }
            var dispatchList = (new DispatchRepository()).GetOpenDispatchSummary(frDate, new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59));
            //ViewData["Dispatches"] = dispatchList;
            //Response.AddHeader("Refresh", AUTO_REFRESH_RATE);
            //return View("~/Views/Dispatch/Overview.cshtml");
            JsonResult lst = Json(dispatchList, JsonRequestBehavior.AllowGet);
            return lst;

        }

        public ActionResult Alerts()
        {
            // TODO: 
            // May need to add filter to just get all dispatch for the day only     
            DateTime frDate = DateTime.Now.Date;
            DateTime toDate = new DateTime(frDate.Year, frDate.Month, frDate.Day, 23, 59, 59);
            List<DispatchSummaryView> dispatchList = (new DispatchRepository()).GetDispatchSummary(frDate, toDate);
            List<DispatchAlertView> alertList = new List<DispatchAlertView>();
            StatusListRepository sl = new StatusListRepository();

            foreach (DispatchSummaryView d in dispatchList)
            {
                if ((d.Status == "Dispatched") || (d.Status == "Open"))
                {
                    long minutes = DateAndTime.DateDiff("n", d.StatusTimestamp, DateTime.Now);

                    if (minutes >= 15)
                    {
                        alertList.Add(new DispatchAlertView
                        {
                            Id = d.Id,
                            OrderNumber = d.OrderNumber,
                            Timestamp = d.StatusTimestamp,
                            Customer = d.Customer,
                            Driver = d.Driver,
                            Status = d.Status,
                            Minutes = minutes,
                            BGcss = (minutes >= 30) ? "red" : "yellow"
                        });
                    }
                }
            }

            //var l1 = alertList.OrderByDescending(a => a.Timestamp).ToList();
            //var l2 = alertList.OrderByDescending(a => a.Minutes).ToList();

            ViewData["Alerts"] = alertList.OrderByDescending(a => a.Minutes).ToList();
            Response.AddHeader("Refresh", AUTO_REFRESH_RATE);
            //return View("~/Views/Dispatch/Alerts.cshtml");
            return View();
        }
        public ActionResult Archives()
        {
            //Session["fromDate"] = DateTime.Now.Date.ToString();
            //Session["toDate"] = DateTime.Now.Date.ToString();
            return View();

        }
        public JsonResult GetArchives(string fr = "", string to = "")
        {
            // TODO: 
            // May need to add filter to just get all dispatch for the day only    
            DateTime frDate = DateTime.Now.Date;
            DateTime toDate = DateTime.Now.Date;
            log.Info("in GetArchives");

            if (fr != "")
            {
                DateTime.TryParse(fr, out frDate);
            }
            else
            {
                if (Session["fromDate"] != null && Session["fromDate"].ToString() != "")
                {
                    fr = Session["fromDate"].ToString();
                    DateTime.TryParse(fr, out frDate);
                }
            }
            if (to != "")
            {
                DateTime.TryParse(to, out toDate);
            }
            else
            {
                if (Session["toDate"] != null && Session["toDate"].ToString() != "")
                {
                    to = Session["toDate"].ToString();
                    DateTime.TryParse(to, out toDate);
                }
            }

            Session["fromDate"] = fr;
            Session["toDate"] = to;
            var dispatchList = (new DispatchRepository()).GetDispatchSummary(frDate, new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59));
            //ViewData["Dispatches"] = dispatchList;
            JsonResult lst = Json(dispatchList, JsonRequestBehavior.AllowGet);
            return lst;
        }
        //public JsonResult GetRoutes(string fr = "", string to = "")
        //{
        //    // TODO: 
        //    // May need to add filter to just get all dispatch for the day only    
        //    DateTime frDate = DateTime.Now.Date;
        //    DateTime toDate = DateTime.Now.Date;
        //    log.Info("in GetRoutes");

        //    if (fr != "")
        //    {
        //        DateTime.TryParse(fr, out frDate);
        //    }
        //    else
        //    {
        //        if (Session["fromDate"] != null && Session["fromDate"].ToString() != "")
        //        {
        //            fr = Session["fromDate"].ToString();
        //            DateTime.TryParse(fr, out frDate);
        //        }
        //    }
        //    if (to != "")
        //    {
        //        DateTime.TryParse(to, out toDate);
        //    }
        //    else
        //    {
        //        if (Session["toDate"] != null && Session["toDate"].ToString() != "")
        //        {
        //            to = Session["toDate"].ToString();
        //            DateTime.TryParse(to, out toDate);
        //        }
        //    }

        //    Session["fromDate"] = fr;
        //    Session["toDate"] = to;
        //    var routeList = (new DispatchRepository()).GetRouteSummary(frDate, new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59));
        //    //ViewData["Dispatches"] = dispatchList;
        //    JsonResult lst = Json(routeList, JsonRequestBehavior.AllowGet);
        //    return lst;
        //}

        public ActionResult Detail(string Id = "")   
        {
            // Dispatch model;
            int dispatchId = 0;
            int.TryParse(Id, out dispatchId);
            if (dispatchId > 0)
            {
                Dispatch dispatch = (new DispatchRepository()).GetByID(dispatchId);
                ViewData["OrderNumber"] = dispatch.Id;
                ViewData["Statuses"] = (new StatusRepository()).GetByDispatchID(dispatchId);
                Response.AddHeader("Refresh", AUTO_REFRESH_RATE);
            
                return View("~/Views/Dispatch/Detail.cshtml", dispatch);
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Route()
        {
            //Session["fromDate"] = DateTime.Now.Date.ToString();
            //Session["toDate"] = DateTime.Now.Date.ToString();
            return View();

        }
        public JsonResult GetRouteSummary(string frDate, string toDate)
        {
            MedtransEntities medtransDB = new MedtransEntities();
            DispatchRepository dispRepo = new DispatchRepository();
            StatusRepository statusRepo = new StatusRepository();
            DriverRepository driverRepo = new DriverRepository();
            Route savedRoute = new Route();

            List<RouteView> summaryList = new List<RouteView>();
            try
            {
                DateTime fr = DateTime.Now;
                if (frDate != "") fr = Convert.ToDateTime(frDate);
                DateTime to = DateTime.Now;
                if (toDate != "") to = Convert.ToDateTime(toDate).AddDays(1);
                if (frDate != "" && toDate != "")
                {                   
                    log.Info("Processing Routes" + fr.Date.ToShortDateString() + " to " + to.Date.ToShortDateString());

                    List<trans_bridge> rtClosed = medtransDB.trans_bridge.Where(
                           p => ((p.bridge_flag.ToLower().Equals("rtclosed"))) &&
                                 p.unit_n == 0 &&
                                 p.serv_date >= fr.Date && p.serv_date <= to.Date).ToList();

                    log.Info("Processing Routes Number = " + rtClosed.Count.ToString());

                    foreach (trans_bridge bridge in rtClosed)
                    {
                       // log.Error("Billed Route for " + bridge.AppointTime + "  = " + bridge.trans_id.ToString());

                        //Only process the route if this MedtransID is not in the Route table
                        Route route = dispRepo.GetRoute(bridge.trans_id);

                        var names = bridge.DriverName.Split(',');
                        string dName = names[1].Trim() + ' ' + names[0].Trim();
                        Driver routeDriver = driverRepo.GetByName(dName);
                        if (routeDriver == null) routeDriver = driverRepo.GetByID(1005);
                        ////log.Info("Route Driver= " + routeDriver.Name);

                        if (route == null)
                        {
                            log.Error("Processing Routes New Route = " + bridge.trans_id.ToString());

                            Route newRoute = new Route();
                            newRoute.Name = bridge.ClientName;
                            newRoute.MedTransId = bridge.trans_id;
                        
                            newRoute.DriverID = routeDriver.Id;
                            newRoute.Status = "No Status";
                            newRoute.Revenue = 0.0;
                            newRoute.Distance = 0.0;
                            newRoute.FC8Id = 0;
                        
                            newRoute.TotalTime = new TimeSpan();
                            newRoute.Timestamp = Convert.ToDateTime(bridge.serv_date);
                        
                            List<trans_bridge> rtTrips = medtransDB.trans_bridge.Where(
                                  p => (p.unit_n == bridge.trans_id) &&
                                   p.bridge_flag.ToLower().Equals("closedattested")).OrderBy(p => p.At_pickup_time).ToList();

                            string tripString = "";
                            bool firstWaypoint = true;
                            bool isPickupRoute = false;
                            bool isDropoffRoute = false;

                            foreach (trans_bridge closedTrip in rtTrips)
                            {
                                log.Info("Processing Routes " + closedTrip.trans_id.ToString() + " Closedtrips = " + rtTrips.Count);
                                if (closedTrip.trip_base_amt != null && closedTrip.trip_mile_amt != null)
                                {
                                    newRoute.Revenue += (double)(closedTrip.trip_base_amt + closedTrip.trip_mile_amt);
                                    log.Info("Processing Routes adding revenue " + closedTrip.trip_base_amt.ToString() + " " + closedTrip.trip_mile_amt.ToString());

                                }
                                // need at least 2 trips to do the waypoint calculations and no cancel clients in the dispatch history                         
                                if (rtTrips.Count >= 2)
                                {
                                    if (firstWaypoint)
                                    {
                                        trans_bridge firstPickup = rtTrips.OrderBy(t => t.At_pickup_time).FirstOrDefault();
                                        trans_bridge lastDropoff = rtTrips.OrderByDescending(t => t.DoneTime).FirstOrDefault();

                                        tripString = firstPickup.Pickup_street + ", " +
                                                            firstPickup.Pickup_city + ", " +
                                                            firstPickup.Pickup_state + " " +
                                                            firstPickup.Pickup_zip.Trim() +
                                                            "&destination=" +
                                                            lastDropoff.DropOff_street + ", " +
                                                            lastDropoff.DropOff_city + ", " +
                                                            lastDropoff.DropOff_state + " " +
                                                            lastDropoff.DropOff_zip.Trim();

                                        TimeSpan routeTime = new TimeSpan(1, 30, 00);
                                        if (lastDropoff.DoneTime != null && firstPickup.At_pickup_time != null)
                                        {
                                            routeTime = Convert.ToDateTime(lastDropoff.DoneTime).Subtract(Convert.ToDateTime(firstPickup.At_pickup_time));
                                        }
                                        log.Error("Processing Routes Time = " + closedTrip.trans_id.ToString() + " " + routeTime.ToString());

                                        //Save the routetime to the Route database table.
                                        newRoute.TotalTime = routeTime;

                                        tripString = firstPickup.Pickup_street + ", " +
                                                            firstPickup.Pickup_city + ", " +
                                                            firstPickup.Pickup_state + " " +
                                                            firstPickup.Pickup_zip.Trim() +
                                                            "&destination=" +
                                                            lastDropoff.DropOff_street + ", " +
                                                            lastDropoff.DropOff_city + ", " +
                                                            lastDropoff.DropOff_state + " " +
                                                            lastDropoff.DropOff_zip.Trim();

                                        isPickupRoute = rtTrips[0].Pickup_street == rtTrips[1].Pickup_street ? true : false;
                                        isDropoffRoute = rtTrips[0].DropOff_street == rtTrips[1].DropOff_street ? true : false;
                                        log.Error("Processing Routes pickup route = " + closedTrip.trans_id.ToString() + " " + isPickupRoute.ToString());
                                        log.Error("Processing Routes dropoff route = " + closedTrip.trans_id.ToString() + " " + isDropoffRoute.ToString());
                                    }

                                    if (isPickupRoute)
                                    {
                                        if (firstWaypoint)
                                        {
                                            tripString += "&waypoints=via:" +
                                                            closedTrip.DropOff_street + ", " +
                                                            closedTrip.DropOff_city + ", " +
                                                            closedTrip.DropOff_state + " " +
                                                            closedTrip.DropOff_zip.Trim();
                                            firstWaypoint = false;
                                        }
                                        else
                                        {
                                            tripString += "|=via:" +
                                                            closedTrip.DropOff_street + ", " +
                                                            closedTrip.DropOff_city + ", " +
                                                            closedTrip.DropOff_state + " " +
                                                            closedTrip.DropOff_zip.Trim();
                                        }
                                    }
                                    else
                                    {
                                        if (firstWaypoint)
                                        {
                                            tripString += "&waypoints=via:" +
                                                            closedTrip.Pickup_street + ", " +
                                                            closedTrip.Pickup_city + ", " +
                                                            closedTrip.Pickup_state + " " +
                                                            closedTrip.Pickup_zip.Trim();
                                            firstWaypoint = false;
                                        }
                                        else
                                        {
                                            tripString += "|via:" +
                                                            closedTrip.Pickup_street + ", " +
                                                            closedTrip.Pickup_city + ", " +
                                                            closedTrip.Pickup_state + " " +
                                                            closedTrip.Pickup_zip.Trim();
                                        }
                                    }
                                }
                            }
                            //string url1 = "https://maps.googleapis.com/maps/api/directions/json?origin=" + "65 Garfield St, Lancaster, NY 14086" + "&destination=" + "2495 Main ST, Buffalo NY 14214" + "&waypoints=via:32 Quincy Ave, Lancaster, NY 14086" + "|via:3852 Lynn Dr, Orchard Park, NY 14127" + "|via:6319 May Ave., Hamburg, NY 14075" + "&key=AIzaSyD0x7RGskXkJEo_LuXC0jrjAHqLv6IBXHc";
                            string url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + tripString + "&key=AIzaSyD0x7RGskXkJEo_LuXC0jrjAHqLv6IBXHc";
                            string url1 = System.Text.RegularExpressions.Regex.Replace(url, "#", "%23");

                            log.Info("Processing Routes URL Request = " + url1);

                            try
                            {
                                System.Net.WebRequest request = System.Net.WebRequest.Create(url1);

                                System.Net.WebResponse response = request.GetResponse();
                                Stream data = response.GetResponseStream();
                                StreamReader reader = new StreamReader(data);

                                string responseFromServer = reader.ReadToEnd();

                                RootObject r = JsonConvert.DeserializeObject<RootObject>(responseFromServer);
                                if (r.status.Equals("OK"))
                                {
                                    log.Info("Processing Routes In OK Status = ");

                                    double distance = 0.0;
                                    foreach (Leg lg in r.routes[0].legs)
                                    {
                                        string[] distanceStrings = lg.distance.text.Split(new string[] { " " }, StringSplitOptions.None);
                                        if (!distanceStrings[1].ToLower().Equals("ft"))
                                        {
                                            distance += Convert.ToDouble(distanceStrings[0]);
                                        }
                                        newRoute.Status = "OK";
                                    }
                                    newRoute.Distance = distance;
                                
                                }
                                else
                                {
                                    log.Info("Processing Routes In status not ok = " + r.status);

                                    string err = r.status + " " + url1;

                                    //Save the error status to the Route table
                                    newRoute.Status = err;
                                    newRoute.Distance = 9.9;

                                }
                                response.Close();
                                try
                                {
                                    dispRepo.SaveRoute(newRoute);
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Processing Routes Route Error = " + ex.Message);
                                    log.Error("Processing Routes Route Error2 = " + ex.InnerException.Message);
                                    log.Error("Processing Routes Route Error3 = " + ex.InnerException.InnerException.Message);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("Processing Routes Bad in Google Maps request" + ex.Message);
                                newRoute.Status = " Bad Google Maps request: " + url1;
                            }
                        }
                    }
                }
                else
                {
                    log.Info("Processing Routes no date range");
                }
                log.Error("Processing Routes Processing Complete ");

                 List<Route> routes = dispRepo.GetRoutes(fr, to);
                log.Error("Processing Routes Route Display " + fr.ToShortDateString() + " " + to.ToShortDateString() + " Count = " + routes.Count);

                foreach (Route route in routes)
                {
                    RouteView rv = new RouteView();
                    rv.Distance = route.Distance.ToString();
                    Driver drv = driverRepo.GetByID(route.DriverID);
                    rv.DriverName = drv != null ? route.Driver.Name : "No Driver";
                    rv.Duration = route.TotalTime.ToString();
                    rv.Name = route.Name;
                    rv.Revenue = route.Revenue.ToString();
                    rv.Status = route.Status;
                    rv.Timestamp = route.Timestamp.ToShortDateString();

                    summaryList.Add(rv);
                }
            }
            catch (Exception ex)
            {
                log.Error("Processing Routes  Error = " + ex.Message);
            }
            JsonResult lst = Json(summaryList, JsonRequestBehavior.AllowGet);
            return lst;
            
        }
        public ActionResult ExportRoutes(string frDate, string toDate)
        {

            DateTime fr = DateTime.Now;
            if (frDate != "") fr = Convert.ToDateTime(frDate);
            DateTime to = DateTime.Now;
            if (toDate != "") to = Convert.ToDateTime(toDate).AddDays(1);
            List<Driver> allDrivers = (new DriverRepository()).GetAll();

            DispatchRepository dispRepo = new DispatchRepository();
            dispRepo.ExportRoutes(fr, to);

            return View("Route");
        }
        public ActionResult DeleteRoutes()
        {
            DispatchRepository dispRepo = new DispatchRepository();
            dispRepo.DeleteRoutes();

            return View("Route");
        }

    }
}