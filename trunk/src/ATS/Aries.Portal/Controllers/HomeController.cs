﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using log4net;
using Aries.Portal.Models;
using Aries.Model.Entities;
using Aries.Model.Repositories;
//test in GIT
namespace Aries.Portal.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HomeController));

        public HomeController()
        {
            ViewBag.UserName = (new EmployeeRepository()).GetEmployeeName(System.Web.HttpContext.Current.User.Identity.Name);
        }

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                // User already logged in - Check if it is a driver and redirect it to Timesheet/Index
                if (System.Web.HttpContext.Current.User.IsInRole("Driver"))
                {
                    log.InfoFormat("Home Index auth ");

                    string username = System.Web.HttpContext.Current.User.Identity.Name;
                    return RedirectPermanent("~/Timesheet/Index/" + (new EmployeeRepository()).GetByName(username).Id);  
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}