﻿using Aries.Model;
using Aries.Model.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using log4net;

namespace Aries.Portal.Controllers
{
    public partial class ManageController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TripController));

        private Aries.Model.AriesDataContext DbContext { get; } = new Model.AriesDataContext();
        
        public ActionResult Groups()
        {
            try
            {
                // Get All Employees
                var employees = DbContext.Employees.ToList()
                    .Select(employee => employee.ToEmployeeViewModel()).ToList();
                return View(employees);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Could not clear Groups.");
                return View("Error");
            }
        }
        
        [HttpGet]
        public JsonResult GetGroups()
        {
            try
            {
                // Get All Active Groups
                List<GroupView> groups = DbContext.Groups.ToList()
                    .Where(group => group.Active == true)
                    .Select(group => group.ToGroupViewModel()).ToList();

                return Json(groups, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Critical error occurred" });
            }
        }

        [HttpPost]
        public JsonResult GetGroupEmployees(int groupId)
        {
            try
            {                
                // Get Group Employees
                var employees = DbContext.GroupEmployeess.Where(employee => employee.GroupId == groupId).ToList();
                var employeeIds = employees.Select(emp => emp.EmployeeId);
                var fullEmployees = DbContext.Employees.Where(employee => employeeIds.Contains(employee.Id)).ToList();
                var groupEmployees = fullEmployees.Select(employee => employee.ToEmployeeViewModel()).ToList();

                return Json(groupEmployees);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", "Could not add Process.");
                return null;
            }
        }

        [HttpPost]
        public JsonResult AddGroup(GroupView groupToAdd)
        {
            try
            {
                // Validate Employee List
                groupToAdd.Employees = groupToAdd.Employees == null
                    ? new List<GroupEmployeeView>() : groupToAdd.Employees;

                // Create Group
                Group group = new Group
                {
                    Id = groupToAdd.Id,
                    Name = groupToAdd.Name,
                    Description = groupToAdd.Description,
                    CreationDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    Active = true,
                    Employees = new List<GroupEmployee>()
                };

                // Save Group
                DbContext.Groups.Add(group);
                DbContext.SaveChanges();
                groupToAdd.Id = group.Id;
                
                // Add Employees To Group
                foreach (var employee in groupToAdd.Employees)
                {
                    GroupEmployee newEmployee = new GroupEmployee()
                    {
                        GroupId = group.Id,
                        EmployeeId = employee.EmployeeId,
                        CreationDate = DateTime.Now
                    };
                    group.Employees.Add(newEmployee);
                }
                DbContext.SaveChanges();

                return Json(groupToAdd);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Critical error occurred" });
            }
        }

        [HttpPost]
        public HttpStatusCodeResult UpdateGroup(GroupView groupToUpdate)
        {
            try
            {
                // Validate Employee List
                groupToUpdate.Employees = groupToUpdate.Employees == null 
                    ? new List<GroupEmployeeView>() : groupToUpdate.Employees;
                
                // Get Group
                Group group = DbContext.Groups.Single(e => e.Id == groupToUpdate.Id);
                group.Name = groupToUpdate.Name;
                group.Description = groupToUpdate.Description;
                group.LastModifiedDate = DateTime.Now;

                // Remove Existing Employees From Group
                DbContext.GroupEmployeess.RemoveRange(group.Employees);
                DbContext.SaveChanges();

                // Add Employees To Group
                foreach (var employee in groupToUpdate.Employees)
                {
                    GroupEmployee newEmployee = new GroupEmployee()
                    {
                        GroupId = group.Id,
                        EmployeeId = employee.EmployeeId,
                        CreationDate = DateTime.Now                        
                    };
                    group.Employees.Add(newEmployee);
                }

                // Save Group Changes
                var result = DbContext.SaveChanges();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpDelete]
        public HttpStatusCodeResult ClearGroups()
        {
            try
            {
                // Set All Groups To Inactive
                List<int> groupIds =
                    DbContext.Groups.Select(e => e.Id).ToList();
                groupIds.ForEach(eId =>
                {
                    Group group = DbContext.Groups.Single(e => e.Id == eId);
                    group.Active = false;
                    //DbContext.Groups.Remove(group);
                });
                DbContext.SaveChanges();
                
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        public HttpStatusCodeResult RemoveGroup(int groupId)
        {
            try
            {
                // Get Group
                Group group = DbContext.Groups.Single(e => e.Id == groupId);

                // Remove Group
                DbContext.Groups.Remove(group);
                DbContext.SaveChanges();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }        
        
        [HttpPost]
        public HttpStatusCodeResult SendAlert(int groupId, string message)
        {
            try
            {
                // Get Group
                Group group = DbContext.Groups.Single(e => e.Id == groupId);
                List<GroupEmployee> grpList = DbContext.GroupEmployeess.Where(e => e.GroupId == group.Id).ToList();
                List<string> empList = new List<string>();
                foreach (GroupEmployee grpEmp in grpList) {
                    string emp = grpEmp.Employee.MobileAppKey;

                    empList.Add(emp);
                }
                //var joined2 = from p in DbContext.Employees
                //              join grp in DbContext.GroupEmployeess
                //              on p.Id equals grp.EmployeeId
                //              select p;
                
                List<HttpStatusCode> alertResponses = new List<HttpStatusCode>();
                
                foreach (var med in empList)
                {
                    // Send Alert To Driver
                    var result = this.SendAlertToDriver(med, message);
                    alertResponses.Add(result);                  
                }

                // Get Number Of Successful Alerts Sent
                var countSuccessful = alertResponses.Where(response => response == HttpStatusCode.OK).Count();
                log.Info("Alerts Sent: " + countSuccessful + " / " + alertResponses.Count);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Info("SendAlert Exception" + ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        private HttpStatusCode SendAlertToDriver(string emp, string message)
        {
            try
            {
                message = "Broadcast: " + message;

                // Send Message
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServerURI"].ToString());                
                var response = client.GetAsync("api/trip/SendNotice?key=" + emp +
                                                "&messageType=" + MobileMessageTypes.Broadcast +
                                                "&message=" + message).Result;

                //log.Info("SendAlertToDriver " + driver.MedTransId.ToString());
                // Read The Response.
                Task<string> result = response.Content.ReadAsStringAsync();
                string res = result.Result;

                ErrorCode resOK;
                Enum.TryParse<ErrorCode>(ErrorCode.OK.ToString(), out resOK);

                HttpStatusCode httpResult = response.StatusCode == System.Net.HttpStatusCode.OK && resOK == ErrorCode.OK 
                    ? HttpStatusCode.OK 
                    : HttpStatusCode.InternalServerError;
                
                return httpResult;
            }
            catch (Exception ex)
            {
                log.Info("SendAlertToDriver Exception" + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }
    }
}