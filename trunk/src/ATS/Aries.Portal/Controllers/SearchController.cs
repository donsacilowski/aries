﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DayPilot;
using DayPilot.Web;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Data;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Calendar;
using DayPilot.Web.Mvc.Json;

using Aries.Portal.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Utility;

using log4net;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

using OfficeOpenXml;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Aries.Portal.Controllers
{
    public class SearchController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SearchController));
      
        /// <summary>
        /// Class Constructor
        /// </summary>
        public SearchController()
        {
            ViewBag.UserName = (new EmployeeRepository()).GetEmployeeName(System.Web.HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Return default page when driver signs in. 
        /// </summary>
        /// <param name="Id">Employee ID</param>
        /// <returns></returns>
        public ActionResult Index(int Id = 0, string date = "")
        {
            SearchViewModel vm = new SearchViewModel();

            CustomerRepository customerRepo = new CustomerRepository();
            DispatchRepository dispatchRepo = new DispatchRepository();
            DriverRepository driverRepo = new DriverRepository();

            vm.DriverList = driverRepo.GetAll().GroupBy(p => p.Name)
              .Select(g => g.First()).OrderBy(p=>p.Name)
              .ToList();

            vm.CustomerList = customerRepo.GetAll().GroupBy(p => p.Name)
              .Select(g => g.First()).OrderBy(p => p.Name)
              .ToList();

            return View(vm);
        }

        public JsonResult _SearchResults(SearchViewModel model)
        {
            JsonResult lst = Json(GetResults(model), JsonRequestBehavior.AllowGet);
            lst.MaxJsonLength = int.MaxValue; // Do not remove this. Will get Ajax error on the search results
            return lst;
        }
        public ActionResult ExportSearch(SearchViewModel model)
        {
            try
            {
                List<SearchResultsViewModel> resModel = GetResults(model);
                string imageFile = "ExportedSearch_" + model.selectedDriverItem + "_" + 
                                                       model.selectedCustomerItem + 
                                                       DateTime.Now.ToString("MM_dd_yyyy HH_mm") + ".csv";
                string searchExcelLocation = ConfigurationManager.AppSettings["ExportedSearchLocation"].ToString();
                string imageFullName = System.IO.Path.Combine(searchExcelLocation, imageFile);

                string[] lines = new string[resModel.Count + 2];
                int lineNumber = 0;
                lines[lineNumber++] = "Driver,Customer,Date of Trip";
                foreach (SearchResultsViewModel res in resModel)
                {
                    lines[lineNumber++] = res.DriverName + "," + res.CustomerName.Replace(",", "") + "," + res.Date;
                }

                System.IO.File.WriteAllLines(imageFullName, lines);

                return Redirect("~/Search/Index");
            }
            catch (Exception _Exception)
            {
                log.ErrorFormat("Error exporting Search results " + _Exception.Message, _Exception);

                return Redirect("~/Search/Index");
            }
        }

        private List<SearchResultsViewModel> GetResults(SearchViewModel model)
        {
            DateTime? dtFill = null;
            DateTime? dtExp = null;

            DateTime temp = new DateTime();
            if (DateTime.TryParse(model.StartDate, out temp))
            {
                dtFill = Convert.ToDateTime(model.StartDate);
            }

            if (DateTime.TryParse(model.EndDate, out temp))
            {
                dtExp = Convert.ToDateTime(model.EndDate);
            }
            int pID = 0;

            CustomerRepository customerRepo = new CustomerRepository();
            DispatchRepository dispatchRepo = new DispatchRepository();
            DriverRepository driverRepo = new DriverRepository();

            List<Dispatch> dispatchList = dispatchRepo.GetAll().Where(d => d.Timestamp >= Convert.ToDateTime(model.StartDate) &&
                                                                           d.Timestamp <= Convert.ToDateTime(model.EndDate)).ToList();

            List<SearchResultsViewModel> vm = new List<SearchResultsViewModel>();
            foreach (Dispatch disp in dispatchList)
            {
                if ((model.selectedCustomerItem != null && disp.Customer.Name == model.selectedCustomerItem) ||
                    (model.selectedDriverItem != null && disp.Driver.Name == model.selectedDriverItem))
                {
                    SearchResultsViewModel searchVM = new SearchResultsViewModel();
                    Customer cust = customerRepo.GetByID(disp.CustomerID);
                    searchVM.Date = Convert.ToDateTime(disp.Timestamp).ToShortDateString();
                    searchVM.DriverName = driverRepo.GetByID(disp.DriverID).Name;
                    searchVM.CustomerName = cust.Name;
                    searchVM.Address = cust.Address.Street + ", " +
                                       cust.Address.City + ", " +
                                       cust.Address.State + ", " +
                                       cust.Address.PostalCode;


                    vm.Add(searchVM);
                }
            }
            return vm;
        }

    }
}
