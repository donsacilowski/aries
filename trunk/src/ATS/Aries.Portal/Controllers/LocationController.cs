﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DayPilot;
using DayPilot.Web;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Data;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Calendar;
using DayPilot.Web.Mvc.Json;

using Aries.Portal.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Utility;

using log4net;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

using OfficeOpenXml;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Aries.Portal.Controllers
{
    public class LocationController : Controller
    {
        #region Some Propeties
    
        private static readonly ILog log = LogManager.GetLogger(typeof(TimesheetController));
        
        int BusinessHourStart
        {
            get
            {
                int iValue = 0;
                string sValue = System.Configuration.ConfigurationManager.AppSettings["BusinessHourStart"].ToString();
                if (int.TryParse(sValue, out iValue) == false)
                {
                    iValue = 7;
                }
                return iValue;
            }
        }

        /// <summary>
        /// Get Business end time from configuration setting. Default to 18 (6 PM)
        /// </summary>
        int BusinessHourEnd
        {
            get
            {
                int iValue = 0;
                string sValue = System.Configuration.ConfigurationManager.AppSettings["BusinessHourEnd"].ToString();
                if (int.TryParse(sValue, out iValue) == false)
                {
                    iValue = 18;
                }
                return iValue;
            }
        }
        bool EmployeeApprovedFirst
        {
            get
            {
                return false;
            }
        }

        bool OfficeApprovedFirst
        {
            get
            {
                return true;
            }
        }
        #endregion


        /// <summary>
        /// Class Constructor
        /// </summary>
        public LocationController()
        {
            ViewBag.UserName = (new EmployeeRepository()).GetEmployeeName(System.Web.HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Return default page when driver signs in. 
        /// </summary>
        /// <param name="Id">Employee ID</param>
        /// <returns></returns>
        public ActionResult Index(int Id = 0, string date = "")
        {
            return View();
        }

        /// <summary>
        /// Summary for all location alerts
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public ActionResult Summary(string date = "")
        {
            DateTime theDate = new DateTime();
            DateTime.TryParse(date, out theDate);
            if (theDate == new DateTime())
            {
                theDate = DateTime.Today.Date;
            }

            ViewBag.StartDate = (new Misc()).FirstDayOfWeek(theDate, DayOfWeek.Monday);
            ViewData["LocationAlertSummary"] = (new LocationRepository()).GetLocationAlertSummary(theDate);
            return View();
        }
         public void Add(string username) {
            Employee emp = (new EmployeeRepository()).GetByName(username);
            log.Info("Logging Location for " + username);
            (new LocationRepository()).Add(emp.Id);
         }
        
    }
}