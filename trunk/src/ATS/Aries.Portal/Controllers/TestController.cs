﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Json;

using Aries.Portal.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Portal.Controllers
{
    [HandleError]
    public class TestController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Address()
        {
            return View(new Address
            {
                Street = "",
                City = "",
                State = "",
                PostalCode = ""
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Address(FormCollection form)
        {
            (new AddressRepository()).Add(new Address
            {
                Street = Convert.ToString(form["Street"]),
                City = Convert.ToString(form["City"]),
                State = Convert.ToString(form["State"]),
                PostalCode = Convert.ToString(form["PostalCode"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        public ActionResult Customer()
        {
            List<Address> addresses = (new AddressRepository()).GetAll();
            List<string> lsAddress = new List<string>();
            foreach (Address addr in addresses)
            {
                lsAddress.Add(addr.Id + "-" + addr.Street + " // " + addr.City + "//" + addr.State + "//" + addr.PostalCode);
            }
            ViewData["Addresses"] = lsAddress;
            return View(new Customer
            {
                Name = "",
                PhoneNumber = "",
                AddressID = 0,
                FC8Id = 0,
                MedTransId = 0
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Customer(FormCollection form)
        {
            string[] addr = Convert.ToString(form["Address"]).Split('-');
            (new CustomerRepository()).Add(new Customer
            {
                Name = Convert.ToString(form["Name"]),
                PhoneNumber = Convert.ToString(form["PhoneNumber"]),
                AddressID = Convert.ToInt32(addr[0]),
                FC8Id = Convert.ToInt32(form["FC8Id"]),
                MedTransId = Convert.ToInt32(form["MedTransId"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }


        public ActionResult Vehicle()
        {
            return View(new Vehicle
            {
                LicensePlateNumber = "",
                Description = "",
                FC8Id = 0,
                MedTransId = 0
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Vehicle(FormCollection form)
        {
            (new VehicleRepository()).Add(new Vehicle
            {
                LicensePlateNumber = Convert.ToString(form["LicensePlateNumber"]),
                Description = Convert.ToString(form["Description"]),
                FC8Id = Convert.ToInt32(form["FC8Id"]),
                MedTransId = Convert.ToInt32(form["MedTransId"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }


        public ActionResult Driver()
        {
            List<Vehicle> vehicles = (new VehicleRepository()).GetAll();
            List<string> lsVehicles = new List<string>();
            foreach (Vehicle vel in vehicles)
            {
                lsVehicles.Add(vel.Id + "-" + vel.LicensePlateNumber + " // " + vel.Description);
            }
            ViewData["Vehicles"] = lsVehicles;
            return View(new Driver
            {
                Name = "",
                PhoneNumber = "",
                DriversLicenseNumber = "",
                VehicleID = 0,
                FC8Id = 0,
                MedTransId = 0
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Driver(FormCollection form)
        {
            string[] addr = Convert.ToString(form["Vehicle"]).Split('-');
            (new DriverRepository()).Add(new Driver
            {
                Name = Convert.ToString(form["Name"]),
                PhoneNumber = Convert.ToString(form["PhoneNumber"]),
                DriversLicenseNumber = Convert.ToString(form["DriversLicenseNumber"]),
                VehicleID = Convert.ToInt32(addr[0]),
                FC8Id = Convert.ToInt32(form["FC8Id"]),
                MedTransId = Convert.ToInt32(form["MedTransId"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        public ActionResult Payment()
        {
            return View(new Payment
            { 
                PaymentToDriver = false,
                PaymentToDriverAmount = 0,
                Paid = false
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Payment(FormCollection form)
        {
            bool paid = false;
            if (Convert.ToString(form["Paid"]).Substring(0, 4).ToUpper() == "TRUE")
            {
                paid = true;
            }

            bool paidToDriver = false;
            if (Convert.ToString(form["PaymentToDriver"]).Substring(0, 4).ToUpper() == "TRUE")
            {
                paidToDriver = true;
            }
            
            (new PaymentRepository()).Add(new Payment
            {
                PaymentToDriver = paidToDriver,
                Paid = paid,
                PaymentToDriverAmount = Convert.ToDecimal(form["PaymentToDriverAmount"]),
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }


        public ActionResult Order()
        {
            List<Driver> drivers = (new DriverRepository()).GetAll();
            List<string> lsDrivers = new List<string>();
            foreach (Driver d in drivers)
            {
                lsDrivers.Add(d.Id + "-" + d.Name);
            }
            ViewData["Drivers"] = lsDrivers;

            List<Customer> customers = (new CustomerRepository()).GetAll();
            List<string> lsCustomers = new List<string>();
            foreach (Customer  c in customers)
            {
                lsCustomers.Add(c.Id + "-" + c.Name);
            }
            ViewData["Customers"] = lsCustomers;

            List<Address> addresses = (new AddressRepository()).GetAll();
            List<string> lsAddress = new List<string>();
            foreach (Address addr in addresses)
            {
                lsAddress.Add(addr.Id + "-" + addr.Street + " // " + addr.City + "//" + addr.State + "//" + addr.PostalCode);
            }
            ViewData["Addresses"] = lsAddress;

            List<Payment> payments = (new PaymentRepository()).GetAll();
            List<string> lsPayments = new List<string>();
            foreach (Payment p in payments)
            {
                lsPayments.Add(p.Id + "-" + p.PaymentToDriverAmount.ToString());
            }
            ViewData["Payments"] = lsPayments;

            return View(new Dispatch
            {
                //Timestamp = DateTime.Now,
                //DriverID  = 0,
                //CustomerID = 0,
                //PaymentID = 0,
                //PickupTime = DateTime.Now,
                //OrderNumber  = "",
                //AddressID = 0 ,
                //Notes = "",
                //Service = "",
                //ServiceType = "",
                //DNLA = false
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Order(FormCollection form)
        {
            string[] addr = Convert.ToString(form["Vehicle"]).Split('-');
            (new DriverRepository()).Add(new Driver
            {
                Name = Convert.ToString(form["Name"]),
                PhoneNumber = Convert.ToString(form["PhoneNumber"]),
                DriversLicenseNumber = Convert.ToString(form["DriversLicenseNumber"]),
                VehicleID = Convert.ToInt32(addr[0]),
                FC8Id = Convert.ToInt32(form["FC8Id"]),
                MedTransId = Convert.ToInt32(form["MedTransId"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

        public ActionResult Status()
        {
            List<Dispatch> dispatches = (new DispatchRepository()).GetAll();
            List<string> lsDispatches = new List<string>();
            foreach (Dispatch d in dispatches)
            {
                //lsDispatches.Add(d.Id + "-" + d.OrderNumber+ " // " + d.Timestamp);
            }
            ViewData["Dispatches"] = lsDispatches;
            return View(new Status
            {
                StatusListID = (int)Statuses.Open,
                Timestamp = DateTime.Now,
                DispatchID = 0
            });
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Status(FormCollection form)
        {
            string[] addr = Convert.ToString(form["Vehicle"]).Split('-');
            (new DriverRepository()).Add(new Driver
            {
                Name = Convert.ToString(form["Name"]),
                PhoneNumber = Convert.ToString(form["PhoneNumber"]),
                DriversLicenseNumber = Convert.ToString(form["DriversLicenseNumber"]),
                VehicleID = Convert.ToInt32(addr[0]),
                FC8Id = Convert.ToInt32(form["FC8Id"]),
                MedTransId = Convert.ToInt32(form["MedTransId"])
            });
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }

    }
}