﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DayPilot;
using DayPilot.Web;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Data;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Calendar;
using DayPilot.Web.Mvc.Json;

using Aries.Portal.Models;
using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;
using Aries.Utility;

using log4net;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

using OfficeOpenXml;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Aries.Portal.Controllers
{
    public class TimesheetController : Controller
    {
        #region Some Propeties

        private const int headerLength = 2;
        private const int dailyLogHeaderLength = 6;
        private static readonly ILog log = LogManager.GetLogger(typeof(TimesheetController));
        public enum EarnCodes
        {
            RegHours = 0, // actual code "2"
            OTHours = 1, // actual code "3"
            VacHours = 2, // actual code "5"
            BereavementHours = 3, // actual code "9"
            HolidayHours = 4 // actual code "4"
        }
        /// <summary>
        /// Get Business start time from configuration setting. Default to 7
        /// </summary>
        string timeSheetLocation = ConfigurationManager.AppSettings["ExportedTimesheetsLocation"].ToString();
        string driverLogLocation = ConfigurationManager.AppSettings["ExportedDriversLogLocation"].ToString();

        int BusinessHourStart
        {
            get
            {
                int iValue = 0;
                string sValue = System.Configuration.ConfigurationManager.AppSettings["BusinessHourStart"].ToString();
                if (int.TryParse(sValue, out iValue) == false)
                {
                    iValue = 7;
                }
                return iValue;
            }
        }

        /// <summary>
        /// Get Business end time from configuration setting. Default to 18 (6 PM)
        /// </summary>
        int BusinessHourEnd
        {
            get
            {
                int iValue = 0;
                string sValue = System.Configuration.ConfigurationManager.AppSettings["BusinessHourEnd"].ToString();
                if (int.TryParse(sValue, out iValue) == false)
                {
                    iValue = 18;
                }
                return iValue;
            }
        }
        bool EmployeeApprovedFirst
        {
            get
            {
                return false;
            }
        }

        bool OfficeApprovedFirst
        {
            get
            {
                return true;
            }
        }
        #endregion


        /// <summary>
        /// Class Constructor
        /// </summary>
        public TimesheetController()
        {
            ViewBag.UserName = (new EmployeeRepository()).GetEmployeeName(System.Web.HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Return default page when driver signs in. 
        /// </summary>
        /// <param name="Id">Employee ID</param>
        /// <returns></returns>
        public ActionResult Index(int Id = 0, string date = "")
        {
            // Parsing/Collecting info 
            int userId = (Id > 0) ? Id : (new EmployeeRepository()).GetByName(HttpContext.User.Identity.Name).Id;
            DateTime theDate = (date == "") ? DateTime.Today : DateTime.Parse(date);
            DateTime thisMonday = (new Misc()).FirstDayOfWeek(DateTime.Today, DayOfWeek.Monday);
            DateTime thisSunday = thisMonday.AddDays(6);
            bool bCurrentWeek = ((theDate.Date >= thisMonday.Date) && (theDate.Date <= thisSunday.Date));
            bool bOfficeApproved = (new Helpers.Misc()).OfficeApproved(userId, thisMonday, thisSunday);
            bool bEmployeeApproved = (new Helpers.Misc()).EmployeeApproved(userId, thisMonday, thisSunday);
            bool bSignedIn = false;     // Assume it is not current work week
            bool bSignedOut = false;    // Assume it is not current work week
            bool bAdmin = (new Helpers.Misc()).IsAdmin();
            bool bManager = (new Helpers.Misc()).IsManager();
            bool bDispatch = (new Helpers.Misc()).IsDispatch();

            DateTime selectMonday = (new Misc()).FirstDayOfWeek(theDate, DayOfWeek.Monday);
            DateTime selectSunday = selectMonday.AddDays(6);


            ViewBag.UserID = userId;
            ViewBag.BusinessHourStart = BusinessHourStart;
            ViewBag.BusinessHourEnd = BusinessHourEnd;
            ViewBag.CurrentWeek = bCurrentWeek;
            ViewBag.WeekOf = selectMonday.ToString(@"MM\/dd\/yyyy") + " - " + selectSunday.ToString(@"MM\/dd\/yyyy");
            ViewBag.StartDate = theDate;
            ViewBag.IsDriver = (new Helpers.Misc()).IsDriver();
            ViewBag.IsAdmin = bAdmin;
            ViewBag.IsManager = bManager;
            ViewBag.IsDispatch = bDispatch;

            if (bCurrentWeek == true)
            {
                // In current work week - Check if employee has signed in yet
                bSignedIn = (new Helpers.Misc()).EmployeeSignedIn(userId);
                bSignedOut = !bSignedIn;
            }

            ViewBag.SignedIn = bSignedIn;
            ViewBag.SignedOut = bSignedOut;
            ViewBag.LunchTaken = (new Helpers.Misc()).EmployeeTookLunch(userId);
            ViewBag.OfficeApproved = bOfficeApproved;
            ViewBag.EmployeeApproved = bEmployeeApproved;

            ViewBag.OfficeApprovedText = bOfficeApproved ? "YES" : "NO";
            ViewBag.EmployeeApprovedText = bEmployeeApproved ? "YES" : "NO";

            Totals(userId, selectMonday, selectSunday);

            return View();
        }

        /// <summary>
        /// Employee clock-in timesheet
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult ClockIn(int Id, DateTime dt)
        {
            (new TimesheetRepository()).SignIn(Id, dt);
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }

        /// <summary>
        /// Employee clock-out timesheet
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 

        public ActionResult ClockOut(int Id, DateTime date)
        {
            (new TimesheetRepository()).SignOut(Id, date);
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }

        /// <summary>
        /// Employee is taking 30 minutes lunch
        /// </summary>
        /// <param name="Id"></param>
        /// /// <returns></returns>
        [OutputCache(Duration = 0, VaryByParam = "*")]
        [HttpGet]        
        public ActionResult Lunch(int Id = 0)
        {
            (new TimesheetRepository()).TakeLunch(Id, 30);
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }
        public ActionResult Lunch30(int Id, DateTime date)
        {
            (new TimesheetRepository()).TakeLunch(Id, 30);
            return Redirect("~/Timesheet/Edit?Id=" + Id.ToString() + "&date=" + date.ToString(@"MM\/dd\/yyyy"));
        }
        public ActionResult Lunch60(int Id, DateTime date)
        {
            (new TimesheetRepository()).TakeLunch(Id, 60);
            return Redirect("~/Timesheet/Edit?Id=" + Id.ToString() + "&date=" + date.ToString(@"MM\/dd\/yyyy"));
        }
        public ActionResult Lunch30Summary(int Id, DateTime date)
        {
            (new TimesheetRepository()).TakeLunch(Id, 30);
            return Redirect("~/Timesheet/LunchSummary");
        }
        public ActionResult Lunch60Summary(int Id, DateTime date)
        {
            (new TimesheetRepository()).TakeLunch(Id, 60);
            return Redirect("~/Timesheet/LunchSummary");
        }

        public ActionResult UpdateLunch(int Id, DateTime startdate, DateTime enddate)
        {
            log.Error("Update Lunch");
            Timesheet timesheet = (new TimesheetRepository()).GetTimesheet(Id, DateTime.Today, false);
            if (timesheet == null)
                Redirect("~/Timesheet/LunchSummary");

            TimesheetDetail lst = (new TimesheetRepository()).FindLunch(timesheet.Id);

            if (lst == null) {
                TimesheetDetail newlst = new TimesheetDetail();
                newlst.ServiceCode = "Lunch";
                newlst.TimeIn = startdate;
                newlst.TimeOut = enddate;
                newlst.TimesheetID = timesheet.Id;

                (new TimesheetRepository()).AddDetail(newlst);
            } else {
                lst.TimeIn = startdate;
                lst.TimeOut = enddate;
                (new TimesheetRepository()).UpdateDetail(lst);
            }
            /* Get the time charged that lies within the lunch period */

            /*  */
            TimesheetDetail tdTC1 = (new TimesheetRepository()).GetEmployeeTimesheetDetails(Id)
                .Where(td => td.TimeIn < startdate && td.TimeOut < enddate && td.TimeOut > startdate && td.TimeIn.Date == DateTime.Now.Date
                       && td.ServiceCode == "Time-Charged").FirstOrDefault();

            TimesheetDetail tdTC2 = (new TimesheetRepository()).GetEmployeeTimesheetDetails(Id)
                .Where(td => td.TimeIn >= startdate && td.TimeOut <= enddate && td.TimeIn.Date == DateTime.Now.Date
                       && td.ServiceCode == "Time-Charged").FirstOrDefault();

            TimesheetDetail tdTC3 = (new TimesheetRepository()).GetEmployeeTimesheetDetails(Id)
                .Where(td => td.TimeIn > startdate && td.TimeOut > enddate && td.TimeIn.Date == DateTime.Now.Date
                       && td.ServiceCode == "Time-Charged").FirstOrDefault();

            TimesheetDetail tdTC4 = (new TimesheetRepository()).GetEmployeeTimesheetDetails(Id)
                .Where(td => td.TimeIn < startdate && td.TimeOut > enddate && td.TimeIn.Date == DateTime.Now.Date
                       && td.ServiceCode == "Time-Charged").FirstOrDefault();

            if (tdTC1 != null)
            {
                /* Lunch is contained within time charged */
                tdTC1.TimeOut = startdate;
                (new TimesheetRepository()).UpdateDetail(tdTC1);
            }

            if (tdTC2 != null)
            {
                /* Time charged is totally contained within lunch */
                (new TimesheetRepository()).DeleteDetail(tdTC2.Id);
            }

            if (tdTC3 != null)
            {
                tdTC3.TimeIn = enddate;
                (new TimesheetRepository()).UpdateDetail(tdTC3);
            }

            if (tdTC4 != null)
            {               
                TimesheetDetail newDetail = new TimesheetDetail();
                newDetail.TimesheetID = timesheet.Id;
                newDetail.ServiceCode = "Time-Charged";
                newDetail.TimeIn = enddate;
                newDetail.TimeOut = tdTC4.TimeOut;
                (new TimesheetRepository()).AddDetail(newDetail);

                tdTC4.TimeOut = startdate;
                (new TimesheetRepository()).UpdateDetail(tdTC4);
            }
            return Redirect("~/Timesheet/LunchSummary");
        }


        public ActionResult PrevWeek(int Id = 0)
        {
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }

        public ActionResult ThisWeek(int Id = 0)
        {
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }

        public ActionResult NextWeek(int Id = 0)
        {
            return RedirectPermanent("~/Timesheet/Index/" + Id);
        }


        /// <summary>
        /// Timesheet summary for all employees
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public ActionResult Summary(string date = "")
        {
            DateTime theDate = new DateTime();
            DateTime.TryParse(date, out theDate);
            if (theDate == new DateTime())
            {
                theDate = DateTime.Today.Date;
            }
            bool bAdmin = (new Helpers.Misc()).IsAdmin();
            bool bManager = (new Helpers.Misc()).IsManager();
            bool bDispatch = (new Helpers.Misc()).IsDispatch();

            ViewBag.isAdmin = bAdmin;
            ViewBag.isManager = bManager;
            ViewBag.isDispatch = bDispatch;

            string role = "ALL";
            if (bDispatch) role = "Dispatch";
            ViewBag.StartDate = (new Misc()).FirstDayOfWeek(theDate, DayOfWeek.Monday);
            ViewData["TimesheetSummary"] = (new TimesheetRepository()).GetTimesheetSummary(theDate, role);
            return View();
        }

        /// <summary>
        /// Editing timesheet landing page
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id, DateTime date)
        {
            Employee editEmployee = (new EmployeeRepository()).GetByID(Id);
            DateTime startDate = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime endDate = startDate.AddDays(6);
            bool bAdmin = (new Helpers.Misc()).IsAdmin();   
            bool bManager = (new Helpers.Misc()).IsManager();
            bool bDispatch = (new Helpers.Misc()).IsDispatch();
            bool bIsDispatchTimesheet = (new Helpers.Misc()).IsDispatch(Id);
            bool bIsAdminTimesheet = (new Helpers.Misc()).IsAdmin(Id);
            bool bIsManagerTimesheet = (new Helpers.Misc()).IsManager(Id);
            bool bDriver = (new Helpers.Misc()).IsDriver(Id);
            int userID = (new EmployeeRepository()).GetByName(HttpContext.User.Identity.Name).Id;
            bool bIsDispatchEditTimeAllowed = bIsDispatchTimesheet && (Id == userID) && !bIsAdminTimesheet && !bIsManagerTimesheet;
            bool bEmployeeApproved = (new Helpers.Misc()).EmployeeApproved(Id, startDate, endDate);
            bool bOfficeApproved = (new Helpers.Misc()).OfficeApproved(Id, startDate, endDate);

            ViewBag.Name = editEmployee.FirstName + " " + editEmployee.LastName;
            ViewBag.EditUserName = editEmployee.LastName + ", " + editEmployee.FirstName + (editEmployee.MI == null ? "" : " " + editEmployee.MI);
            ViewBag.UserID = Id;
            ViewBag.ViewType = DayPilot.Web.Mvc.Enums.Calendar.ViewType.Days;
            ViewBag.BusinessHourStart = BusinessHourStart;
            ViewBag.BusinessHourEnd = BusinessHourEnd;
            ViewBag.StartDate = startDate;            
            ViewBag.Days = 7;
            ViewBag.OfficeApproved = bOfficeApproved;
            ViewBag.EmployeeApproved = bEmployeeApproved;
            ViewBag.Editable = (!bOfficeApproved);
            ViewBag.CanDisApproved = ((bAdmin || bManager) && bOfficeApproved);
            ViewBag.isAdmin = bAdmin;
            ViewBag.isDispatch = bDispatch;
            ViewBag.isDispatchEditTimeAllowed = bIsDispatchEditTimeAllowed;
            ViewBag.isDriver = (editEmployee.Role == "Driver") ? true : false;
            ViewBag.isManager = bManager;
            ViewBag.CanApprove = ((bManager && 
                                    (editEmployee.Role == "Office" ||
                                     editEmployee.Role == "Maintenance" ||
                                     editEmployee.Role == "Monitors" ||
                                     editEmployee.Role == "Trainers" ||
                                     editEmployee.Role == "Driver") ||
                                     editEmployee.Role == "Dispatch")) 
                                   || (bAdmin);
            ViewBag.OfficeApprovedText = bOfficeApproved ? "YES" : "NO";
            ViewBag.EmployeeApprovedText = bEmployeeApproved ? "YES" : "NO";
            DateTime thisMonday = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime thisSunday = thisMonday.AddDays(6);

            ViewBag.WeekOf = thisMonday.ToString(@"MM\/dd\/yyyy") + " - " + thisSunday.ToString(@"MM\/dd\/yyyy");

            Totals(Id, startDate, endDate);

            //log.ErrorFormat("Error exporting timesheet " + _Exception.Message, _Exception);

            return View();
        }

        public ActionResult OfficeApproveAll(DateTime date)
        {
            (new TimesheetRepository()).OfficeApproveAllTimesheet(date);
            return Redirect("~/Timesheet/Summary?date=" + date.ToString(@"MM\/dd\/yyyy"));
        }

        public ActionResult ExportTimesheet(DateTime date)
        {
            string imageFile = "ExportedTimesheets" + Timestamped(date.ToString()).ToString("MM_dd_yyyy HH_mm")+".csv";
            string imageFullName = System.IO.Path.Combine(timeSheetLocation, imageFile);

            try
            {
                EmployeeRepository empRepo = new EmployeeRepository();
                TimesheetRepository timeRepo = new TimesheetRepository();
                List<TimesheetSummary> allTimesheets = timeRepo.GetTimesheetSummary(date.Date);

                string delimiter = ",";
                string[] lines = new string[(allTimesheets.Count*5)+headerLength];
                int lineNumber = 0;

                lines[lineNumber++] = "Employeee Number,Employee Name,Department,Job,Shift,D/E,Earn Code,Rate,Hours,Year," +
                                      "Month,Day,Hour,Minute,Amount,Seq Number,Override Division,Override Branch,Override State,Override Local," + 
                                      "Fill,Deduction hours,Deduction amount,SSN,Workers Compensation,Rate Number";
                                      //,Regular Hours," +
                                      //"Overtime Hours,Holiday Hours,Vacation Hours, Bereavement Hours";

                log.Info("Exporting timesheets number " + allTimesheets.Count.ToString());

                string[] earnCode = { "02", "03", "05", "09", "04" };
                string department = "";
                
                foreach (TimesheetSummary ts in allTimesheets)
                {
                    string name = ts.EmployeeName;
                    log.Info("Exporting timesheets for " + ts.EmployeeName);
                    
                    string empNumber = "Not Entered";
                    Employee empobj = empRepo.GetByID(ts.EmployeeId);
                    if (!String.IsNullOrEmpty(empobj.EmployeeNumber)) {
                        empNumber = "=" + "\"" + Convert.ToUInt32(empobj.EmployeeNumber).ToString("D4") + "\"";
                        if (empobj.Role == "Driver") department = "705.01";
                        if (empobj.Role == "Office") department = "706.01";
                        if (empobj.Role == "Dispatch") department = "706.05";
                        if (empobj.Role == "Trainers") department = "706.10";
                        if (empobj.Role == "Monitors") department = "706.15";
                        if (empobj.Role == "Maintenance") department = "706.20";
                        if (empobj.Role == "Manager") department = "706.25";
                        if (empobj.Role == "Admin") department = "Admin";

                        if (empobj.DeptCodeOverride) department = empobj.DeptCode;

                        department = "=" + "\"" + department + "\"";
                    }


                    string holHours = Convert.ToString(ts.Sun.Holiday + ts.Mon.Holiday +
                                      ts.Tue.Holiday + ts.Wed.Holiday +
                                      ts.Thu.Holiday + ts.Fri.Holiday +
                                      ts.Sat.Holiday);

                    string vacHours = Convert.ToString(ts.Sun.Vacation + ts.Mon.Vacation +
                                      ts.Tue.Vacation + ts.Wed.Vacation +
                                      ts.Thu.Vacation + ts.Fri.Vacation +
                                      ts.Sat.Vacation);

                    string bereavementHours = Convert.ToString(ts.Sun.Bereavement + ts.Mon.Bereavement +
                                      ts.Tue.Bereavement + ts.Wed.Bereavement +
                                      ts.Thu.Bereavement + ts.Fri.Bereavement +
                                      ts.Sat.Bereavement);

                    name = name.Replace(',', ' ');

                    if (ts.Total.Reg > 0) {
                        lines[lineNumber++] = empNumber + delimiter + name + delimiter + department + delimiter + delimiter + delimiter +
                                              "E" + delimiter + earnCode[(int)EarnCodes.RegHours] + delimiter + delimiter + ts.Total.Reg + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              "0" + delimiter + "1" +
                                              delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              delimiter;
                    }
                    if (ts.Total.OT > 0)
                    {
                        lines[lineNumber++] = empNumber + delimiter + name + delimiter + department + delimiter + delimiter + delimiter +
                                              "E" + delimiter + earnCode[(int)EarnCodes.OTHours] + delimiter + delimiter + ts.Total.OT + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              "0" + delimiter + "1" +
                                              delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              delimiter;
                    }
                    if (Convert.ToDecimal(holHours) > 0)
                    {
                        lines[lineNumber++] = empNumber + delimiter + name + delimiter + department + delimiter + delimiter + delimiter +
                                              "E" + delimiter + earnCode[(int)EarnCodes.HolidayHours] + delimiter  + delimiter + holHours + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              "0" + delimiter + "1" +
                                              delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              delimiter;
                    }
                    if (Convert.ToDecimal(vacHours) > 0)
                    {
                        lines[lineNumber++] = empNumber + delimiter + name + delimiter + department + delimiter + delimiter + delimiter +
                                              "E" + delimiter + earnCode[(int)EarnCodes.VacHours] + delimiter + delimiter + vacHours + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              "0" + delimiter + "1" +
                                              delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              delimiter;
                    }
                    if (Convert.ToDecimal(bereavementHours) > 0)
                    {
                        lines[lineNumber++] = empNumber + delimiter + name + delimiter + department + delimiter + delimiter + delimiter +
                                              "E" + delimiter + earnCode[(int)EarnCodes.BereavementHours] + delimiter + delimiter + bereavementHours + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              "0" + delimiter + "1" +
                                              delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter +
                                              delimiter;
                    }
                    //holHours + delimiter + 
                    //vacHours + delimiter +
                    //bereavementHours;
                }

                System.IO.File.WriteAllLines(imageFullName, lines);

                return Redirect("~/Timesheet/Summary?date=" + date.ToString(@"MM\/dd\/yyyy"));
            }
            catch (Exception _Exception)
            {
                log.ErrorFormat("Error exporting timesheet " + _Exception.Message, _Exception);

                return Redirect("~/Timesheet/Summary?date=" + date.ToString(@"MM\/dd\/yyyy"));
            }
        }

        public ActionResult ExportIndividualTimesheet(string employeeName, DateTime date)
        {
            ExportSingleTimesheetPDF(employeeName, date);
            //string imageFile = "ExportedTimesheets" + employeeName + Timestamped(date.ToString()).ToString("MM_dd_yyyy HH_mm") + ".csv";
            //string imageFullName = System.IO.Path.Combine(timeSheetLocation, imageFile);
            ////string empName = employee.Last + ", " + employee.First + (employee.MI == null ? "" : " " + employee.MI);
            //try
            //{
            EmployeeRepository empRepo = new EmployeeRepository();
            Employee emp = empRepo.GetEmployeeByBridgeName(employeeName);
            //    TimesheetRepository timeRepo = new TimesheetRepository();
            //    TimesheetSummary ts = timeRepo.GetTimesheetSummary(date).Where(t=>t.EmployeeName==employeeName).FirstOrDefault();

            //    string delimiter = ",";
            //    string[] lines = new string[1 + headerLength];
            //    int lineNumber = 0;

            //    lines[lineNumber++] = "Employeee Number,Employee Name,Regular Hours,Overtime Hours,Holiday Hours,Vacation Hours";

            //    //foreach (TimesheetSummary ts in allTimesheets)
            //    //{
            //        string name = ts.EmployeeName;

            //        string empNumber = "Not Entered";
            //        Employee empobj = empRepo.GetByID(ts.EmployeeId);
            //        if (!String.IsNullOrEmpty(empobj.EmployeeNumber))
            //        {
            //            empNumber = empobj.EmployeeNumber;
            //        }
            //        string holHours = Convert.ToString(ts.Sun.Holiday + ts.Mon.Holiday +
            //                          ts.Tue.Holiday + ts.Wed.Holiday +
            //                          ts.Thu.Holiday + ts.Fri.Holiday +
            //                          ts.Sat.Holiday);

            //        string vacHours = Convert.ToString(ts.Sun.Vacation + ts.Mon.Vacation +
            //                          ts.Tue.Vacation + ts.Wed.Vacation +
            //                          ts.Thu.Vacation + ts.Fri.Vacation +
            //                          ts.Sat.Vacation);

            //        name = name.Replace(',', ' ');
            //        lines[lineNumber++] = empNumber + delimiter +
            //                              name + delimiter +
            //                              ts.Total.Reg + delimiter +
            //                              ts.Total.OT + delimiter +
            //                              holHours + delimiter +
            //                              vacHours;
            //   // }

            //    System.IO.File.WriteAllLines(imageFullName, lines);

            return Redirect("~/Timesheet/Edit?Id=" + emp.Id + "&date=" + date.ToString(@"MM\/dd\/yyyy"));
               // return Redirect("~/Timesheet/Summary?date=" + DateTime.Now.ToString(@"MM\/dd\/yyyy"));
            //    //return Redirect("~/Timesheet/Edit?Id =" + ts.EmployeeId.ToString() + " & date = " + date.ToString(@"MM\/ dd\/ yyyy"));


            //}

            //catch (Exception _Exception)
            //{
            //    log.ErrorFormat("Error exporting timesheet " + _Exception.Message, _Exception);

            //    return Redirect("~/Timesheet/Summary?date=" + DateTime.Now.ToString(@"MM\/dd\/yyyy"));
            //}
        }

        public ActionResult ExportAllDriverLog (DateTime date) {

            DispatchRepository dispRepo = new DispatchRepository();
            DriverRepository driverRepo = new DriverRepository();

            List<Dispatch> todaysDispatches = dispRepo.GetAllByDate(date).ToList();
            log.Info("Exporting Driver Log Number Dispatches = " + todaysDispatches.Count.ToString());

            List<string> allDrivers = new List<string>();
            foreach (Dispatch disp in todaysDispatches)
            {
                Driver dri = driverRepo.GetByID(disp.DriverID);
                if (!allDrivers.Contains(dri.Name)) allDrivers.Add(dri.Name);
            }
            log.Info("Exporting Driver Log Number Drivers = " + allDrivers.Count.ToString());

            foreach (string dri in allDrivers)
            {
                ExportSingleLog(dri, date);
            }

            return Redirect("~/Timesheet/Summary?date=" + DateTime.Now.ToString(@"MM\/dd\/yyyy"));
        }

        private void ExportSingleLog(string driverName, DateTime date) {

            try
            {
                string imageFile = "ExportedDriverLog_" + driverName + "_" +
                                        Timestamped(date.ToString()).ToString("MM_dd_yyyy HH_mm") + ".pdf";

                string imageFullName = System.IO.Path.Combine(driverLogLocation, imageFile);

                DispatchRepository dispRepo = new DispatchRepository();
                DriverRepository driverRepo = new DriverRepository();
                StatusRepository staRepo = new StatusRepository();

                List<Driver> driverList = driverRepo.GetListByName(driverName);

                List<Dispatch> allDispatches = dispRepo.GetDispatchListByDriver(driverList,
                                                                                date.Date,
                                                                                date.Date.AddDays(1));
                if (allDispatches.Count > 0)
                {
                    log.Info("Exporting Driver Log for " + driverName + " " + allDispatches.Count.ToString() + " " + driverList.Count.ToString());
                    PdfDocument myDoc = new PdfDocument();
                    PdfPage myPage = myDoc.AddPage();
                    myPage.Orientation = PdfSharp.PageOrientation.Landscape;
                    myPage.Size = PdfSharp.PageSize.Letter;
                    XGraphics graph = XGraphics.FromPdfPage(myPage);
                    XTextFormatter tf = new XTextFormatter(graph);

                    XFont font = new XFont("Verdana", 14, XFontStyle.Bold);
                    graph.DrawString("Aries Transportation Services", font, XBrushes.Black,
                                            new XRect(0, 0, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopCenter);
                    graph.DrawString("Medical Transportation Verification Log", font, XBrushes.Black,
                                            new XRect(0, 15, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopCenter);

                    font = new XFont("Verdana", 10, XFontStyle.Regular);

                    Pen pen = new Pen(Color.Black, 1);
                    pen.Alignment = PenAlignment.Inset; //<-- this
                    XRect rect = new XRect(10, 33, 780, 30);
                    graph.DrawRectangle(pen, XBrushes.LightGray, rect);

                    tf.Alignment = XParagraphAlignment.Left;
                    tf.DrawString("Driver: " + driverName, font, XBrushes.Black,
                                            new XRect(15, 35, myPage.Width.Point, 0), XStringFormats.TopLeft);
                    tf.Alignment = XParagraphAlignment.Right;
                    tf.DrawString("Date: " + date.Date.ToShortDateString(), font, XBrushes.Black,
                                            new XRect(0, 35, myPage.Width.Point - 30, 0), XStringFormats.TopLeft);

                    font = new XFont("Verdana", 10, XFontStyle.Bold);

                    int height = 40;

                    int xpos = 10;
                    int ypos = 63;
                    int width = 150;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Client Name/Address", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    xpos += width;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Client Destination", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    xpos += width;
                    width = 270;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Client Signature", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    xpos += width;
                    width = 50;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Vehicle", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    xpos += width;
                    width = 80;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Arrival Time", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    xpos += width;
                    rect = new XRect(xpos, ypos, width, height);
                    graph.DrawRectangle(pen, XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Center;
                    tf.DrawString("Dropoff Time", font, XBrushes.Black,
                                            rect, XStringFormats.TopLeft);

                    font = new XFont("Verdana", 10, XFontStyle.Regular);

                    int index = 0;
                    height = 40;
                    ypos += 30;

                    int count = 0;
                    bool addFooter = false;
                    foreach (Dispatch disp in allDispatches)
                    {
                        try
                        {
                            count++;
                            if (count == 7)
                            {
                                addFooter = false;
                                height = 40;

                                ypos += 20;
                                xpos = 12;
                                width = 780;
                                font = new XFont("Verdana", 12, XFontStyle.Bold);
                                rect = new XRect(xpos, ypos, width, height);
                                pen = new Pen(Color.LightGray, 1);
                                tf.Alignment = XParagraphAlignment.Center;
                                graph.DrawRectangle(XBrushes.White, rect);
                                tf.DrawString("*** IMPORTANT NOTICE TO PASSENGERS!***", font, XBrushes.Black,
                                                        new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                                ypos += 20;
                                rect = new XRect(xpos, ypos, width, height);
                                pen = new Pen(Color.LightGray, 1);
                                tf.Alignment = XParagraphAlignment.Left;
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                font = new XFont("Verdana", 8, XFontStyle.Regular);
                                string passNotice = "Please explain to all passengers that their signature is acknowledgement for us to provide their transportation. " +
                                                    "It gives us permission to transport them and acknowledges their acceptance of the service. " +
                                                    "***Transportation does not share your personal information., but is committed to protect such information.\r\n " +
                                                    "This acknowledgement is required by NYS Medicaid.";
                                tf.DrawString(passNotice, font, XBrushes.Black,
                                                        new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                                ypos += height + 20;
                                xpos = 180;
                                width = 300;
                                font = new XFont("Verdana", 8, XFontStyle.Bold);
                                rect = new XRect(xpos, ypos, width, height);
                                tf.Alignment = XParagraphAlignment.Center;
                                pen = new Pen(Color.LightGray, 1);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                passNotice = "I attest that transportation was provided to all of the persons on this roster";
                                tf.DrawString(passNotice, font, XBrushes.LightGray,
                                                        new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                                ypos += height + 40;
                                width = 500;
                                xpos = 12;
                                font = new XFont("Verdana", 12, XFontStyle.Regular);
                                rect = new XRect(xpos, ypos, width, height);
                                tf.Alignment = XParagraphAlignment.Left;
                                pen = new Pen(Color.Black, 1);

                                Driver driver = driverRepo.GetByName(driverName);
                                string driverFileName = "D" + driver.MedTransId.ToString() + ".bmp";
                                string driverFileFullName = System.IO.Path.Combine
                                        (ConfigurationManager.AppSettings["DriversSignedFilesLocation"].ToString(), driverFileName);

                                if (System.IO.File.Exists(driverFileFullName))
                                {

                                    XImage image = XImage.FromFile(driverFileFullName);
                                    graph.DrawImage(image, xpos + 100, ypos - 35, width - 250, height + 30);
                                }

                                passNotice = "Driver Signature:____________________________________";
                                tf.DrawString(passNotice, font, XBrushes.Black,
                                                        new XRect(xpos, ypos, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopLeft);

                                count = 0;
                                addFooter = true;
                                myPage = myDoc.AddPage();
                                myPage.Orientation = PdfSharp.PageOrientation.Landscape;
                                myPage.Size = PdfSharp.PageSize.Letter;
                                graph = XGraphics.FromPdfPage(myPage);
                                tf = new XTextFormatter(graph);

                                font = new XFont("Verdana", 14, XFontStyle.Bold);
                                graph.DrawString("Aries Transportation Services", font, XBrushes.Black,
                                                        new XRect(0, 0, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopCenter);
                                graph.DrawString("Medical Transportation Verification Log", font, XBrushes.Black,
                                                        new XRect(0, 15, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopCenter);

                                font = new XFont("Verdana", 10, XFontStyle.Regular);

                                pen = new Pen(Color.Black, 1);
                                pen.Alignment = PenAlignment.Inset; //<-- this
                                rect = new XRect(10, 33, 780, 30);
                                graph.DrawRectangle(pen, XBrushes.LightGray, rect);

                                tf.Alignment = XParagraphAlignment.Left;
                                tf.DrawString("Driver: " + driverName, font, XBrushes.Black,
                                                        new XRect(15, 35, myPage.Width.Point, 0), XStringFormats.TopLeft);
                                tf.Alignment = XParagraphAlignment.Right;
                                tf.DrawString("Date: " + date.Date.ToShortDateString(), font, XBrushes.Black,
                                                        new XRect(0, 35, myPage.Width.Point - 30, 0), XStringFormats.TopLeft);

                                font = new XFont("Verdana", 10, XFontStyle.Bold);

                                height = 40;

                                xpos = 10;
                                ypos = 63;
                                width = 150;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Client Name/Address", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                xpos += width;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Client Destination", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                xpos += width;
                                width = 270;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Client Signature", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                xpos += width;
                                width = 50;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Vehicle", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                xpos += width;
                                width = 80;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Arrival Time", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                xpos += width;
                                rect = new XRect(xpos, ypos, width, height);
                                graph.DrawRectangle(pen, XBrushes.White, rect);
                                tf.Alignment = XParagraphAlignment.Center;
                                tf.DrawString("Dropoff Time", font, XBrushes.Black,
                                                        rect, XStringFormats.TopLeft);

                                font = new XFont("Verdana", 10, XFontStyle.Regular);

                                index = 0;
                                height = 40;
                                ypos += 30;
                            }
                            //string name = disp.Customer.Name + " " + disp.Customer.Address.Street + " " +
                            //                                            disp.Customer.Address.City + " " +
                            //                                            disp.Customer.Address.State + " " +
                            //                                            disp.Customer.Address.PostalCode;
                            string name = disp.Customer.Name + " " + disp.PickupFullAddress;

                            name = name.Replace(',', ' ');

                            //string dest = disp.DropoffAddress.Street + " " +
                            //              disp.DropoffAddress.City + " " +
                            //              disp.DropoffAddress.State + " " +
                            //              disp.DropoffAddress.PostalCode;

                            string dest = disp.DropoffFullAddress;

                            string vehicleDesc = disp.Driver.Vehicle.Description;

                            Status orderStatus = staRepo.GetByStatusID((int)Statuses.ArrivedPickedUp, disp.Id);

                            string arrivalTime = "";
                            if (orderStatus != null) arrivalTime = orderStatus.Timestamp.ToLongTimeString();

                            orderStatus = staRepo.GetByStatusID((int)Statuses.DroppedOff, disp.Id);
                            string dropoffTime = "";
                            if (orderStatus != null) dropoffTime = orderStatus.Timestamp.ToLongTimeString();

                            XBrush brush = new XSolidBrush(XBrushes.White);
                            if ((index % 2 == 1))
                            {
                                brush = new XSolidBrush(XBrushes.LightGray);
                            }

                            if (disp.SignaturePath != null)
                            {
                                height = 102;
                            }
                            else
                            {
                                height = 40;
                            }

                            xpos = 10;
                            width = 150;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);
                            tf.Alignment = XParagraphAlignment.Center;
                            tf.DrawString(name, font, XBrushes.Black,
                                                    rect, XStringFormats.TopLeft);

                            xpos += width;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);
                            tf.Alignment = XParagraphAlignment.Center;
                            tf.DrawString(dest, font, XBrushes.Black,
                                                rect, XStringFormats.TopLeft);

                            xpos += width;
                            width = 270;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);

                            if (disp.SignaturePath != null)
                            {
                                log.Info("trying to get customer sig");
                                System.Threading.Thread.Sleep(1000);

                                XImage image = XImage.FromFile(disp.SignaturePath);
                                graph.DrawImage(image, xpos + 2, ypos + 2, width - 4, height - 4);
                            }
                            //tf.Alignment = XParagraphAlignment.Center;
                            //tf.DrawString("", font, XBrushes.Black,
                            //                     new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                            xpos += width;
                            width = 50;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);
                            tf.Alignment = XParagraphAlignment.Center;
                            tf.DrawString(vehicleDesc, font, XBrushes.Black,
                                                    rect, XStringFormats.TopLeft);

                            xpos += width;
                            width = 80;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);
                            tf.Alignment = XParagraphAlignment.Center;
                            tf.DrawString(arrivalTime, font, XBrushes.Black,
                                                    rect, XStringFormats.TopLeft);

                            xpos += width;
                            rect = new XRect(xpos, ypos, width, height);
                            graph.DrawRectangle(pen, brush, rect);
                            tf.Alignment = XParagraphAlignment.Center;
                            tf.DrawString(dropoffTime, font, XBrushes.Black,
                                                    rect, XStringFormats.TopLeft);

                            index++;

                            if (disp.SignaturePath != null)
                            {
                                ypos += 102;
                            }
                            else
                            {
                                ypos += 40;
                            }

                            if (addFooter)
                            {

                            }
                        }

                        catch (Exception ex)
                        {
                            log.Info("Error exporting Daily Log in loop" + ex.Message, ex);
                            System.Threading.Thread.Sleep(1000);
                        }
                    }

                    if (addFooter || count < 6)
                    {
                        addFooter = false;
                        height = 40;

                        ypos += 20;
                        xpos = 12;
                        width = 780;
                        font = new XFont("Verdana", 12, XFontStyle.Bold);
                        rect = new XRect(xpos, ypos, width, height);
                        pen = new Pen(Color.LightGray, 1);
                        tf.Alignment = XParagraphAlignment.Center;
                        graph.DrawRectangle(XBrushes.White, rect);
                        tf.DrawString("*** IMPORTANT NOTICE TO PASSENGERS!***", font, XBrushes.Black,
                                                new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                        ypos += 20;
                        rect = new XRect(xpos, ypos, width, height);
                        pen = new Pen(Color.LightGray, 1);
                        tf.Alignment = XParagraphAlignment.Left;
                        graph.DrawRectangle(pen, XBrushes.White, rect);
                        font = new XFont("Verdana", 8, XFontStyle.Regular);
                        string passNotice = "Please explain to all passengers that their signature is acknowledgement for us to provide their transportation. " +
                                            "It gives us permission to transport them and acknowledges their acceptance of the service. " +
                                            "***Transportation does not share your personal information., but is committed to protect such information.\r\n " +
                                            "This acknowledgement is required by NYS Medicaid.";
                        tf.DrawString(passNotice, font, XBrushes.Black,
                                                new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                        ypos += height + 20;
                        xpos = 180;
                        width = 300;
                        font = new XFont("Verdana", 8, XFontStyle.Bold);
                        rect = new XRect(xpos, ypos, width, height);
                        tf.Alignment = XParagraphAlignment.Center;
                        pen = new Pen(Color.LightGray, 1);
                        graph.DrawRectangle(pen, XBrushes.White, rect);
                        passNotice = "I attest that transportation was provided to all of the persons on this roster";
                        tf.DrawString(passNotice, font, XBrushes.LightGray,
                                                new XRect(xpos, ypos, width, myPage.Height.Point), XStringFormats.TopLeft);

                        ypos += height + 40;
                        width = 500;
                        xpos = 12;
                        font = new XFont("Verdana", 12, XFontStyle.Regular);
                        rect = new XRect(xpos, ypos, width, height);
                        tf.Alignment = XParagraphAlignment.Left;
                        pen = new Pen(Color.Black, 1);

                        Driver driver = driverRepo.GetByName(driverName);
                        string driverFileName = "D" + driver.MedTransId.ToString() + ".bmp";
                        string driverFileFullName = System.IO.Path.Combine
                                (ConfigurationManager.AppSettings["DriversSignedFilesLocation"].ToString(), driverFileName);

                        if (System.IO.File.Exists(driverFileFullName))
                        {

                            XImage image = XImage.FromFile(driverFileFullName);
                            graph.DrawImage(image, xpos + 100, ypos - 35, width - 250, height + 30);
                        }

                        passNotice = "Driver Signature:____________________________________";
                        tf.DrawString(passNotice, font, XBrushes.Black,
                                                new XRect(xpos, ypos, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopLeft);

                    }
                    myDoc.Save(imageFullName);
                }
            }
            catch (Exception ex)
            {
                log.Info("Exporting Driver log Error " + ex.Message);
            }
        }
        public ActionResult GetPdf(string filename)
        {
            string imageFile = "D:\\Timesheets\\ExportedTimesheetPDF_Cothran, Sam_10_22_2018 00_00.pdf";

            string imageFullName = System.IO.Path.Combine(driverLogLocation, imageFile);

            string filePath = imageFullName;
            return File(filePath, "application/pdf", imageFullName);
        }
        public ActionResult ExportAllTimesheets(DateTime date)
        {
            EmployeeRepository empRepo = new EmployeeRepository();
            List<Employee> empList = empRepo.GetByActive(true);

            foreach (Employee emp in empList)
            {
                ExportSingleTimesheetPDF(emp.LastName + ", " + emp.FirstName + (emp.MI == null ? "" : " " + emp.MI), date);
            }
            return Redirect("~/Timesheet/Summary?date=" + date.ToString(@"MM\/dd\/yyyy"));
        }

        private void ExportSingleTimesheetPDF(string employeeName, DateTime date)
        {

            string imageFile = "ExportedTimesheetPDF_" + employeeName + "_" +
                                    Timestamped(date.ToString()).ToString("MM_dd_yyyy HH_mm") + ".pdf";

            string imageFullName = System.IO.Path.Combine(timeSheetLocation, imageFile);
            DateTime thisMonday = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime thisSunday = thisMonday.AddDays(6);

            string weekOf = "(" + thisMonday.ToString(@"MM\/dd\/yyyy") + " - " + thisSunday.ToString(@"MM\/dd\/yyyy") + ")";

            EmployeeRepository empRepo = new EmployeeRepository();
            TimesheetRepository timeRepo = new TimesheetRepository();
            TimesheetSummary ts = timeRepo.GetTimesheetSummary(date).Where(t => t.EmployeeName == employeeName).FirstOrDefault();
            
            PdfDocument myDoc = new PdfDocument();
            PdfPage myPage = myDoc.AddPage();
            myPage.Orientation = PdfSharp.PageOrientation.Landscape;
            myPage.Size = PdfSharp.PageSize.Letter;
            XGraphics graph = XGraphics.FromPdfPage(myPage);
            XTextFormatter tf = new XTextFormatter(graph);

            XFont font = new XFont("Arial", 12, XFontStyle.Bold);
            graph.DrawString("Timesheet for " + ts.EmployeeName + " " + weekOf, font, XBrushes.Black,
                                    new XRect(0, 0, myPage.Width.Point, myPage.Height.Point), XStringFormats.TopCenter);

            font = new XFont("Arial", 10, XFontStyle.Bold);

            Pen pen = new Pen(Color.Black, 1);
            pen.Alignment = PenAlignment.Inset; 
            XRect rect = new XRect(400, 30, 370, 50);
            graph.DrawRectangle(pen, XBrushes.White, rect);

            tf.Alignment = XParagraphAlignment.Right;
            tf.DrawString("Hours Worked ", font, XBrushes.Black,
                                    new XRect(0, 35, myPage.Width.Point - 305, 0), XStringFormats.TopLeft);
            tf.DrawString("Credit Hours ", font, XBrushes.Black,
                                    new XRect(0, 35, myPage.Width.Point - 37, 0), XStringFormats.TopLeft);

            tf.DrawString("Regular ", font, XBrushes.Black,
                                    new XRect(0, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            tf.DrawString("Overtime ", font, XBrushes.Black,
                                    new XRect(50, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);

            tf.DrawString("Holiday ", font, XBrushes.Black,
                                    new XRect(100, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            tf.DrawString("Bereavement ", font, XBrushes.Black,
                                    new XRect(170, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            tf.DrawString("Vacation ", font, XBrushes.Black,
                                    new XRect(220, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            tf.DrawString("Regular ", font, XBrushes.Black,
                                    new XRect(270, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            tf.DrawString("Overtime ", font, XBrushes.Black,
                                    new XRect(320, 50, myPage.Width.Point - 350, 0), XStringFormats.TopLeft);
            string holHours = Convert.ToString(ts.Sun.Holiday + ts.Mon.Holiday +
                                                ts.Tue.Holiday + ts.Wed.Holiday +
                                                ts.Thu.Holiday + ts.Fri.Holiday +
                                                ts.Sat.Holiday);

            string vacHours = Convert.ToString(ts.Sun.Vacation + ts.Mon.Vacation +
                                                ts.Tue.Vacation + ts.Wed.Vacation +
                                                ts.Thu.Vacation + ts.Fri.Vacation +
                                                ts.Sat.Vacation);

            string bereavementHours = Convert.ToString(ts.Sun.Bereavement + ts.Mon.Bereavement +
                                                ts.Tue.Bereavement + ts.Wed.Bereavement +
                                                ts.Thu.Bereavement + ts.Fri.Bereavement +
                                                ts.Sat.Bereavement);

            CreditHour creditHour = (new TimesheetRepository()).GetEmployeeCreditHours(ts.EmployeeId, (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday));
            double regHour = 0;
            double overtime = 0;

            if (creditHour != null)
            {
                regHour = creditHour.RegularTime;
                overtime = creditHour.OverTime;
            }

            font = new XFont("Arial", 10, XFontStyle.Regular);

           // tf.DrawString((ts.Total.Reg - Convert.ToDouble(vacHours) - Convert.ToDouble(holHours) - Convert.ToDouble(bereavementHours)).ToString(), font, XBrushes.Black,
            tf.DrawString((ts.Total.Reg).ToString(), font, XBrushes.Black,
                new XRect(0, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
            tf.DrawString(ts.Total.OT.ToString(), font, XBrushes.Black,
                new XRect(50, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
            tf.DrawString(holHours, font, XBrushes.Black,
                new XRect(100, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
            tf.DrawString(bereavementHours.ToString(), font, XBrushes.Black,
                new XRect(155, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
            tf.DrawString(vacHours, font, XBrushes.Black,
                new XRect(220, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
           
            tf.DrawString(regHour.ToString(), font, XBrushes.Black,
                new XRect(270, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);
            tf.DrawString(overtime.ToString(), font, XBrushes.Black,
                new XRect(312, 65, myPage.Width.Point - 365, 0), XStringFormats.TopLeft);

            pen.Color = Color.LightGray;
            graph.DrawLine(pen, new Point(400, 50), new Point(770, 50));
            graph.DrawLine(pen, new Point(400, 65), new Point(770, 65));

            pen.Color = Color.Black;
            graph.DrawLine(pen, new Point(500, 30), new Point(500, 80));
            graph.DrawLine(pen, new Point(670, 30), new Point(670, 80));

            graph.DrawLine(pen, new Point(445, 50), new Point(445, 80));
            graph.DrawLine(pen, new Point(545, 50), new Point(545, 80));
            graph.DrawLine(pen, new Point(615, 50), new Point(615, 80));
            graph.DrawLine(pen, new Point(715, 50), new Point(715, 80));

            pen.Color = Color.LightGray;
            rect = new XRect(20, 100, 760, 500);
            graph.DrawRectangle(pen, XBrushes.White, rect);

            DateTime startdate = (new Misc()).FirstDayOfWeek(date, DayOfWeek.Monday);
            DateTime enddate = startdate.AddDays(6);

            var tsDetails = (new TimesheetRepository()).GetEmployeeTimesheetDetails(ts.EmployeeId, startdate, enddate).OrderBy(t => t.TimeIn);

            tf.DrawString("Monday " + startdate.Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 710, 0), XStringFormats.TopLeft);
            tf.DrawString("Tuesday " + startdate.AddDays(1).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 600, 0), XStringFormats.TopLeft);
            tf.DrawString("Wednesday " + startdate.AddDays(2).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 480, 0), XStringFormats.TopLeft);
            tf.DrawString("Thursday " + startdate.AddDays(3).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 370, 0), XStringFormats.TopLeft);
            tf.DrawString("Friday " + startdate.AddDays(4).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 270, 0), XStringFormats.TopLeft);
            tf.DrawString("Saturday " + startdate.AddDays(5).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 160, 0), XStringFormats.TopLeft);
            tf.DrawString("Sunday " + startdate.AddDays(6).Day, font, XBrushes.Black,
                                    new XRect(0, 105, myPage.Width.Point - 50, 0), XStringFormats.TopLeft);
            graph.DrawLine(pen, new Point(20, 120), new Point(780, 120));

            graph.DrawLine(pen, new Point(128, 100), new Point(128, 600));
            graph.DrawLine(pen, new Point(236, 100), new Point(236, 600));
            graph.DrawLine(pen, new Point(344, 100), new Point(344, 600));
            graph.DrawLine(pen, new Point(452, 100), new Point(452, 600));
            graph.DrawLine(pen, new Point(560, 100), new Point(560, 600));
            graph.DrawLine(pen, new Point(668, 100), new Point(668, 600));

            //Mondays intial position
            int height = 40;
            int xpos = 0;
            int ypos = 135;
            int width = 250;
            font = new XFont("Arial", 8, XFontStyle.Regular);
            int tsPrev = 0;
            int tsPres = 0;

            foreach (TimesheetDetail t in tsDetails)
            {
                tsPres = tsPrev;
                TimeSpan tSpan = t.TimeOut.Date.Subtract(startdate.Date);
                xpos = (tSpan.Days) * 108;
                tsPrev = tSpan.Days;

                if (tsPrev != tsPres) {
                    ypos = 135;
                }

                tf.Alignment = XParagraphAlignment.Center;
                tf.DrawString(t.ServiceCode, font, XBrushes.Black,
                                    new XRect(xpos, ypos, 140, 0), XStringFormats.TopLeft);
                ypos += 10;
                tf.DrawString("(" + t.TimeIn.ToShortTimeString() + " - " + t.TimeOut.ToShortTimeString() + ")", font, XBrushes.Black,
                                    new XRect(xpos, ypos, 140, 0), XStringFormats.TopLeft);
                ypos += 10;
                tf.DrawString(t.TimeOut.Subtract(t.TimeIn).ToString(@"h\:mm"), font, XBrushes.Black,
                                    new XRect(xpos, ypos, 140, 0), XStringFormats.TopLeft);
                ypos += 20;
            }

            xpos = 0;
            tf.DrawString((ts.Mon.Holiday + ts.Mon.Vacation + ts.Mon.Worked + ts.Mon.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Tue.Holiday + ts.Tue.Vacation + ts.Tue.Worked + ts.Tue.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Wed.Holiday + ts.Wed.Vacation + ts.Wed.Worked + ts.Wed.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Thu.Holiday + ts.Thu.Vacation + ts.Thu.Worked + ts.Thu.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Fri.Holiday + ts.Fri.Vacation + ts.Fri.Worked + ts.Fri.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Sat.Holiday + ts.Sat.Vacation + ts.Sat.Worked + ts.Sat.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);
            xpos += 108;
            tf.DrawString((ts.Sun.Holiday + ts.Sun.Vacation + ts.Sun.Worked + ts.Sun.Bereavement).ToString(), font, XBrushes.Black,
                                new XRect(xpos, 580, 140, 0), XStringFormats.TopLeft);

            myDoc.Save(imageFullName);
         }

        public ActionResult ExportDriverLog(string driverName, DateTime date)
        {
            try {
                ExportSingleLog(driverName, date);

                ////string imageFile = "D:\\Driver Logs\\ExportedDriverLog_Johnny Williams_05_23_2018 19_06.pdf";

                ////string imageFullName = System.IO.Path.Combine(driverLogLocation, imageFile);
                
                ////string filePath = imageFullName;
                ////return File(filePath, "application/pdf", imageFullName);
                return Redirect("~/Timesheet/Summary?date=" + DateTime.Now.ToString(@"MM\/dd\/yyyy"));
            } 
            catch (Exception _Exception)
            {
                log.Info("Error exporting Daily Log " + _Exception.Message, _Exception);
                
                return Redirect("~/Timesheet/Summary?date=" + DateTime.Now.ToString(@"MM\/dd\/yyyy"));
            }
        }
        private DateTime Timestamped(string timestamped)
        {
            DateTime dateValue;

            DateTime.TryParse(timestamped, out dateValue);
            // TODO: Need to implement conversion timestamped string into DateTime.

            return dateValue;
        }

        public ActionResult OfficeApprove(int Id, DateTime date)
        {
            (new TimesheetRepository()).OfficeApproveTimesheet(Id, date);
            return Redirect("~/Timesheet/Edit?Id=" + Id.ToString() + "&date=" + date.ToString(@"MM\/dd\/yyyy"));
        }

        public ActionResult OfficeUnApprove(int Id, DateTime date)
        {
            (new TimesheetRepository()).OfficeUnApproveTimesheet(Id, date);
            return Redirect("~/Timesheet/Edit?Id=" + Id.ToString() + "&date=" + date.ToString(@"MM\/dd\/yyyy"));
        }

        private void Totals(int userId, DateTime start_date, DateTime end_date)
        {
            TimeSpan[] ts = new TimeSpan[5];
            var tsDetails = (new TimesheetRepository()).GetEmployeeTimesheetDetails(userId, start_date, end_date);
            foreach (TimesheetDetail t in tsDetails)
            {
                switch (t.ServiceCode)
                {
                    case "Time-Charged":
                        ts[0] += t.TimeOut - t.TimeIn;
                        break;
                    case "Vacation":
                        ts[1] += t.TimeOut - t.TimeIn;
                        break;
                    case "Holiday":
                        ts[2] += t.TimeOut - t.TimeIn;
                        break;
                    case "Bereavement":
                        ts[3] += t.TimeOut - t.TimeIn;
                        break;
                }
                ts[4] += t.TimeOut - t.TimeIn;
            }

            double regHour = ts[0].TotalHours;
            double overtime = 0;
            if (ts[0].TotalHours > 40)
            {
                regHour = 40;
                overtime = ts[0].TotalHours - 40;
            }

            ViewBag.RegularHours = Math.Round(regHour, 2).ToString();
            ViewBag.OverTimeHours = Math.Round(overtime, 2).ToString();
            ViewBag.Holiday = Math.Round(ts[2].TotalHours, 2).ToString();
            ViewBag.Bereavement = Math.Round(ts[3].TotalHours, 2).ToString();
            ViewBag.Vacation = Math.Round(ts[1].TotalHours, 2).ToString();

            CreditHour creditHour = (new TimesheetRepository()).GetEmployeeCreditHours(userId, start_date);
            regHour = 0;
            overtime = 0;

            if (creditHour != null)
            {
                regHour = creditHour.RegularTime;
                overtime = creditHour.OverTime;
            }
            ViewBag.CreditRegularHours = Math.Round(regHour, 2).ToString();
            ViewBag.CreditOverTimeHours = Math.Round(overtime, 2).ToString();
        }


        public string EventEditable(string TimesheetDetailId, int employeeId)
        {
            if ((new EmployeeRepository()).GetByID(employeeId).Role != "Driver")
                return "Non-Driver";
            else
                return (new TimesheetRepository()).GetDetailByID(int.Parse(TimesheetDetailId)).ServiceCode;
        }


        #region DayPilot Calendar Event
        public ActionResult Backend(int Id = 0)
        {
            Dpc dpc = new Dpc();
            dpc.UserID = Id;
            
            return dpc.CallBack(this);
        }
        class Dpc : DayPilotCalendar
        {
            // Current/Selected UserId
            public int UserID { get; set; }

            // Default view-type to week.
            DayPilot.Web.Mvc.Enums.Calendar.ViewType viewType = DayPilot.Web.Mvc.Enums.Calendar.ViewType.Days;

            protected override void OnInit(InitArgs e)
            {
                Update(CallBackUpdateType.Full);
            }

            protected override void OnEventResize(EventResizeArgs e)
            {
                TimesheetRepository repo = new TimesheetRepository();
                repo.UpdateDetail(e.Id, (new Utility.Misc()).RoundTo15Minutes(e.NewStart), (new Utility.Misc()).RoundTo15Minutes(e.NewEnd));
                int employeeId = repo.GetDetailByID(int.Parse(e.Id)).Timesheet.EmployeeID;
                DateTime weekStartDate = (new Misc()).FirstDayOfWeek(e.NewStart.Date, DayOfWeek.Monday);
                
                //string editorName = System.Web.HttpContext.Current.User.Identity.Name;
                //int editorId = (new EmployeeRepository()).GetByName(editorName).Id;

                //
                //if (editorId == employeeId)
                //    Redirect("~/Timesheet/Index" + employeeId + "&date=" + weekStartDate.ToString(@"MM\/dd\/yyyy"));
                //else
                    Redirect("~/Timesheet/Edit?Id=" + employeeId + "&date=" + weekStartDate.ToString(@"MM\/dd\/yyyy"));
            }

            protected override void OnEventMove(EventMoveArgs e)
            {
                (new TimesheetRepository()).UpdateDetail(e.Id, (new Utility.Misc()).RoundTo15Minutes(e.NewStart), (new Utility.Misc()).RoundTo15Minutes(e.NewEnd));
                Update();
            }

            protected override void OnTimeRangeSelected(TimeRangeSelectedArgs e)
            {
                Update();
            }

            protected override void OnCommand(CommandArgs e)
            {
                int employeeId = 0;
                switch (e.Command)
                {
                    case "view-week":
                        WeekStarts = DayPilot.Web.Mvc.Enums.WeekStarts.Monday;
                        viewType = DayPilot.Web.Mvc.Enums.Calendar.ViewType.Days;
                        StartDate = (new Misc()).FirstDayOfWeek(DateTime.Today, DayOfWeek.Monday);
                        Days = 7;
                        Update(CallBackUpdateType.Full);
                        break;
                    case "view-day":
                        viewType = DayPilot.Web.Mvc.Enums.Calendar.ViewType.Day;
                        StartDate = DateTime.Today;
                        Days = 1;
                        Update(CallBackUpdateType.Full);
                        break;

                    case "navigate":
                        StartDate = (DateTime)e.Data["start"];
                        Update(CallBackUpdateType.Full);
                        break;

                    case "refresh":
                        //Update();
                        employeeId = (int)e.Data["employeeId"];
                        if (employeeId == 0)
                        {
                            Redirect("~/Timesheet/Index?date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                            //Redirect("~/Timesheet/Index");
                        }
                        else
                        {
                            Redirect("~/Timesheet/Edit?Id=" + employeeId + "&date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                        }
                        break;

                    case "previous":
                        StartDate = StartDate.AddDays(-1 * Days);
                        //Update();
                        employeeId = (int)e.Data["employeeId"];
                        Redirect("~/Timesheet/Index?Id=" + employeeId + "&date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                        break;

                    case "next":
                        StartDate = StartDate.AddDays(Days);
                        //Update();
                        employeeId = (int)e.Data["employeeId"];
                        Redirect("~/Timesheet/Index?Id=" + employeeId + "&date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                        break;

                    case "today":
                        StartDate = DateTime.Today;
                        //Update();
                        (new TimesheetRepository()).EmployeeApproveTimesheet(employeeId, StartDate);
                        Redirect("~/Timesheet/Index?Id=" + employeeId + "&date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                        break;
                    case "employeeApprove":
                        employeeId = (int)e.Data["employeeId"];
                        (new TimesheetRepository()).EmployeeApproveTimesheet(employeeId, StartDate);
                        Redirect("~/Timesheet/Index?Id=" + employeeId + "&date=" + StartDate.ToString(@"MM\/dd\/yyyy"));
                        break;
                }
            }

            protected override void OnFinish()
            {
                // only load the data if an update was requested by an Update() call
                if (UpdateType == CallBackUpdateType.None)
                {
                    return;
                }

                //EventCorners = DayPilot.Web.Mvc.Enums.Calendar.

                // Set view type
                ViewType = DayPilot.Web.Mvc.Enums.Calendar.ViewType.Days;
                Days = 7;
                WeekStarts = DayPilot.Web.Mvc.Enums.WeekStarts.Monday;
                BusinessBeginsHour = 7;
                BusinessEndsHour = 16;

                StartDate = (new Misc()).FirstDayOfWeek(StartDate, DayOfWeek.Monday);
                
                CellDuration = 15;
                TimeHeaderCellDuration = 30;
                HeaderDateFormat = "dddd d";


                // grab timesheetdetails (events) list 
                Events = (new TimesheetRepository()).GetEmployeeTimesheetDetailsDisplay(UserID, StartDate, StartDate.AddDays(Days - 1));

                // Assigning fields 
                DataIdField = "Id";
                DataTextField = "ServiceCode";
                DataStartField = "TimeIn";
                DataEndField = "TimeOut";
            }
        }
        #endregion

        #region Time Off Requests
        public JsonResult GetTimeOffRequests()
        {
            List<TimeoffSummary> lst = (new TimeOffRepository()).GetAll();
            JsonResult jlst = Json(lst, JsonRequestBehavior.AllowGet);
            return jlst;
        }
        public ActionResult TimeOffRequests()
        {
            return View();
        }
        public ActionResult LunchSummary()
        {
            return View();
        }
        public JsonResult GetLunchSummary()
        {
            List<Employee> empList = (new EmployeeRepository()).GetAll().OrderBy(e=>e.LastName).ToList();
            List<LunchSummary> lunchSummary = new List<LunchSummary>();

            foreach (Employee emp in empList)
            {
                LunchSummary lSum = new LunchSummary();
                Timesheet timesheet = (new TimesheetRepository()).GetByEmployeeIdAndDate(emp.Id, DateTime.Now.Date);
                if (timesheet != null)
                {
                    TimesheetDetail lst = (new TimesheetRepository()).FindLunch(timesheet.Id);

                    if (lst != null)
                    {
                        lSum.EmployeeName = timesheet.Employee.FirstName + " " + timesheet.Employee.LastName;
                        lSum.ID = timesheet.EmployeeID;
                        lSum.LunchStart = lst.TimeIn.ToString("hh:mm tt");
                        lSum.LunchStart = Convert.ToDateTime(lst.TimeIn).ToShortTimeString();

                        lSum.LunchEnd = lst.TimeOut.ToString("HH:mm");
                        lSum.LunchEnd = Convert.ToDateTime(lst.TimeOut).ToShortTimeString();

                        lunchSummary.Add(lSum);
                    } else {
                        lSum.EmployeeName = timesheet.Employee.FirstName + " " + timesheet.Employee.LastName;
                        lSum.ID = timesheet.EmployeeID;
                        lSum.LunchStart = "Not Entered";
                        lSum.LunchEnd = "Not Entered";

                        lunchSummary.Add(lSum);

                    }
                }
            }
            JsonResult jlst = Json(lunchSummary, JsonRequestBehavior.AllowGet);
            return jlst;
        }

        public ActionResult HolidaySummary()
        {
            return View();
        }
        public ActionResult UpdateHoliday(int Id, int length)
        {
            log.Error("Update Holiday");
            (new EmployeeRepository()).UpdateHoliday(Id, length);
            return Redirect("~/Timesheet/HolidaySummary");
        }
        [HttpPost]
        public JsonResult Holiday(DateTime date)
        {
            log.Error("Updating Holiday All Employees");
            try { 
                List<Employee> empList = (new EmployeeRepository()).GetAll().OrderBy(e => e.LastName).Where(e => e.Active == true).ToList();
                foreach (Employee emp in empList)
                {
                    log.Error("Updating Holiday Employee " + emp.LastName + " " + emp.FirstName + " " + emp.Id.ToString());

                    HolidayHours empHrs = (new EmployeeRepository().GetHoliday(emp.Id));
                    log.Error("Updating Holiday after emphrs " + emp.LastName + " " + emp.FirstName + " " + emp.Id.ToString());
                    Timesheet timesheet = (new TimesheetRepository()).GetByEmployeeIdAndDate(emp.Id, date);
                    log.Error("Updating Holiday after timesheet " + emp.LastName + " " + emp.FirstName + " " + emp.Id.ToString());
                    if (timesheet == null)
                    {
                        log.Error("Updating Holiday timesheet null ");
                        Timesheet ts = new Timesheet();
                        ts.EmployeeID = emp.Id;
                        ts.Date = date;
                        ts.EmployeeApproved = false;
                        ts.OfficeApproved = false;
                        ts.Notes = "";
                        (new TimesheetRepository()).Add(ts);
                    }
                    //log.Error("Updating Holiday after adding new timesheet " + emp.LastName + " " + emp.FirstName + " " + emp.Id.ToString());
                    //timesheet = (new TimesheetRepository()).GetByEmployeeIdAndDate(emp.Id, date);
                    //log.Error("Updating Holiday after gettin newly added timesheet " + emp.LastName + " " + emp.FirstName + " " + emp.Id.ToString());

                    (new TimesheetRepository()).AddHoliday(emp.Id, date);
                }
            }
            catch (Exception ex)
            {
                log.Error("Updating Holiday Error" + ex.Message);
            }
            return Json(new {success = true});
        }
        public JsonResult GetHolidaySummary()
        {
            List<Employee> empList = (new EmployeeRepository()).GetAll().OrderBy(e => e.LastName).Where(e=>e.Active==true).ToList();
            List<HolidaySummary> holidaySummary = new List<HolidaySummary>();

            foreach (Employee emp in empList)
            {
                HolidaySummary lSum = new HolidaySummary();
                HolidayHours holiday = (new EmployeeRepository()).GetHoliday(emp.Id);
                lSum.EmployeeName = emp.FirstName + " " + emp.LastName;
                lSum.ID = emp.Id;
                if (holiday != null)
                {
                    lSum.HolidayLength = holiday.Length.ToString();
                }
                else
                {
                    lSum.HolidayLength = "Not Entered";
                }
                holidaySummary.Add(lSum);

            }
            JsonResult jlst = Json(holidaySummary, JsonRequestBehavior.AllowGet);
            return jlst;
        }

        public ActionResult UpdateApproval(int id, string state)
        {
            TimeOffRequest toff = (new TimeOffRepository()).GetByID(id);

            if (state.Equals("Accepted")) toff.ApprovalStatus = true;
            if (state.Equals("Rejected")) toff.ApprovalStatus = false;

            toff.ApprovalState = "Entered";

            (new TimeOffRepository()).Update(toff);

            return RedirectToAction("TimeOffRequests", "Timesheet");
        }
        public ActionResult Submit(int id)
        {
            try
            {
                TimeOffRequest toff = (new TimeOffRepository()).GetByID(id);

                toff.ApprovalState = "Message Sent";

                (new TimeOffRepository()).Update(toff);
                
                Employee emp = (new EmployeeRepository()).GetByID(toff.EmployeeID);
                
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServerURI"].ToString());

                string app = toff.ApprovalStatus == true ? "Approved" : "Rejected";
                string message = "Time Off Request for "+toff.TimeOffRequested.ToShortDateString()  +
                                 " for " + toff.AmountOfTime + "hrs was " + app;
                var response = client.GetAsync("api/trip/SendNotice?key=" + emp.MobileAppKey +
                                                "&messageType=" + MobileMessageTypes.Broadcast +
                                                "&message=" + message).Result;
                log.Info("AfterGetAsync");
                HttpContent content = response.Content;

                // ... Read the string.
                Task<string> result = content.ReadAsStringAsync();
                string res = result.Result;
                log.Info("After Reading resonse string");

                ErrorCode resOK;
                Enum.TryParse<ErrorCode>(ErrorCode.OK.ToString(), out resOK);

                if (response.StatusCode == System.Net.HttpStatusCode.OK && resOK == ErrorCode.OK)
                {
                    log.Info("After Reading resonse string Successful");
                }
                else
                {
                    log.Info("After Reading resonse string Unsuccessful");
                }
            } catch (Exception ex) {
                log.Error("Submit Error" + ex.Message);
            }

            return RedirectToAction("TimeOffRequests", "Timesheet");
        }
        public bool TimeOffRequest(string username, string timeRequested, string dateRequested, string comments)
        {
            Employee emp = (new EmployeeRepository()).GetByName(username);

            TimeOffRequest tReq = new TimeOffRequest();
            tReq.AmountOfTime = Convert.ToDouble(timeRequested);
            tReq.ApprovalStatus = false;
            tReq.Comments = comments;
            tReq.EmployeeID = emp.Id;
            tReq.ApprovalState = "Not Entered";

            DateTime theDate = new DateTime();
            DateTime.TryParse(dateRequested, out theDate);
            tReq.TimeOffRequested = theDate;

            tReq.TimeOfRequest = DateTime.Now;

            (new TimeOffRepository()).Add(tReq);
            return true;
        }
        #endregion
    }
}