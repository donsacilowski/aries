﻿using System.Web;
using System.Web.Optimization;

namespace Aries.Portal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/underscore.js",
                "~/Scripts/bootbox.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/calendar.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/SiteBootstrapBundle").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-datepicker.css",
                "~/Content/layout.css",
                "~/Content/elements.css",
                "~/Content/calendar.css",
                "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/DataTableBundle").Include(
                "~/Content/jquery.dataTables.css",
                "~/Content/jquery.dataTables.min.css"));

            bundles.Add(new StyleBundle("~/Content/calendar").Include(
                "~/Content/calendar/*.html"));


            // JEO
            
            bundles.Add(new ScriptBundle("~/bundles/dataTablesScripts").Include(
                      "~/Content/DataTables/dataTables.js"));

            bundles.Add(new StyleBundle("~/Content/DataTableBundle").Include(
                   "~/Content/jquery.dataTables.css",
                   "~/Content/jquery.dataTables.min.css"));

            // Datatables - add after we confirm direct links contain everything we need
            bundles.Add(new StyleBundle("~/Content/dataTablesCss").Include(
                     "~/Content/DataTables/dataTables.min.css"));

            //BundleTable.EnableOptimizations = false;
        }
    }
}
