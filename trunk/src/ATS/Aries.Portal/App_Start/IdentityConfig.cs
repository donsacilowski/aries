﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Aries.Portal.Models;
using Aries.Utility;

using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Portal
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            (new EmailUtility()).AsyncSendMessage(message.Destination, message.Subject, message.Body);

            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<Model.Entities.ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<Model.Entities.ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<Model.Entities.ApplicationUser>(context.Get<Model.AriesDataContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<Model.Entities.ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<Model.Entities.ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<Model.Entities.ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<Model.Entities.ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            string[] userNames = new string[] { "jieraci" };
            string[] LastNames = new string[] { "Ieraci" };
            string[] FirstNames = new string[] { "Joe",  };
            string[] MIs = new string[] { "" };
            string[] Roles = new string[] { "Admin" };

            for (int i = 0; i < userNames.Length; i++)
            {
                if (manager.FindByName(userNames[i]) == null)
                {
                    var user = new Model.Entities.ApplicationUser { UserName = userNames[i], First = FirstNames[i], Last = LastNames[i], MI = MIs[i] };
                    manager.Create(user, "Passord1!");
                    manager.AddToRole(user.Id, Roles[i]);
                }
            }
            for (int i = 0; i < userNames.Length; i++)
            {
                // Add limited info to the Employee table
                if ((new EmployeeRepository()).GetByName(userNames[i]) == null)
                {
                    Employee employee = new Employee
                    {
                        UserName = userNames[i],
                        LastName = LastNames[i],
                        FirstName = FirstNames[i],
                        MI = MIs[i],
                        Role = Roles[i],
                        MobileAppKey = "",
                        Active = true,
                    };
                    (new EmployeeRepository()).Add(employee);
                };
            }

            return manager;
        }
    }

    public class ApplicationRoleManager : RoleManager<IdentityRole> {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore) {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context) {
            var manager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<Model.AriesDataContext>()));

            string[] roles = new string[] { "Admin",  "Dispatch", "Driver", "Maintenance" , "Manager", "Office", "Monitors", "Trainers" };

            foreach (string role in roles)
            {
                var userRole = new IdentityRole(role);
                if (!manager.RoleExists(userRole.Name))
                {
                    manager.Create(userRole);
                }
            }

            return manager;
        }
    }


    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<Model.Entities.ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(Model.Entities.ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
