﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aries.Model;
using Aries.Model.Entities;
using Aries.Model.Repositories;

namespace Aries.Portal.Helpers
{
    public class Misc
    {
        public bool IsAdmin()
        {
            return System.Web.HttpContext.Current.User.IsInRole("Admin");
        }
        public bool IsManager()
        {
            return System.Web.HttpContext.Current.User.IsInRole("Manager");
        }
        public bool IsDispatch()
        {
            return System.Web.HttpContext.Current.User.IsInRole("Dispatch");
        }
        public bool IsDispatch(int userId)
        {
            return ((new EmployeeRepository()).GetByID(userId).Role == "Dispatch");
        }
        public bool IsDriver()
        {
            return System.Web.HttpContext.Current.User.IsInRole("Driver");
        }

        public bool IsDriver(int userId)
        {
            return ((new EmployeeRepository()).GetByID(userId).Role == "Driver");
        }

        public bool IsManager(int userId)
        {
            return ((new EmployeeRepository()).GetByID(userId).Role == "Manager");
        }
        public bool IsAdmin(int userId)
        {
            return ((new EmployeeRepository()).GetByID(userId).Role == "Admin");
        }

        public bool OfficeApproved(int userId, DateTime startDate, DateTime endDate)
        {
            List<TimesheetDetail> timesheets = (new TimesheetRepository()).GetEmployeeTimesheetDetails(userId, startDate, endDate);
            return OfficeApproved(timesheets);          
        }
        public bool OfficeApproved(List<TimesheetDetail> userTimesheets)
        {
            bool bOfficeApproved = (userTimesheets.Count > 0);
            foreach (TimesheetDetail td in userTimesheets)
            {
                if (td.Timesheet.OfficeApproved == false)
                {
                    bOfficeApproved = false;
                    break;
                }
            }
            return bOfficeApproved;       
        }

        public bool EmployeeApproved(int userId, DateTime startDate, DateTime endDate)
        {
            List<TimesheetDetail> timesheets = (new TimesheetRepository()).GetEmployeeTimesheetDetails(userId, startDate, endDate);
            return EmployeeApproved(timesheets);
        }
        public bool EmployeeApproved(List<TimesheetDetail> userTimesheets)
        {
            bool bEmployeeApproved = (userTimesheets.Count > 0);
            foreach (TimesheetDetail td in userTimesheets)
            {
                if (td.Timesheet.EmployeeApproved== false)
                {
                    bEmployeeApproved = false;
                    break;
                }
            }
            return bEmployeeApproved;              
        }

        public bool EmployeeTookLunch(int userId)
        {
            Timesheet timesheet = (new TimesheetRepository()).GetByEmployeeIdAndDate(userId, DateTime.Now.Date);
            if (timesheet == null)
                return false;

            TimesheetDetail timesheetDetail = (new TimesheetRepository()).FindLunch(timesheet.Id);
            if (timesheetDetail == null)
                return false;
            else
                return true;
            
            //List<TimesheetDetail> timesheets = (new TimesheetRepository()).GetEmployeeTimesheetDetails(userId, DateTime.Today, DateTime.Today);
            //return (timesheets.Count > 1);
        }
        public bool EmployeeSignedIn(int userId)
        {
            List<TimesheetDetail> timesheets = (new TimesheetRepository()).GetEmployeeTimesheetDetails(userId, DateTime.Today, DateTime.Today);
            bool bSignedIn = false;
            foreach (TimesheetDetail t in timesheets)
            {
                if (t.TimeIn == t.TimeOut)
                {
                    bSignedIn = true;
                    break;
                }
            }
            return bSignedIn;
        }

    }
}