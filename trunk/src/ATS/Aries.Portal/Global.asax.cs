﻿using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using System.Web.Http;
//using Aries.DataAccess;
using log4net;
using log4net.Config;

namespace Aries.Portal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ILog log;

        protected void Application_Start()
        {
            // Setup log4net
            XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(Server.MapPath("~/log4net.config")));
            log = LogManager.GetLogger("Aries Portal");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebAPI.WebApiConfig.Register(GlobalConfiguration.Configuration);

            log.InfoFormat("In Application_start");
            //Database.SetInitializer(new AriesDataInitializer ());

        }
    }
}
