﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Aries.WebAPI;

[assembly: OwinStartupAttribute(typeof(Aries.Portal.Startup))]
namespace Aries.Portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}
