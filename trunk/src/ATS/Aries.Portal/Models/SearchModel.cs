﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Aries.Model.Entities;

namespace Aries.Portal.Models
{
    public class SearchViewModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public List<Driver> DriverList { get; set; }
        public string selectedDriverItem { get; set; }
        public IEnumerable<SelectListItem> DriverDd
        {
            get { return new SelectList(DriverList, "Name", "Name", selectedDriverItem); }
        }
        public List<Customer> CustomerList { get; set; }
        public string selectedCustomerItem { get; set; }
        public IEnumerable<SelectListItem> CustomerDd
        {
            get { return new SelectList(CustomerList, "Name", "Name", selectedCustomerItem); }
        }
    }
    public class SearchResultsViewModel
    {
        public string DriverName { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Date { get; set; }

    }
}
