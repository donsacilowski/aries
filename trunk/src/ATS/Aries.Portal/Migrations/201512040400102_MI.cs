namespace Aries.Portal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MI : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "MI", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "MI");
        }
    }
}
