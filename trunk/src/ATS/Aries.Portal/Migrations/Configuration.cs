namespace Aries.Portal.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Aries.Portal.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Aries.Portal.Models.ApplicationDbContext";
        }

        protected override void Seed(Aries.Portal.Models.ApplicationDbContext context)
        {
           
        }
    }
}
