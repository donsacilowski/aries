﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using System.Web.Security;
using Aries.Model;

//using WebMatrix.WebData;
using Aries.Model.Entities;

using System.Data.Entity.Migrations;
using System.Data.Entity.Infrastructure;

using log4net;
using log4net.Config;
using System.Web.Http;

namespace Aries.DataAccess
{
    public class AriesDataInitializer : DropCreateDatabaseIfModelChanges<AriesDataContext>
    {
        public static ILog log = LogManager.GetLogger(typeof(AriesDataInitializer));

        public AriesDataInitializer()
        {
            // Setup log4net
            log.InfoFormat("In AriesDataInitializer");
            AriesDataContext context = new AriesDataContext();

            if (!context.Database.Exists())
            {
                context.Database.Initialize(true);
            }

            Seed(context);
        }

        protected override void Seed(AriesDataContext context)
        {
            try
            {
                log.InfoFormat("In AriesDataInitializer Seed ");

                //base.Seed(context);
                //  This method will be called after migrating to the latest version.
                context.StatusLists.AddOrUpdate(p => p.Description,
                    new StatusList { Description = "Open" },
                    new StatusList { Description = "Assigned" },
                    new StatusList { Description = "Dispatched" },
                    new StatusList { Description = "Accepted" },
                    new StatusList { Description = "AtPickUp" },
                    new StatusList { Description = "PickedUp" },
                    new StatusList { Description = "AtDropOff" },
                    new StatusList { Description = "DroppedOff" },
                    new StatusList { Description = "Rejected" },
                    new StatusList { Description = "CancelDispatch" },
                    new StatusList { Description = "CancelClient" },
                    new StatusList { Description = "GotCheck" },
                    new StatusList { Description = "NotAlone" },
                    new StatusList { Description = "DriverAttested" },
                    new StatusList { Description = "Closed" },
                    new StatusList { Description = "BadOrder" });

                log.InfoFormat("In AriesDataInitializer Seed added status list");

                context.CancelReasonLists.AddOrUpdate(p => p.Reason,
                                new CancelReasonList { Reason = "Flat Tire" },
                                new CancelReasonList { Reason = "Lost" },
                                new CancelReasonList { Reason = "Don't Care" });

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                log.InfoFormat("In AriesDataInitializer Seed exception " + ex.Message);
            }
        }
    }
}