USE [Aries]
GO

DELETE FROM [dbo].[Customer]
GO

DELETE FROM [dbo].[Address]
GO

DELETE FROM [dbo].[Driver]
GO

DELETE FROM [dbo].[Vehicle]
GO

DELETE FROM [dbo].[Payment]
GO

DELETE FROM [dbo].[Dispatch]
GO

DELETE FROM [dbo].[Status]
GO

