﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aries.Model;
using Aries.WebAPI;
using MedTrans;
using Aries.Model.UpdateDB;

using Geocoding;
using Geocoding.Google;
using System.Collections.Generic;

namespace DriverVehicle
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test()
        {
            UpdateDB db = new UpdateDB();
            MedtransEntities medtransDB = new MedtransEntities();

            trans_bridge tb = new trans_bridge();

            tb.VanIdentifier = 2057;
            tb.VanLicense = "DDS1234";

            db.UpdateDriver(tb);
        }
        [TestMethod]
        public void GeoTest() {
            IGeocoder geocoder = new GoogleGeocoder() { };
            Location loc = new Location(42.89907883401784, -78.6783481044947);
            IEnumerable<Geocoding.Address> addr = geocoder.ReverseGeocode(loc);

            string a = "";
            foreach (Geocoding.Address add in addr) {
                a = add.ToString();
                break;
            }
        }
    }
}
