﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Aries.FleetComplete.Models {

    //TODO It looks like according to the FC Order Service Guide_1 2.docx there are methods
    //to retrieve and send the information without having t hadle the Soap calls. If this is the case, 
    //Then all the XML... tags will be removed.
    //[XmlRoot]
    //public class Order {

    //    [XmlElement]
    //    public string AccountNumber { get; set; }

    //    [XmlElement]
    //    public string Reference { get; set; }

    //    [XmlElement]
    //    public string PickUpName { get; set; }

    //    [XmlElement]
    //    public string PickUpStreet { get; set; }

    //    [XmlElement]
    //    public string PickUpCity { get; set; }

    //    [XmlElement]
    //    public string PickUpProvince { get; set; }

    //    [XmlElement]
    //    public string PickUpPostalCode { get; set; }

    //    [XmlElement]
    //    public string PickUpCountry { get; set; }

    //    [XmlElement]
    //    public string PickUpContact { get; set; }

    //    [XmlElement]
    //    public string DeliveryName { get; set; }

    //    [XmlElement]
    //    public string DeliveryStreet { get; set; }

    //    [XmlElement]
    //    public string DeliveryCity { get; set; }

    //    [XmlElement]
    //    public string DeliveryProvince { get; set; }

    //    [XmlElement]
    //    public string DeliveryPostalCode { get; set; }

    //    [XmlElement]
    //    public string DeliveryCountry { get; set; }

    //    [XmlElement]
    //    public string DeliveryContact { get; set; }

    //    [XmlElement]
    //    public string Service { get; set; }

    //    [XmlElement]
    //    public string Vehicle { get; set; }

    //    [XmlElement]
    //    public DateTime PickUpTime { get; set; }

    //    [XmlElement]
    //    public DateTime EstimatedStartTime { get; set; }

    //    [XmlElement]
    //    public DateTime ReadyTime { get; set; }

    //    [XmlElement]
    //    public string TotalWeight { get; set; }
    //}

    public class OrderStatus {

        public string Description { get; set; }
    }
}
