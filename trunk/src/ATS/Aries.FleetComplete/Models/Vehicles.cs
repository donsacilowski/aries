﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Aries.FleetComplete.Models {

    [XmlRoot]
    public class Vehicles {

        [XmlElement]
        public string NumberofRecords { get; set; }

        [XmlElement("VehicleLocationList", Type = typeof(VehicleLocationList))]
        public VehicleLocationList vehicleList { get; set; }


    }

    [Serializable]
    public class VehicleLocationList {

        [XmlElement]
        public string VehicleLable { get; set; }

        [XmlElement]
        public string VehicleID { get; set; }

        //TODO: Don;t think we will need this, but its here.
        [XmlElement("Address_Location", Type = typeof(AddressLocationList))]
        public VehicleLocationList vehicleList { get; set; }

        [XmlElement]
        public string PhoneNumber { get; set; }

        [XmlElement]
        public string Description { get; set; }

        //public string[] description { get; set; }

        //[XmlElement]
        //public string[] events { get; set; }

        //<asset description='' street='' city='' province='' postalcode='' country='' speed='' timestamp='' direction='' zone='' events='' sensors='' lon='' lat='' TimeZoneOffset='' hdop='' />
    }

    [Serializable]
    public class AddressLocationList {

        [XmlElement]
        public string Street { get; set; }

        [XmlElement]
        public string City { get; set; }

        [XmlElement]
        public string State { get; set; }

        [XmlElement]
        public string CountryCode { get; set; }

        [XmlElement]
        public string ProvinceCode { get; set; }

        [XmlElement]
        public string PostalCode { get; set; }

    }
}
