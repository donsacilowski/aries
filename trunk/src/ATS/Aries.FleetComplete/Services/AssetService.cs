﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

using Aries.FleetComplete.Assets;
using Aries.FleetComplete.Models;
 

namespace Aries.FleetComplete.Services {
    public class AssetService {

        private readonly AssetsSoapClient client;
        private readonly Credentials credentials;
        private readonly string callingClient = ConfigurationManager.AppSettings["FleetCompleteCallingClient"];

        private Vehicles vehicles;

        public AssetService() {

            credentials = new Credentials {
                        ClientID = ConfigurationManager.AppSettings["FleetCompleteClientID"], 
                        Password = ConfigurationManager.AppSettings["FleetCompletePassword"],
                        };
            
            client = new AssetsSoapClient();

            Vehicles vehicles = new Vehicles();
        }

        public Vehicles GetAssets() {

            string xml = client.GetAllAssetInfo(credentials, callingClient, credentials.Password, credentials.ClientID);

            // For testing until the data is available from Fleet Complete
            //xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Vehicles><NumberofRecords>'0'</NumberofRecords><VehicleLocationList><description>'dons 1'</description><Address>43 way st.</Address><events>'event 1'</events><description>'dons 2'</description><events>'event 2'</events></VehicleLocationList></Vehicles>";
            var serializer = new XmlSerializer(typeof(Vehicles));
            
            StringReader stream = new StringReader(xml);

            vehicles = (Vehicles)serializer.Deserialize(stream);

            return vehicles;
        }
    }
}
