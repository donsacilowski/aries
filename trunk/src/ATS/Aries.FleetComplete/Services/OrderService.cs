﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

using log4net;
using log4net.Config;

//TODO: Place holder for when FC has the Orders API ready. Also, depending on what the API calls are,
//which may be different from the FC Orders manual, the authentication may be setup different.
//using Aries.FleetComplete.Orders;
//using Aries.FleetComplete.Assets;
using Aries.Model.Entities;
using Aries.FleetComplete.Orders;
using Aries.FleetComplete.Models;


namespace Aries.FleetComplete.Services {
    public class OrderService {

        public static readonly ILog log = LogManager.GetLogger(typeof(OrderService));

        private  FCOrderServiceClient client;
        private  AuthenticationInfo credentials;
        private readonly string callingClient = ConfigurationManager.AppSettings["FleetCompleteCallingClient"];

        //private OrderStatus orderStatus;

        public OrderService() {

            credentials = new AuthenticationInfo {
                AccountNumber = Convert.ToInt32(ConfigurationManager.AppSettings["AccountNumber"]),
                ClientId = ConfigurationManager.AppSettings["FleetCompleteClientID"],
                Password = ConfigurationManager.AppSettings["FleetCompletePassword"],
                UserName = ConfigurationManager.AppSettings["Username"],
            };

            client = new FCOrderServiceClient();
            
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["Username"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["FleetCompletePassword"];
            client.Open();
            
            
            // orderStatus = new OrderStatus();
        }

        public long CreateFCOrder(Dispatch disp) {
            TaskDelivery td = client.GetOrderDetails(credentials, 374);
            long orderNumber=0;
            Guid? sig = td.Details.Legs[0].Delivery.SignatureID;
            int accnt = Convert.ToInt32(ConfigurationManager.AppSettings["AccountNumber"]);
            try {
                
                Order newOrder = new Order();

                    newOrder.DoAddressValidation = Convert.ToBoolean("False");

                    newOrder.AccountNumber = accnt;
                    newOrder.Reference = "Test order reference" + disp.Service + ": " + disp.ServiceType;
                    newOrder.Instructions = ""; 
                    
                    newOrder.PickUpName = disp.PickupAddress.Description;
                    newOrder.PickUpCode = "";
                    newOrder.PickUpStreet = disp.PickupAddress.Street; //"207 MILL STREET";
                    newOrder.PickUpStreet2 = "";
                    newOrder.PickUpCity = disp.PickupAddress.City; //"LEOMINSTER";
                    newOrder.PickUpProvince = disp.PickupAddress.State; //"MA";
                    newOrder.PickUpPostalCode = disp.PickupAddress.PostalCode; //"01453";
                    newOrder.PickUpCountry = "US";
                    newOrder.PickUpInstructions = disp.Service + ": " + disp.ServiceType + "\r\n" +
                                         "Contact: " + disp.Customer.Name + "\r\n";

                    if (disp.Payment.PaymentToDriver) {
                        newOrder.PickUpInstructions += "Pickup a check for " + disp.Payment.PaymentToDriverAmount.ToString();
                    }

                    newOrder.PickupContact = "";
                    newOrder.PickUpPhone = "";
                    newOrder.DeliveryName = disp.DropoffAddress.Description;
                    newOrder.DeliveryCode = "";
                    newOrder.DeliveryStreet = disp.DropoffAddress.Street; //"130 Monahan Ave";
                    newOrder.DeliveryStreet2 = "";
                    newOrder.DeliveryCity = disp.DropoffAddress.City; //"Dunmore";
                    newOrder.DeliveryProvince = disp.DropoffAddress.State; //"PA";
                    newOrder.DeliveryPostalCode = disp.DropoffAddress.PostalCode; //"18512";
                    newOrder.DeliveryCountry = "US";
                    newOrder.DeliveryInstructions = disp.DNLA ? "Confirm to not leave person alone" : "";
                    newOrder.DeliveryContact = "";
                    newOrder.DeliveryPhone = "";
                    newOrder.Service = "Standard1";
                    newOrder.Vehicle = "Car";
                    newOrder.ReadyTime = disp.PickupTime.ToUniversalTime().ToString();
                    newOrder.PickupByTime= disp.PickupTime.ToUniversalTime(); 
                    newOrder.EstimatedStartTime = disp.PickupTime.ToUniversalTime(); 
                    newOrder.EstimatedCompletionTimeStamp = disp.PickupTime.ToUniversalTime(); 
                    newOrder.TotalWeight = 25;

                    
                    //if (disp.Payment.PaymentToDriver) {
                    //    newOrder.Packages = new Package[]{
                    //        new Package {
                    //            PackageType = "Pickup"
                    //        }
                    //    };
                    //    newOrder.DeclaredValue = disp.Payment.PaymentToDriverAmount;
                    //    newOrder.InsuranceRequired = true;
                    //}
                    Package dnlaPkg = new Package();
                    dnlaPkg.PackageType = "Do No leave Alone"; 

                    Package checkPkg = new Package();
                    checkPkg.PackageType = "Pickup Check";

                    List<Package> pkgList = new List<Package>();

                    if (disp.DNLA) {
                        pkgList.Add(dnlaPkg);                        
                    }

                    if (disp.Payment.PaymentToDriver) {
                        pkgList.Add(checkPkg);
                    }

                    newOrder.Packages = pkgList.ToArray();
                    
                    //newOrder.Packages = new Package[]{
                    //    new Package{
                    //        PackageType = "Pickup"
                    //    }
                    //};

                    newOrder.DriverName = disp.Driver.Name;
                    
                    //newOrder.
                    //if (disp.DNLA) {
                    //    newOrder.CustomFields = new CustomField[]
                    //    {
                    //        new CustomField()
                    //        {
                    //                CustomFieldLabel = "DNLA",
                    //                CustomFieldValue = "Confirm to not leave person alone "
                                 
                    //        },
                    //    };
                    //}

                    newOrder.CustomFields = new CustomField[]
                        {
                        };
                    //if (disp.Payment.PaymentToDriver) {
                    //    newOrder.CustomFields = new CustomField[]
                    //    {
                    //        new CustomField()
                    //        {
                    //                CustomFieldLabel = "PickupCheck",
                    //                CustomFieldValue = "Confirm to pickup a check for " + disp.Payment.PaymentToDriverAmount.ToString()
                    //        },
                    //    };
                    //}

                    orderNumber = client.CreateOrderInFC(credentials, newOrder);
                
               client.Close();
            } catch (Exception ex) {
                client.Close();
                log.Error(ex);
                throw new Exception(ex.InnerException.Message);
            }
            return orderNumber;
        }

        //TODO: This may change to a simple call 
        public string GetOrderStatus(int orderNumber) {
            string status = "";
            try {
                status = client.GetOrderStatus(credentials, orderNumber);
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return status;
        }

        public TaskDelivery GetOrderDetails(int orderNumber) {
            TaskDelivery td = null;
            try {
                td = client.GetOrderDetails(credentials, orderNumber);
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return td;
        }
    }
}

